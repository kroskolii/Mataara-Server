# We need Django 1.8 at the moment
# Thus, assorted packages are restricted to old versions
Django<1.9
cryptography
django_db_mutex<1.0
fuzzywuzzy
django-polymorphic<2.0
Werkzeug
dateutils
django_extensions
django-filter

# REST API
djangorestframework<3.7.0
django-rest-swagger

# Compress CSS and precompilers
django-compressor
django-pyscss
django-libsass

# For Sentry integration
raven

# For receiving emails
django-mailbox

# For scheduled tasks
django-celery

# For parsing advisories
beautifulsoup4
lxml
feedparser

# Only used for archimedes to find the bower static file without an explicit 'collectstatic' call
# django-bower

# Used for unit tests
django-nose
mock
sqlparse

# For LDAP authentication
# 1.3 removes support for Django 1.9 which may make it harder for when we do the upgrade from 1.8 to 1.11
django-auth-ldap<1.3.0

# PostgreSQL support
psycopg2

# Local cache
django-redis<4.9

# Jenkins integration, which we're not really using
django_coverage_plugin
django-jenkins

# Probably not required, or only in a dev environment
python-Levenshtein
gunicorn

# Required to ensure that slugify is always unique for advisory sources
unidecode

# For SAML authentication
djangosaml2

# Webpack for ReactJS stuff
django-webpack-loader

# For version comparisons
packaging

# For testing against Drupal API
vcrpy

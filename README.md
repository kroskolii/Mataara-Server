About
=====
Mataara (formerly Archimedes) is a security reporting tool that is installed as a client on various websites (Drupal etc.) and reports package versions and config information
back to a central server. The server maintains information on security vulnerabilities and updates to these packages and can tell us useful things like which
sites need upgrades or patches installed, and which sites are running a specific package and/or version.

Architecture
============
This project (the Mataara server) is a Django site, and uses Celery to offload various tasks (e.g. report processing, external API access) to a queue.

### Technology:
* Python 3
* Django 1.8
* Postgres
* Redis (mq & cache)

Documentation
=============

Most of the documentation is in the directory docs/source - it can be compiled using Sphinx to nice HTML pages, although the it can also be viewed through gitlab.

If the developer environment is correctly configured this should be possible using the command 'fab make_docs'.


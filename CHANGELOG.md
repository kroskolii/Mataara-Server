# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## Template
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [Unreleased]
### Added
### Changed

## 0.1.20180830.1
### Added
- Drupal advisory parsing via API
- Users can now subscribe to particular sites of interest

### Changed
- Quite a lot of things. The ReactJS frontend has been well and truly revamped.

### Deprecated
- Old Drupal Advisory RSS feeds


## 0.1.20180517
### Added
- Fabric commands for showing Django logs in development and reload the server
- Fabric commands serving the ReactJS frontend in development
- Added a login page
- Updates for parsing of Drupal advisories
### Changed
- Fabric runs ReactJS tests
- Changed base template so that it does not use Angular
### Deprecated
- Fabric run command as it is not needed.
- Documentation has been updated
- Vagrant provisioning network setting configurable
- Vagrant puppet manifests to fix dependancies
### Removed
- Tests for the old AngularJS views
- URLS that went to the old AngularJS views.
### Fixed
- Gitlab CI environment was failing tests

## 0.1.20180326
### Added
- Added CI to run some tests and build wheels automatically
### Changed
- Updated some Python modules

## 0.1.20180321
### Added
- Optimised wheel building to include creation of JavaScript bundles
### Changed
- JavaScript bundle compilation for production to match development
### Removed
- Bower JavaScript files and djangobower related code
- JavaScript compiled bundles

## 0.1.20180315
### Added
- Add ability for users to (un)subscribe to sites

## 0.1.20171115
### Fixed
- Some bug fixes to the API as used by the ReactJS pages

## 0.1.20171016
### Added
- added bundles dir
- added webpack-stats.json

## 0.1.20170725
### Added
- Added SAML authentication support
- Added initial SilverStripe support

## 0.1.20170612
### Removed
- Legacy Jenkins integration support

## 0.1.20170519
### Added
- Special handling function for Drupal security advisories that are badly formed

## 0.1.20170516
### Added
- Handle Drupal security advisories that affect all versions of a module

## 0.1.20170503
### Added
- Command-line option to configure an existing user as an superuser. Especially useful in development
### Changed
- Updates to development and build process to support to use of a build agent on deployment server
- Detect advisory parsing failure earlier and set failed flag
- Make vagrant managed venv directly accessible from host
- Upgrade built-in bootstrap version of pip used in vagrant
### Removed
- Fabric tasks related to publishing the wheelhouse ready for deployments

## 0.1-20170405
### Changed
- Finished upgrading all dependencies to latest versions

## 0.1-20170331
### Added
- Added new fabric task and command for initialising advisory sources

### Changed
- Bugfix in pagination in views

## 0.1-20170330
### Changed
- Updated Django API modules

## 0.1-20170328
### Added
- Add support for pip-tools within development environment and start upgrading dependencies
- Add user profile model

## 0.1-20170320
### Added
- Add support for configuration file checking within the Vagrantfile

### Changed
- Switch from Catalyst specific LDAP module to django_auth_LDAP

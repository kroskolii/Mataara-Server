"""
Archimedes
==========
"""

from __future__ import absolute_import

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celeryapp import app as celery_app  # noqa


def version():
    import pkg_resources
    return pkg_resources.get_distribution("archimedes").version


def management_command():
    """Entry-point for the 'archimedes' command-line admin utility."""
    import os
    import sys

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "archimedes.settings")

    # Are we testing?
    testing = 'test' in sys.argv[1:2]
    testing_with_coverage = testing and '--with-coverage' in sys.argv

    from django.core.management import execute_from_command_line
    from django.conf import settings

    # Start coverage
    if testing_with_coverage:
        sys.argv.remove('--with-coverage')

        import coverage
        cov = coverage.coverage(
            source=[settings.PROJECT_DIR],
            omit=[
                '*/migrations/*',
                '*/tests/*',
                '*/test_*',
                '*/exceptions.py',
                '*/wsgi.py',
                '*/configuration.py',
                '*/settings.py',
                '*/celeryapp.py',
            ]
        )
        cov.erase()
        cov.start()

    execute_from_command_line(sys.argv)

    # End coverage & report
    if testing_with_coverage:
        cov.stop()
        cov.save()
        cov.report()

# -*- coding: utf-8 -*-
from django.contrib.auth.models import Group, User
from rest_framework import serializers

from archimedes.core.models import UserProfile


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('api_url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('api_url', 'name')


class UserProfileSerializer(serializers.HyperlinkedModelSerializer):
    pk = serializers.IntegerField(read_only=True)

    class Meta:
        model = UserProfile
        fields = ('api_url', 'pk', 'user', 'active_days')

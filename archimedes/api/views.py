# -*- coding: utf-8 -*-
from django.contrib.auth.models import Group, User
from rest_framework import viewsets

from archimedes.core.models import UserProfile

from . import serializers


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = serializers.UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = serializers.GroupSerializer


class UserProfileViewSet(viewsets.ModelViewSet):
    """
    API endpoint to allow current user's profile to be viewed (TODO: or edited).
    """
    queryset = UserProfile.objects.all()
    serializer_class = serializers.UserProfileSerializer

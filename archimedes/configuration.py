# -*- coding: utf-8 -*-

import configparser
import logging
import logging.config
import os

from django.core.exceptions import ImproperlyConfigured


DEFAULT_CONFIGURATION = """
# Archimedes DEFAULT CONFIGURATION
# ================================

# This section defines all of the Archimedes configuration settings used
# and defines default values.
# (Leave blank if there is no default value)

[django]
# The secret key must be set or the app will fail to run.
secret_key =
# Make sure that debug is only enabled on non-production servers
debug = false
allowed_hosts = localhost archimedes
static_root =
# If the media_root is set, it defines a specific defined location
# that must be writeable by the application for storage of uploaded files
# and processed emails (if you're receiving reports that way)
media_root =

# additional configuration files to read, split by newlines.
# (a configuration value can be multiline as long as each
# line starts with whitespace)
# configuration_files =

# Email address(es) to receive error reports
# admin_email =

# PostgreSQL connection details
[database]
name =
user =
host =
password =

[redis]
# URL for core redis service, e.g. redis://localhost:6379/0
location =

[celery]
# redis URL for celery, e.g. redis://localhost:6379/1
broker_url =

# If you need a web proxy to talk to the Drupal server or Sentry configure it here
[proxy]
http =
https =

# If you are using Sentry for logging configure the DSN here
[sentry]
dsn =

[archimedes]
# Set type of authentication to use: 'local' for local user accounts only,
# 'ldap' to use an LDAP server or 'saml' to connect to an IdP using SAML
# authentication = ldap

# email_subject_prefix = [Archimedes]

# From email address to use when sending email from the server
# from_email = archimedes@localhost

# Mailbox URI to check for emailed reports. Can use 'imap' or 'pop3'.
# e.g. mailbox_uri = imap+ssl://myusername:mypassword@someserver
# Urlencode the username and password if they contain illegal characters
mailbox_uri =

# Filename to the file that contains the private key used for decrypting incoming reports
reports_keyfile =
# Optional passphrase for the private key file
# reports_keyfile_passphrase =

# reports_processing_retry_delay = 10
# reports_processing_max_retries = 10
# drupal_update_server = https://updates.drupal.org/

[email]
# hostname of mail server for outgoing email
server =
port =

# If LDAP is being used for authentication, configure the LDAP URI and settings here
[ldap]
# server URI should be of the format "ldap://hostname/" or "ldaps://hostname/"
# e.g. server = "ldap://ldap.example.com"
server =
bind_dn =
bind_password =

# example simple search DN "uid=%(user)s,ou=users,dc=example,dc=com"
search_dn =

# Set to True if non-SSL LDAP connections use STARTTLS
# ldap_starttls = False

# Set to True to disable TLS server certificate checks
# ldap_ignore_cert = False

# If SAML is being used for authentication, configure the core settings here
[saml]
# Unique URL for this Archimedes instance. It does not need to point to
# a real webpage, however, this may be advantageous for the future
# e.g. http://archimedes.company.com/saml2/metadata
entityid =

# Root URL for the server, used in metadata for IdP to connect to Archimedes
# Defaults to that specified in the entityid
# root_url =

# Optional list of required attributes that the IDP must provide for a successful authentication
# required_attributes =

# To workaround bugs in IdP implementations that do not correctly provide
# attributes in a standard format, set this variable to 'True' and it will
# rely on matching the primary_attribute value for the user identifier.
# allow_unknown_attributes = false

# If you have downloaded (or created) some attribute mail files, place theme
# into a directory which Archimedes will be able to read
# A good source is https://github.com/rohe/pysaml2/tree/master/example/attributemaps
# attribute_map_dir =

# Define the SAML user attributes
# username = uid
# email =
# first_name =
# last_name =

# How long is the generated metadata valid for (in hours)?
# Set to 0 for never expires
# expires = 24

# File containing the metadata from the IdP (default = 'remote_metadata.xml')
remote_metadata_file =

# Define the public/private key files for cryptographic signing
key_file =
cert_file =

# Optional, secondary public/private key files used cryptographic encryption
# If not specified, will use the above key file and cert file.
# enc_key_file =
# cert_key_file =

# Path to the xmlsec1 binary.
# xmlsec_binary = /usr/bin/xmlsec1

# This will often be the URL for the IdP's metadata, e.g.
# https://idp.company.com/simplesaml/saml2/idp/metadata.php
idp_entityid =
# Specify the binding URL for the IdP's single sign-on service
idp_sso =
# Specify the binding URL for the IdP's single logout service
idp_sls =

# Optional, define a SAML technical contact to be used in the meta-data
[saml-technical]
# company =
# name =
# surname =
# email =

# Optional, define a SAML administrative contact to be used in the meta-data
[saml-administrative]
# company =
# name =
# surname =
# email =

# Logging Configuration
# ---------------------
#
# This configuration effectively suppresses all logging.
# The archimedes.ini file is where actual configuration can go.

[loggers]
keys = root
handlers =

[logger_root]
level = NOTSET
handlers =

[handlers]
keys =

[formatters]
keys =
"""


def read_config():
    config = configparser.ConfigParser(interpolation=None)
    # config.readfp(io.StringIO(DEFAULT_CONFIGURATION))
    try:
        config.read_file(open("/etc/archimedes/archimedes.ini"))
    except PermissionError:  # noqa: F821
        raise ImproperlyConfigured("You do not have permission to read the Archimedes configuration file")
    except FileNotFoundError:  # noqa: F821
        raise ImproperlyConfigured("Archimedes configuration file not found")

    # Read additional configuration files if they are named in
    # [django][configuration_files]. Note that this will fail SILENTLY if they
    # don't read.
    if 'configuration_files' in config['django']:
        for additional_file in config['django']['configuration_files'].split('\n'):
            filename = additional_file.strip()
            if filename:
                config.read(filename)

    # This strips blank items from the proxy section of the config, so that we
    # can just pass the proxy section as a dictionary to requests and suds.
    if 'proxy' in config:
        config['proxy'] = {
            k: v for k, v in dict(config.items('proxy')).items() if v != ""}

        # Set http_proxy and https_proxy environment variables for modules
        # that we can't control directly
        for proto in ['http', 'https']:
            if proto in config['proxy']:
                proxy_key = proto + '_proxy'
                os.environ[proxy_key.upper()] = config['proxy'][proto]
                os.environ[proxy_key] = config['proxy'][proto]

    return config


CONFIG = read_config()
logging.config.fileConfig(CONFIG, disable_existing_loggers=False)

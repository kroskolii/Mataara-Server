
from celery import shared_task
from celery.utils.log import get_task_logger
from django.core.management import call_command
import json

from .models import Advisory
from .parser import AdvisoryParser
from .do_advisory_parser import DOAdvisoryParser
from .utils import get_guid

logger = get_task_logger(__name__)


@shared_task
def import_advisories():
    """Import advisories from all RSS sources."""
    logger.warning("import_advisories task is deprecated in favour of import_api_advisories")
    call_command('importadvisories')  # pragma: no cover


@shared_task
def parse_advisories():
    """Parse all available advisories."""
    logger.warning("parse_advisories task is deprecated in favour of import_api_advisories")
    call_command('parseadvisories')  # pragma: no cover


@shared_task
def import_api_advisories():
    """
    Import advisories from all API sources.

    This task will also schedule all new and updated advisories discovered for parsing.
    """
    call_command('import_drupal_advisories')  # pragma: no cover


@shared_task
def parse_advisory(advisory_pk, force=False):
    """If an advisory has been parsed, output a message on the logger and return,\
    otherwise, give the advisory to AdvisoryParser.parse() """
    # Fetch the advisory from the database
    advisory = Advisory.objects.get(pk=advisory_pk)

    # Don't re-parse advisories unless forced to.
    if advisory.date_parsed and not force:
        logger.warning('Advisory #%d already parsed.' % advisory_pk)
        return

    logger.info('Parsing advisory #%s...' % advisory_pk)
    parser = AdvisoryParser()
    parse_status = parser.parse(advisory, forcedownload=False)
    # If parsing unsuccessful, set a failed status flag.
    if parse_status is False:
        logger.error('Parsing advisory #%s failed.' % advisory_pk)
        advisory.failed = True
        advisory.save()
    else:
        logger.info("Advisory #%s parsed." % advisory_pk)

    return parse_status


@shared_task
def parse_api_advisory(advisory_json):
    """If an advisory has been parsed, output a message on the logger and return,\
    otherwise, give the advisory to DOAdvisoryParser.parse() """

    advisory_dict = json.loads(advisory_json)
    nid = advisory_dict['nid']
    guid = get_guid(nid)

    logger.info('Parsing advisory #{}...'.format(nid))
    parser = DOAdvisoryParser()
    parse_status = parser.parse(advisory_json, forcedownload=False)
    # If parsing unsuccessful, set a failed status flag.
    if parse_status is False:
        logger.error('Parsing advisory #{} failed.'.format(nid))
        advisory = Advisory.get(guid=guid)
        advisory.failed = True
        advisory.save()
    else:
        logger.info("Advisory #{} parsed.".format(nid))

    return parse_status

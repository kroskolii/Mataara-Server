from django_filters import OrderingFilter
from rest_framework import viewsets
from rest_framework.filters import BaseFilterBackend
from rest_framework.settings import api_settings
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from . import serializers
from .. import models
from archimedes.sites.models import Site
from archimedes.drupal.calcstats import get_drupal_stats


class DrupalSiteViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Drupal sites to be viewed or edited.
    """
    queryset = models.DrupalSite.objects.select_related('environment',
                                                        'core_version').prefetch_related('keys', 'modules', 'themes')
    serializer_class = serializers.DrupalSiteSerializer

    def get_queryset(self):
        drupal_version = self.request.query_params.get('drupal_version')
        if drupal_version:
            self.queryset = models.DrupalSite.objects.filter(core_version__version=drupal_version)
            return self.queryset
        else:
            return self.queryset


class ProjectSitesCountOrderingFilter(OrderingFilter):
    """
    Filter that orders the queryset by the number of sites in the sites_using property.
    """
    def __init__(self, *args, **kwargs):
            super(ProjectSitesCountOrderingFilter, self).__init__(*args, **kwargs)
            self.extra['choices'] += [
                ('sites_using__count', 'Number of sites using project'),
                ('-sites_using__count', 'Number of sites using project (descending)'),
            ]

    def filter(self, qs, value):
        # OrderingFilter is CSV-based, so `value` is a list
        if 'sites_using__count' in value:
            qs = sorted(qs, key=lambda q: q.sites_using.count())
            return qs
        elif '-sites_using__count' in value:
            qs = sorted(qs, key=lambda q: -q.sites_using.count())
            return qs
        else:
            return super(ProjectSitesCountOrderingFilter, self).filter(qs, value)


class ProjectSitesUsingFilterBackend(BaseFilterBackend):
    """
    Filter backend for Project sites_using property.

    sites_using__pk: filter queryset to Projects used by a particular Site.
    [-]sites_using__count: ordering queryset by the number of Sites using a particular Project.
    """
    def filter_queryset(self, request, queryset, view):
        # Reduce queryset if keyed against a particular Site pk.
        pk = request.GET.get('sites_using__pk')
        if pk:
            filter_site = Site.objects.get(pk=pk)
            filtered_queryset = [p for p in queryset if filter_site in p.sites_using]
        else:
            filtered_queryset = queryset

        # Check for sites_using__count ordering.
        # TODO: Get this reviewed so that multiple orderings are applied in order.
        order_params = request.GET.get('ordering')
        if order_params:
            fields = [param.strip() for param in order_params.split(',')]
            filtered_queryset = ProjectSitesCountOrderingFilter().filter(filtered_queryset, fields)

        return filtered_queryset


class ProjectViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Drupal projects to be viewed or edited.
    """
    queryset = models.Project.objects.all()
    serializer_class = serializers.ProjectSerializer
    search_fields = ('name', 'slug',)
    filter_backends = api_settings.DEFAULT_FILTER_BACKENDS + [ProjectSitesUsingFilterBackend]
    filter_fields = ('type',)


class ReleaseViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Drupal project releases to be viewed or edited.
    """
    queryset = models.Release.objects.select_related('project')
    serializer_class = serializers.ReleaseSerializer
    filter_fields = ('project',)


class AdvisorySourceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Drupal advisory sources to be viewed or edited.
    """
    queryset = models.AdvisorySource.objects.all()
    serializer_class = serializers.AdvisorySourceSerializer


class AdvisoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Drupal advisories to be viewed or edited.
    """
    queryset = models.Advisory.objects.all()
    serializer_class = serializers.AdvisorySerializer
    search_fields = ('title', 'advisory_id', 'project__slug', )
    filter_fields = ('source__slug',)


class DrupalStatsViewSet(viewsets.ViewSet):
    """
    API endpoint to allow easy visualization of Drupal statistics.
    """
    permission_classes = (IsAuthenticated, )

    def list(self, request):
        return Response(get_drupal_stats())

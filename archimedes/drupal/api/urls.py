from archimedes.api.urls import router

from ..api import views

# Register viewsets witht the router created in api.urls
router.register(r'drupal/sites', views.DrupalSiteViewSet)
router.register(r'drupal/projects', views.ProjectViewSet)
router.register(r'drupal/releases', views.ReleaseViewSet)
router.register(
    r'drupal/advisory_sources',
    views.AdvisorySourceViewSet
)
router.register(
    r'drupal/advisories',
    views.AdvisoryViewSet
)
router.register(
    r'drupal/stats',
    views.DrupalStatsViewSet,
    base_name='drupal/stats'
)

# No URL patterns needed - they're done by api.urls.router
urlpatterns = []

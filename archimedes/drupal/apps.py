from django.apps import AppConfig


class DrupalConfig(AppConfig):
    name = "archimedes.drupal"

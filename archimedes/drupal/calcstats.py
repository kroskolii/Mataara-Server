from collections import Counter

from packaging import version

from . import models


def get_drupal_stats():
    stats = {}
    # Obtain a list of active sites
    active_sites = [site for site in models.DrupalSite.objects.all() if site.active]

    # Collate Drupal sites by major core version
    core_pks = [c.core for c in active_sites]
    stats['core_version_data'] = Counter(core_pks).most_common()

    # Collate Drupal sites by core version number
    drupal_pks = [c.core_version.version for c in active_sites]
    drupal_counted = Counter(drupal_pks).most_common()

    # Convert version string to a sortable Version object.
    # packaging.version follows PEP 440 for version comparisons.
    # Documentation: https://packaging.pypa.io/en/latest/version/
    def version_key(x):
        return version.parse(x[0])

    drupal_counted = sorted(drupal_counted, key=version_key, reverse=True)
    stats['drupal_version_data'] = [x[1] for x in drupal_counted]
    stats['drupal_version_labels'] = ["%s" % x[0] for x in drupal_counted]

    latest_advisories = models.Advisory.objects.all().order_by('-date_posted')[:10]
    stats['latest_drupal_advisories'] = [
        {'title': x.title, 'date_posted': x.date_posted, 'pk': x.pk, 'affected': len(x.affected_sites)}
        for x in latest_advisories
    ]

    return stats

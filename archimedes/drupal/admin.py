from django.contrib import admin
from django.core.management import call_command
from polymorphic.admin import PolymorphicChildModelAdmin

from . import models


class AdvisoryAdmin(admin.ModelAdmin):
    raw_id_fields = ('releases',)
    search_fields = ('title', 'advisory_id', 'project__slug')
    readonly_fields = ('status',)
    list_display = ('title', 'status', 'url', 'project', 'risk')
    actions = ['parse_advisories']

    def get_queryset(self, request):
        return super(AdvisoryAdmin, self).get_queryset(request).select_related('risk', 'project')  # pragma: no cover

    def parse_advisories(self, request, queryset):
        from archimedes.drupal.tasks import parse_advisory

        for obj in queryset:
            parse_advisory.delay(obj.pk, force=True)

        self.message_user(request, "Submitted advisory(ies) for processing")

    parse_advisories.short_description = "Schedule advisories for parsing"


class AdvisorySourceAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'url')
    actions = ['import_api_advisories']

    def import_api_advisories(self, request, queryset):
        for source in queryset:
            call_command('import_drupal_advisories', source.slug)

        self.message_user(request, 'Importing advisories from the "{}" API source'.format(source.slug))

    import_api_advisories.short_description = "Import advisories"


class ProjectAdmin(admin.ModelAdmin):
    search_fields = ('name', 'slug', 'type', )
    list_display = ('slug', 'name', 'url', 'type', 'custom')


class ReleaseAdmin(admin.ModelAdmin):
    search_fields = ('version', 'project__slug')
    list_display = ('__str__', 'project', 'version', 'url')


class DrupalSiteAdmin(PolymorphicChildModelAdmin):
    base_model = models.DrupalSite
    raw_id_fields = ('modules', 'themes')
    list_display = ('__str__', 'core_version', 'environment', 'url', 'hostname', 'root', 'title')

    def get_queryset(self, request):
        queryset = super(DrupalSiteAdmin, self).get_queryset(request)
        return queryset.select_related('core_version', 'environment')  # pragma: no cover


admin.site.register(models.Advisory, AdvisoryAdmin)
admin.site.register(models.AdvisorySource, AdvisorySourceAdmin)
admin.site.register(models.Project, ProjectAdmin)
admin.site.register(models.Release, ReleaseAdmin)
admin.site.register(models.DrupalSite, DrupalSiteAdmin)

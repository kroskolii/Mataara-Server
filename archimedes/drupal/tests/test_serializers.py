from collections import OrderedDict

from archimedes.reports.models import Report
from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APIClient

from ..models import Advisory, AdvisorySource, DrupalSite, Project, Release

TEST_SITE = OrderedDict([
    ('count', 1),
    ('next', None),
    ('previous', None),
    ('results', [
        OrderedDict([
            ('api_url', 'http://testserver/api/drupal/sites/1/'),
            ('pk', 1),
            ('string', 'test-site'),
            ('name', 'test-site'),
            ('hostname', ''),
            ('url', ''),
            ('root', ''),
            ('title', ''),
            ('environment', None),
            ('keys', []),
            ('type', 'Drupal'),
            ('core', '7.41'),
            ('date_last_received', None),
            ('vulnerability_count', 0),
            ('decommissioned', False),
            ('subscribed', False),
            ('core_version', '7.41'),
            ('slogan', ''),
            ('nodes', None),
            ('revisions', None),
            ('users', None),
            ('modules', []),
            ('themes', []),
            ('core_vulnerabilities', []),
            ('vulnerable_core', []),
            ('vulnerable_modules', []),
            ('vulnerable_themes', [])
        ])
    ])
])


class APITest(TestCase):
    def setUp(self):
        self.username = "test"
        self.password = "tester:1012"
        self.user = User.objects.create_user(
            username=self.username,
            email="testuser@example.com",
            password=self.password)

        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

        self.dsite = DrupalSite.objects.create(name='test-site')
        self.dsite.set_core('7.41')
        self.dsite.save()

    def test_drupal_sites_drupal_version(self):
        """
        Given a :drupal_version return a list of Drupal :Sites with that version
        :ac: return a list of Drupal Sites with s given version
        :ac: return status 200
        """
        resp = self.client.get('/api/drupal/sites/?drupal_version=7.41')

        self.assertEqual(resp.data, TEST_SITE)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_drupal_sites_drupal_version_fail(self):
        """
        :ac: return an empty list
        :ac: return status 200
        """
        resp = self.client.get('/api/drupal/sites/?drupal_version=6.38')

        empty_response = OrderedDict([
            ('count', 0),
            ('next', None),
            ('previous', None),
            ('results', [])
        ])

        self.assertEqual(resp.data, empty_response)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_drupal_statistics(self):
        """
        Return a list of statistics about all DrupalSites
        :ac: a valid data structure of Drupal statistics should be returned
        :ac: return status 200
        """
        now = timezone.now()
        src = AdvisorySource.objects.create(slug='test')
        Advisory.objects.create(date_posted=now, source=src)

        dsite0 = DrupalSite.objects.create(name='test-site')
        dsite0.set_core('6.38')
        dsite0.save()
        dsite1 = DrupalSite.objects.create(name='test-site')
        dsite1.set_core('7.3')
        dsite1.save()
        dsite2 = DrupalSite.objects.create(name='test-site')
        dsite2.set_core('7.41')
        dsite2.save()
        dsite3 = DrupalSite.objects.create(name='test-site')
        dsite3.set_core('7.41')
        dsite3.save()

        Report.objects.create(
            date_generated=timezone.now(),
            date_expires=timezone.now(),
            processed_site=dsite0,
        )
        Report.objects.create(
            date_generated=timezone.now(),
            date_expires=timezone.now(),
            processed_site=dsite1,
        )
        Report.objects.create(
            date_generated=timezone.now(),
            date_expires=timezone.now(),
            processed_site=dsite2,
        )
        Report.objects.create(
            date_generated=timezone.now(),
            date_expires=timezone.now(),
            processed_site=dsite3,
        )

        stats_response = {
            'latest_drupal_advisories': [
                {'title': '', 'pk': 1, 'affected': 0, 'date_posted': now}
            ],
            'core_version_data': [(7, 3), (6, 1)],
            'drupal_version_labels': ['7.41', '7.3', '6.38'],
            'drupal_version_data': [2, 1, 1]}

        resp = self.client.get('/api/drupal/stats/')
        self.assertDictEqual(resp.data, stats_response)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_advisories_sites_affected(self):
        """
        Check the number of affected sites from the API call
        :ac: API response contains correct number of affected sites
        """
        project = Project.objects.create(slug='testproject')
        release = Release.objects.create(project=project)
        src = AdvisorySource.objects.create(slug='testsource')
        adv = Advisory.objects.create(date_posted=timezone.now(), source_id=src.pk)
        adv.releases.add(release)

        self.dsite.modules.add(release)
        dsite = DrupalSite.objects.create()
        dsite.modules.add(release)

        resp = self.client.get('/api/drupal/stats/')
        self.assertEqual(resp.data['latest_drupal_advisories'][0]['affected'], 2)

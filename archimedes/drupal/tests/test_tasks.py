from django.test import TestCase
from django.utils import timezone
from mock import patch

from ..models import Advisory, AdvisorySource
from ..tasks import parse_advisory


# from mock import Mock

class TasksTestCase(TestCase):

    def mock_init():
        pass

    @patch('archimedes.drupal.tasks.logger')
    @patch('archimedes.drupal.parser.AdvisoryParser.parse')
    def test_parse_advisory_date_parsed_is_not_set(self, mock_parse, mock_logger):
        """Given an :Advisory, if the advisory.date_parsed is not set,\
        logger.info('Parsing <adv.pk>...') and AdvisoryParser.parse() are called"""
        src = AdvisorySource.objects.create(slug='testsource')
        adv = Advisory.objects.create(date_posted=timezone.now(), source_id=src.pk)

        # Test normal parse
        parse_advisory(adv.pk)
        mock_parse.assert_called_once_with(adv, forcedownload=False)
        mock_logger.info.assert_any_call('Parsing advisory #%s...' % adv.pk)
        mock_logger.info.assert_any_call('Advisory #%s parsed.' % adv.pk)

        # Test attempt to re-parse
        adv.date_parsed = timezone.now()
        adv.save()
        mock_parse.reset_mock()
        parse_advisory(adv.pk)
        mock_parse.assert_not_called()
        mock_logger.warning.assert_called_with('Advisory #%d already parsed.' % adv.pk)

    @patch('archimedes.drupal.tasks.logger')
    @patch('archimedes.drupal.parser.AdvisoryParser.parse')
    def test_parse_advisory_date_parsed_is_set(self, mock_parse, mock_logger):
        """Given an :Advisory, if the advisory.date_parsed is set,\
        logger.warning('Advisory <adv.pk> already parsed') is called and\
        the AdvisoryParser.parse() method is not called"""
        src = AdvisorySource.objects.create(slug='testsource')
        adv = Advisory.objects.create(date_posted=timezone.now(), source_id=src.pk)

        # Test attempt to re-parse
        adv.date_parsed = timezone.now()
        adv.save()
        mock_parse.reset_mock()
        parse_advisory(adv.pk)
        mock_parse.assert_not_called()
        mock_logger.warning.assert_called_with('Advisory #%d already parsed.' % adv.pk)

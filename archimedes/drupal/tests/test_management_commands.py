import logging
import os
from io import StringIO

import vcr
from django.conf import settings
from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import TestCase
from django.utils import timezone
from mock import mock_open, patch

from ..models import Advisory, AdvisorySource, Release, ReleaseType
from ..utils import download_releases

# Suppress debug info from VCR logging.
logging.getLogger("vcr").setLevel(logging.WARNING)

sync_vcr = vcr.VCR(
    cassette_library_dir=os.path.join(settings.SITE_ROOT, 'archimedes/drupal/tests/vcr_cassettes'),
    record_mode='once'
)


class CommandsTestCase(TestCase):

    def test_setsuperuser(self):
        """setsuperuser command is called with the correct args, otherwise aborts"""

        # Test calling with no parameters
        out = StringIO()
        call_command('setsuperuser', stderr=out)
        self.assertEqual(
            out.getvalue(),
            'Username must be specified\n'
        )
        out.close()

        # Test calling with user that doesn't exist
        out = StringIO()
        call_command('setsuperuser', username='doesnotexist', stderr=out)
        self.assertEqual(
            out.getvalue(),
            'User does not exist\n'
        )
        out.close()

        # Test calling with user that does exist. Create a user first
        # using call to 'createsuperuser'. This is okay because
        # 'setsuperuser' doesn't care if the account is already a superuser
        out = StringIO()
        call_command('createsuperuser',
                     username='specialtemptestaccount',
                     email='test@test.org',
                     interactive=False,
                     stdout=out)
        self.assertEqual(
            out.getvalue(),
            'Superuser created successfully.\n'
        )
        out.close()

        out = StringIO()
        call_command('setsuperuser', username='specialtemptestaccount', stdout=out)
        self.assertEqual(
            out.getvalue(),
            'User promoted to a superuser\n'
        )
        out.close()

    def test_addadvisorysource(self):
        """addadvisorysource command is called with the correct args, otherwise aborts"""

        # Test adding valid sources
        out = StringIO()
        call_command('addadvisorysource',
                     module="drupal",
                     title="Drupal Test",
                     url="https://www.drupal.org/security/test/rss.xml",
                     stdout=out)
        self.assertEqual(
            out.getvalue(),
            'Added advisory source. You will still need to run "importadvisories"\n'
        )
        out.close()

        # Test adding source with title, but no url
        out = StringIO()
        call_command('addadvisorysource', module="drupal", title="Test", stderr=out)
        self.assertEqual(
            out.getvalue(),
            'Error: url must be specified\n'
        )
        out.close()

        # Test adding source with url, but no title
        out = StringIO()
        call_command('addadvisorysource', module="drupal",
                     url="https://www.drupal.org/security/test/rss.xml", stderr=out)
        self.assertEqual(
            out.getvalue(),
            'Error: title must be specified\n'
        )
        out.close()

        # Test adding source with invalid url
        out = StringIO()
        call_command('addadvisorysource',
                     module="drupal",
                     title="Invalid URL",
                     url="file://www.drupal.org/security/test/rss.xml",
                     stderr=out)
        self.assertIn(
            'Error processing URL.',
            out.getvalue(),
        )
        out.close()

    @patch('archimedes.drupal.utils.import_advisories')
    def test_importadvisories(self, mock_import):
        """utils.import_advisories is called with the correct args, otherwise throw an exception"""
        src1 = AdvisorySource.objects.create(slug='source-1', title='Source 1', url='http://test.com/source1')
        src2 = AdvisorySource.objects.create(slug='source-2', title='Source 2', url='http://test.com/source2')

        # Test "all" sources
        out = StringIO()
        mock_import.reset_mock()
        call_command('importadvisories', stdout=out)
        self.assertEqual(
            mock_import.call_args_list,
            [
                ((src1, None),),
                ((src2, None),),
            ]
        )
        self.assertEqual(
            out.getvalue(),
            'Importing advisories from the "Source 1" source\nImporting advisories from the "Source 2" source\n'
        )
        out.close()

        # Test a specific source
        out = StringIO()
        mock_import.reset_mock()
        call_command('importadvisories', sources=['source-1'], stdout=out)
        self.assertEqual(mock_import.call_args, ((src1, None),))
        self.assertEqual(out.getvalue(), 'Importing advisories from the "Source 1" source\n')
        out.close()

        # Test a non-existent source
        out = StringIO()
        mock_import.reset_mock()
        with self.assertRaises(CommandError) as cm:
            call_command('importadvisories', sources=['source-2', 'source-3'], stdout=out)
        self.assertEqual(str(cm.exception), 'Could not find advisory source(s): source-3')
        mock_import.assert_not_called()
        out.close()

        # Mismatch of files and sources
        out = StringIO()
        mock_import.reset_mock()
        with self.assertRaises(CommandError) as cm:
            call_command('importadvisories', sources=['source-1', 'source-2'], files=['onlyonefile'], stdout=out)
        self.assertEqual(str(cm.exception),
                         'Mismatch - the number of XML files specified (1) does not match the number of sources (2)')
        mock_import.assert_not_called()
        out.close()

        # Source with a file to get the XML from
        out = StringIO()
        mock_import.reset_mock()
        m = mock_open(read_data='supercoolxmldata')
        with patch('archimedes.drupal.management.commands.importadvisories.open', m):
            call_command('importadvisories', sources=['source-1'], files=['file1.xml'], stdout=out)
        self.assertEqual(m.call_args, (('file1.xml', 'r'),))
        self.assertEqual(mock_import.call_args, ((src1, 'supercoolxmldata'),))
        out.close()

        # Source with a non-existent file to get the XML from
        out = StringIO()
        mock_import.reset_mock()
        with self.assertRaises(CommandError) as cm:
            call_command('importadvisories', sources=['source-1'], files=['file1.xml'], stdout=out)
        self.assertEqual(str(cm.exception), "[Errno 2] No such file or directory: 'file1.xml'")
        mock_import.assert_not_called()
        out.close()

    def fake_download_releases(project_slug, force, core, retry):
        return

    @patch('archimedes.drupal.utils.download_releases', side_effect=fake_download_releases)
    def test_syncreleases(self, mock_download):
        """utils.download_releases is called with the correct args"""
        out = StringIO()
        mock_download.reset_mock()
        call_command('syncreleases', 'some_module', stdout=out)
        self.assertEqual(
            mock_download.call_args_list,
            [
                (('some_module',), {'force': True, 'retry': True, 'core': 'all'}),
                (('some_module',), {'force': True, 'retry': True, 'core': '6.x'}),
                (('some_module',), {'force': True, 'retry': True, 'core': '7.x'}),
                (('some_module',), {'force': True, 'retry': True, 'core': '8.x'}),
            ]
        )
        out.close()

    def test_syncrelease_terms(self):
        """Ensure releases, when synced, include 'Release type' terms."""
        with sync_vcr.use_cassette('updates-releases-views.yaml') as cassette:
            download_releases('views', force=True, core='7.x', retry=True)
        self.assertTrue(cassette.all_played)
        # At time of VCR recording, 28 7.x releases.
        self.assertEquals(Release.objects.filter(project__slug='views').count(), 28)
        # Expecting four unique release types:
        # - Bug fixes
        # - New features
        # - Security update
        # - Insecure
        self.assertEquals(ReleaseType.objects.count(), 4)
        self.assertTrue(Release.objects.get(project__slug='views', version='7.x-3.8').is_security_release)
        self.assertFalse(Release.objects.get(project__slug='views', version='7.x-3.7').is_security_release)
        self.assertTrue(Release.objects.get(project__slug='views', version='7.x-3.7').is_marked_insecure)
        self.assertFalse(Release.objects.get(project__slug='views', version='7.x-3.6').has_bug_fixes)
        self.assertTrue(Release.objects.get(project__slug='views', version='7.x-3.5').has_bug_fixes)
        self.assertFalse(Release.objects.get(project__slug='views', version='7.x-3.5').has_new_features)

    def fake_parse_advisory(advisory_pk):
        return

    @patch('archimedes.drupal.tasks.parse_advisory.delay', side_effect=fake_parse_advisory)
    def test_parseadvisories(self, mock_parse):
        """Given the archimedes parseadvisories command in the terminal If there are no advisories, inform the user.\
        Otherwise call tasks.parse_advisory with all advisories where ignored !=True """
        src = AdvisorySource.objects.create(slug='testsource')

        out = StringIO()
        mock_parse.reset_mock()
        call_command('parseadvisories', stdout=out)
        self.assertEqual(
            out.getvalue(),
            'No advisories are awaiting processing at the moment\n'
        )
        out.close()

        adv1 = Advisory.objects.create(title='Test Advisory 1', guid='test-adv1',
                                       date_posted=timezone.now(), source_id=src.pk)
        # adv2 = Advisory.objects.create(title='Test Advisory 2', guid='test-adv2',
        #                                date_posted=timezone.now(), source_id=src.pk, ignored=True)
        adv3 = Advisory.objects.create(title='Test Advisory 3', guid='test-adv3',
                                       date_posted=timezone.now(), source_id=src.pk)

        out = StringIO()
        mock_parse.reset_mock()
        call_command('parseadvisories', stdout=out)
        self.assertCountEqual(
            [x[0][0] for x in mock_parse.call_args_list],
            [adv1.pk, adv3.pk]
        )
        out.close()

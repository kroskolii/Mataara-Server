

from django.core.management.base import BaseCommand

from ...utils import download_releases


class Command(BaseCommand):
    help = 'Download releases for a Drupal project'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('project', nargs=1, type=str,
                            help='project slug (machine name) to sync')

    def handle(self, *args, **options):
        for core in ['all', '6.x', '7.x', '8.x']:
            download_releases(options['project'][0], force=True, core=core, retry=True)

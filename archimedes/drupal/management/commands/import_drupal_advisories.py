from django.core.management.base import BaseCommand, CommandError

from ...do_advisory_parser import import_api_advisories
from ...models import AdvisorySource


class Command(BaseCommand):
    help = 'Import Drupal security advisories via drupal.org API'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('sources', nargs='*', type=str, help='advisory source slug(s) to import from')

        # Named (optional) arguments
        parser.add_argument(
            '--nid',
            action='append',
            dest='nid',
            default=[],
            help="Drupal.org node ID(s) to import from, instead of the source's configured URL. Comma-separated."
        )

    def handle(self, *args, **options):

        # Validate advisory sources against system AdvisorySource list.
        sources = AdvisorySource.objects.filter(slug__in=options['sources'])
        if len(sources) != len(options['sources']):
            # Prune all valid advisories from options before displaying the
            # invalid (missing) ones in the error.
            for s in sources:
                if s.slug in options['sources']:
                    options['sources'].remove(s.slug)
            raise CommandError('Could not find advisory source(s): %s' % ', '.join(options['sources']))

        # If no sources specified, import all API sources.
        if len(sources) == 0:
            sources = AdvisorySource.objects.filter(slug__endswith="api")

        # If node ids specified, use those instead of any sources.
        nids = None
        if options['nid']:
            # Allows for multiple --nid values and comma-separated arguments.
            # e.g.: --nid 202385 --nid 238538,284822
            nids = ','.join(options['nid']).split(',')
            self.stdout.write('Importing advisories from the specified node IDs.')
            import_api_advisories(None, nids)
        else:
            # Import advisories by source
            for i, source in enumerate(sources):
                self.stdout.write('Importing advisories from the "{}" source'.format(source.title))
                self.stdout.write('URL: {}'.format(source.url))
                counts = import_api_advisories(source, None)
                self.stdout.write("Advisories processed:\n"
                                  "    {checked} checked, {new} new, {updated} updated.".format(
                                    **counts
                                  ))

import datetime
import re
import urllib.error
import urllib.parse
import urllib.request
from collections import deque

from bs4 import BeautifulSoup
from dateutil.parser import parse

import pytz
from django.conf import settings
from django.core.exceptions import MultipleObjectsReturned

from .exceptions import (AdvisoryParserError, DrupalProjectDownloadError,
                         DrupalProjectNotFoundError)
from .models import Advisory, Project, Release, ReleaseType


def get_guid(nid):
    """Given a node ID, return in legacy GUID form."""
    guid = "{} at https://www.drupal.org".format(nid)
    return(guid)


def import_advisories(source, xml=None):
    """DEPRECATED: Download the RSS feed for the given advisory source"""

    # Download the feed xml, if not supplied
    if xml is None:
        # Fetch feed. Note URLs have been filtered through admin interface model and in 'addadvisorysource command'
        response = urllib.request.urlopen(source.url)  # nosec
        # Check that response is a valid XML RSS feed.
        response_type = response.getheader('content-type')
        if "rss+xml" in response_type:
            xml = response.read()
            import_rss_advisories(source, xml)
        elif "application/json" in response_type:
            raise AdvisoryParserError("Use import_drupal_advisories to import from API sources.")
        else:
            print("Advisory source URL {} did not return an RSS feed or JSON API. [Type: {}]".format(
                source.url,
                response_type
            ))
            return
    else:
        import_rss_advisories(source, xml)


def import_rss_advisories(source, xml):
    # Get a list of existing advisories
    existing = Advisory.objects.all()

    # Parse the XML and create advisory objects
    soup = BeautifulSoup(xml, 'lxml-xml')
    items = soup.find_all('item')

    for i, item in enumerate(items):

        # Check if the advisory has already been imported
        guid = item.guid.string.strip()
        matching = existing.filter(guid=guid)
        if len(matching) > 0:
            a = matching[0]
        else:
            a = Advisory()

        # Set the advisory data
        a.guid = guid
        a.title = fix_title(item.title.string.strip())
        a.url = item.link.string.strip()
        a.source = source
        a.date_posted = parse(item.pubDate.string)
        a.html = item.description.string

        # Save the advisory
        a.save()


def update_server_get(project, core='all', version=None):
    """Fetch data for the Drupal update server"""

    # Build URL
    url = '%s/release-history/%s' % (settings.DRUPAL_UPDATE_SERVER.strip("/"), project)
    if core:
        url += '/' + core
    if version:
        url += '/' + version

    # Fetch feed. Note URLs have been filtered through admin interface model and in 'addadvisorysource command'
    response = urllib.request.urlopen(url)  # nosec
    xml = response.read()

    # Return parsed XML
    return BeautifulSoup(xml, 'lxml-xml')


def download_releases(project_slug, force=False, core='all', retry=False):
    """Download all releases for a given project"""

    # Stop if we've already got releases for this project
    if not force and len(Release.objects.filter(project__slug=project_slug)):
        return

    # Get information on all releases
    soup = update_server_get(project_slug, core)

    # Check we've got the project we expected
    try:
        if soup.project.short_name.string != project_slug:
            raise DrupalProjectDownloadError('Incorrect project was downloaded')
    except AttributeError:
        if not retry:
            raise DrupalProjectNotFoundError('Could not find a project named "%s"' % project_slug)
        else:
            return

    # Get a project object
    p, created = Project.objects.get_or_create(slug=project_slug)

    # Update the project info
    p.name = soup.project.title.string
    p.slug = soup.project.short_name.string
    p.url = soup.project.link.string if soup.project.link else ''
    try:
        p.type = soup.project.type.string
    except AttributeError:
        pass
    try:
        p.maintenance_status = soup.project.terms.find(text='Maintenance status').parent.next_sibling.string
    except AttributeError:
        pass
    try:
        p.development_status = soup.project.terms.find(text='Development status').parent.next_sibling.string
    except AttributeError:
        pass
    p.save()

    # Create the releases if they don't exist
    try:
        releases = soup.project.releases.find_all('release')
    except AttributeError:
        releases = []

    # Sometimes the Drupal update server doesn't respond to 'all' properly, so we do it manually
    if len(releases) == 0 and not retry:
        for core in ['6.x', '7.x', '8.x']:
            download_releases(project_slug, force, core, retry=True)

    for release in releases:

        try:
            r, created = Release.objects.get_or_create(project=p, version=release.version.string)
        except MultipleObjectsReturned:
            continue

        r.project = p
        r.url = release.release_link.string if release.release_link else ''
        if release.date.string:
            r.date = datetime.datetime.fromtimestamp(int(release.date.string), pytz.utc)
        r.core = release.version.string.split('-')[0]
        r.version = release.version.string

        r.version_major = release.version_major.string if release.version_major else ''
        r.version_patch = release.version_patch.string if release.version_patch else ''
        r.version_extra = release.version_extra.string if release.version_extra else ''

        # Extract release types from XML for taxonomy terms.
        terms_tag = release.find('terms')
        if terms_tag is not None:
            term_tags = terms_tag.find_all('term')
            for term in term_tags:
                term_name = term.find('name').string
                term_value = term.value.string
                if (term_name == 'Release type'):
                    rt, created = ReleaseType.objects.get_or_create(label=term_value)
                    if created:
                        # Save new types immediately to avoid unique constraint violations.
                        rt.save()
                    r.release_types.add(rt)

        r.save()


def get_or_download(release_list):
    single = (type(release_list) != list)
    if single:
        release_list = [release_list]

    release_objects = []
    for release in release_list:
        if type(release) is str:
            project, version = release.split(' ')
            name = project
            url = ''
            description = ''
        else:
            project = release.get('Module', release.get('Theme', ''))
            name = release.get('Name', project)
            version = release.get('Version', '')
            url = release.get('Url', '')
            description = release.get('Description')
            if release.get('Package', '') == 'Core':
                continue  # Don't import core modules

        # Try to find it locally
        objs = Release.objects.filter(project__slug=project, version=version)
        if len(objs) > 0:
            release_objects.append(objs[0])
            continue

        # Sync all releases for this project, and try again
        try:
            download_releases(project)
        except DrupalProjectNotFoundError:
            # Treat this as a custom module
            custom_project, created = Project.objects.get_or_create(
                slug=project,
                defaults={
                    'name': name,
                    'custom': True,
                    'url': url,
                    'description': description,
                }
            )
            core, major, patch, extra = split_version(version)
            custom_release = Release.objects.create(
                project=custom_project,
                core=core or '',
                version=version,
                version_major=major or '',
                version_patch=patch or '',
                version_extra=extra or '',
            )
            release_objects.append(custom_release)
            continue

        # Otherwise, we've sync'd with Drupal.org, try to find the release
        objs = Release.objects.filter(project__slug=project, version=version)
        if len(objs) > 0:
            release_objects.append(objs[0])
            continue

        # Otherwise, release not found - create a custom release
        project_objs = Project.objects.filter(slug=project)
        if len(project_objs) > 0:
            core, major, patch, extra = split_version(version)
            custom_release = Release.objects.create(
                project=project_objs[0],
                core=core or '',
                version=version,
                version_major=major or '',
                version_patch=patch or '',
                version_extra=extra or ''
            )
            release_objects.append(custom_release)

    # Return the objects
    if single:
        return release_objects[0] if len(release_objects) != 0 else []
    else:
        return release_objects


def fix_title(s):

    # Make sure the ID is at the start
    if not s.startswith('SA-CO'):
        # Try to rotate things around a bit
        d = deque(s.split(' - '))
        d.rotate()
        sr = " - ".join(d)

        # Did that fix it?
        if sr.startswith('SA-CO') or sr.startswith('DRUPAL-SA-CO'):
            s = sr.replace('DRUPAL-SA-CO', 'SA-CO')

    # Make sure the ID is separated correctly with ' - '
    p = s.split(' ')
    if p[1] != '-':
        # Insert the correct separator
        p.insert(1, '-')

        # Fix whatever was there
        if p[0].endswith('-'):
            p[0] = p[0][:-1]
        if p[2].startswith('-'):
            p[2] = p[2][1:]

        s = ' '.join(p)

    # Remove double spaces
    s = re.sub(' +', ' ', s)

    return s


def normalize_whitespace(text):
    """Trim preceding, trailing and doubled spaces from a text string."""
    if text is None:
        return None
    if type(text) is not str:
        raise TypeError("Attempted to normalise whitespace on a non-string.")

    return ' '.join(text.split())


def process_report(report, drupalsite):
    """Process an incoming report"""

    # Update the basic information
    drupalsite.slogan = report.dict.get('SiteSlogan', '') or ''
    drupalsite.users = report.dict.get('Users', None)
    if type(report.dict.get('Nodes', False)) == dict:
        drupalsite.nodes = report.dict['Nodes'].get('Nodes', None)
        drupalsite.revisions = report.dict['Nodes'].get('Revisions', None)

    # Update the core version
    core = report.dict.get('DrupalVersion', False)
    if core:
        drupalsite.set_core(core)

    # Update the modules
    module_list = report.dict.get('Modules', {})
    module_objects = []
    for module in module_list:
        obj = get_or_download(module)
        if obj:
            module_objects.append(obj)
    drupalsite.modules.clear()
    drupalsite.modules = module_objects

    # Update the themes
    theme_list = report.dict.get('Themes', {})
    theme_objects = []
    for theme in theme_list:
        obj = get_or_download(theme)
        if obj:
            theme_objects.append(obj)
    drupalsite.themes.clear()
    drupalsite.themes = theme_objects

    # Save the drupalsite
    drupalsite.save()


def split_version(version):
    """Splits a release version into parts"""

    parts = version.split('-')

    if len(parts) == 1:
        return None, None, None, None
    elif len(parts) == 2:
        core, extra = parts
        major, patch = None, None
    elif len(parts) == 3:
        core, middle, extra = parts
        major, patch = middle.split('.')
    else:
        return None, None, None, None

    return core, major, patch, extra

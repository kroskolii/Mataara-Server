# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0048_advisory_solution'),
    ]

    operations = [
        migrations.AlterField(
            model_name='release',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='release',
            name='url',
            field=models.URLField(),
        ),
        migrations.AlterField(
            model_name='release',
            name='version',
            field=models.CharField(max_length=50),
        ),
    ]

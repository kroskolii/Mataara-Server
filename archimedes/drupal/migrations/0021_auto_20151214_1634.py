# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0006_auto_20151214_1634'),
        ('drupal', '0020_advisory_ignore'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='drupalsite',
            name='id',
        ),
        migrations.AddField(
            model_name='drupalsite',
            name='site_ptr',
            field=models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, default=0, serialize=False, to='sites.Site'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='advisory',
            name='date_parsed',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]

# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0022_auto_20151215_1152'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisory',
            name='cores',
            field=models.ManyToManyField(to='drupal.Core', blank=True),
        ),
        migrations.AlterField(
            model_name='advisory',
            name='cves',
            field=models.ManyToManyField(to='drupal.CVE', blank=True),
        ),
        migrations.AlterField(
            model_name='advisory',
            name='releases',
            field=models.ManyToManyField(to='drupal.Release', blank=True),
        ),
        migrations.AlterField(
            model_name='advisory',
            name='vulnerabilities',
            field=models.ManyToManyField(to='drupal.Vulnerability', blank=True),
        ),
    ]

# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0004_auto_20151125_1824'),
    ]

    operations = [
        migrations.AlterField(
            model_name='release',
            name='version_extra',
            field=models.CharField(default=b'', max_length=50, blank=True),
        ),
        migrations.AlterField(
            model_name='release',
            name='version_patch',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0040_auto_20170323_1146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisorysource',
            name='url',
            field=models.URLField(unique=True, help_text='Enter the URL for the advisory source. Only http(s):// or ftp(s):// are supported'),
        ),
    ]

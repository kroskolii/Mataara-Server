# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0024_auto_20151215_1214'),
    ]

    operations = [
        migrations.AddField(
            model_name='advisory',
            name='advisory_id',
            field=models.CharField(max_length=200, blank=True),
        ),
    ]

# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0014_drupalsite_core'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='development_status',
            field=models.CharField(max_length=200, blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='maintenance_status',
            field=models.CharField(max_length=200, blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='url',
            field=models.URLField(blank=True),
        ),
        migrations.AlterField(
            model_name='release',
            name='version_major',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
    ]

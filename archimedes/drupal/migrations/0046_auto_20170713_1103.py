# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0045_auto_20170711_1338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisory',
            name='reviewer',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='drupal_advisory', blank=True, null=True),
        ),
    ]

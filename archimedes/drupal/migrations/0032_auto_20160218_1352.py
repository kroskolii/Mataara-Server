# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0031_auto_20160204_1455'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='release',
            options={'ordering': ['-version', '-date']},
        ),
        migrations.AlterField(
            model_name='advisory',
            name='project',
            field=models.ForeignKey(related_name='advisories', blank=True, to='drupal.Project', null=True),
        ),
        migrations.AlterField(
            model_name='advisory',
            name='releases',
            field=models.ManyToManyField(related_name='advisories', to='drupal.Release', blank=True),
        ),
        migrations.AlterField(
            model_name='advisory',
            name='status',
            field=models.CharField(default=b'downloaded', max_length=50, choices=[(b'downloaded', b'Downloaded'), (b'parsed', b'Needs Review'), (b'reviewed', b'Reviewed'), (b'ignored', b'Ignored')]),
        ),
    ]

# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0005_auto_20151125_1827'),
    ]

    operations = [
        migrations.AddField(
            model_name='release',
            name='version',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
    ]

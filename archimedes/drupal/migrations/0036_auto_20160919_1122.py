# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0035_auto_20160915_1230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisory',
            name='reviewer',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True, null=True),
        ),
    ]

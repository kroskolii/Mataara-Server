# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0016_auto_20151126_1404'),
    ]

    operations = [
        migrations.AlterField(
            model_name='release',
            name='core',
            field=models.CharField(default=b'', max_length=50, blank=True),
        ),
    ]

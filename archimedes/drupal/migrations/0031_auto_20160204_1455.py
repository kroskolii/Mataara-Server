# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0030_auto_20160204_1452'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisory',
            name='status',
            field=models.CharField(default=b'downloaded', max_length=50, choices=[(b'downloaded', b'Downloaded'), (b'queued', b'Queued for processing'), (b'parsed', b'Processed (needs review)'), (b'reviewed', b'Reviewed'), (b'ignored', b'Ignored')]),
        ),
    ]

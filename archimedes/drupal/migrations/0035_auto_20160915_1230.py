# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0034_auto_20160915_1214'),
    ]

    operations = [
        migrations.AddField(
            model_name='advisory',
            name='cves',
            field=models.ManyToManyField(blank=True, to='drupal.CVE'),
        ),
        migrations.AddField(
            model_name='advisory',
            name='vulnerabilities',
            field=models.ManyToManyField(blank=True, to='drupal.Vulnerability'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0032_auto_20160218_1352'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisory',
            name='status',
            field=models.CharField(default='downloaded', max_length=50, choices=[('downloaded', 'Downloaded'), ('parsed', 'Needs Review'), ('reviewed', 'Reviewed'), ('ignored', 'Ignored')]),
        ),
        migrations.AlterField(
            model_name='release',
            name='core',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='release',
            name='version_extra',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='release',
            name='version_major',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='release',
            name='version_patch',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
    ]

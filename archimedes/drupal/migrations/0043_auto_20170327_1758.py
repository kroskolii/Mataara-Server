# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0042_auto_20170327_1756'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisorysource',
            name='title',
            field=models.CharField(max_length=200, help_text='Enter the name for this advisory source.', unique=True),
        ),
        migrations.AlterField(
            model_name='advisorysource',
            name='url',
            field=models.URLField(help_text='Enter URL for the advisory source. Only http(s):// or ftp(s):// are supported.', unique=True),
        ),
    ]

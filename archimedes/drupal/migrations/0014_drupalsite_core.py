# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0013_auto_20151126_1259'),
    ]

    operations = [
        migrations.AddField(
            model_name='drupalsite',
            name='core',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
    ]

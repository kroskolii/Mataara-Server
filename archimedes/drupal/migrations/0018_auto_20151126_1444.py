# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0017_auto_20151126_1404'),
    ]

    operations = [
        migrations.AlterField(
            model_name='release',
            name='version_major',
            field=models.CharField(default=b'', max_length=50, blank=True),
        ),
        migrations.AlterField(
            model_name='release',
            name='version_patch',
            field=models.CharField(default=b'', max_length=50, blank=True),
        ),
    ]

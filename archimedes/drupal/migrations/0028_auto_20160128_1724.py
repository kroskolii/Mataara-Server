# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0027_auto_20160127_1435'),
    ]

    operations = [
        migrations.AlterField(
            model_name='release',
            name='project',
            field=models.ForeignKey(related_name='releases', to='drupal.Project'),
        ),
    ]

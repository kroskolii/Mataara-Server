# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0019_project_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='advisory',
            name='ignore',
            field=models.BooleanField(default=False),
        ),
    ]

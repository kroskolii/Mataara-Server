# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('drupal', '0029_auto_20160204_1209'),
    ]

    operations = [
        migrations.RenameField(
            model_name='advisory',
            old_name='ignore',
            new_name='ignored',
        ),
        migrations.AddField(
            model_name='advisory',
            name='reviewer',
            field=models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='advisory',
            name='status',
            field=models.CharField(default=b'downloaded', max_length=50),
        ),
    ]

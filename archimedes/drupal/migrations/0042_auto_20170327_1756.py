# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0041_auto_20170327_1751'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisorysource',
            name='title',
            field=models.CharField(help_text='Enter the name you would like to use to refer to this source', max_length=200, unique=True),
        ),
        migrations.AlterField(
            model_name='advisorysource',
            name='url',
            field=models.URLField(help_text='Enter URL for the advisory source. Only http(s):// or ftp(s):// are supported', unique=True),
        ),
    ]

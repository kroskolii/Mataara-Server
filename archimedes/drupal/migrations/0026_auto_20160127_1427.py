# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0025_advisory_advisory_id'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='project',
            options={'ordering': ['name']},
        ),
        migrations.AlterField(
            model_name='core',
            name='version',
            field=models.CharField(unique=True, max_length=50),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0036_auto_20160919_1122'),
    ]

    operations = [
        migrations.AddField(
            model_name='advisory',
            name='failed',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='advisory',
            name='status',
            field=models.CharField(default='downloaded', max_length=50, choices=[('downloaded', 'Downloaded'), ('parsed', 'Needs Review'), ('reviewed', 'Reviewed'), ('ignored', 'Ignored'), ('failed', 'Parsing Failed')]),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def disable_old_advisory_tasks(apps, schema_editor):
    """Disable scheduled tasks, prior to their eventual retirement."""
    PeriodicTask = apps.get_model('djcelery', 'PeriodicTask')
    targets = [
        'archimedes.drupal.tasks.import_advisories',
        'archimedes.drupal.tasks.parse_advisories'
    ]

    for target in targets:
        task_qs = PeriodicTask.objects.filter(task=target)
        for task in task_qs:
            task.enabled = False
            task.save()


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0002_auto_20180518_1525'),
    ]

    operations = [
        migrations.RunPython(disable_old_advisory_tasks)
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0047_project_drupal_org_nid'),
    ]

    operations = [
        migrations.AddField(
            model_name='advisory',
            name='solution',
            field=models.TextField(null=True, blank=True),
        ),
    ]

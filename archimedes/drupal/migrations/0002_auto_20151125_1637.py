# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('slug', models.CharField(max_length=200)),
                ('url', models.URLField()),
                ('custom', models.BooleanField(default=False)),
                ('type', models.CharField(max_length=50)),
                ('maintenance_status', models.CharField(max_length=200)),
                ('development_status', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Release',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('core', models.CharField(max_length=50)),
                ('version_major', models.PositiveIntegerField()),
                ('version_patch', models.PositiveIntegerField()),
                ('version_tag', models.CharField(max_length=50)),
                ('project', models.ForeignKey(to='drupal.Project')),
            ],
        ),
        migrations.AddField(
            model_name='drupalsite',
            name='core',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='drupalsite',
            name='nodes',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='drupalsite',
            name='revisions',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='drupalsite',
            name='slogan',
            field=models.CharField(max_length=200, blank=True),
        ),
        migrations.AddField(
            model_name='drupalsite',
            name='users',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='drupalsite',
            name='modules',
            field=models.ManyToManyField(related_name='site_modules', to='drupal.Release'),
        ),
        migrations.AddField(
            model_name='drupalsite',
            name='themes',
            field=models.ManyToManyField(related_name='site_themes', to='drupal.Release'),
        ),
    ]

# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0010_auto_20151126_1253'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='drupalsite',
            name='core',
        ),
    ]

# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drupal', '0028_auto_20160128_1724'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vulnerability',
            name='name',
            field=models.CharField(max_length=200),
        ),
    ]

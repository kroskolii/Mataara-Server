import json
import logging
import re
# import operator
# import string
import sys
# from dateutil.parser import parse
from datetime import datetime
# from pprint import pformat
# from os.path import commonprefix
from urllib.parse import urlparse

from bs4 import BeautifulSoup as Soup

from celery import current_app
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils import timezone
from packaging import version as version_parser
from raven.contrib.django.raven_compat.models import client as raven_client

from .do_api import DrupalOrgAPI as do_api
from .exceptions import AdvisoryParserError, AffectedVersionError
from .models import (Advisory, AdvisorySource, Core, Project, Release,  # , CVE
                     Risk, Vulnerability)
from .parser import AdvisoryParser as LegacyAdvisoryParser
from .utils import (download_releases, fix_title,  # , normalize_whitespace
                    get_guid)

# Ignore advisories earlier than 2017-01-01 UTC.
#
# EARLIEST_TIMESTAMP is a relatively arbitrary cutoff for elderly data, but
# since there are a lot of old advisories that are even less standardised in
# their formatting, this allows us to parse all relatively recent advisories
# with a minimum of extra code for special handling.

EARLIEST_TIMESTAMP = 1483228800

# Is Raven enabled? Assume it is if there is a RAVEN_CONFIG setting.
has_raven = hasattr(settings, 'RAVEN_CONFIG')

logger = logging.getLogger(__name__)

# TODO: This should eventually be in Fabric setup script, but we have a
# get_or_create for compatibility with older deployments for now.
sa_source = {
    "title": "Drupal SA API",
    "slug": "drupal-sa-api",
    "url": "https://www.drupal.org/api-d7/node.json?type=sa&status=1"
}

valid_project_types = ["project_module", "project_theme", "project_core"]


def import_api_advisories(source, nids=None):
    """
    Import security advisories from a Drupal.org API source.

    If a source value is supplied, pull all new or updated advisories from the API.

    If no source is applied, nids are supplied, pull specific node IDs and
    verify that their contents are of the correct type before attempting to
    parse them. Nominating node IDs explicitly forces a re-parse.
    If an advisory comes in from a source it will be parsed unless it has been
    set to be ignored in the Advisory model.
    """
    logger.debug("API Source: {}, Nids: {}".format(source, nids))

    # Statistics
    check_count = 0
    new_count = 0
    update_count = 0

    existing = Advisory.objects.all()

    if source is not None:
        pages = {}
        logger.debug("Fetch source URL: {}".format(source.url))
        response = do_api.request(source.url)
        page_number = do_api.get_current_page_number(response)
        # pages[page_number] = response['list']

        if(not do_api.is_first_page(response)):
            raise AdvisoryParserError("Please supply URL for first page of API source.")

        if(do_api.has_multi_pages(response)):
            logger.debug("Source has multiple pages.")
            last_url = do_api.get_last_page_url(response)
            logger.debug("Last page URL: {}".format(last_url))
            logger.debug("Page: {}".format(page_number))
            # Start at the very end
            response = do_api.request(last_url)
            while(True):
                page_number = do_api.get_current_page_number(response)
                # If we have the page that we started with, exit
                # if page_number in pages.keys():
                #     break
                logger.debug("Page: {}".format(page_number))
                pages[page_number] = response['list']

                # Traverse pagination back to first page.
                if do_api.is_first_page(response):
                    break

                # If all advisories on page are too old, don't look further.
                timestamps_old = [int(a['changed']) < EARLIEST_TIMESTAMP for a in response['list']]
                if False not in timestamps_old:
                    break
                prev_url = do_api.get_prev_page_url(response)
                response = do_api.request(prev_url)

        else:
            logger.debug("Source has single page.")
            # No further pagination needed
            pages[0] = response['list']

        logger.debug("API page count: {}".format(len(pages)))

        if nids is None:
            # Start populating a node ID array if one does not already exist.
            nids = []

        for page in list(pages):
            logger.debug("Page {}:".format(page))

            for advisory in pages[page]:
                check_count += 1
                guid = get_guid(advisory['nid'])

                # Ignore all advisories that are too old to worry about.
                if int(advisory['changed']) < EARLIEST_TIMESTAMP:
                    # logger.debug("Ignoring Advisory ID #{}: too old. ({})".format(
                    #     advisory['nid'],
                    #     datetime.fromtimestamp(int(advisory['changed']))
                    # ))
                    continue

                # Check for matching
                matching = existing.filter(guid=guid)
                if (len(matching) > 0):
                    # Already exists
                    target_advisory = matching[0]
                    # Has the 'ignored' field been set?
                    if target_advisory.ignored:
                        logger.debug(
                            "Ignoring Advisory ID #%s: Set to Ignored in model",
                            advisory['nid'])
                        continue
                    old_timestamp = int(target_advisory.date_updated.timestamp())

                    # Has the advisory been changed since last time it was updated?
                    if old_timestamp < int(advisory['changed']):
                        logger.info("Update for advisory #{}: {}".format(
                            advisory['nid'],
                            advisory['title']
                        ))
                        update_count += 1
                        if advisory['nid'] not in nids:
                            nids.append(advisory['nid'])
                    else:
                        logger.info("Advisory #{} checked and up to date".format(advisory['nid']))
                else:
                    # New advisory
                    logger.info("New advisory #{}: {}".format(
                        advisory['nid'],
                        advisory['title']
                    ))
                    new_count += 1
                    if advisory['nid'] not in nids:
                        nids.append(advisory['nid'])

    if nids is not None:
        for nid in nids:
            # Request and parse each node.
            logger.info("Fetching node {}".format(nid))
            advisory_json = do_api.request_node(nid, return_raw=True)

            current_app.send_task('archimedes.drupal.tasks.parse_api_advisory', [advisory_json])
            # parse_api_advisory.delay(advisory_json)
        pass

    else:
        raise AdvisoryParserError(
            "Could not import advisories from API: neither Advisory source nor specific node IDs supplied."
        )

    return({
        "checked": check_count,
        "new": new_count,
        "updated": update_count
    })


class DOAdvisoryParser(object):
    """
    Parses security advisories from Drupal.org for Django to store in the ORM.
    """

    def cores(self, versions):
        """
        Determine core versions affected by this advisory.
        Returns a set of core versions.
        """
        cores = set()
        for (match, project, version) in versions:
            core = self.get_core_from_version(version)
            cores.add(core)
        return(cores)

    def find_key(self, key):
        """
        Get values from within the json structure in advisory_dict.

        Takes either strings or lists. For lists, it walks through the dict.
        Returns None if key/path element is not found.

        Raises AdvisoryParserError if advisory_dict is not populated.
        """
        if not hasattr(self, "advisory_dict"):
            raise AdvisoryParserError("Attempted to get advisory data without loading JSON.")

        if isinstance(key, str):
            if key in self.advisory_dict:
                return (self.advisory_dict[key])
            else:
                return None
        elif isinstance(key, list):
            cursor = self.advisory_dict
            try:
                for k in key:
                    cursor = cursor[k]
                return cursor
            except KeyError:
                return None
        else:
            raise AdvisoryParserError("Attempted to find key with unknown type: {}".format(type(key)))

    def get_core_from_version(self, version):
        """Return a core signature given a version string."""
        if '-' in version:
            # Assuming a C.x-y.z signature.
            core = version.split('-')[0]
        else:
            parts = version.split('.')
            if len(parts) == 3:
                # Assume a C.x.y signature.
                (major, minor, patch) = parts
                core = "{}.{}.x".format(major, minor)
            elif len(parts) == 2:
                (major, minor) = parts
                core = "{}.x".format(major)
            else:
                raise AffectedVersionError("Unexpected version signature: {}".format(
                    version
                ))

        return core

    def get_or_fetch_project(self, nid):
        """
        Retrieve project details for a particular Drupal.org node ID.
        """
        try:
            project = Project.objects.get(drupal_org_nid=nid)
            logger.debug("Project found: {} {}".format(nid, project.slug))
        except Project.DoesNotExist:
            logger.debug("No project found in Mataara with NID {}; fetching.".format(nid))

            # Retrieve project details from Drupal API.
            response = do_api.request_node(nid)
            # logger.debug("Get node {}:\n{}".format(nid, response))
            logger.debug("Project machine name: {}".format(response['field_project_machine_name']))

            if 'type' not in response.keys() or response['type'] not in valid_project_types:
                logger.error("Response from project node ID {} does not appear to have a valid type: {}".format(
                    nid,
                    response['type'] if 'type' in response else "no type found"
                ))
                project = None

            elif 'field_project_machine_name' in response.keys():
                try:
                    machine_name = response['field_project_machine_name']
                    # Fetch existing project, add node ID and save.
                    project = Project.objects.get(slug=machine_name)
                    project.drupal_org_nid = nid
                    project.save()
                except Project.DoesNotExist:
                    # Project new to Mataara database.
                    # Borrowing old project/releases download code until it can be refactored.
                    download_releases(machine_name)
                    project = Project.objects.get(slug=machine_name)
                    project.drupal_org_nid = nid
                    project.save()
            else:
                # No project found.
                logger.error("No machine name found for Project [nid:{}]".format(nid))
                project = None

        return(project)

    def get_risk_scores(self):
        """
        Fetch risk scores from sa advisory type.

        See: https://www.drupal.org/drupalorg/docs/api for format.

        Returns three values:
        - risk_label
        - risk_score
        - risk_score_total
        """
        sa_risk = self.find_key("sa_risk")
        risk_score, risk_label = sa_risk
        risk_score_total = 25  # Per "Security advisory risk" section in API docs.

        return(risk_label, risk_score, risk_score_total)

    def get_projects_affected(self):
        """
        Get a set of projects affected in a parsed Advisory.
        """
        return {project for [_, project, _] in self.match_versions}

    def get_release_versions_affected(self):
        """
        Get a list of project releases affected in a parsed Advisory.
        """
        # Default: no matches
        releases = []
        for mv in self.match_versions:
            (match, project, version) = mv
            core = self.get_core_from_version(version)
            if match == '<':
                candidates = Release.objects.filter(project__slug=project, core=core)
                for c in candidates:
                    if version_parser.parse(c.version) < version_parser.parse(version):
                        releases.append(c)
            else:
                raise AdvisoryParserError("Tried to match releases with unknown operator: {}".format(
                    match
                ))

        return releases

    def parse(self, advisory, forcedownload=False):
        """
        Parse an incoming Drupal.org advisory in JSON, or from a dictionary.

        Determine whether to use new or legacy parsing by checking keys:
        - type=sa : new
        - type=forum, taxonomy_forums.id=44: legacy contrib
        - type=forum, taxonomy_forums.id=1852: legacy core

        Any other results should raise an AdvisoryParserError.

        advisory : a JSON source string or dictionary.
        forcedownload : force (re-)downloading of release data.
        """
        if type(advisory) == str:
            # If JSON string, generate dictionary form.
            self.advisory_json = advisory
            self.advisory_dict = json.loads(advisory)
        elif type(advisory) == dict:
            # If dictionary, regenerate JSON form.
            self.advisory_dict = advisory
            self.advisory_json = json.dumps(advisory)
        else:
            raise AdvisoryParserError("Neither JSON string or dictionary found in input (Type: {}).".format(
                type(advisory)
            ))

        self.validate_required_keys(['type', 'nid'])

        self.advisory_dict = self.parser_fixes(self.advisory_dict)

        advisory_type = self.find_key('type')
        if advisory_type == 'sa':
            return self.parse_sa_type()
        elif advisory_type == 'forum':
            # Verify taxonomy ID for core or contrib.
            forum_tid = self.find_key(['taxonomy_forums', 'id'])
            logger.debug("Parsing forum-type SA [nid: {}]".format(self.advisory_dict['nid']))

            if forum_tid == "44":
                # Legacy Contrib
                return self.parse_forum_type(AdvisorySource.objects.get(slug='drupal-contrib-api'))
            elif forum_tid == "1852":
                # Legacy Core
                return self.parse_forum_type(AdvisorySource.objects.get(slug='drupal-core-api'))
            else:
                raise AdvisoryParserError(
                    "Node {} is not a legacy security advisory [forum type: {}]".format(
                        self.advisory_dict['nid'],
                        self.advisory_dict['type']
                    ))
        else:
            raise AdvisoryParserError(
                "Node {} is not a security advisory node [actual type: {}]".format(
                    self.advisory_dict['nid'],
                    self.advisory_dict['type']
                ))

    def parse_forum_type(self, advisory_source):
        """
        Parse an incoming Drupal.org advisory in legacy JSON.

        advisory_source : the AdvisorySource for the incoming advisory.
        """
        self.validate_required_keys([
            'nid',
            'url',
            'changed',
            'title',
            ['body', 'value']
        ])
        logger.debug("Parsing legacy advisory from: {}".format(advisory_source.title))

        # Link into legacy parser code.
        legacy_parser = LegacyAdvisoryParser()
        # Construct guid to match old guid format.
        guid = get_guid(self.find_key('nid'))
        # Update if GUID found, otherwise create a new advisory.
        search = Advisory.objects.filter(guid=guid)
        if (len(search) == 0):
            legacy_advisory = Advisory()
        else:
            legacy_advisory = search[0]
        # Derive advisory ID from title.
        legacy_advisory.title = fix_title(self.find_key('title'))
        legacy_advisory.html = self.find_key(['body', 'value'])
        legacy_advisory.url = self.find_key('url')
        legacy_advisory.guid = guid
        # Link to API AdvisorySource.
        legacy_advisory.source = advisory_source
        # - date_posted
        # Uses "changed" date in case the advisory is / has been updated.
        try:
            legacy_advisory.date_posted = timezone.make_aware(
                datetime.fromtimestamp(int(self.find_key("changed"))),
                timezone.get_default_timezone()
            )
        except ValueError:
            raise AdvisoryParserError("Could not parse 'changed' field '{}' as unix timestamp".format(
                self.find_key("changed")
            ))

        legacy_advisory.save()
        logger.info("Parsing Advisory #{}: {}".format(
            self.find_key('nid'),
            legacy_advisory.title
        ))
        legacy_parser.parse(legacy_advisory, api_source=True)
        self.advisory = legacy_parser.advisory
        return self.advisory

    def parse_sa_type(self):
        """
        Parse an incoming Drupal.org advisory from the 'sa' content type.
        """

        # Ensure required top-level keys are present.
        self.validate_required_keys([
            'nid',
            'url',
            'changed',
            ['field_sa_description', 'value'],
            ['field_sa_solution', 'value']
        ])

        logger.info("Parsing Advisory #{}: {}".format(
            self.find_key('nid'),
            self.find_key('title')
        ))

        # Construct guid to match old guid format.
        guid = get_guid(self.find_key('nid'))

        # Placeholder Advisory object. Replace with get_or_create later.
        try:
            self.advisory = Advisory.objects.get(guid=guid)
            logger.debug("Found GUID {}".format(guid))
        except Advisory.DoesNotExist:
            logger.debug("Creating GUID {}".format(guid))
            self.advisory = Advisory()
            self.advisory.guid = guid

        self.advisory.title = self.find_key('title')
        self.advisory.url = self.find_key('url')
        self.advisory.description = self.find_key(['field_sa_description', 'value'])

        # Link to 'sa' AdvisorySource.
        logger.debug("Source: {}".format(sa_source['title']))
        self.advisory.source, created = AdvisorySource.objects.get_or_create(**sa_source)

        # TODO: refactor name in model/migration.
        # Not actually HTML; we should rename this field so it works for both
        # legacy and API advisories, or add another field.
        self.advisory.html = self.advisory_json
        # logger.debug(self.advisory_dict.keys())

        # - date_posted
        # Uses "changed" date in case the advisory is / has been updated.
        try:
            self.advisory.date_posted = timezone.make_aware(
                datetime.fromtimestamp(int(self.find_key("changed"))),
                timezone.get_default_timezone()
            )
        except ValueError:
            raise AdvisoryParserError("Could not parse 'changed' field '{}' as unix timestamp".format(
                self.find_key("changed")
            ))

        # - advisory_id
        advisory_id_candidate = self.find_key('sa_id')
        if advisory_id_candidate is None:
            raise AdvisoryParserError("No 'sa_id' key found for Advisory #{}".format(
                self.find_key('nid')
            ))

        # Check it's consistent with the regular "SA-$SOURCE-$YEAR-$ADVISORY_ID" pattern.
        matches = re.match(r'^SA-\w+-\d+-\d+$', advisory_id_candidate)
        if matches is None:
            raise AdvisoryParserError("Advisory ID does not appear to be valid: '{}'".format(
                advisory_id_candidate
            ))

        self.advisory.advisory_id = advisory_id_candidate

        # - Find and set the project
        project_machine_name = self.find_key(["field_project", "machine_name"])
        project_nid = self.find_key(["field_project", "id"])

        if project_machine_name is None and project_nid is None:
            raise AdvisoryParserError(
                "Could not retrieve project details [no field_project id or machine_name]."
            )

        # Attempt to get via machine name, then node ID.
        if project_machine_name is not None:
            try:
                project = Project.objects.get(slug=project_machine_name)
                # Update nid field if not present (from legacy DB).
                if project.drupal_org_nid is None and project_nid is not None:
                    project.drupal_org_nid = project_nid
                    project.save()
            except Project.DoesNotExist:
                project = None
        else:
            project = None

        # Try via nid instead.
        if project is None:
            project = self.get_or_fetch_project(project_nid)

        # Match project if found.
        if project is not None:
            self.advisory.project = project
        else:
            raise AdvisoryParserError("Could not retrieve project details [ID: {}, name: {}].".format(
                project_nid, project_machine_name
            ))

        # - Set risk info
        label, score, total = self.get_risk_scores()

        self.advisory.risk_score = score
        self.advisory.risk_score_total = total
        obj, created = Risk.objects.get_or_create(name=label)
        logger.debug("Risk label '{}' {}.".format(label, "created" if created else "found"))
        self.advisory.risk = obj

        # - Set vulnerabilities
        # Unsure if the 'sa' type allows multiples in this field, but we'll assume it does.

        vulnerabilities = self.find_key('field_sa_type')
        if not isinstance(vulnerabilities, list):
            vulnerabilities = [vulnerabilities]

        self.advisory.save()  # Save advisory first, so it can be linked to.
        for v in vulnerabilities:
            obj, created = Vulnerability.objects.get_or_create(name=v)
            self.advisory.vulnerabilities.add(
                obj
            )

        # - Set CVE(s)
        # New SA type advisories don't seem to have any CVEs.
        # This is a placeholder, in case they do at some point in the future.

        # Solution
        # New to 'sa' type.
        self.advisory.solution = self.find_key(['field_sa_solution', 'value'])

        html = Soup(self.advisory.solution, 'html.parser')

        urls = []
        for li in html.find_all('li'):
            for a in li.find_all('a'):
                urls.append(a['href'])
        logger.debug("URLs: {}".format(urls))

        # Version matching
        self.match_versions = []
        for url in urls:
            url_result = urlparse(url)
            # Only attempt to process project URLs on drupal.org.
            if url_result.netloc in ['www.drupal.org', 'drupal.org', '']:
                if url_result.path.startswith('/node'):
                    # Visit node NID if present, verify it is a release, get details there.
                    result = re.match(r"\/node\/(\d+)\/?", url_result.path)
                    if result is None:
                        logger.warning("Advisory {}: No release match found in solution URL {}.".format(
                            self.advisory.advisory_id,
                            url))
                        continue

                    nid = result.group(1)
                    node = do_api.request_node(nid)
                    if node['type'] == 'project_release':
                        # Rewrite using the canonical URL of the release.
                        url_result = urlparse(node['url'])
                    else:
                        logger.warning("Attempting to parse release reference node: {}, type was: {}".format(
                            nid,
                            node['type']
                        ))

                if url_result.path.startswith('/project'):
                    result = re.match(r"\/project\/(.*)\/releases\/(.*)", url_result.path)
                    if result is None:
                        logger.warning("Advisory {}: No release match found in solution URL {}.".format(
                            self.advisory.advisory_id,
                            url))
                        continue

                    project_slug = result.group(1)
                    version = result.group(2)
                    logger.debug("Project: {} Release: {}".format(project_slug, version))
                    # Store matched versions in list for release matching.
                    self.match_versions.append(['<', project_slug, version])

        # Check for advisories with no version matches.
        if len(self.match_versions) == 0:
            sa_type = self.find_key('field_sa_type')
            # No warning needed for now-unsupported modules.
            if sa_type not in [
                "Unsupported",
                "Module Unsupported",
                "Unsupported Module"
            ]:
                logger.warning("Advisory {}: No matchable releases in solution field.".format(
                    self.advisory.advisory_id
                ), extra=self.find_key('field_sa_solution'))
        else:
            logger.debug("Advisory matches versions: {}".format(self.match_versions))

        # Core matching
        try:
            # Set core(s)
            try:
                cores = self.cores(self.match_versions)
            except (AffectedVersionError) as exc:
                # Report error.
                error_msg = "An unexpected error occurred for advisory #%s: %s" % (self.advisory.pk, exc)
                logger.error(error_msg)
                tb = sys.exc_info()[2]
                raise AdvisoryParserError(error_msg).with_traceback(tb)

            if isinstance(cores, set):
                self.advisory.cores.clear()
                for c in cores:
                    obj, created = Core.objects.get_or_create(version=c)
                    self.advisory.cores.add(
                        obj
                    )
            else:
                self.advisory.cores = []
        except (AdvisoryParserError) as exc:
            logger.error("Couldn't get the affected Drupal cores: %s" % str(exc))
            if has_raven:
                ident = raven_client.get_ident(raven_client.captureException())
                logger.error("Sentry ident: %s" % ident)
            return False

        # Release matching
        self.advisory.releases = self.get_release_versions_affected()

        self.advisory.date_parsed = timezone.now()
        self.advisory.failed = False
        try:
            self.advisory.full_clean()
        except ValidationError as exc:
            raise AdvisoryParserError("Could not validate advisory fields: {}".format(exc.message_dict))

        # Save advisory when we are happy with it.
        # logger.debug("Saving Advisory: {}".format(pformat(vars(self.advisory))))
        self.advisory.save()
        logger.info("Advisory #{} saved.".format(self.advisory.guid))

        # Inform caller that parsing has successfully completed.
        return True

    def parser_fixes(self, advisory_dict):
        """
        Adds fixes to specific advisories that are known to have bad formatting.

        This should be called after the JSON has been loaded into a dictionary,
        but before attempting to parse other fields.

        Returns a dictionary, updated with any necessary insertions or
        modifications.
        """
        if advisory_dict['nid'] == '3010569':
            # SA-CONTRIB-2018-071 has spaces in the ID name.
            if advisory_dict['field_sa_advisory_id'] == ' 071 ':
                advisory_dict['field_sa_advisory_id'] = '071'
            if advisory_dict['sa_id'] == 'SA-CONTRIB-2018- 071 ':
                advisory_dict['sa_id'] = 'SA-CONTRIB-2018-071'
            if advisory_dict['title'] == 'Decoupled Router - Critical - Access bypass - SA-CONTRIB-2018- 071 ':
                advisory_dict['title'] = 'Decoupled Router - Critical - Access bypass - SA-CONTRIB-2018-071'

        return(advisory_dict)

    def validate_required_keys(self, required_keys=[]):
        "Ensure advisory has keys required for its type."
        for key in required_keys:
            if self.find_key(key) is None:
                raise AdvisoryParserError("Required key '{}' not found in input.".format(key))

from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify
from unidecode import unidecode

from archimedes.sites.models import Site, SiteManager


class Advisory(models.Model):

    STATUS = (
        ('downloaded', 'Downloaded'),
        ('parsed', 'Needs Review'),
        ('reviewed', 'Reviewed'),
        ('ignored', 'Ignored'),
        ('failed', 'Parsing Failed'),
    )

    guid = models.CharField(max_length=200, unique=True)
    title = models.CharField(max_length=200)
    url = models.URLField()
    source = models.ForeignKey('AdvisorySource', default=None)

    date_posted = models.DateTimeField()
    date_updated = models.DateTimeField(auto_now=True)
    date_parsed = models.DateTimeField(null=True, blank=True)

    ignored = models.BooleanField(default=False)
    failed = models.BooleanField(default=False)
    reviewer = models.ForeignKey(User, null=True, blank=True, related_name='drupal_advisory')
    status = models.CharField(max_length=50, choices=STATUS, default='downloaded')

    html = models.TextField()
    # New in sa type.
    solution = models.TextField(null=True, blank=True)

    advisory_id = models.CharField(max_length=200, blank=True)

    cores = models.ManyToManyField('Core', blank=True)
    project = models.ForeignKey('Project', blank=True, null=True, related_name='advisories')
    releases = models.ManyToManyField('Release', blank=True, related_name='advisories')

    risk = models.ForeignKey('Risk', blank=True, null=True)
    risk_score = models.PositiveSmallIntegerField(blank=True, null=True)
    risk_score_total = models.PositiveSmallIntegerField(blank=True, null=True)

    vulnerabilities = models.ManyToManyField('Vulnerability', blank=True)
    cves = models.ManyToManyField('CVE', blank=True)

    class Meta:
        ordering = ['-date_posted']
        verbose_name_plural = 'Advisories'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        """Set the status before saving"""
        if self.ignored:
            self.status = 'ignored'
        elif self.failed:
            self.status = 'failed'
        elif not self.date_parsed:
            self.status = 'downloaded'
        elif self.reviewer:
            self.status = 'reviewed'
        else:
            self.status = 'parsed'
        super(Advisory, self).save(*args, **kwargs)

    @property
    def affected_sites(self):
        """Sites with releases affected by this advisory"""
        if (self.project and self.project.slug == 'drupal'):
            release_versions = [v.version for v in self.releases.all()]
            return DrupalSite.objects.filter(core_version__version__in=release_versions)
        else:
            return DrupalSite.objects.filter(modules__advisories__pk=self.pk)


class AdvisorySource(models.Model):
    title = models.CharField(max_length=200, unique=True,
                             help_text='Enter the name for this advisory source.')
    slug = models.SlugField(unique=True, editable=False)
    # URL validation checks enforced through admin web interface
    url = models.URLField(unique=True,
                          help_text='Enter URL for the advisory source. Only http(s):// or ftp(s):// are supported.')

    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.slug = slugify(unidecode(self.title))

        return super(AdvisorySource, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class Risk(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Vulnerability(models.Model):
    # Vulnerability list item on Druapl.org/security advisory source
    # acts like a type, although there does not seem to be a restricted set of choices
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class CVE(models.Model):
    identifier = models.CharField(max_length=50)

    def __str__(self):
        return self.identifier


class Project(models.Model):
    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=200, unique=True)
    url = models.URLField(blank=True)
    description = models.TextField(blank=True)
    date_updated = models.DateTimeField(auto_now=True)
    custom = models.BooleanField(default=False)
    type = models.CharField(max_length=50)
    maintenance_status = models.CharField(max_length=200, blank=True)
    development_status = models.CharField(max_length=200, blank=True)
    drupal_org_nid = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.slug

    @property
    def type_name(self):
        if self.type == 'project_module':
            return 'Module'
        elif self.type == 'project_theme':
            return 'Theme'
        else:
            return 'Project'

    @property
    def sites_using(self):
        return DrupalSite.objects.filter(modules__project__pk=self.pk)

    @property
    def sites_vulnerable(self):
        """All sites that have a release of this project that contain an advisory"""
        return DrupalSite.objects.filter(modules__project__pk=self.pk, modules__advisories__isnull=False)


class ReleaseManager(models.Manager):
    use_for_related_fields = True

    def get_queryset(self):
        return super(ReleaseManager, self).get_queryset().select_related('project')


class Release(models.Model):
    """A release of a project, such as a theme or model"""
    objects = ReleaseManager()

    project = models.ForeignKey('Project', related_name='releases', related_query_name='release')
    url = models.URLField()
    date = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    core = models.CharField(max_length=50, blank=True, default='')
    version = models.CharField(max_length=50)
    version_major = models.CharField(max_length=50, blank=True, default='')
    version_patch = models.CharField(max_length=50, blank=True, default='')
    version_extra = models.CharField(max_length=50, blank=True, default='')
    release_types = models.ManyToManyField('ReleaseType', blank=True, related_name='releases')

    class Meta:
        ordering = ['-version', '-date']

    @property
    def has_bug_fixes(self):
        return self.release_types.filter(label='Bug fixes').exists()

    @property
    def has_new_features(self):
        return self.release_types.filter(label='New features').exists()

    @property
    def is_marked_insecure(self):
        return self.release_types.filter(label='Insecure').exists()

    @property
    def is_security_release(self):
        return self.release_types.filter(label='Security update').exists()

    @property
    def slug(self):
        return self.project.slug + ':' + self.version

    @property
    def core_number(self):
        try:
            val = int(self.core.split('.')[0])
        except ValueError:
            return None
        return val

    def __str__(self):
        return self.slug


class ReleaseType(models.Model):
    label = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.label


class Core(models.Model):
    version = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.version


class DrupalSite(Site):
    label = 'Drupal'
    objects = SiteManager()
    core = models.PositiveIntegerField(blank=True, null=True)
    core_version = models.ForeignKey('Core', blank=True, null=True)
    slogan = models.CharField(max_length=200, blank=True)
    nodes = models.PositiveIntegerField(blank=True, null=True)
    revisions = models.PositiveIntegerField(blank=True, null=True)
    users = models.PositiveIntegerField(blank=True, null=True)

    modules = models.ManyToManyField(Release, related_name='site_modules', related_query_name='module_site')
    themes = models.ManyToManyField(Release, related_name='site_themes', related_query_name='theme_site')

    def set_core(self, core):
        """set the core field and create and add the :Core foreign key"""
        try:
            self.core = int(core.split('.')[0])
        except ValueError:
            self.core = None
        self.core_version, created = Core.objects.get_or_create(version=core.lower())

    def compare_json_against_latest_report(self, report_json, field_paths=None):
        """
        Check whether report has unexpected values based on last report from this site.
        Return a dict of changed fields and their old values. If no field_paths argument
        is specified, a default_fields set may be used.
        """
        default_field_paths = ['ReportType', ['Environment', 'Name'], ['Environment', 'Label'], 'ServerHostname']
        if field_paths is None:
            field_paths = default_field_paths

        latest = self.reports.latest('date_received')
        return latest.compare_json(report_json, field_paths=field_paths)

    @property
    def vulnerability_count(self):
        """Return total count of vulnerable components across core, modules and themes."""
        count = (1 if len(self.core_vulnerabilities) > 0 else 0)
        count += len(self.vulnerable_modules)
        count += len(self.vulnerable_themes)
        return count

    @property
    def core_vulnerabilities(self):
        """Returns any vulnerabilities for this site's core version."""
        return Advisory.objects.filter(releases__version=self.core_version.version).distinct()

    @property
    def vulnerable_modules(self):
        """All releases in self.modules that have an advisory"""
        return Release.objects.filter(module_site=self, advisories__pk__isnull=False).distinct()

    @property
    def vulnerable_themes(self):
        """All releases in self.themes that have an advisory"""
        return Release.objects.filter(theme_site=self, advisories__pk__isnull=False).distinct()

    @property
    def vulnerability_count_by_release(self):
        """Return total count of vulnerable releases across core, modules and themes."""
        count = len(self.vulnerable_core_by_release)
        count += len(self.vulnerable_modules_by_release)
        count += len(self.vulnerable_themes_by_release)
        return count

    @property
    def vulnerable_core_by_release(self):
        """Returns whether the core is on a vulnerable release."""
        core_release = Release.objects.filter(project__slug='drupal', version=self.core_version.version)
        return [c for c in core_release if c.is_marked_insecure]

    @property
    def vulnerable_modules_by_release(self):
        """All releases in self.modules that are marked insecure."""
        return Release.objects.filter(module_site=self, release_types__label='Insecure')

    @property
    def vulnerable_themes_by_release(self):
        """All releases in self.themes that are marked insecure."""
        return Release.objects.filter(theme_site=self, release_types__label='Insecure')

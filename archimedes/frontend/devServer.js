const path = require('path');
const express = require('express');
const webpack = require('webpack');
const config = require('./webpack.config.dev');

const app = express();
const compiler = webpack(config);

// const host = 'http://localhost';
const host = 'http://mataara-server.local';
const port = process.env.npm_config_port ? process.env.npm_config_port : 3000;

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath,
  index: "index_frontend.html",
  watchOptions: {
    poll: true,
    ignored: /node_modules/,
  },
  headers: {
    "Access-Control-Allow-Origin": "*", // * is fine as this is only used on the dev box
    "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
  },
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'index_frontend.html'));
});

app.listen(port, '0.0.0.0', (err) => {
  if (err) {
    console.log(err);
    return;
  }
  console.info('==> Listening on port %s. Open up %s:%s/ in your browser.', port, host, port);
});

import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Link } from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import C3Chart from 'react-c3js';
import 'c3/c3.css';

import '../styles/dashboard.scss';

class Dashboard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      drupalVisible: true,
      pieChartData: null,
      barChartData: null
    };

    this.doFetch = this.doFetch.bind(this);
    this.togglePanel = this.togglePanel.bind(this);
  }

  componentDidMount() {
    this.doFetch(this.props.page);
  }

  UNSAFE_componentWillReceiveProps(props) {
    const { drupalStats } = props;

    if (drupalStats) {
      const { core_version_data, drupal_version_labels, drupal_version_data } = drupalStats;

      // format pieChart data
      if (core_version_data) {
        this.setState({
          pieChartData: {
            type: 'pie',
            columns: core_version_data.map(item => [`Drupal ${item[0]}`, item[1]])
          }
        });
      }

      // format barChart data
      if (drupal_version_labels && drupal_version_data) {
        this.setState({
          barChartData: {
            type: 'bar',
            x: 'x',
            columns: [
              ['x', ...drupal_version_labels],
              ['Site count', ...drupal_version_data]
            ]
          }
        });

        this.setState({ barChartVisible: true });
      }
    }
  }

  doFetch(page) {
      this.props.fetchAdvisories(page);
      this.props.fetchDrupalStats(page);
      this.props.fetchSubscribedSites();
  }

  togglePanel() {
    this.setState({
      drupalVisible: !this.state.drupalVisible
    });
  }

  render() {
    const { drupalVisible, pieChartData, barChartData } = this.state;
    const { latest_drupal_advisories } = this.props.drupalStats;
    const subscribedSites = this.props.subscribedSites;
    const ObservatoryUrl = 'https://observatory.mozilla.org/analyze.html?host=';

    const bar = { width: { ratio: 0.5 } };
    const legend = { show: false };
    const axis = { x: { type: 'category' } };

    return (
      <div>
        <h2>Dashboard</h2>
        <div className="row">

          <div className="site-subscriptions-panel dashboard-panel width-half">
            <div className="panel-heading">My Sites</div>
            <Table style={{ width: '100%' }}>
              <TableHead>
                <TableRow style={{ height: '100%' }}>
                  <TableCell style={{ width: '80%', height: '100%' }}>Sites</TableCell>
                  <TableCell style={{ width: '10%', height: '100%' }}>Vuln&#39;s</TableCell>
                  <TableCell style={{ width: '10%', height: '100%' }}>Moz Obs</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {
                  !subscribedSites ? <TableRow><TableCell>no data</TableCell></TableRow> :
                  subscribedSites.map((site, i) => (
                    <TableRow key={i} style={{ height: '100%' }}>
                      <TableCell style={{ width: '80%', height: '100%' }}>
                        <Link to={`sites/${site.pk}`}><strong>{site.title}</strong>(on {site.hostname})</Link>
                      </TableCell>
                      <TableCell style={{ width: '10%', height: '100%' }}>{site.vulnerability_count}</TableCell>
                      <TableCell style={{ width: '10%', height: '100%' }}>
                        <a href={ObservatoryUrl + site.hostname}>view</a>
                      </TableCell>
                    </TableRow>
                  ))
                }
              </TableBody>
            </Table>
          </div>

          <div className="advisories-panel dashboard-panel width-half">
            <div className="panel-heading">Latest Drupal security advisories</div>
            <Table style={{ width: '100%' }}>
              <TableHead>
                <TableRow style={{ height: '100%' }}>
                  <TableCell style={{ width: '80%', height: '100%' }}>Advisory</TableCell>
                  <TableCell style={{ width: '20%', height: '100%' }}>All Sites Affected</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {
                  !latest_drupal_advisories ? <TableRow><TableCell>no data</TableCell></TableRow> :
                  latest_drupal_advisories.map((advisorie, i) => (
                    <TableRow key={i} style={{ height: '100%' }}>
                      <TableCell style={{ width: '80%', height: '100%' }}><Link to={`drupal/advisories/${advisorie.pk}`}><strong>{advisorie.title}</strong></Link></TableCell>
                      <TableCell style={{ width: '20%', height: '100%' }}>{advisorie.affected}</TableCell>
                    </TableRow>
                  ))
                }
              </TableBody>
            </Table>
          </div>

          <div className="core-panel dashboard-panel width-narrow">
            <div className="panel-heading">Active Drupal sites by major version</div>
            {pieChartData && <C3Chart data={pieChartData} />}
          </div>

          <div className={`drupal-panel ${drupalVisible ? 'drupal' : 'sstripe'} dashboard-panel width-wide`}>
            <div className="panel-heading">Active Drupal sites by version
              <button type="button" className="floatr" onClick={this.togglePanel}>
                {drupalVisible ? 'Silverstripe' : 'Drupal'}
              </button>
            </div>
            {barChartData &&
              <C3Chart data={barChartData} bar={bar} legend={legend} axis={axis} />
            }
          </div>

          <div className={`sstripe-panel ${drupalVisible ? 'drupal' : 'sstripe'} dashboard-panel width-wide`}>
            <div className="panel-heading">Active Silverstripe sites by version
              <button type="button" className="floatr" onClick={this.togglePanel}>
                {drupalVisible ? 'Silverstripe' : 'Drupal'}
              </button>
            </div>
            <div id="chart-sstripe">
              Silverstripe site data to be shown here in form of bar charts.
            </div>
          </div>

        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  page: PropTypes.number.isRequired,
  drupalStats: PropTypes.object.isRequired,
  fetchAdvisories: PropTypes.func.isRequired,
  fetchDrupalStats: PropTypes.func.isRequired,
  fetchSubscribedSites: PropTypes.func.isRequired
};

export default Dashboard;

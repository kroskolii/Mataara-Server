import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';

import InfoTable from '../helpers/InfoTable';
import Expansion from '../helpers/Expansion';
import { getDate, getPk, renderList } from '../helpers/formatData';

const renderProject = project => {
  if (!project) return null;
  const pk = getPk(project);
  return <Link to={`/drupal/modules/${pk}`}>Project detail</Link>;
};

const renderCVEs = cves => {
  if (!cves || cves.length === 0) return null;
  const links = cves.map(cve => {
    const id = cve.match(/\d+-\d+$/);
    if (!id) return cve; // if id doesn't match just show the strings
    return (
      <a 
        href={`https://cve.mitre.org/cgi-bin/cvename.cgi?name=${id[0]}`} 
        target="_blank" rel="noopener noreferrer"
      >
        {cve}
      </a>
    );
  });
  return renderList(links);
};

const renderReleasesTable = (releases, classes) => {
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell padding='dense'>Release</TableCell>
          <TableCell padding='dense'>Date Published</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {releases.map((release, i) => (
          <TableRow key={i} className={classes.innerTableRow}>
            <TableCell padding='dense'>{release.slug}</TableCell>
            <TableCell padding='dense'>{getDate(release.date)}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};

const renderReleases = (releases, classes) => {
  if (!releases || releases.length === 0) return null;
  return <Expansion summary={releases.length} details={renderReleasesTable(releases, classes)} />;
};

export const ParsedData = ({ advisoryDetails, classes }) => {
  const data = [
    { key: 'Advisory ID', value: advisoryDetails.advisory_id },
    { key: 'Cores', value: renderList(advisoryDetails.cores) },
    { key: 'Project', value: renderProject(advisoryDetails.project) },
    { key: 'Affected releases', value: renderReleases(advisoryDetails.releases, classes) },
    { key: 'Risk', value: advisoryDetails.risk },
    {
      key: 'Risk score',
      value: (advisoryDetails.risk_score && advisoryDetails.risk_score_total)
        && `${advisoryDetails.risk_score} / ${advisoryDetails.risk_score_total}`
      },
    { key: 'Vulnerabilities', value: renderList(advisoryDetails.vulnerabilities) },
    { key: 'CVEs', value: renderCVEs(advisoryDetails.cves) },
  ];

  return (
    <InfoTable title='Parsed data' data={data} />
  );
};

ParsedData.propTypes = {
  advisoryDetails: PropTypes.shape({
    advisory_id: PropTypes.string,
    cores: PropTypes.arrayOf(PropTypes.string),
    project: PropTypes.string,
    releases: PropTypes.arrayOf(
      PropTypes.shape({
        slug: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
      })
    ),
    risk: PropTypes.string,
    risk_score: PropTypes.number,
    risk_score_total: PropTypes.number,
    vulnerabilities: PropTypes.arrayOf(PropTypes.string),
    cves: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
  classes: PropTypes.shape({
    innerTableRow: PropTypes.string.isRequired,
  }).isRequired,
};

const styles = theme => ({
  innerTableRow: {
    height: 'auto',
  },
});

export default withStyles(styles)(ParsedData);

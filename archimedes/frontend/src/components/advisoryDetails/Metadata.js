import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import InfoTable from '../helpers/InfoTable';
import CustomChip from '../helpers/CustomChip';
import { getDate, renderUrl, renderList, removeDecommissioned } from '../helpers/formatData';

const renderAffectedSites = affectedSites => {
  affectedSites = removeDecommissioned(affectedSites);
  if (!affectedSites || affectedSites.length === 0) {
    return <CustomChip label='No sites affected' color='success' />;
  };
  const summary = (
    <CustomChip
      label={`${affectedSites.length} ${affectedSites.length === 1 ? 'site' : 'sites'} affected`}
      color="error"
    />
  );
  const links = affectedSites.map(site => (
    <Link to={`/sites/${site.pk}`} key={site.pk} >{site.string}</Link>
  ));
  return renderList([summary].concat(links));
};

const renderStatus = status => {
  if (!status) return null;
  if (status === 'Needs Review') return <CustomChip label={status} color='success' />;
  if (status === 'Parsing Failed') return <CustomChip label={status} color='error' />;
  return <CustomChip label={status} />;
};

const Metadata = ({ advisoryDetails }) => {
  const data = [
    { key: 'GUID', value: advisoryDetails.guid },
    { key: 'Title', value: advisoryDetails.title },
    { key: 'URL', value: renderUrl(advisoryDetails.url) },
    { key: 'Source', value: advisoryDetails.source },
    { key: 'Date posted', value: getDate(advisoryDetails.date_posted) },
    { key: 'Date updated', value: getDate(advisoryDetails.date_updated) },
    { key: 'Date parsed', value: getDate(advisoryDetails.date_parsed) },
    { key: 'Status', value: renderStatus(advisoryDetails.status_display) },
    { key: 'Affected sites', value: renderAffectedSites(advisoryDetails.affected_sites) },
  ];

  return (
    <InfoTable title='Metadata' data={data} />
  );
};

Metadata.propTypes = {
  advisoryDetails: PropTypes.shape({
    guid: PropTypes.string,
    title: PropTypes.string,
    url: PropTypes.string,
    source: PropTypes.string,
    date_posted: PropTypes.string,
    date_updated: PropTypes.string,
    date_parsed: PropTypes.string,
    status_display: PropTypes.string,
    affected_sites: PropTypes.arrayOf(
      PropTypes.shape({
        pk: PropTypes.number.isRequired,
        string: PropTypes.string.isRequired,
      }).isRequired,
    ),
  }).isRequired,
};

export default Metadata;

import React from 'react';
import { shallow } from 'enzyme';

import { SitesUsing } from '../SitesUsing';
import InfoTable from '../../helpers/InfoTable';
import CustomChip from '../../helpers/CustomChip';
import { Link } from 'react-router-dom';

describe('SitesUsing', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<SitesUsing {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      moduleDetails: {
        sites_vulnerable: [
          { pk: 1, string: 'site 1', decommissioned: false },
          { pk: 2, string: 'site 2', decommissioned: true },
          { pk: 3, string: 'site 3', decommissioned: false },
        ],
        sites_using: [
          { pk: 11, string: 'site 11', decommissioned: false },
          { pk: 12, string: 'site 12', decommissioned: true },
          { pk: 13, string: 'site 13', decommissioned: false },
          { pk: 14, string: 'site 14', decommissioned: false },
        ],
      },
      classes: {
        vulnerable: 'vulnerable',
      },
    };
    wrapper = undefined;
  });

  it('renders an `InfoTable`', () => {
    expect(setup().find(InfoTable).length).toBe(1);
  });

  describe('data passed to `InfoTable`', () => {
    it('contains `Sites using a vulnerable release` and its value is a node', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Sites using a vulnerable release');
      const value = shallow(item.value);
      expect(value.exists()).toBe(true);
    });

    describe('`Sites using a vulnerable release` when no sites affected', () => {
      it('renders a `CustomChip` with color=success', () => {
        props.moduleDetails.sites_vulnerable = [];
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Sites using a vulnerable release');
        expect(item.value).toEqual(
          <CustomChip label='No sites using a vulnerable release' color='success' />
        );
      });
    });

    describe('`Sites using a vulnerable release` when there are sites affected', () => {
      it('renders a `CustomChip` with color=error', () => {
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Sites using a vulnerable release');
        const value = shallow(item.value);
        expect(value.find(CustomChip).length).toBe(1);
        expect(value.find(CustomChip).props().color).toBe('error');
      });

      it('renders `Link`s to affected sites', () => {
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Sites using a vulnerable release');
        const value = shallow(item.value);
        expect(value.find(Link).length).toBe(2);
        expect(value.contains('site 1')).toBe(true);
        expect(value.contains('site 3')).toBe(true);
      });

      it('does not render `Link`s of decommissioned sites', () => {
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Sites using a vulnerable release');
        const value = shallow(item.value);
        expect(value.contains('site 2')).toBe(false);
      });
    });

    it('contains `Sites using this module` and its value is a node', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Sites using this module');
      const value = shallow(item.value);
      expect(value.exists()).toBe(true);
    });

    describe('`Sites using this module` when no sites using', () => {
      it('renders null', () => {
        props.moduleDetails.sites_using = [];
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Sites using this module');
        expect(item.value).toBeNull();
      });
    });

    describe('`Sites using this module` when there are sites using', () => {
      let item;
      beforeEach(() => {
        const data = setup().find(InfoTable).props().data;
        item = data.find(item => item.key === 'Sites using this module');
      });

      it('renders `Expansion`', () => {
        const value = shallow(item.value);
        expect(value.text()).toBe('<Expansion />');
      });

      it('passes the number of live sites as `summary` to `Expansion`', () => {
        const value = shallow(item.value);
        expect(value.props().summary).toBe(3);
      });

      it('renders `Link`s to live sites', () => {
        const value = shallow(item.value);
        const list = shallow(value.props().details);
        expect(list.find(Link).length).toBe(3);
        expect(list.contains('site 11')).toBe(true);
        expect(list.contains('site 13')).toBe(true);
        expect(list.contains('site 14')).toBe(true);
      });

      it('does not render `Link`s to decommissioned sites', () => {
        const value = shallow(item.value);
        const list = shallow(value.props().details);
        expect(list.contains('site 12')).toBe(false);
      });
    });

  });
});

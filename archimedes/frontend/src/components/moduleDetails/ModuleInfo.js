import React from 'react';
import PropTypes from 'prop-types';

import InfoTable from '../helpers/InfoTable';
import Expansion from '../helpers/Expansion';
import { getDate, renderUrl, renderList } from '../helpers/formatData';

const renderReleases = releases => {
  if (!Array.isArray(releases) || releases.length === 0) return null;
  const slugs = releases.map(release => release.slug);
  const details = renderList(slugs);
  return <Expansion summary={releases.length} details={details} />;
};

const ModuleInfo = ({ moduleDetails }) => {
  const data = [
    { key: 'Name', value: moduleDetails.name },
    { key: 'Slug', value: moduleDetails.slug },
    { key: 'URL', value: renderUrl(moduleDetails.url) },
    { key: 'Description', value: moduleDetails.description },
    { key: 'Date updated', value: getDate(moduleDetails.date_updated) },
    { key: 'Custom', value: moduleDetails.custom ? 'True' : 'False' },
    { key: 'Type', value: moduleDetails.type },
    { key: 'Type name', value: moduleDetails.type_name },
    { key: 'Maintenance status', value: moduleDetails.maintenance_status },
    { key: 'Development status', value: moduleDetails.development_status },
    { key: 'Releases', value: renderReleases(moduleDetails.results) },
  ];

  return (
    <InfoTable title={`${moduleDetails.type_name || 'Project'} info`} data={data} />
  );
};

ModuleInfo.propTypes = {
  moduleDetails: PropTypes.shape({
    name: PropTypes.string,
    slug: PropTypes.string,
    url: PropTypes.string,
    description: PropTypes.string,
    date_updated: PropTypes.string,
    custom: PropTypes.bool,
    type: PropTypes.string,
    type_name: PropTypes.string,
    maintenance_status: PropTypes.string,
    development_status: PropTypes.string,
    results: PropTypes.arrayOf(PropTypes.shape({
      slug: PropTypes.string,
    })),
  }).isRequired,
};

export default ModuleInfo;

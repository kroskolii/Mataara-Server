import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import { BreadcrumbItem } from '../BreadcrumbItem';

describe('BreadcrumbItem', () => {
  let props;
  let wrapper;
  const breadcrumbItem = () => {
    if (!wrapper) {
      wrapper = shallow(<BreadcrumbItem {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      children: 'Test',
    };
    wrapper = undefined;
  });

  it('matches snapshot', () => {
    const tree = renderer.create(<BreadcrumbItem {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders passed child string', () => {
    expect(breadcrumbItem().contains('Test')).toBeTruthy();
  });

  it('renders passed child react element', () => {
    props = {
      children: <p>Test</p>,
      classes: { breadcrumb: 'breadcrumb' },
    };
    wrapper = undefined;
    expect(breadcrumbItem().contains(<p>Test</p>)).toBeTruthy();
  });
});

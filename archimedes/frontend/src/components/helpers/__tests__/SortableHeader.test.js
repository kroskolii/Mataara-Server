import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import { SortableHeader } from '../SortableHeader';
import ButtonBase from '@material-ui/core/ButtonBase';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

describe('SortableHeader', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<SortableHeader {...props} />);
    }
    return wrapper;
  };

  const onSortChange = jest.fn();

  beforeEach(() => {
    props = {
      title: 'test',
      sortKey: 'testKey',
      currentOptions: {
        sortBy: 'testKey',
        sortDir: 'desc'
      },
      onSortChange,
      classes: { 
        text: 'text', 
        icon: 'icon', 
        inactiveIcon: 'inactiveIcon',
      },
    };
    wrapper = undefined;
  });

  it('matches snapshot', () => {
    const tree = renderer.create(<SortableHeader {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders a `ButtonBase`', () => {
    expect(setup().find(ButtonBase).length).toBe(1);
  });

  describe('rendered `ButtonBase`', () => {
    it('renders title', () => {
      const buttonBase = setup().find(ButtonBase);
      expect(buttonBase.contains(props.title)).toBe(true);
    });
  });

  describe('when sortKey matches current sortBy', () => {
    it('shows `ArrowDownwardIcon` with `icon` class when sortDir desc', () => {
      const icon = setup().find(ButtonBase).find(ArrowDownwardIcon);
      expect(icon.length).toBe(1);
      expect(icon.props().className.includes('icon')).toBe(true);
    });

    it('shows `ArrowUpwardIcon` with `icon` class when sortDir asc', () => {
      props.currentOptions.sortDir = 'asc';
      const icon = setup().find(ButtonBase).find(ArrowUpwardIcon);
      expect(icon.length).toBe(1);
      expect(icon.props().className.includes('icon')).toBe(true);
    });
  });

  describe('when sortKey does not match current sortBy', () => {
    it('shows both Up and Down icons with `inactiveIcon` class', () => {
      props.currentOptions.sortBy = 'other';
      const buttonBase = setup().find(ButtonBase);
      expect(buttonBase.find(ArrowUpwardIcon).length).toBe(1);
      expect(buttonBase.find(ArrowUpwardIcon).props().className.includes('inactiveIcon')).toBe(true);
      expect(buttonBase.find(ArrowDownwardIcon).length).toBe(1);
      expect(buttonBase.find(ArrowDownwardIcon).props().className.includes('inactiveIcon')).toBe(true);
    });
  });

  describe('onClick', () => {
    describe('when `sortKey` === `sortBy`', () => {
      it('calls `onSortChange` to change order from desc to asc', () => {
        const buttonBase = setup().find(ButtonBase);
        buttonBase.simulate('click', { preventDefault() {} });
        expect(onSortChange).toBeCalledWith('testKey', 'asc');
      });
  
      it('calls `onSortChange` to change order from asc to desc', () => {
        props.currentOptions.sortDir = 'asc';
        const buttonBase = setup().find(ButtonBase);
        buttonBase.simulate('click', { preventDefault() {} });
        expect(onSortChange).toBeCalledWith('testKey', 'desc');
      });
    });

    describe('when `sortKey` !== `sortBy`', () => {
      it('calls `onSortChange` with the sortKey asc', () => {
        props.currentOptions.sortBy = 'other';
        const buttonBase = setup().find(ButtonBase);
        buttonBase.simulate('click', { preventDefault() {} });
        expect(onSortChange).toBeCalledWith('testKey', 'asc');
      });
    });
  });
});

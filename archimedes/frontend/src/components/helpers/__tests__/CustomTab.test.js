import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import { CustomTab } from '../CustomTab';
import Tab from '@material-ui/core/Tab';

describe('CustomTab', () => {
  let props;
  let wrapper;
  const customTab = () => {
    if (!wrapper) {
      wrapper = shallow(<CustomTab {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      label: 'test',
      value: 'test-value',
    };
    wrapper = undefined;
  });

  it('matches snapshot', () => {
    const tree = renderer.create(<CustomTab {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders `Tab`', () => {
    expect(customTab().find(Tab).length).toBe(1);
  });

  it('passes all props to `Tab`', () => {
    const chip = customTab().find(Tab);
    expect(chip.props().label).toBe('test');
    expect(chip.props().value).toBe('test-value');
  });
});

import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import { Expansion } from '../Expansion';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

describe('Expansion', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<Expansion {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      summary: 'summary',
      details: 'details',
      classes: {
        panel: 'panel',
        summary: 'summary',
        details: 'details',
      },
    };
    wrapper = undefined;
  });

  it('matches snapshot', () => {
    const tree = renderer.create(<Expansion {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders `ExpansionPanel`', () => {
    expect(setup().find(ExpansionPanel).length).toBe(1);
  });

  it('renders `ExpansionPanelSummary`', () => {
    expect(setup().find(ExpansionPanelSummary).length).toBe(1);
  });

  describe('rendered `ExpansionPanelSummary`', () => {
    it('contains `summary` node', () => {
      const expansionPanelSummary = setup().find(ExpansionPanelSummary);
      expect(expansionPanelSummary.contains(props.summary)).toBeTruthy();
    });

    it('is passed `ExpandMoreIcon` as `expandIcon` prop', () => {
      const expansionPanelSummary = setup().find(ExpansionPanelSummary);
      expect(expansionPanelSummary.props().expandIcon).toEqual(<ExpandMoreIcon />);
    });
  });

  it('renders `ExpansionPanelDetails`', () => {
    expect(setup().find(ExpansionPanelDetails).length).toBe(1);
  });

  describe('rendered `ExpansionPanelDetails`', () => {
    it('contains `details` node', () => {
      const expansionPanelDetails = setup().find(ExpansionPanelDetails);
      expect(expansionPanelDetails.contains(props.details)).toBeTruthy();
    });
  });
});

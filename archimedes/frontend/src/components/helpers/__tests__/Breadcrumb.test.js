import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import { Breadcrumb } from '../Breadcrumb';
import ArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

describe('Breadcrumb', () => {
  let props;
  let wrapper;
  const breadcrumb = () => {
    if (!wrapper) {
      wrapper = shallow(<Breadcrumb {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      children: [
        <p>Test</p>,
        <p>Breadcrumb</p>,
      ],
      classes: { breadcrumb: 'breadcrumb' },
    };
    wrapper = undefined;
  });

  it('matches snapshot', () => {
    const tree = renderer.create(<Breadcrumb {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = breadcrumb().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = breadcrumb().find("div");
      const wrappingDiv = divs.first();

      expect(wrappingDiv.children()).toEqual(breadcrumb().children());
    });
  });

  describe('with only one child', () => {
    beforeEach(() => {
      props = {
        children: [
          <p>Test</p>,
        ],
        classes: { breadcrumb: 'breadcrumb' },
      };
      wrapper = undefined;
    });

    it('renders the child', () => {
      expect(breadcrumb().contains(<p>Test</p>)).toBeTruthy();
    });

    it('does not render a right arrow', () => {
      expect(breadcrumb().find(ArrowRightIcon).length).toBe(0);
    });
  });

  describe('with two children', () => {
    it('renders a right arrow', () => {
      expect(breadcrumb().find(ArrowRightIcon).length).toBe(1);
    });

    it('renders the second child', () => {
      expect(breadcrumb().contains(<p>Breadcrumb</p>)).toBeTruthy();
    });
  });

});

import React from 'react';

import { getDate, renderUrl, renderList, getPk, removeDecommissioned, renderMozillaObservatory } from '../formatData';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';

describe('getDate', () => {
  it('takes date input to moment and returns the date in DATE_FORMAT format', () => {
    expect(getDate('2018-10-17T22:14:52Z')).toBe('18 Oct 2018, 11:14 am');
  });

  it('returns null when no input', () => {
    expect(getDate()).toBeNull();
  });
});

describe('renderUrl', () => {
  it('takes url string and returns anchor element', () => {
    expect(renderUrl('https://example.com')).toEqual(
      <a href='https://example.com' target="_blank" rel="noopener noreferrer">https://example.com</a>
    );
  });

  it('returns null when no input', () => {
    expect(renderUrl()).toBeNull();
  });
});

describe('renderList', () => {
  it('takes an array and returns unordered list of the items', () => {
    const list = ['aa', 'bb', 'cc'];
    expect(renderList(list)).toEqual(
      <ul>
        <li key={0}>aa</li>
        <li key={1}>bb</li>
        <li key={2}>cc</li>
      </ul>
    );
  });

  it('returns null when no input', () => {
    expect(renderList()).toBeNull();
  });
});

describe('getPk', () => {
  it('takes url and returns number in the end of the url', () => {
    const url='http://archimedes-server.local:8000/api/drupal/projects/141/';
    expect(getPk(url)).toBe('141');
  });

  it('returns null if no numbers in the end of the url', () => {
    const url='http://url/without/numbers/';
    expect(getPk(url)).toBeNull();
  })

  it('returns null when no input', () => {
    expect(getPk()).toBeNull();
  });
});

describe('removeDecommissioned', () => {
  it('takes array of sites, removes decommissioned and returns it', () => {
    const sites = [
      { name: 'aa', decommissioned: false },
      { name: 'bb', decommissioned: true },
      { name: 'cc', decommissioned: false },
    ];
    expect(removeDecommissioned(sites).length).toBe(2);
    expect(removeDecommissioned(sites).find(site => site.name === 'bb')).toBeUndefined();
  });

  it('returns empty array when no input', () => {
    expect(removeDecommissioned()).toEqual([]);
  });
});

describe('renderMozillaObservatory', () => {
  it('extracts hostname from passed url and returns mozilla observatory link', () => {
    const url='http://www.maoritelevision.com/';
    expect(renderMozillaObservatory(url)).toEqual(
      <a href='https://observatory.mozilla.org/analyze.html?host=www.maoritelevision.com' target="_blank" rel="noopener noreferrer">
      <OpenInNewIcon />
    </a>
    );
  });

  it('returns null when no input', () => {
    expect(renderMozillaObservatory()).toBeNull();
  });
});

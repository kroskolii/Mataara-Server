import React from 'react';
import { shallow } from 'enzyme';

import { ScrollToTop } from '../ScrollToTop';

describe('ScrollToTop', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<ScrollToTop {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      location: '/',
    };
    wrapper = undefined;
  });

  it('does not render (returns null)', () => {
    expect(setup().type()).toBeNull();
  });

  it('scrolls to top when location is changed', () => {
    const scrollToSpy = jest.fn();
    global.scrollTo = scrollToSpy;
    setup().setProps({ location: '/test' });
    expect(scrollToSpy).toBeCalledWith(0, 0);
  });
});

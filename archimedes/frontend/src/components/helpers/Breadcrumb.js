import React from 'react';
import PropTypes from 'prop-types';
import ArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import { withStyles } from '@material-ui/core/styles';

export const Breadcrumb = props => (
  <div className={props.classes.breadcrumb}>
    {props.children.map((child, i) => (
      <React.Fragment key={i}>
        {i !== 0 &&
          <ArrowRightIcon />
        }
        {child}
      </React.Fragment>
    ))}
  </div>
);

Breadcrumb.propTypes = {
  classes: PropTypes.shape({
    breadcrumb: PropTypes.string.isRequired,
  }).isRequired,
};

const styles = theme => ({
  breadcrumb: {
    alignItems: 'center',
    color: theme.palette.grey[500],
    display: 'flex',
    marginBottom: '1rem',
  },
});

export default withStyles(styles)(Breadcrumb);

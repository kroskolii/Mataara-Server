import React from 'react';
import { withStyles } from '@material-ui/core/styles';

export const NoData = ({ classes }) => <span className={classes.noData}>No data</span>;

const styles = theme => ({
  noData: {
    color: theme.palette.grey[500],
  },
});

export default withStyles(styles)(NoData);

import { Component } from 'react';
import { withRouter } from 'react-router';

export class ScrollToTop extends Component {
  /* Scrolls to top of page when location changes to mimic typical web behaviour */

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    return null;
  }
}

export default withRouter(ScrollToTop);

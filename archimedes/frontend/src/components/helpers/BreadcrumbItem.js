import React from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

export const BreadcrumbItem = props => (
  <Typography variant="caption" classes={props.classes}>{props.children}</Typography>
);

const styles = theme => ({
  root: {
    fontWeight: 500,
  },
});

export default withStyles(styles)(BreadcrumbItem);

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';

export const CustomTab = props => (
  <Tab {...props} />
);

const styles = theme => ({
  root: {
    minWidth: 72,
  },
});

export default withStyles(styles)(CustomTab);

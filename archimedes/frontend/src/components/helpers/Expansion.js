import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export const Expansion = props => {
  const { summary, details, classes } = props;
  return (
    <ExpansionPanel className={classes.panel}>
      <ExpansionPanelSummary className={classes.summary} expandIcon={<ExpandMoreIcon />}>
        {summary}
      </ExpansionPanelSummary>
      <ExpansionPanelDetails className={classes.details}>
        {details}
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

Expansion.propTypes = {
  summary: PropTypes.node.isRequired,
  details: PropTypes.node.isRequired,
  classes: PropTypes.shape({
    panel: PropTypes.string.isRequired,
    summary: PropTypes.string.isRequired,
    details: PropTypes.string.isRequired,
  }).isRequired,
};

const styles = theme => ({
  panel: {
    boxShadow: 'none',
    marginRight: -24,
  },
  summary: {
    paddingLeft: 0,
  },
  details: {
    paddingLeft: 0,
    display: 'block',
  },
});

export default withStyles(styles)(Expansion);

import React from 'react';
import { shallow } from 'enzyme';

import SiteInfo from '../SiteInfo';
import InfoTable from '../../helpers/InfoTable';
import CustomChip from '../../helpers/CustomChip';

describe('SiteInfo', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<SiteInfo {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      siteDetails: {
        string: 'Test site',
        hostname: 'testsite.co.nz',
        url: 'https://testsite.co.nz',
        root: '/var/www/drupal/testsite',
        environment: 'Development',
        keys: ['key 1', 'key 2'],
        results: [
          { date_generated: '2018-08-02T06:56:22Z', type: 'drupalLegacy' },
          { date_generated: '2018-08-04T06:56:22Z', type: 'drupalLegacy' },
        ],
        decommissioned: false,
      },
    };
    wrapper = undefined;
  });

  it('renders an `InfoTable`', () => {
    expect(setup().find(InfoTable).length).toBe(1);
  });

  describe('data passed to `InfoTable`', () => {
    it('contains `Name` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Name');
      expect(item.value).toBe(props.siteDetails.string);
    });

    it('contains `Hostname` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Hostname');
      expect(item.value).toBe(props.siteDetails.hostname);
    });

    it('contains `URL` and its value is a link to the passed url', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'URL');
      const value = shallow(item.value);
      expect(value.find('a').length).toBe(1);
      expect(value.find('a').props().href).toBe(props.siteDetails.url);
    });

    it('contains `Root` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Root');
      expect(item.value).toBe(props.siteDetails.root);
    });

    it('contains `Environment` and its value is the passed string', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Environment');
      expect(item.value).toBe(props.siteDetails.environment);
    });

    it('contains `Keys` and its value is the list of keys', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Keys');
      const value = shallow(item.value);
      expect(value.find('li').length).toBe(2);
      expect(value.contains('key 1')).toBe(true);
      expect(value.contains('key 2')).toBe(true);
    });

    describe('`Reports` when no reports', () => {
      it('renders null', () => {
        props.siteDetails.results = [];
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Reports');
        expect(item.value).toBeNull();
      });
    });

    describe('`Reports` when there is one report', () => {
      it('renders the report info', () => {
        props.siteDetails.results = [
          { date_generated: '2018-08-02T06:56:22Z', type: 'drupalLegacy' },
        ];
        const data = setup().find(InfoTable).props().data;
        const item = data.find(item => item.key === 'Reports');
        expect(item.value).toBe('[drupalLegacy] 02 Aug 2018, 06:56 pm');
      });
    });

    describe('`Reports` when there are more than two reports', () => {
      let item;
      beforeEach(() => {
        const data = setup().find(InfoTable).props().data;
        item = data.find(item => item.key === 'Reports');
      });

      it('renders `Expansion`', () => {
        const value = shallow(item.value);
        expect(value.text()).toBe('<Expansion />');
      });

      it('passes the latest report as `summary` to `Expansion`', () => {
        const value = shallow(item.value);
        expect(value.props().summary).toBe('Latest: [drupalLegacy] 02 Aug 2018, 06:56 pm');
      });

      it('renders list of reports except latest', () => {
        const value = shallow(item.value);
        const list = shallow(value.props().details);
        expect(list.find('li').length).toBe(1);
        expect(list.contains('[drupalLegacy] 04 Aug 2018, 06:56 pm')).toBe(true);
      });
    });

    it('contains `Mozilla observatory` and its value is a link to mozilla observatory', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Mozilla observatory');
      const value = shallow(item.value);
      expect(value.find('a').length).toBe(1);
      expect(value.find('a').props().href).toBe('https://observatory.mozilla.org/analyze.html?host=testsite.co.nz');
    });

    it('contains `Status` with `Decommissioned` CustomChip if the site is decommissioned', () => {
      props.siteDetails.decommissioned = true;
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Status');
      expect(item.value).toEqual(
        <CustomChip label="Decommissioned" />
      );
    });

    it('does not contain `Status` field if the site is not decommissioned', () => {
      const data = setup().find(InfoTable).props().data;
      const item = data.find(item => item.key === 'Status');
      expect(item).toBeUndefined();
    });

  });
});

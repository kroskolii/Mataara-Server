import React from 'react';
import PropTypes from 'prop-types';

import InfoTable from '../helpers/InfoTable';
import Expansion from '../helpers/Expansion';
import CustomChip from '../helpers/CustomChip';
import { getDate, renderUrl, renderList, renderMozillaObservatory } from '../helpers/formatData';

const renderReports = reports => {
  if (!Array.isArray(reports) || reports.length === 0) return null;
  if (reports.length <= 1) return reports[0];

  const summary = `Latest: ${reports[0]}`;
  const details = renderList(reports.slice(1));
  return <Expansion summary={summary} details={details} />;
};

const SiteInfo = ({ siteDetails }) => {
  const reports = siteDetails.results && siteDetails.results.map(report => {
    const { date_generated, type } = report;
    return `[${type}] ${getDate(date_generated)}`;
  });

  const data = [
    { key: 'Name', value: siteDetails.string },
    { key: 'Hostname', value: siteDetails.hostname },
    { key: 'URL', value: renderUrl(siteDetails.url) },
    { key: 'Root', value: siteDetails.root },
    { key: 'Environment', value: siteDetails.environment },
    { key: 'Keys', value: renderList(siteDetails.keys) },
    { key: 'Reports', value: renderReports(reports) },
    { key: 'Mozilla observatory', value: renderMozillaObservatory(siteDetails.url) },
  ];

  // Explicitly show it's decommissioned. Field itself is not necessary for active sites.
  if (siteDetails.decommissioned) {
    data.push({ key: 'Status', value: <CustomChip label="Decommissioned" /> });
  }

  return (
    <InfoTable title='Site info' data={data} />
  );
};

SiteInfo.propTypes = {
  siteDetails: PropTypes.shape({
    string: PropTypes.string,
    hostname: PropTypes.string,
    url: PropTypes.string,
    root: PropTypes.string,
    environment: PropTypes.string,
    keys: PropTypes.arrayOf(PropTypes.string.isRequired),
    results: PropTypes.arrayOf(PropTypes.shape({
      date_generated: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
    })),
    decommissioned: PropTypes.bool,
  }).isRequired,
};

export default SiteInfo;

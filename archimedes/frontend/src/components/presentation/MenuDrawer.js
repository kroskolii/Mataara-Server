import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import DashboardIcon from '@material-ui/icons/Assessment';
import SiteIcon from '@material-ui/icons/Web';
import AdvisoryIcon from '@material-ui/icons/Warning';
import ModuleIcon from '@material-ui/icons/Extension';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Drawer from '@material-ui/core/Drawer';
import { NavLink } from 'react-router-dom';

import logo from '../../assets/logo.svg';

export const menuItems = [
  { label: 'Dashboard', icon: <DashboardIcon />, route: '/' },
  { label: 'Sites', icon: <SiteIcon />, route: '/sites' },
  { label: 'Advisories', icon: <AdvisoryIcon />, route: '/advisories' },
  { label: 'Modules & Themes', icon: <ModuleIcon />, route: '/modules' },
];

export const MenuDrawer = props => {
  const { classes, open, handleDrawerClose } = props;
  const mataaraLogo = (
    <div>
      <NavLink to='/' className={classes.logo}>
        <img src={logo} alt="Mataara" className={classes.logoIcon} />
        <Typography variant="title" noWrap color="inherit" className={classes.logoText}>
          Mataara
        </Typography>
      </NavLink>
    </div>
  );

  return (
    <Drawer
      variant="persistent"
      anchor='left'
      open={open}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <Toolbar className={classes.drawerHeader}>
        {mataaraLogo}
        <IconButton onClick={handleDrawerClose}>
          <ChevronLeftIcon />
        </IconButton>
      </Toolbar>
      <Divider />
      <List>
        {menuItems.map(item => (
          <MenuItem component={NavLink} to={item.route} exact key={item.label} className={classes.drawerText}>
            <ListItemIcon>{item.icon}</ListItemIcon>
            {item.label}
          </MenuItem>
        ))}
      </List>
    </Drawer>
  );
};

MenuDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  handleDrawerClose: PropTypes.func.isRequired,
};

const styles = theme => ({
  logo: {
    color: theme.palette.primary.main,
    display: 'flex',

    '&:hover': {
      textDecoration: 'none',
    },
  },

  logoIcon: {
    marginRight: '.5rem',
  },

  logoText: {
    fontWeight: 900,
  },

  drawerPaper: {
    backgroundColor: theme.drawer.backgroundColor,
    width: theme.drawer.width,
    position: 'fixed',
  },

  drawerHeader: {
    backgroundColor: theme.palette.background.default,
    display: 'flex',
    justifyContent: 'space-between',
    paddingLeft: 18, // align Mataara icon with menu
    paddingRight: 8, // close button position adjustment
  },

  drawerText: {
    color: theme.drawer.color,

    '&.active, &.active svg': {
      color: theme.drawer.activeColor,
    },
  },
});

export default withStyles(styles)(MenuDrawer);

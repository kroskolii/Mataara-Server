import React from 'react';
import { shallow } from 'enzyme';

import { Frame } from '../Frame';
import TopBar from '../TopBar';
import MenuDrawer from '../MenuDrawer';

describe('Frame', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(
        <Frame {...props}>
          <p>child</p>
        </Frame>
      );
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      classes: {
        root: 'root',
        content: 'content',
        contentShift: 'contentShift',
        toolbar: 'toolbar',
      },
      theme: {
        breakpoints: {
          values: {
            md: 768,
          },
        },
      },
    };
    wrapper = undefined;
  });

  it('renders a wrapping `div`', () => {
    expect(setup().find('div').length).toBeGreaterThan(0);
  });

  describe('rendered wrapping div', () => {
    it('has `root` class', () => {
      const divs = setup().find("div");
      const wrappingDiv = divs.first();
      expect(wrappingDiv.props().className.includes('root')).toBe(true);
    });
  });

  it('renders a `TopBar`', () => {
    expect(setup().find(TopBar).length).toBe(1);
  });

  describe('rendered `TopBar`', () => {
    it('is passed `state.open` as `open` prop', () => {
      const topbar = setup().find(TopBar);
      expect(topbar.props().open).toEqual(setup().state().open);
    });

    it('is passed `this.handleDrawerOpen` as `handleDrawerOpen` prop', () => {
      const topbar = setup().find(TopBar);
      expect(topbar.props().handleDrawerOpen).toEqual(setup().instance().handleDrawerOpen);
    });
  });

  it('renders a `MenuDrawer`', () => {
    expect(setup().find(MenuDrawer).length).toBe(1);
  });

  describe('rendered `MenuDrawer`', () => {
    it('is passed `state.open` as `open` prop', () => {
      const menu = setup().find(MenuDrawer);
      expect(menu.props().open).toEqual(setup().state().open);
    });

    it('is passed `this.handleDrawerClose` as `handleDrawerClose` prop', () => {
      const menu = setup().find(MenuDrawer);
      expect(menu.props().handleDrawerClose).toEqual(setup().instance().handleDrawerClose);
    });
  });

  it('renders `main`', () => {
    expect(setup().find('main').length).toBe(1);
  });

  describe('rendered `main`', () => {
    it('has `content` class', () => {
      const main = setup().find('main');
      expect(main.props().className.includes('content')).toBe(true);
    });

    it('has `contentShift` class when `state.open` is true', () => {
      setup().setState({ open: true });
      const main = setup().find('main');
      expect(main.props().className.includes('contentShift')).toBe(true);
    });

    it('does not have `contentShift` class when `state.open` is false', () => {
      setup().setState({ open: false });
      const main = setup().find('main');
      expect(main.props().className.includes('contentShift')).toBe(false);
    });

    it('contains toolbar div', () => {
      const main = setup().find('main');
      const toolbar = main.find('div').first();
      expect(toolbar.exists()).toBe(true);
      expect(toolbar.props().className.includes('toolbar')).toBe(true);
    });
  
    it('renders `props.children` inside it', () => {
      const main = setup().find('main');
      expect(main.contains(<p>child</p>)).toBe(true);
    });
  });

  describe('`handleDrawerOpen`', () => {
    it('sets `state.open` to true', () => {
      const wrapper = setup();
      wrapper.setState({ open: false });
      wrapper.instance().handleDrawerOpen();
      expect(wrapper.state().open).toBe(true);
    });
  });

  describe('`handleDrawerClose`', () => {
    it('sets `state.open` to false', () => {
      const wrapper = setup();
      wrapper.setState({ open: true });
      wrapper.instance().handleDrawerClose();
      expect(wrapper.state().open).toBe(false);
    });
  });
});

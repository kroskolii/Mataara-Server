import React from 'react';
import { shallow } from 'enzyme';

import { TopBar } from '../TopBar';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';

describe('TopBar', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<TopBar {...props} />);
    }
    return wrapper;
  };

  const handleDrawerOpen = jest.fn();

  beforeEach(() => {
    props = {
      open: true,
      handleDrawerOpen,
      pageTitle: 'Test Title',
      classes: {
        appBar: 'appBar',
        appBarShift: 'appBarShift',
        disableLeftGutter: 'disableLeftGutter',
        menuButton: 'menuButton',
        hide: 'hide',
        flex: 'flex',
        appBarLink: 'appBarLink',
      },
    };
    wrapper = undefined;
  });

  it('renders a `AppBar`', () => {
    expect(setup().find(AppBar).length).toBe(1);
  });

  describe('rendered `AppBar`', () => {
    it('has `appBar` class', () => {
      const appbar = setup().find(AppBar);
      expect(appbar.props().className.includes('appBar')).toBe(true);
    });

    it('has `appBarShift` class when `props.open` is true', () => {
      const appbar = setup().find(AppBar);
      expect(appbar.props().className.includes('appBarShift')).toBe(true);
    });

    it('does not have `appBarShift` class when `props.open` is false', () => {
      props.open = false;
      const appbar = setup().find(AppBar);
      expect(appbar.props().className.includes('appBarShift')).toBe(false);
    });

    it('contains `Toolbar`', () => {
      const appbar = setup().find(AppBar);
      expect(appbar.find(Toolbar).length).toBe(1);
    });
  });

  describe('rendered `Toolbar`', () => {
    it('has `disableLeftGutter` class when `props.open` is false', () => {
      props.open = false;
      const toolbar = setup().find(AppBar).find(Toolbar);
      expect(toolbar.props().className.includes('disableLeftGutter')).toBe(true);
    });

    it('does not have `disableLeftGutter` class when `props.open` is true', () => {
      const toolbar = setup().find(AppBar).find(Toolbar);
      expect(toolbar.props().className.includes('disableLeftGutter')).toBe(false);
    });

    it('contains `IconButton`', () => {
      const toolbar = setup().find(AppBar).find(Toolbar);
      expect(toolbar.find(IconButton).length).toBe(1);
    });

    it('contains `Typography`', () => {
      const toolbar = setup().find(AppBar).find(Toolbar);
      expect(toolbar.find(Typography).length).toBe(1);
    });

    it('contains a link to `/logout`', () => {
      const toolbar = setup().find(AppBar).find(Toolbar);
      expect(toolbar.find('a').length).toBe(1);
      expect(toolbar.find('a').props().href).toBe('/logout');
    });
  });

  describe('rendered `IconButton`', () => {
    it('is passed `props.handleDrawerOpen` as `onClick` prop', () => {
      const iconbutton = setup().find(IconButton);
      expect(iconbutton.props().onClick).toEqual(props.handleDrawerOpen);
    });

    it('has `menuButton` class', () => {
      const iconbutton = setup().find(IconButton);
      expect(iconbutton.props().className.includes('menuButton')).toBe(true);
    });

    it('has `hide` class when `props.open` is true', () => {
      const iconbutton = setup().find(IconButton);
      expect(iconbutton.props().className.includes('hide')).toBe(true);
    });

    it('does not have `hide` class when `props.open` is false', () => {
      props.open = false;
      const iconbutton = setup().find(IconButton);
      expect(iconbutton.props().className.includes('hide')).toBe(false);
    });

    it('contains `MenuIcon`', () => {
      const iconbutton = setup().find(IconButton);
      expect(iconbutton.find(MenuIcon).length).toBe(1);
    });
  });

  describe('rendered `Typography`', () => {
    it('has `flex` class', () => {
      const typography = setup().find(Typography);
      expect(typography.props().className.includes('flex')).toBe(true);
    });

    it('contains `props.pageTitle`', () => {
      const typography = setup().find(Typography);
      expect(typography.contains(props.pageTitle)).toBe(true);
    });
  });

  describe('rendered link', () => {
    it('has `appBarLink` class', () => {
      const link = setup().find('a');
      expect(link.props().className.includes('appBarLink')).toBe(true);
    });

    it('contains Sign Out `Button`', () => {
      const link = setup().find('a');
      expect(link.find(Button).length).toBe(1);
      expect(link.find(Button).contains('Sign Out')).toBe(true);
    });
  });

});

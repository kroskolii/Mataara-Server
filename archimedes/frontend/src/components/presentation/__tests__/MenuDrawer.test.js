import React from 'react';
import { shallow } from 'enzyme';

import { MenuDrawer } from '../MenuDrawer';
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import { NavLink } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import logo from '../../../assets/logo.svg';
import Typography from '@material-ui/core/Typography';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

describe('MenuDrawer', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<MenuDrawer {...props} />);
    }
    return wrapper;
  };

  const handleDrawerClose = jest.fn();

  beforeEach(() => {
    props = {
      classes: {
        logo: 'logo',
        logoIcon: 'logoIcon',
        logoText: 'logoText',
        drawerPaper: 'drawerPaper',
        drawerHeader: 'drawerHeader',
        drawerText: 'drawerText',
      },
      open: true,
      handleDrawerClose,
    };
    wrapper = undefined;
  });

  it('renders a `Drawer`', () => {
    expect(setup().find(Drawer).length).toBe(1);
  });

  describe('rendered `Drawer`', () => {
    it('is passed `props.open` as `open` prop', () => {
      const drawer = setup().find(Drawer);
      expect(drawer.props().open).toBe(props.open);
    });

    it('has `drawerPaper` class', () => {
      const drawer = setup().find(Drawer);
      expect(drawer.props().classes.paper).toBe(props.classes.drawerPaper);
    });

    it('contains `Toolbar`', () => {
      const drawer = setup().find(Drawer);
      expect(drawer.find(Toolbar).length).toBe(1);
    });

    it('contains `Divider`', () => {
      const drawer = setup().find(Drawer);
      expect(drawer.find(Divider).length).toBe(1);
    });

    it('contains `List`', () => {
      const drawer = setup().find(Drawer);
      expect(drawer.find(List).length).toBe(1);
    });
  });

  describe('rendered `Toolbar`', () => {
    it('has `drawerHeader` class', () => {
      const toolbar = setup().find(Drawer).find(Toolbar);
      expect(toolbar.props().className.includes('drawerHeader')).toBe(true);
    });

    it('contains Mataara logo', () => {
      const toolbar = setup().find(Drawer).find(Toolbar);
      expect(toolbar.find(NavLink).length).toBe(1);
    });

    it('contains `IconButton`', () => {
      const toolbar = setup().find(Drawer).find(Toolbar);
      expect(toolbar.find(IconButton).length).toBe(1);
    });
  });

  describe('rendered Mataara logo', () => {
    it('contains `NavLink` with `logo` class', () => {
      const navlink = setup().find(Drawer).find(Toolbar).find(NavLink);
      expect(navlink.props().className.includes('logo')).toBe(true);
    });

    it('is a link to `/`', () => {
      const navlink = setup().find(Drawer).find(Toolbar).find(NavLink);
      expect(navlink.props().to).toBe('/');
    });

    it('contains img that is Mataara logo', () => {
      const navlink = setup().find(Drawer).find(Toolbar).find(NavLink);
      const img = navlink.find('img');
      expect(img.props().src).toBe(logo);
    });

    it('contains `Typography` that says `Mataara`', () => {
      const navlink = setup().find(Drawer).find(Toolbar).find(NavLink);
      expect(navlink.find(Typography).length).toBe(1);
      expect(navlink.find(Typography).contains('Mataara')).toBe(true);
    });
  });

  describe('rendered `IconButton`', () => {
    it('contains `ChevronLeftIcon`', () => {
      const iconbutton = setup().find(Drawer).find(Toolbar).find(IconButton);
      expect(iconbutton.find(ChevronLeftIcon).length).toBe(1);
    });

    it('is passed `props.handleDrawerClose` as `onClick` prop', () => {
      const iconbutton = setup().find(Drawer).find(Toolbar).find(IconButton);
      expect(iconbutton.props().onClick).toEqual(props.handleDrawerClose);
    });
  });

  describe('rendered `List`', () => {
    let list;
    beforeEach(() => {
      list = setup().find(Drawer).find(List);
    });

    it('contains `Dashboard` menu', () => {
      const menu = list.childAt(0);
      expect(menu.props().to).toBe('/');
      expect(menu.contains('Dashboard')).toBe(true);
    });

    it('contains `Sites` menu', () => {
      const menu = list.childAt(1);
      expect(menu.props().to).toBe('/sites');
      expect(menu.contains('Sites')).toBe(true);
    });

    it('contains `Advisories` menu', () => {
      const menu = list.childAt(2);
      expect(menu.props().to).toBe('/advisories');
      expect(menu.contains('Advisories')).toBe(true);
    });

    it('contains `Modules & Themes` menu', () => {
      const menu = list.childAt(3);
      expect(menu.props().to).toBe('/modules');
      expect(menu.contains('Modules & Themes')).toBe(true);
    });
  });
});

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import TopBar from './TopBar';
import MenuDrawer from './MenuDrawer';

export class Frame extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // Default to closed in a small screen / open in a medium+ screen
      open: document.documentElement.clientWidth >= props.theme.breakpoints.values.md,
    };
    this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
    this.handleDrawerClose = this.handleDrawerClose.bind(this);
  }

  handleDrawerOpen() {
    this.setState({ open: true });
  };

  handleDrawerClose() {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <TopBar
          open={this.state.open}
          handleDrawerOpen={this.handleDrawerOpen}
        />

        <MenuDrawer
          open={this.state.open}
          handleDrawerClose={this.handleDrawerClose}
        />

        <main className={classNames(classes.content, {
          [classes.contentShift]: this.state.open,
        })}>
          <div className={classes.toolbar} />
          {this.props.children}
        </main>
      </div>
    );
  }
}

Frame.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },

  toolbar: theme.mixins.toolbar,

  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    marginLeft: 0,
    width: '100%',

    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },

  contentShift: {
    marginLeft: theme.drawer.width,
    width: 100% - theme.drawer.width,

    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
});

export default withStyles(styles, { withTheme: true })(Frame);

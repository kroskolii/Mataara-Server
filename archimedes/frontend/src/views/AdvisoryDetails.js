import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';

import Breadcrumb from '../components/helpers/Breadcrumb';
import BreadcrumbItem from '../components/helpers/BreadcrumbItem';
import Metadata from '../components/advisoryDetails/Metadata';
import ParsedData from '../components/advisoryDetails/ParsedData';
import fetchAdvisoryDetails from '../actions/advisoryDetailsActions';
import { setPageTitle } from '../actions/frameActions';

export class AdvisoryDetails extends Component {
  componentDidMount() {
    const { dispatch, advisoryDetails } = this.props;
    const { advisoryId } = this.props.match.params;
    dispatch(fetchAdvisoryDetails(advisoryId));
    dispatch(setPageTitle(advisoryDetails.title)); // Needed for when details are already fetched
  }

  // Needed for updating page title when details are fetched
  componentDidUpdate(prevProps) {
    const { dispatch, advisoryDetails } = this.props;
    if (advisoryDetails.title !== prevProps.advisoryDetails.title) {
      dispatch(setPageTitle(advisoryDetails.title));
    }
  }

  render() {
    const { advisoryDetails } = this.props;
    return (
      <div>
        <Helmet title={`${advisoryDetails.title} | Advisory Details | Mataara`} />

        <Breadcrumb>
          <BreadcrumbItem><Link to='/advisories'>Advisories</Link></BreadcrumbItem>
          <BreadcrumbItem>{advisoryDetails.title}</BreadcrumbItem>
        </Breadcrumb>

        <Grid container spacing={24}>
          <Grid item xm={12} md={6}>
            <Metadata advisoryDetails={advisoryDetails} />
          </Grid>

          <Grid item xm={12} md={6}>
            <ParsedData advisoryDetails={advisoryDetails} />
          </Grid>

        </Grid>
      </div>
    );
  }
}

AdvisoryDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      advisoryId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  advisoryDetails: PropTypes.shape({
    title: PropTypes.string,
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  advisoryDetails: state.advisoryDetails,
});

export default connect(mapStateToProps)(AdvisoryDetails);

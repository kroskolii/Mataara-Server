import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';

import Breadcrumb from '../components/helpers/Breadcrumb';
import BreadcrumbItem from '../components/helpers/BreadcrumbItem';
import SiteInfo from '../components/siteDetails/SiteInfo';
import DrupalInfo from '../components/siteDetails/DrupalInfo';
import { fetchSiteDetails, fetchReportDetails } from '../actions/siteDetailsActions';
import { setPageTitle } from '../actions/frameActions';

export class SiteDetails extends Component {
  componentDidMount() {
    const { dispatch, siteDetails } = this.props;
    const { siteId } = this.props.match.params;
    dispatch(fetchSiteDetails(siteId));
    dispatch(fetchReportDetails(siteId));
    dispatch(setPageTitle(siteDetails.string)); // Needed for when site details are already fetched
  }

  // Needed for updating page title when site details are fetched
  componentDidUpdate(prevProps) {
    const { dispatch, siteDetails } = this.props;
    if (siteDetails.string !== prevProps.siteDetails.string) {
      dispatch(setPageTitle(siteDetails.string));
    }
  }

  render() {
    const { siteDetails } = this.props;
    return (
      <div>
        <Helmet title={`${siteDetails.string} | Site Details | Mataara`} />

        <Breadcrumb>
          <BreadcrumbItem><Link to='/sites'>Sites</Link></BreadcrumbItem>
          <BreadcrumbItem active>{siteDetails.string}</BreadcrumbItem>
        </Breadcrumb>

        <Grid container spacing={24}>
          <Grid item xm={12} md={6}>
            <SiteInfo siteDetails={siteDetails} />
          </Grid>

          <Grid item xm={12} md={6}>
            <DrupalInfo siteDetails={siteDetails} />
          </Grid>

        </Grid>
      </div>
    );
  }
}

SiteDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      siteId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  siteDetails: PropTypes.shape({
    string: PropTypes.string,
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  siteDetails: state.siteDetails,
});

export default connect(mapStateToProps)(SiteDetails);

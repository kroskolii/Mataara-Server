import React from 'react';
import { shallow } from 'enzyme';

import { Modules } from '../Modules';
import { Helmet } from 'react-helmet';
import CustomTabs from '../../components/helpers/CustomTabs';
import CustomTab from '../../components/helpers/CustomTab';
import Input from '@material-ui/core/Input';
import InfiniteTable from '../../components/helpers/InfiniteTable';
import { Link } from 'react-router-dom';
import * as modulesActions from '../../actions/modulesActions';
import * as frameActions from '../../actions/frameActions';
import debounce from 'lodash/debounce';
jest.mock('lodash/debounce', () => jest.fn(fn => fn));

describe('Modules', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<Modules {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      modules: [
        {
          pk: 1,
          name: 'name 1',
          slug: 'slug1',
          sites_vulnerable: [
            { string: 'vulnerable 1', decommissioned: false },
            { string: 'vulnerable 2', decommissioned: true },
            { string: 'vulnerable 3', decommissioned: false },
          ],
          sites_using: [
            { string: 'using 1', decommissioned: true },
            { string: 'using 2', decommissioned: false },
            { string: 'using 3', decommissioned: false },
            { string: 'using 4', decommissioned: false },
          ],
          maintenance_status: 'Actively maintained',
          development_status: 'Under active development',
          url: 'https://www.drupal.org/project/blocks404',
        },
        {
          pk: 2,
          name: 'name 2',
          slug: 'slug2',
          sites_vulnerable: [],
          sites_using: [],
          maintenance_status: 'Unsupported',
          development_status: 'Obsolete',
          url: 'https://www.drupal.org/project/adminrole',
        },
      ],
      page: 1,
      hasMore: true,
      pending: false,
      dispatch: jest.fn(),
      classes: {
        search: 'search',
      },
    };
    wrapper = undefined;
  });

  it('renders a `Helmet`', () => {
    expect(setup().find(Helmet).length).toBe(1);
  });

  it('renders `CustomTabs`', () => {
    expect(setup().find(CustomTabs).length).toBe(1);
  });

  it('renders `Input`', () => {
    expect(setup().find(Input).length).toBe(1);
  });

  it('renders `InfiniteTable`', () => {
    expect(setup().find(InfiniteTable).length).toBe(1);
  });

  describe('rendered `Helmet`', () => {
    it('is passed `Modules & Themes | Mataara` as `title` prop', () => {
      const helmet = setup().find(Helmet);
      expect(helmet.prop('title')).toBe('Modules & Themes | Mataara');
    });
  });

  describe('rendered `CustomTabs`', () => {
    it('is passed `state.activeTab` as `value` prop', () => {
      const tabs = setup().find(CustomTabs);
      expect(tabs.prop('value')).toBe(setup().state('activeTab'));
    });

    it('is passed `this.handleTabChange` fn as `onChange` prop', () => {
      const instance = setup().instance();
      const tabs = setup().find(CustomTabs);
      expect(tabs.prop('onChange')).toBe(instance.handleTabChange);
    });

    it('contains `All` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(0);
      expect(tab.prop('label')).toBe('All');
      expect(tab.prop('value')).toBe('all');
    });

    it('contains `Modules` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(1);
      expect(tab.prop('label')).toBe('Modules');
      expect(tab.prop('value')).toBe('project_module');
    });

    it('contains `Themes` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(2);
      expect(tab.prop('label')).toBe('Themes');
      expect(tab.prop('value')).toBe('project_theme');
    });
  });

  describe('rendered `Input`', () => {
    it('is passed `state.filters.search` as `value` prop when the state has value', () => {
      setup().setState({ filters: { search: 'test search' } });
      const input = setup().find(Input);
      expect(input.prop('value')).toBe(setup().state('filters').search);
    });

    it('is passed empty strings as `value` prop when the state has no value', () => {
      const input = setup().find(Input);
      expect(input.prop('value')).toBe('');
    });

    it('is passed `this.handleSearch` fn as `onChange` prop', () => {
      const instance = setup().instance();
      const input = setup().find(Input);
      expect(input.prop('onChange')).toBe(instance.handleSearch);
    });

    it('has `search` class', () => {
      const input = setup().find(Input);
      expect(input.prop('className').includes('search')).toBe(true);
    });
  });

  describe('rendered `InfiniteTable`', () => {
    it('is passed `this.loadMore` fn as `loadMore` prop', () => {
      const instance = setup().instance();
      const table = setup().find(InfiniteTable);
      expect(table.prop('loadMore')).toBe(instance.loadMore);
    });

    it('is passed `props.hasMore` as `hasMore` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(table.prop('hasMore')).toBe(props.hasMore);
    });

    it('is passed an array as `tableHeader` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(Array.isArray(table.prop('tableHeader'))).toBe(true);
    });

    it('is passed an array as `tableData` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(Array.isArray(table.prop('tableData'))).toBe(true);
    });

    it('is passed `state.sortOptions` as `sortOptions` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(table.prop('sortOptions')).toBe(setup().state('sortOptions'));
    });

    it('is passed `this.handleSort` fn as `onSortChange` prop', () => {
      const instance = setup().instance();
      const table = setup().find(InfiniteTable);
      expect(table.prop('onSortChange')).toBe(instance.handleSort);
    });

    describe('passed `tableHeader`', () => {
      it('has `Name` and it is sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[0].title).toBe('Name');
        expect(header[0].sortable).toBe(true);
      });

      it('has `Slug` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[1].title).toBe('Slug');
        expect(header[1].sortable).toBe(false);
      });

      it('has `Sites Using Vulnerable Release` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[2].title).toBe('Sites Using Vulnerable Release');
        expect(header[2].sortable).toBe(false);
      });

      it('has `Sites Using` and it is sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[3].title).toBe('Sites Using');
        expect(header[3].sortable).toBe(true);
      });

      it('has `Maintenance Status` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[4].title).toBe('Maintenance Status');
        expect(header[4].sortable).toBe(false);
      });

      it('has `Development Status` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[5].title).toBe('Development Status');
        expect(header[5].sortable).toBe(false);
      });

      it('has `URL` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[6].title).toBe('URL');
        expect(header[6].sortable).toBe(false);
      });
    });

    describe('passed `tableData`', () => {
      it('has `Link` to module details page', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][0]).toEqual(
          <Link to='drupal/modules/1'><strong>name 1</strong></Link>
        );
        expect(data[1][0]).toEqual(
          <Link to='drupal/modules/2'><strong>name 2</strong></Link>
        );
      });

      it('shows `[No name]` when name is empty', () => {
        props.modules[0].name = '';
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][0]).toEqual(
          <Link to='drupal/modules/1'><strong>[No name]</strong></Link>
        );
      });

      it('has `slug` strings', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][1]).toBe('slug1');
        expect(data[1][1]).toBe('slug2');
      });

      it('has the number of vulnerable sites that are not decommissioned', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][2]).toBe(2);
        expect(data[1][2]).toBe(0);
      });

      it('has the number of sites using that are not decommissioned', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][3]).toBe(3);
        expect(data[1][3]).toBe(0);
      });

      it('has `maintenance_status` strings', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][4]).toBe('Actively maintained');
        expect(data[1][4]).toBe('Unsupported');
      });

      it('has `development_status` strings', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][5]).toBe('Under active development');
        expect(data[1][5]).toBe('Obsolete');
      });

      it('has a link to the url', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][6]).toEqual(<a href={props.modules[0].url}>{props.modules[0].url}</a>);
        expect(data[1][6]).toEqual(<a href={props.modules[1].url}>{props.modules[1].url}</a>);
      });
    });
  });

  describe('`componentDidMount` method', () => {
    it('calls `this.doFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');

      instance.componentDidMount();

      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(1);
    });

    it('dispatches `setPageTitle` with param `Advisories`', () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      props.dispatch.mockClear();
      frameActions.setPageTitle.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(2); // inside doFetch + setPageTitle
      expect(frameActions.setPageTitle).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledWith('Modules & Themes');
    });
  });

  describe('`loadMore` method', () => {
    it('calls `this.doFetch` with param `props.page + 1` if `props.pending` is false', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');

      instance.loadMore();

      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(2);
    });

    it('does not call `this.doFetch` if `props.pending` is true', () => {
      props.pending = true;
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');

      instance.loadMore();

      expect(instance.doFetch).not.toHaveBeenCalled();
    });
  });
  
  describe('`doFetch` method', () => {
    it('dispatches `fetchModules` with params `state.filters`, given `page`, `state.sortOptions`', () => {
      modulesActions.fetchModules = jest.fn();
      const instance = setup().instance();
      props.dispatch.mockClear();
      modulesActions.fetchModules.mockClear();
      const filters = setup().state('filters');
      const page = 1;
      const sortOptions = setup().state('sortOptions');

      instance.doFetch(page);

      expect(props.dispatch).toHaveBeenCalledTimes(1);
      expect(modulesActions.fetchModules).toHaveBeenCalledTimes(1);
      expect(modulesActions.fetchModules).toHaveBeenCalledWith(filters, page, sortOptions);
    });
  });

  describe('`debouncedFetch` method', () => {
    it('calls `doFetch` with 500ms debounce', () => {
      debounce.mockClear();
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');

      instance.debouncedFetch(3);

      expect(debounce).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).not.toHaveBeenCalled();
      setTimeout(() => {
        expect(instance.doFetch).toHaveBeenCalledWith(3);
      }, 600);
    });
  });

  describe('`handleTabChange` method', () => {
    it('sets `state.activeTab` with given `value`', () => {
      const instance = setup().instance();
      instance.handleTabChange(null, 'project_module');
      expect(setup().state('activeTab')).toBe('project_module');
    });

    it('sets `state.filters` with {} when `value` is `all`', () => {
      const instance = setup().instance();
      instance.handleTabChange(null, 'all');
      expect(setup().state('filters')).toEqual({});
    });

    it('sets `state.filters` with { type: value } when `value` is not `all`', () => {
      const instance = setup().instance();
      instance.handleTabChange(null, 'project_module');
      expect(setup().state('filters')).toEqual({ type: 'project_module' });
    });

    it('calls `doFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');
      instance.handleTabChange(null, 'project_module');
      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(1);
    });
  });

  describe('`handleSearch` method', () => {
    it('sets `state.filters.search` with given `event.target.value`', () => {
      const instance = setup().instance();
      const event = { target: { value: 'search' } };
      instance.handleSearch(event);
      expect(setup().state('filters').search).toBe('search');
    });

    it('calls `debouncedFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'debouncedFetch');
      const event = { target: { value: 'search' } };
      instance.handleSearch(event);
      expect(instance.debouncedFetch).toHaveBeenCalledTimes(1);
      expect(instance.debouncedFetch).toHaveBeenCalledWith(1);
    });
  });

  describe('`handleSort` method', () => {
    it('sets state with given `sortBy` and `sortDir`', () => {
      const instance = setup().instance();
      expect(setup().state('sortOptions').sortBy).toBe('name');
      expect(setup().state('sortOptions').sortDir).toBe('asc');
      instance.handleSort('sites_using__count', 'desc');
      expect(setup().state('sortOptions').sortBy).toBe('sites_using__count');
      expect(setup().state('sortOptions').sortDir).toBe('desc');
    });

    it('calls `doFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');
      instance.handleSort('sites_using__count', 'desc');
      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(1);
    });
  });
});

import React from 'react';
import { shallow } from 'enzyme';

import { Advisories } from '../Advisories';
import { Helmet } from 'react-helmet';
import CustomTabs from '../../components/helpers/CustomTabs';
import CustomTab from '../../components/helpers/CustomTab';
import Input from '@material-ui/core/Input';
import InfiniteTable from '../../components/helpers/InfiniteTable';
import { Link } from 'react-router-dom';
import { setPageTitle } from '../../actions/frameActions';
import * as advisoriesActions from '../../actions/advisoriesActions';
import debounce from 'lodash/debounce';
jest.mock('lodash/debounce', () => jest.fn(fn => fn));

describe('Advisories', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<Advisories {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      advisories: [
        {
          pk: 1,
          title: 'title 1',
          affected_sites: [
            { string: 'affected 1', decommissioned: false },
            { string: 'affected 2', decommissioned: true },
            { string: 'affected 3', decommissioned: false },
          ],
          source: 'Drupal Contrib',
          date_posted: '2018-08-15T12:32:53Z',
          risk_score: 5,
          risk_score_total: 25,
          status_display: 'Needs Review',
        },
        {
          pk: 2,
          title: 'title 2',
          affected_sites: [],
          source: 'Drupal SA API',
          date_posted: '2018-11-05T06:02:29.678960Z',
          risk_score: 10,
          risk_score_total: 25,
          status_display: 'Needs Review',
        },
      ],
      page: 1,
      hasMore: true,
      pending: false,
      dispatch: jest.fn(),
      classes: {
        search: 'search',
      },
    };
    wrapper = undefined;
  });

  it('renders a `Helmet`', () => {
    expect(setup().find(Helmet).length).toBe(1);
  });

  it('renders `CustomTabs`', () => {
    expect(setup().find(CustomTabs).length).toBe(1);
  });

  it('renders `Input`', () => {
    expect(setup().find(Input).length).toBe(1);
  });

  it('renders `InfiniteTable`', () => {
    expect(setup().find(InfiniteTable).length).toBe(1);
  });

  describe('rendered `Helmet`', () => {
    it('is passed `Advisories | Mataara` as `title` prop', () => {
      const helmet = setup().find(Helmet);
      expect(helmet.prop('title')).toBe('Advisories | Mataara');
    });
  });

  describe('rendered `CustomTabs`', () => {
    it('is passed `state.activeTab` as `value` prop', () => {
      const tabs = setup().find(CustomTabs);
      expect(tabs.prop('value')).toBe(setup().state('activeTab'));
    });

    it('is passed `this.handleTabChange` fn as `onChange` prop', () => {
      const instance = setup().instance();
      const tabs = setup().find(CustomTabs);
      expect(tabs.prop('onChange')).toBe(instance.handleTabChange);
    });

    it('contains `All` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(0);
      expect(tab.prop('label')).toBe('All');
      expect(tab.prop('value')).toBe('all');
    });

    it('contains `Drupal Core` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(1);
      expect(tab.prop('label')).toBe('Drupal Core');
      expect(tab.prop('value')).toBe('drupal-core');
    });

    it('contains `Durpal Contrib` `CustomTab`', () => {
      const tabs = setup().find(CustomTabs);
      const tab = tabs.find(CustomTab).at(2);
      expect(tab.prop('label')).toBe('Drupal Contrib');
      expect(tab.prop('value')).toBe('drupal-contrib');
    });
  });

  describe('rendered `Input`', () => {
    it('is passed `state.filters.search` as `value` prop when the state has value', () => {
      setup().setState({ filters: { search: 'test search' } });
      const input = setup().find(Input);
      expect(input.prop('value')).toBe(setup().state('filters').search);
    });

    it('is passed empty strings as `value` prop when the state has no value', () => {
      const input = setup().find(Input);
      expect(input.prop('value')).toBe('');
    });

    it('is passed `this.handleSearch` fn as `onChange` prop', () => {
      const instance = setup().instance();
      const input = setup().find(Input);
      expect(input.prop('onChange')).toBe(instance.handleSearch);
    });

    it('has `search` class', () => {
      const input = setup().find(Input);
      expect(input.prop('className').includes('search')).toBe(true);
    });
  });

  describe('rendered `InfiniteTable`', () => {
    it('is passed `this.loadMore` fn as `loadMore` prop', () => {
      const instance = setup().instance();
      const table = setup().find(InfiniteTable);
      expect(table.prop('loadMore')).toBe(instance.loadMore);
    });

    it('is passed `props.hasMore` as `hasMore` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(table.prop('hasMore')).toBe(props.hasMore);
    });

    it('is passed an array as `tableHeader` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(Array.isArray(table.prop('tableHeader'))).toBe(true);
    });

    it('is passed an array as `tableData` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(Array.isArray(table.prop('tableData'))).toBe(true);
    });

    it('is passed `state.sortOptions` as `sortOptions` prop', () => {
      const table = setup().find(InfiniteTable);
      expect(table.prop('sortOptions')).toBe(setup().state('sortOptions'));
    });

    it('is passed `this.handleSort` fn as `onSortChange` prop', () => {
      const instance = setup().instance();
      const table = setup().find(InfiniteTable);
      expect(table.prop('onSortChange')).toBe(instance.handleSort);
    });

    describe('passed `tableHeader`', () => {
      it('has `Title` and it is sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[0].title).toBe('Title');
        expect(header[0].sortable).toBe(true);
      });

      it('has `Affected` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[1].title).toBe('Affected');
        expect(header[1].sortable).toBe(false);
      });

      it('has `Source` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[2].title).toBe('Source');
        expect(header[2].sortable).toBe(false);
      });

      it('has `Date` and it is sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[3].title).toBe('Date');
        expect(header[3].sortable).toBe(true);
      });

      it('has `Risk` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[4].title).toBe('Risk');
        expect(header[4].sortable).toBe(false);
      });

      it('has `Status` and it is not sortable', () => {
        const header = setup().find(InfiniteTable).prop('tableHeader');
        expect(header[5].title).toBe('Status');
        expect(header[5].sortable).toBe(false);
      });
    });

    describe('passed `tableData`', () => {
      it('has `Link` to advisory details page', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][0]).toEqual(
          <Link to='drupal/advisories/1'>title 1</Link>
        );
        expect(data[1][0]).toEqual(
          <Link to='drupal/advisories/2'>title 2</Link>
        );
      });

      it('has the number of affected site that are not decommissioned', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][1]).toBe(2);
        expect(data[1][1]).toBe(0);
      });

      it('has `source` strings', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][2]).toBe('Drupal Contrib');
        expect(data[1][2]).toBe('Drupal SA API');
      });

      it('has formatted `date_posted` strings', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][3]).toBe('16 August 2018');
        expect(data[1][3]).toBe('05 November 2018');
      });

      it('has `progress` that shows risk_score / risk_score_total', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][4]).toEqual(<progress value={5} max={25} />);
        expect(data[1][4]).toEqual(<progress value={10} max={25} />);
      });

      it('has `status_display` strings', () => {
        const data = setup().find(InfiniteTable).prop('tableData');
        expect(data[0][5]).toBe('Needs Review');
        expect(data[1][5]).toBe('Needs Review');
      });
    });
  });

  describe('`componentDidMount` method', () => {
    it('calls `this.doFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');
      instance.componentDidMount();
      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(1);
    });

    it('dispatches `setPageTitle` with param `Advisories`', () => {
      const instance = setup().instance();
      instance.componentDidMount();
      expect(props.dispatch).toHaveBeenCalledWith(setPageTitle('Advisories'));
    });
  });

  describe('`loadMore` method', () => {
    it('calls `this.doFetch` with param `props.page + 1` if `props.pending` is false', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');
      instance.loadMore();
      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(2);
    });

    it('does not call `this.doFetch` if `props.pending` is true', () => {
      props.pending = true;
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');
      instance.loadMore();
      expect(instance.doFetch).not.toHaveBeenCalled();
    });
  });
  
  describe('`doFetch` method', () => {
    it('dispatches `fetchAdvisories` with params `state.filters`, given `page`, `state.sortOptions`', () => {
      advisoriesActions.fetchAdvisories = jest.fn();
      const instance = setup().instance();
      const filters = setup().state('filters');
      const page = 1;
      const sortOptions = setup().state('sortOptions');
      instance.doFetch(page);
      expect(props.dispatch).toHaveBeenCalled();
      expect(advisoriesActions.fetchAdvisories).toHaveBeenCalledWith(filters, page, sortOptions);
    });
  });

  describe('`debouncedFetch` method', () => {
    it('calls `doFetch` with 500ms debounce', () => {
      debounce.mockClear();
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');
      instance.debouncedFetch(3);

      expect(debounce).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).not.toHaveBeenCalled();
      setTimeout(() => {
        expect(instance.doFetch).toHaveBeenCalledWith(3);
      }, 600);
    });
  });

  describe('`handleTabChange` method', () => {
    it('sets `state.activeTab` with given `value`', () => {
      const instance = setup().instance();
      instance.handleTabChange(null, 'drupal-core');
      expect(setup().state('activeTab')).toBe('drupal-core');
    });

    it('sets `state.filters` with {} when `value` is `all`', () => {
      const instance = setup().instance();
      instance.handleTabChange(null, 'all');
      expect(setup().state('filters')).toEqual({});
    });

    it('sets `state.filters` with { source__slug: value } when `value` is not `all`', () => {
      const instance = setup().instance();
      instance.handleTabChange(null, 'drupal-core');
      expect(setup().state('filters')).toEqual({ source__slug: 'drupal-core' });
    });

    it('calls `doFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');
      instance.handleTabChange(null, 'drupal-core');
      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(1);
    });
  });

  describe('`handleSearch` method', () => {
    it('sets `state.filters.search` with given `event.target.value`', () => {
      const instance = setup().instance();
      const event = { target: { value: 'search' } };
      instance.handleSearch(event);
      expect(setup().state('filters').search).toBe('search');
    });

    it('calls `debouncedFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'debouncedFetch');
      const event = { target: { value: 'search' } };
      instance.handleSearch(event);
      expect(instance.debouncedFetch).toHaveBeenCalledTimes(1);
      expect(instance.debouncedFetch).toHaveBeenCalledWith(1);
    });
  });

  describe('`handleSort` method', () => {
    it('sets state with given `sortBy` and `sortDir`', () => {
      const instance = setup().instance();
      expect(setup().state('sortOptions').sortBy).toBe('date_posted');
      expect(setup().state('sortOptions').sortDir).toBe('desc');
      instance.handleSort('title', 'asc');
      expect(setup().state('sortOptions').sortBy).toBe('title');
      expect(setup().state('sortOptions').sortDir).toBe('asc');
    });

    it('calls `doFetch` with param `1`', () => {
      const instance = setup().instance();
      jest.spyOn(instance, 'doFetch');
      instance.handleSort('title', 'asc');
      expect(instance.doFetch).toHaveBeenCalledTimes(1);
      expect(instance.doFetch).toHaveBeenCalledWith(1);
    });
  });
});

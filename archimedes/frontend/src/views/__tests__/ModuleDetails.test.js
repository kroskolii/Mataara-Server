import React from 'react';
import { shallow } from 'enzyme';

import { ModuleDetails } from '../ModuleDetails';
import { Helmet } from 'react-helmet';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import Breadcrumb from '../../components/helpers/Breadcrumb';
import BreadcrumbItem from '../../components/helpers/BreadcrumbItem';
import ModuleInfo from '../../components/moduleDetails/ModuleInfo';
import SitesUsing from '../../components/moduleDetails/SitesUsing';
import * as moduleDetailsActions from '../../actions/moduleDetailsActions';
import * as frameActions from '../../actions/frameActions';

describe('ModuleDetails', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<ModuleDetails {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      match: {
        params: {
          moduleId: '100',
        },
      },
      moduleDetails: {
        name: 'test name',
        type_name: 'Module',
      },
      dispatch: jest.fn(),
    };
    wrapper = undefined;
  });

  it('renders a `div`', () => {
    expect(setup().find('div').length).toBe(1);
  });

  describe('rendered `div`', () => {
    it('contains `Helmet`', () => {
      const div = setup().find('div');
      expect(div.find(Helmet).length).toBe(1);
    });

    it('contains `Breadcrumb`', () => {
      const div = setup().find('div');
      expect(div.find(Breadcrumb).length).toBe(1);
    });

    it('contains `Grid`', () => {
      const div = setup().find('div');
      expect(div.find(Grid).length).toBe(3);
    });
  });

  describe('rendered `Helmet`', () => {
    it('is passed `${moduleDetails.name} | ${moduleDetails.type_name} Details | Mataara` as `title` prop', () => {
      const helmet = setup().find(Helmet);
      expect(helmet.prop('title')).toBe('test name | Module Details | Mataara');
    });
  });

  describe('rendered `Breadcrumb`', () => {
    it('contains `BreadcrumbItem` of `Link` to `/modules`', () => {
      const breadcrumb = setup().find(Breadcrumb);
      const item = breadcrumb.find(BreadcrumbItem).at(0);
      expect(item.find(Link).length).toBe(1);
      expect(item.find(Link).prop('to')).toBe('/modules');
      expect(item.find(Link).contains('Modules & Themes')).toBe(true);
    });

    it('contains `BreadcrumbItem` of `name` text', () => {
      const breadcrumb = setup().find(Breadcrumb);
      const item = breadcrumb.find(BreadcrumbItem).at(1);
      expect(item.contains('test name')).toBe(true);
    });
  });

  describe('rendered `Grid`', () => {
    it('contains `Grid` of `ModuleInfo`', () => {
      const grid = setup().find(Grid).at(1);
      expect(grid.find(ModuleInfo).length).toBe(1);
    });

    it('contains `Grid` of `SitesUsing`', () => {
      const grid = setup().find(Grid).at(2);
      expect(grid.find(SitesUsing).length).toBe(1);
    });
  });

  describe('rendered `ModuleInfo`', () => {
    it('is passed `props.moduleDetails` as `moduleDetails` prop', () => {
      const moduleinfo = setup().find(ModuleInfo);
      expect(moduleinfo.prop('moduleDetails')).toBe(props.moduleDetails);
    });
  });

  describe('rendered `SitesUsing`', () => {
    it('is passed `props.moduleDetails` as `moduleDetails` prop', () => {
      const sitesusing = setup().find(SitesUsing);
      expect(sitesusing.prop('moduleDetails')).toBe(props.moduleDetails);
    });
  });

  describe('`componentDidMount` method', () => {
    it('dispatches `fetchModuleDetails` with param `props.match.params.moduleId`', () => {
      moduleDetailsActions.fetchModuleDetails = jest.fn();
      const instance = setup().instance();
      moduleDetailsActions.fetchModuleDetails.mockClear();
      props.dispatch.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(3);
      expect(moduleDetailsActions.fetchModuleDetails).toHaveBeenCalledTimes(1);
      expect(moduleDetailsActions.fetchModuleDetails).toHaveBeenCalledWith('100');
    });

    it('dispatches `fetchModuleReleases` with param `props.match.params.moduleId`', () => {
      moduleDetailsActions.fetchModuleReleases = jest.fn();
      const instance = setup().instance();
      moduleDetailsActions.fetchModuleReleases.mockClear();
      props.dispatch.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(3);
      expect(moduleDetailsActions.fetchModuleReleases).toHaveBeenCalledTimes(1);
      expect(moduleDetailsActions.fetchModuleReleases).toHaveBeenCalledWith('100');
    });

    it('dispatches `setPageTitle` with param `moduleDetails.name`', () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(3);
      expect(frameActions.setPageTitle).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledWith('test name');
    });
  });

  describe('`componentDidUpdate` method', () => {
    it('dispatches `setPageTitle` with param `moduleDetails.name` if name is changed', () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      const prevProps = {
        ...props,
        moduleDetails: { name: 'previous name' },
      };
      instance.componentDidUpdate(prevProps);

      expect(props.dispatch).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledWith('test name');
    });

    it('does not dispatch `setPageTitle` if name is not changed', () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      const prevProps = props;
      instance.componentDidUpdate(prevProps);

      expect(props.dispatch).not.toHaveBeenCalled();
      expect(frameActions.setPageTitle).not.toHaveBeenCalled();
    });
  });
});

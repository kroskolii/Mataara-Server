import React from 'react';
import { shallow } from 'enzyme';

import { SiteDetails } from '../SiteDetails';
import { Helmet } from 'react-helmet';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import Breadcrumb from '../../components/helpers/Breadcrumb';
import BreadcrumbItem from '../../components/helpers/BreadcrumbItem';
import SiteInfo from '../../components/siteDetails/SiteInfo';
import DrupalInfo from '../../components/siteDetails/DrupalInfo';
import * as siteDetailsActions from '../../actions/siteDetailsActions';
import * as frameActions from '../../actions/frameActions';

describe('SiteDetails', () => {
  let props;
  let wrapper;
  const setup = () => {
    if (!wrapper) {
      wrapper = shallow(<SiteDetails {...props} />);
    }
    return wrapper;
  };

  beforeEach(() => {
    props = {
      match: {
        params: {
          siteId: '100',
        },
      },
      siteDetails: {
        string: 'test',
      },
      dispatch: jest.fn(),
    };
    wrapper = undefined;
  });

  it('renders a `div`', () => {
    expect(setup().find('div').length).toBe(1);
  });

  describe('rendered `div`', () => {
    it('contains `Helmet`', () => {
      const div = setup().find('div');
      expect(div.find(Helmet).length).toBe(1);
    });

    it('contains `Breadcrumb`', () => {
      const div = setup().find('div');
      expect(div.find(Breadcrumb).length).toBe(1);
    });

    it('contains `Grid`', () => {
      const div = setup().find('div');
      expect(div.find(Grid).length).toBe(3);
    });
  });

  describe('rendered `Helmet`', () => {
    it('is passed `${siteDetails.string} | Site Details | Mataara` as `title` prop', () => {
      const helmet = setup().find(Helmet);
      expect(helmet.prop('title')).toBe('test | Site Details | Mataara');
    });
  });

  describe('rendered `Breadcrumb`', () => {
    it('contains `BreadcrumbItem` of `Link` to `/sites`', () => {
      const breadcrumb = setup().find(Breadcrumb);
      const item = breadcrumb.find(BreadcrumbItem).at(0);
      expect(item.find(Link).length).toBe(1);
      expect(item.find(Link).prop('to')).toBe('/sites');
      expect(item.find(Link).contains('Sites')).toBe(true);
    });

    it('contains `BreadcrumbItem` of `string` text', () => {
      const breadcrumb = setup().find(Breadcrumb);
      const item = breadcrumb.find(BreadcrumbItem).at(1);
      expect(item.contains('test')).toBe(true);
    });
  });

  describe('rendered `Grid`', () => {
    it('contains `Grid` of `SiteInfo`', () => {
      const grid = setup().find(Grid).at(1);
      expect(grid.find(SiteInfo).length).toBe(1);
    });

    it('contains `Grid` of `DrupalInfo`', () => {
      const grid = setup().find(Grid).at(2);
      expect(grid.find(DrupalInfo).length).toBe(1);
    });
  });

  describe('rendered `SiteInfo`', () => {
    it('is passed `props.siteDetails` as `siteDetails` prop', () => {
      const siteinfo = setup().find(SiteInfo);
      expect(siteinfo.prop('siteDetails')).toBe(props.siteDetails);
    });
  });

  describe('rendered `DrupalInfo`', () => {
    it('is passed `props.siteDetails` as `siteDetails` prop', () => {
      const durpalinfo = setup().find(DrupalInfo);
      expect(durpalinfo.prop('siteDetails')).toBe(props.siteDetails);
    });
  });

  describe('`componentDidMount` method', () => {
    it('dispatches `fetchSiteDetails` with param `props.match.params.siteId`', () => {
      siteDetailsActions.fetchSiteDetails = jest.fn();
      const instance = setup().instance();
      siteDetailsActions.fetchSiteDetails.mockClear();
      props.dispatch.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(3);
      expect(siteDetailsActions.fetchSiteDetails).toHaveBeenCalledTimes(1);
      expect(siteDetailsActions.fetchSiteDetails).toHaveBeenCalledWith('100');
    });

    it('dispatches `fetchReportDetails` with param `props.match.params.siteId`', () => {
      siteDetailsActions.fetchReportDetails = jest.fn();
      const instance = setup().instance();
      siteDetailsActions.fetchReportDetails.mockClear();
      props.dispatch.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(3);
      expect(siteDetailsActions.fetchReportDetails).toHaveBeenCalledTimes(1);
      expect(siteDetailsActions.fetchReportDetails).toHaveBeenCalledWith('100');
    });

    it('dispatches `setPageTitle` with param `siteDetails.string`', () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      instance.componentDidMount();

      expect(props.dispatch).toHaveBeenCalledTimes(3);
      expect(frameActions.setPageTitle).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledWith('test');
    });
  });

  describe('`componentDidUpdate` method', () => {
    it('dispatches `setPageTitle` with param `siteDetails.string` if string is changed', () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      const prevProps = {
        ...props,
        siteDetails: { string: 'previous string' },
      };
      instance.componentDidUpdate(prevProps);

      expect(props.dispatch).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledTimes(1);
      expect(frameActions.setPageTitle).toHaveBeenCalledWith('test');
    });

    it('does not dispatch `setPageTitle` if string is not changed', () => {
      frameActions.setPageTitle = jest.fn();
      const instance = setup().instance();
      frameActions.setPageTitle.mockClear();
      props.dispatch.mockClear();

      const prevProps = props;
      instance.componentDidUpdate(prevProps);

      expect(props.dispatch).not.toHaveBeenCalled();
      expect(frameActions.setPageTitle).not.toHaveBeenCalled();
    });
  });
});

import { withStyles } from '@material-ui/core/styles';

const GlobalStyles = () => null;
const styles = (theme) => ({
  '@global': {
    html: {
      fontFamily: "'Roboto', sans-serif",
      // copy from @material-ui/core/CssBaseline
      // CssBaseline is not configuragle so don't use it and set all global styles in this file
      WebkitFontSmoothing: 'antialiased',
      MozOsxFontSmoothing: 'grayscale',
      boxSizing: 'border-box'
    },

    '*, *::before, *::after': {
      boxSizing: 'inherit'
    },

    body: {
      margin: 0,
      backgroundColor: theme.palette.background.default,

      '@media print': {
        backgroundColor: theme.palette.common.white
      }
    },

    a: {
      color: theme.palette.primary.main,
      textDecoration: 'none',

      '&:hover': {
        textDecoration: 'underline',
      },
    },

    progress: {
      width: 80,
    },

    ul: {
      listStyleType: 'none',
      padding: 0,
      margin: 0,
    },
  }
});

export default withStyles(styles)(GlobalStyles);

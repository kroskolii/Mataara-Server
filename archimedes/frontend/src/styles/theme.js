import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2763bc',
    },
    secondary: {
      main: '#20c893',
    },
    error: {
      main: '#F22939',
    },
    background: {
      default: '#fff',
    },
    border: '#ddd',
    yellow: '#ffba21',
  },
  drawer: {
    backgroundColor: '#f5f5f5',
    color: '#333333',
    activeColor: '#2763bc',
    width: 240,
  },
  head: {
    backgroundColor: '#f5f5f5',
    color: '#333333',
  },
});

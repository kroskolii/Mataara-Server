import React from 'react';
import { hot } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { Helmet } from 'react-helmet';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import OLDMuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import ThemeDefault from './theme-default';
import Advisories from './views/Advisories';
import Sites from './views/Sites';
import Modules from './views/Modules';
import AdvisoryDetails from './views/AdvisoryDetails';
import SiteDetails from './views/SiteDetails';
import ModuleDetails from './views/ModuleDetails';

// Need migration
import DashboardContainer from './containers/DashboardContainer';

import { configureStore } from './store/configureStore';
import { theme } from './styles/theme';
import GlobalStyles from './styles/GlobalStyles';
import ScrollToTop from './components/helpers/ScrollToTop';
import Frame from './components/presentation/Frame';

const store = configureStore();

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <MuiThemeProvider theme={ theme }>
          <OLDMuiThemeProvider muiTheme={ThemeDefault}>
            <React.Fragment>
              <Helmet title="Mataara" />

              <GlobalStyles />
              <ScrollToTop />
              <Frame>
                <Switch>
                  <Route exact path="/" component={DashboardContainer} />
                  <Route path="/sites/:siteId" component={SiteDetails} />
                  <Route path="/sites" component={Sites} />
                  <Route path="/modules" component={Modules} />
                  <Route path="/advisories" component={Advisories} />
                  <Route path="/drupal/advisories/:advisoryId" component={AdvisoryDetails} />
                  <Route path="/drupal/modules/:moduleId" component={ModuleDetails} />
                  {/* TODO: add nomatch route */}
                </Switch>
              </Frame>
            </React.Fragment>
          </OLDMuiThemeProvider>
        </MuiThemeProvider>
      </Router>
    </Provider>
  );
};

export default hot(module)(App);

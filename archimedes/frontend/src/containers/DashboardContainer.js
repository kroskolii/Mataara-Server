import React from 'react'; // eslint-disable-line no-unused-vars
import { connect } from 'react-redux';
import Dashboard from '../components/Dashboard';
import { fetchAdvisories, fetchDrupalStats, fetchSubscribedSites } from '../actions/dashboardActions';

const mapStateToProps = state => ({
  advisories: state.advisories.list,
  drupalStats: state.dashboard.drupalStats,
  drupalSites: state.dashboard.drupalSites,
  subscribedSites: state.dashboard.subscribedSites,
  page: state.advisories.page
});

export default connect(
  mapStateToProps,
  {
    fetchAdvisories,
    fetchDrupalStats,
    fetchSubscribedSites
  }
)(Dashboard);

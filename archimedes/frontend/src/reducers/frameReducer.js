import {
  SET_PAGETITLE,
} from '../constants/ActionTypes';

const initialState = {
  pageTitle: '',
};

export const frame = (state = initialState, action) => {
  switch(action.type) {
    case SET_PAGETITLE:
      return {
        pageTitle: action.pageTitle,
      };
    default:
      return state;
  }
};

import {
  SITES_FETCHING,
  SITES_FETCHED,
  SITES_TOGGLE_SUBSCRIBING,
  SITES_TOGGLE_SUBSCRIPTION_SUCCESS,
  SITES_TOGGLE_FAILED,
} from '../constants/ActionTypes';

const initialState = {
  pending: false,
  filters: {},
  payload: {},
  list: [],
  page: 0,
  total: 9999999,
  sorting: {
    sortBy: 'title',
    sortDir: 'asc'
  }
};

export default function sites(state = initialState, action) {
  switch (action.type) {
    case SITES_FETCHING:
      return {
        ...state,
        list: action.page === 1 ? [] : state.list,
        pending: true
      };
    case SITES_FETCHED:
      const filters = action.filters || {};
      const sorting = action.sorting || {};

      return {
        ...state,
        filters,
        sorting,
        pending: false,
        payload: action.payload,
        list: action.payload.previous
          ? state.list.concat(action.payload.results)
          : action.payload.results,
        total: action.payload.count,
        page: action.page,
      };
    case SITES_TOGGLE_SUBSCRIBING:
      return state;
    case SITES_TOGGLE_SUBSCRIPTION_SUCCESS:
      const itemIdx = state.list.findIndex(item => item.pk === action.pk);

      if (itemIdx > -1) {
        // spread the state.list and update subscrbed state.
        const updatedItem = {
          ...state.list[itemIdx],
          ...action.payload,
        };

        // state.list[itemIdx] = updatedItem

        const newList = state.list.map((item, idx) => {
          if (idx !== itemIdx) {
            return item;
          } else {
            return updatedItem;
          }
        });

        return {
          ...state,
          list: newList
        };
      }
      return state;
    case SITES_TOGGLE_FAILED:
    default:
      return state;
  }
}

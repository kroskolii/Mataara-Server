import {
  ADVISORIES_REQUEST,
  ADVISORIES_SUCCESS,
  STATS_FETCHING,
  STATS_FETCHED,
  SITES_SUBSCRIBED_FETCHED
} from '../constants/ActionTypes';

const initialState = {
  list: [],
  drupalStats: {},
  subscribedSites: [],
  page: 0,
  total: 9999999
};

export default function dashboard(state = initialState, action) {
  switch (action.type) {
    case ADVISORIES_REQUEST:
      return {
        ...state,
        list: action.page === 1 ? [] : state.list
      };
    case ADVISORIES_SUCCESS: {
      const nextUrl = action.payload.next || '';
      const nextPageParams = nextUrl.match(/page=([^&]*)/);
      const nextPage = (nextPageParams && nextPageParams.length) ?
        parseInt(nextPageParams[1], 10) : null;

      const page = nextPage ? nextPage - 1 : state.page;

      return {
        ...state,
        list: page > 1 ? [...state.list, ...action.payload.results] : action.payload.results,
        total: action.payload.count,
        page
      };
    }
    case STATS_FETCHING:
      return {
        ...state
      };
    case STATS_FETCHED: {
      return {
        ...state,
        drupalStats: (action.payload === 'undefined') ? {} : action.payload
      };
    }
    case SITES_SUBSCRIBED_FETCHED: {
      return {
        ...state,
        subscribedSites: action.payload
      };
    }
    default:
      return state;
  }
}

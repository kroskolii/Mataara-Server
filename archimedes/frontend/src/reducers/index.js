import { combineReducers } from 'redux';
import sites from './sites';
import modules from './modules';
import dashboard from './dashboard';
import { advisories } from './advisories';
import siteDetails from './siteDetails';
import advisoryDetails from './advisoryDetails';
import moduleDetails from './moduleDetails';
import { frame } from './frameReducer';

const rootReducer = combineReducers({
  sites,
  modules,
  dashboard,
  advisories,
  siteDetails,
  advisoryDetails,
  moduleDetails,
  frame,
});

export default rootReducer;

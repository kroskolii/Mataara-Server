import {
  ADVISORIES_DETAILS_FETCHING,
  ADVISORIES_DETAILS_FETCHED

} from '../constants/ActionTypes';

const initialState = {
  advisoryId: 0,
  pending: false
};

const advisoryDetails = (state = initialState, action) => {
  switch (action.type) {
    case ADVISORIES_DETAILS_FETCHING:
      return {
        ...state,
        pending: true
      };

    case ADVISORIES_DETAILS_FETCHED:
      return {
        ...state,
        pending: false,
        ...action.payload
      };

    default:
      return state;
  }
};

export default advisoryDetails;

import {
  MODULES_DETAILS_FETCHING,
  MODULES_DETAILS_FETCHED,
  MODULES_RELEASES_FETCHING,
  MODULES_RELEASES_FETCHED
} from '../constants/ActionTypes';

const initialState = {
  moduleId: 0,
  pending: false
};

const moduleDetails = (state = initialState, action) => {
  switch (action.type) {
    case MODULES_DETAILS_FETCHING:
      return {
        ...state,
        pending: true
      };

    case MODULES_DETAILS_FETCHED:
      return {
        ...state,
        pending: false,
        ...action.payload
      };

    case MODULES_RELEASES_FETCHING:
      return {
        ...state,
        pending: true
      };

    case MODULES_RELEASES_FETCHED:
      return {
        ...state,
        pending: false,
        ...action.payload
      };

    default:
      return state;
  }
};

export default moduleDetails;

import {
  SITES_DETAILS_FETCHING,
  SITES_DETAILS_FETCHED,
  REPORTS_DETAILS_FETCHING,
  REPORTS_DETAILS_FETCHED,
  // SITES_TOGGLE_SUBSCRIBING,
  // SITES_TOGGLE_SUBSCRIPTION_SUCCESS,
  // SITES_TOGGLE_FAILED

} from '../constants/ActionTypes';

const initialState = {
  siteId: 0,
  pending: false
};

const siteDetails = (state = initialState, action) => {
  switch (action.type) {
    case SITES_DETAILS_FETCHING:
      return {
        ...state,
        pending: true
      };

    case SITES_DETAILS_FETCHED:
      return {
        ...state,
        pending: false,
        ...action.payload
      };

    case REPORTS_DETAILS_FETCHING:
      return {
        ...state,
        pending: true
      };

    case REPORTS_DETAILS_FETCHED:
      return {
        ...state,
        pending: false,
        ...action.payload
      };


    default:
      return state;
  }
};

export default siteDetails;

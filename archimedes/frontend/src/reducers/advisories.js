import {
  ADVISORIES_REQUEST,
  ADVISORIES_SUCCESS,
  ADVISORIES_FAILURE,
} from '../constants/ActionTypes';

const initialState = {
  pending: false,
  rejected: false,
  fulfilled: false,
  payload: {},
  list: [],
  page: 0, // is this necessary? 'next' should do the job.
  total: 9999999, // is this necessary? 'next' should do the job (when next === null).
};

export const advisories = (state = initialState, action) => {
  switch (action.type) {
    case ADVISORIES_REQUEST:
      return {
        ...state,
        pending: true,
        rejected: false,
        fulfilled: false,
        // Clear list to show empty table
        list: action.page === 1 ? [] : state.list,
      };
    case ADVISORIES_SUCCESS: {
      return {
        pending: false,
        rejected: false,
        fulfilled: true,
        payload: action.payload,
        // List also needs to be reset here because multiple fetch
        // can be done at the same time by switching tabs quickly
        // TODO: Abort ongoing request on new request (redux-saga takeLatest will do)
        // Currently later return wins and so the tab and the table can unmatch
        list: action.payload.previous
          ? state.list.concat(action.payload.results)
          : action.payload.results,
        total: action.payload.count,
        page: action.page
      };
    }
    case ADVISORIES_FAILURE:
      return {
        ...state,
        pending: false,
        rejected: true,
        fulfilled: false,
        payload: action.payload,
      };
    default:
      return state;
  }
};

import modules from '../modules';
import * as types from '../../constants/ActionTypes';

describe('modules reducer', () => {
  const initialState = {
    pending: false,
    filters: {},
    payload: {},
    list: [],
    page: 0,
    total: 9999999,
    sorting: {
      sortBy: 'name',
      sortDir: 'asc'
    }
  };

  it('should return the initial state', () => {
    expect(modules(undefined, {})).toEqual(initialState);
  });

  describe('on MODULES_FETCHING', () => {
    it('sets pending to true', () => {
      const state = {
        ...initialState,
      };

      const action = { type: types.MODULES_FETCHING, page: 1 };

      const expectedState = {
        ...state,
        pending: true,
        list: [],
      };
      expect(modules(state, action)).toEqual(expectedState);
    });

    it('clears list on page 1', () => {
      const state = {
        ...initialState,
        list: ['a', 'b', 'c']
      };

      const action = { type: types.MODULES_FETCHING, page: 1 };

      const expectedState = {
        ...state,
        pending: true,
        list: [],
      };
      expect(modules(state, action)).toEqual(expectedState);
    });

    it('does not clear list on page > 1', () => {
      const state = {
        ...initialState,
        list: ['a', 'b', 'c']
      };

      const action = { type: types.MODULES_FETCHING, page: 2 };

      const expectedState = {
        ...state,
        pending: true,
        list: ['a', 'b', 'c'],
      };
      expect(modules(state, action)).toEqual(expectedState);
    });
  });

  describe('on MODULES_FETCHED', () => {
    it('sets pending to false', () => {
      const state = {
        ...initialState,
        pending: true,
      };

      const payload = {
        previous: null,
        results: ['d', 'e', 'f'],
        count: 100,
      };
      const filters = {};
      const sorting = {};
      const page = 1;
      const action = { type: types.MODULES_FETCHED, payload, filters, sorting, page };

      const expectedState = {
        ...state,
        filters,
        sorting,
        pending: false,
        payload,
        list: ['d', 'e', 'f'],
        total: 100,
        page,
      };
      expect(modules(state, action)).toEqual(expectedState);
    });

    it('replaces list with fetched results on page 1', () => {
      const state = {
        ...initialState,
        pending: true,
        results: ['a', 'b', 'c'],
      };

      const payload = {
        previous: null,
        results: ['d', 'e', 'f'],
        count: 100,
      };
      const filters = {};
      const sorting = {};
      const page = 1;
      const action = { type: types.MODULES_FETCHED, payload, filters, sorting, page };

      const expectedState = {
        ...state,
        filters,
        sorting,
        pending: false,
        payload,
        list: ['d', 'e', 'f'],
        total: 100,
        page,
      };
      expect(modules(state, action)).toEqual(expectedState);
    });

    it('concats list with fetched results on page > 1', () => {
      const state = {
        ...initialState,
        pending: true,
        list: ['a', 'b', 'c'],
      };

      const payload = {
        previous: 'previousExists',
        results: ['d', 'e', 'f'],
        count: 100,
      };
      const filters = {};
      const sorting = {};
      const page = 2;
      const action = { type: types.MODULES_FETCHED, payload, filters, sorting, page };

      const expectedState = {
        ...state,
        filters,
        sorting,
        pending: false,
        payload,
        list: ['a', 'b', 'c', 'd', 'e', 'f'],
        total: 100,
        page,
      };
      expect(modules(state, action)).toEqual(expectedState);
    });
  });
});

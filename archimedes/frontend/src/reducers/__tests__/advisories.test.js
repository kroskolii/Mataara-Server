import { advisories } from '../advisories';
import * as types from '../../constants/ActionTypes';

describe('advisories reducer', () => {
  const initialState = {
    pending: false,
    rejected: false,
    fulfilled: false,
    payload: {},
    list: [],
    page: 0, // is this necessary? 'next' should do the job.
    total: 9999999, // is this necessary? 'next' should do the job (when next === null).
  };

  it('should return the initial state', () => {
    expect(advisories(undefined, {})).toEqual(initialState);
  });

  describe('on ADVISORIES_REQUEST', () => {
    it('clears list to show empty table if page === 1', () => {
      const action = { type: types.ADVISORIES_REQUEST, page: 1 };
      const state = {
        ...initialState,
        fulfilled: true,
        list: ['a', 'b', 'c'],
      };
      const expectedState = {
        ...state,
        pending: true,
        rejected: false,
        fulfilled: false,
        list: [],
      };
      expect(advisories(state, action)).toEqual(expectedState);
    });

    it('does not clear list if page > 1', () => {
      const action = { type: types.ADVISORIES_REQUEST, page: 2 };
      const state = {
        ...initialState,
        fulfilled: true,
        list: ['a', 'b', 'c'],
      };
      const expectedState = {
        ...state,
        pending: true,
        rejected: false,
        fulfilled: false,
        list: state.list,
      };
      expect(advisories(state, action)).toEqual(expectedState);
    });
  });

  describe('on ADVISORIES_SUCCESS', () => {
    it('replaces list with fetched results on page 1', () => {
      const payload = {
        previous: null,
        results: ['d', 'e', 'f'],
        count: 100,
      };
      const action = { type: types.ADVISORIES_SUCCESS, page: 1, payload };
      const state = {
        ...initialState,
        fulfilled: true,
        list: ['a', 'b', 'c'],
      };
      const expectedState = {
        ...state,
        pending: false,
        rejected: false,
        fulfilled: true,
        payload,
        list: payload.results,
        total: payload.count,
        page: 1,
      };
      expect(advisories(state, action)).toEqual(expectedState);
    });

    it('concats list with fetched results on page > 1', () => {
      const payload = {
        previous: 'previousExists',
        results: ['d', 'e', 'f'],
        count: 100,
      };
      const action = { type: types.ADVISORIES_SUCCESS, page: 2, payload };
      const state = {
        ...initialState,
        fulfilled: true,
        list: ['a', 'b', 'c'],
      };
      const expectedState = {
        ...state,
        pending: false,
        rejected: false,
        fulfilled: true,
        payload,
        list: ['a', 'b', 'c', 'd', 'e', 'f'],
        total: payload.count,
        page: 2,
      };
      expect(advisories(state, action)).toEqual(expectedState);
    });
  });

  describe('on ADVISORIES_FAILURE', () => {
    it('stores error in advisories.payload', () => {
      const error = { status: 500, statusText: 'failed' };
      const action = { type: types.ADVISORIES_FAILURE, payload: error };
      const state = {
        ...initialState,
        fulfilled: true,
        list: ['a', 'b', 'c'],
      };
      const expectedState = {
        ...state,
        pending: false,
        rejected: true,
        fulfilled: false,
        payload: error,
      };
      expect(advisories(state, action)).toEqual(expectedState);
    });
  });
});

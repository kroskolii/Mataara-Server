import sites from '../sites';
import * as types from '../../constants/ActionTypes';

describe('sites reducer', () => {
  const initialState = {
    pending: false,
    filters: {},
    payload: {},
    list: [],
    page: 0,
    total: 9999999,
    sorting: {
      sortBy: 'title',
      sortDir: 'asc'
    }
  };

  it('should return the initial state', () => {
    expect(sites(undefined, {})).toEqual(initialState);
  });

  describe('on SITES_FETCHING', () => {
    it('sets pending to true', () => {
      const state = {
        ...initialState,
      };

      const action = { type: types.SITES_FETCHING, page: 1 };

      const expectedState = {
        ...state,
        pending: true,
        list: [],
      };
      expect(sites(state, action)).toEqual(expectedState);
    });

    it('clears list on page 1', () => {
      const state = {
        ...initialState,
        list: ['a', 'b', 'c']
      };

      const action = { type: types.SITES_FETCHING, page: 1 };

      const expectedState = {
        ...state,
        pending: true,
        list: [],
      };
      expect(sites(state, action)).toEqual(expectedState);
    });

    it('does not clear list on page > 1', () => {
      const state = {
        ...initialState,
        list: ['a', 'b', 'c']
      };

      const action = { type: types.SITES_FETCHING, page: 2 };

      const expectedState = {
        ...state,
        pending: true,
        list: ['a', 'b', 'c'],
      };
      expect(sites(state, action)).toEqual(expectedState);
    });
  });

  describe('on SITES_FETCHED', () => {
    it('sets pending to false', () => {
      const state = {
        ...initialState,
        pending: true,
      };

      const payload = {
        previous: null,
        results: ['d', 'e', 'f'],
        count: 100,
      };
      const filters = {};
      const sorting = {};
      const page = 1;
      const action = { type: types.SITES_FETCHED, payload, filters, sorting, page };

      const expectedState = {
        ...state,
        filters,
        sorting,
        pending: false,
        payload,
        list: ['d', 'e', 'f'],
        total: 100,
        page,
      };
      expect(sites(state, action)).toEqual(expectedState);
    });

    it('replaces list with fetched results on page 1', () => {
      const state = {
        ...initialState,
        pending: true,
        results: ['a', 'b', 'c'],
      };

      const payload = {
        previous: null,
        results: ['d', 'e', 'f'],
        count: 100,
      };
      const filters = {};
      const sorting = {};
      const page = 1;
      const action = { type: types.SITES_FETCHED, payload, filters, sorting, page };

      const expectedState = {
        ...state,
        filters,
        sorting,
        pending: false,
        payload,
        list: ['d', 'e', 'f'],
        total: 100,
        page,
      };
      expect(sites(state, action)).toEqual(expectedState);
    });

    it('concats list with fetched results on page > 1', () => {
      const state = {
        ...initialState,
        pending: true,
        list: ['a', 'b', 'c'],
      };

      const payload = {
        previous: 'previousExists',
        results: ['d', 'e', 'f'],
        count: 100,
      };
      const filters = {};
      const sorting = {};
      const page = 2;
      const action = { type: types.SITES_FETCHED, payload, filters, sorting, page };

      const expectedState = {
        ...state,
        filters,
        sorting,
        pending: false,
        payload,
        list: ['a', 'b', 'c', 'd', 'e', 'f'],
        total: 100,
        page,
      };
      expect(sites(state, action)).toEqual(expectedState);
    });
  });

  describe('on SITES_TOGGLE_SUBSCRIBING', () => {
    it('does not change state', () => {
      const state = {
        ...initialState,
      };

      const action = { type: types.SITES_TOGGLE_SUBSCRIBING, pk: 1 };

      const expectedState = {
        ...state,
      };
      expect(sites(state, action)).toEqual(expectedState);
    });
  });

  describe('on SITES_TOGGLE_SUBSCRIPTION_SUCCESS', () => {
    it('updates corresponding list item', () => {
      const state = {
        ...initialState,
        list: [
          { pk: 1, subscribed: false },
        ],
      };

      const payload = { subscribed: true };
      const action = { type: types.SITES_TOGGLE_SUBSCRIPTION_SUCCESS, pk: 1, payload };

      const expectedState = {
        ...state,
        list: [
          { pk: 1, subscribed: true },
        ],
      };
      expect(sites(state, action)).toEqual(expectedState);
    });

    it('does not update other items', () => {
      const state = {
        ...initialState,
        list: [
          { pk: 1, subscribed: false },
          { pk: 2, subscribed: false },
          { pk: 3, subscribed: false },
        ],
      };

      const payload = { subscribed: true };
      const action = { type: types.SITES_TOGGLE_SUBSCRIPTION_SUCCESS, pk: 2, payload };

      const expectedState = {
        ...state,
        list: [
          { pk: 1, subscribed: false },
          { pk: 2, subscribed: true },
          { pk: 3, subscribed: false },
        ],
      };
      expect(sites(state, action)).toEqual(expectedState);
    });

    it('does not change state if there is no corresponding items', () => {
      const state = {
        ...initialState,
        list: [
          { pk: 1, subscribed: false },
        ],
      };

      const payload = { subscribed: true };
      const action = { type: types.SITES_TOGGLE_SUBSCRIPTION_SUCCESS, pk: 2, payload };

      const expectedState = {
        ...state,
      };
      expect(sites(state, action)).toEqual(expectedState);
    });
  });

  describe('on SITES_TOGGLE_FAILED', () => {
    it('does not change state', () => {
      const state = {
        ...initialState,
      };

      const action = { type: types.SITES_TOGGLE_FAILED };

      const expectedState = {
        ...state,
      };
      expect(sites(state, action)).toEqual(expectedState);
    });
  });
});

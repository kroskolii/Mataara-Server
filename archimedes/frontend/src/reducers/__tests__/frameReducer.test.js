import { frame } from '../frameReducer';
import * as types from '../../constants/ActionTypes';

describe('frame reducer', () => {
  const initialState = {
    pageTitle: '',
  };

  it('should return the initial state', () => {
    expect(frame(undefined, {})).toEqual(initialState);
  });

  describe('on SET_PAGETITLE', () => {
    it('sets pending to true', () => {
      const pageTitle = 'Test Page Title';
      const action = { type: types.SET_PAGETITLE, pageTitle };
      const state = {
        ...initialState,
      };
      const expectedState = {
        ...state,
        pageTitle,
      };
      expect(frame(state, action)).toEqual(expectedState);
    });
  });
});

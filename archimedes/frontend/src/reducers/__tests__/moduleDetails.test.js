import moduleDetails from '../moduleDetails';
import * as types from '../../constants/ActionTypes';

describe('moduleDetails reducer', () => {
  const initialState = {
    moduleId: 0,
    pending: false
  };

  it('should return the initial state', () => {
    expect(moduleDetails(undefined, {})).toEqual(initialState);
  });

  describe('on MODULES_DETAILS_FETCHING', () => {
    it('sets pending to true', () => {
      const action = { type: types.MODULES_DETAILS_FETCHING };
      const state = {
        ...initialState,
      };
      const expectedState = {
        ...state,
        pending: true,
      };
      expect(moduleDetails(state, action)).toEqual(expectedState);
    });
  });

  describe('on MODULES_DETAILS_FETCHED', () => {
    it('sets pending to false and set payload', () => {
      const payload = { some: 'thing' };
      const action = { type: types.MODULES_DETAILS_FETCHED, payload };
      const state = {
        ...initialState,
        pending: true,
      };
      const expectedState = {
        ...state,
        pending: false,
        ...payload,
      };
      expect(moduleDetails(state, action)).toEqual(expectedState);
    });
  });

  describe('on MODULES_RELEASES_FETCHING', () => {
    it('sets pending to true', () => {
      const action = { type: types.MODULES_RELEASES_FETCHING };
      const state = {
        ...initialState,
      };
      const expectedState = {
        ...state,
        pending: true,
      };
      expect(moduleDetails(state, action)).toEqual(expectedState);
    });
  });

  describe('on MODULES_RELEASES_FETCHED', () => {
    it('sets pending to false and set payload', () => {
      const payload = { some: 'thing' };
      const action = { type: types.MODULES_RELEASES_FETCHED, payload };
      const state = {
        ...initialState,
        pending: true,
      };
      const expectedState = {
        ...state,
        pending: false,
        ...payload,
      };
      expect(moduleDetails(state, action)).toEqual(expectedState);
    });
  });
});

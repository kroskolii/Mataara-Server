import siteDetails from '../siteDetails';
import * as types from '../../constants/ActionTypes';

describe('siteDetails reducer', () => {
  const initialState = {
    siteId: 0,
    pending: false
  };

  it('should return the initial state', () => {
    expect(siteDetails(undefined, {})).toEqual(initialState);
  });

  describe('on SITES_DETAILS_FETCHING', () => {
    it('sets pending to true', () => {
      const action = { type: types.SITES_DETAILS_FETCHING };
      const state = {
        ...initialState,
      };
      const expectedState = {
        ...state,
        pending: true,
      };
      expect(siteDetails(state, action)).toEqual(expectedState);
    });
  });

  describe('on SITES_DETAILS_FETCHED', () => {
    it('sets pending to false and set payload', () => {
      const payload = { some: 'thing' };
      const action = { type: types.SITES_DETAILS_FETCHED, payload };
      const state = {
        ...initialState,
        pending: true,
      };
      const expectedState = {
        ...state,
        pending: false,
        ...payload,
      };
      expect(siteDetails(state, action)).toEqual(expectedState);
    });
  });

  describe('on REPORTS_DETAILS_FETCHING', () => {
    it('sets pending to true', () => {
      const action = { type: types.REPORTS_DETAILS_FETCHING };
      const state = {
        ...initialState,
      };
      const expectedState = {
        ...state,
        pending: true,
      };
      expect(siteDetails(state, action)).toEqual(expectedState);
    });
  });

  describe('on REPORTS_DETAILS_FETCHED', () => {
    it('sets pending to false and set payload', () => {
      const payload = { some: 'thing' };
      const action = { type: types.REPORTS_DETAILS_FETCHED, payload };
      const state = {
        ...initialState,
        pending: true,
      };
      const expectedState = {
        ...state,
        pending: false,
        ...payload,
      };
      expect(siteDetails(state, action)).toEqual(expectedState);
    });
  });
});

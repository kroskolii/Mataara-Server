import advisoryDetails from '../advisoryDetails';
import * as types from '../../constants/ActionTypes';

describe('advisoryDetails reducer', () => {
  const initialState = {
    advisoryId: 0,
    pending: false
  };

  it('should return the initial state', () => {
    expect(advisoryDetails(undefined, {})).toEqual(initialState);
  });

  describe('on ADVISORIES_DETAILS_FETCHING', () => {
    it('sets pending to true', () => {
      const action = { type: types.ADVISORIES_DETAILS_FETCHING };
      const state = {
        ...initialState,
      };
      const expectedState = {
        ...state,
        pending: true,
      };
      expect(advisoryDetails(state, action)).toEqual(expectedState);
    });
  });

  describe('on ADVISORIES_DETAILS_FETCHED', () => {
    it('sets pending to false and set payload', () => {
      const payload = { some: 'thing' };
      const action = { type: types.ADVISORIES_DETAILS_FETCHED, payload };
      const state = {
        ...initialState,
        pending: true,
      };
      const expectedState = {
        ...state,
        pending: false,
        ...payload,
      };
      expect(advisoryDetails(state, action)).toEqual(expectedState);
    });
  });
});

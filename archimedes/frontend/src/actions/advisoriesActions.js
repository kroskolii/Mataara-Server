import * as api from '../api';
import {
  ADVISORIES_REQUEST,
  ADVISORIES_SUCCESS,
  ADVISORIES_FAILURE,
} from '../constants/ActionTypes';

export function fetchAdvisories(filters, page, sorting) {
  const request = () => ({ type: ADVISORIES_REQUEST, page });
  const success = payload => ({ type: ADVISORIES_SUCCESS, payload, page });
  const failure = payload => ({ type: ADVISORIES_FAILURE, payload });

  return (dispatch) => {
    dispatch(request());

    return api.fetchAdvisories(filters, page, sorting)
      .then(
        payload => { dispatch(success(payload)); },
        error => { dispatch(failure(error)); }
      );
  };
}

import * as api from '../api';
import {
  SITES_FETCHING,
  SITES_FETCHED,
  SITES_FAILED,
  SITES_TOGGLE_SUBSCRIBING,
  SITES_TOGGLE_SUBSCRIPTION_SUCCESS,
  SITES_TOGGLE_FAILED
} from '../constants/ActionTypes';

function sitesTogglingSite(pk) {
  return {
    type: SITES_TOGGLE_SUBSCRIBING,
    pk
  };
}
function sitesToggledSite(pk, payload) {
  return {
    type: SITES_TOGGLE_SUBSCRIPTION_SUCCESS,
    pk,
    payload
  };
}
function sitesToggleFailed(err) {
  return {
    type: SITES_TOGGLE_FAILED,
    ...err
  };
}

export function sitesToggleSubscription(pk) {
  // API call here.
  // reference fetchhSites() below.
  return (dispatch) => {
    dispatch(sitesTogglingSite(pk));

    return api.sitesToggleSubscription(pk)
      .then(payload => dispatch(sitesToggledSite(pk, payload)))
      .catch(err => dispatch(sitesToggleFailed(err)));
  };
}

export function fetchSites(filters, page, sorting) {
  const request = () => ({ type: SITES_FETCHING, page });
  const success = payload => ({ type: SITES_FETCHED, payload, filters, sorting, page });
  const failure = payload => ({ type: SITES_FAILED, payload });

  return (dispatch) => {
    dispatch(request());

    return api.fetchSites(filters, page, sorting)
    .then(payload => dispatch(success(payload)))
    .catch(err => dispatch(failure(err)));
  };
}

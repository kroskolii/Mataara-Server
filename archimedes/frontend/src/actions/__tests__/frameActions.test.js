import * as actions from '../frameActions';
import * as types from '../../constants/ActionTypes';

describe('frameActions', () => {
  it('should create an action to set page title', () => {
    // Input for the function
    const title = 'Test Page Title';

    // Expectations
    const expectedAction = { type: types.SET_PAGETITLE, pageTitle: title };

    expect(actions.setPageTitle(title)).toEqual(expectedAction);
  });
});

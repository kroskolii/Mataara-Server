import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../moduleDetailsActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('moduleDetailsActions', () => {
  afterEach(() => {
    fetch.resetMocks();
  });

  describe('fetchModuleDetails function', () => {
    it('creates MODULES_DETAILS_FETCHING and MODULES_DETAILS_FETCHED on success', () => {
      // Input for the function
      const moduleId = 1;

      // Mock response
      const init = { status: 200, statusText: 'success' };
      const body = { mockedApiResponse: 'completed' };
      fetch.mockResponseOnce(JSON.stringify(body), init);

      // Expectations
      const expectedActions = [
        { type: types.MODULES_DETAILS_FETCHING },
        { type: types.MODULES_DETAILS_FETCHED, payload: body },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchModuleDetails(moduleId))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('creates MODULES_DETAILS_FETCHING and MODULES_DETAILS_FAILED on failure', () => {
      // Input for the function
      const moduleId = 1;

      // Mock response
      const error = { status: 500, statusText: 'failed' };
      fetch.mockRejectOnce(error);

      // Expectations
      const expectedActions = [
        { type: types.MODULES_DETAILS_FETCHING },
        { type: types.MODULES_DETAILS_FAILED, ...error },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchModuleDetails(moduleId))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('fetchModuleReleases function', () => {
    it('creates MODULES_RELEASES_FETCHING and MODULES_RELEASES_FETCHED on success', () => {
      // Input for the function
      const moduleId = 1;

      // Mock response
      const init = { status: 200, statusText: 'success' };
      const body = { mockedApiResponse: 'completed' };
      fetch.mockResponseOnce(JSON.stringify(body), init);

      // Expectations
      const expectedActions = [
        { type: types.MODULES_RELEASES_FETCHING },
        { type: types.MODULES_RELEASES_FETCHED, payload: body },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchModuleReleases(moduleId))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('creates MODULES_RELEASES_FETCHING and MODULES_RELEASES_FAILED on failure', () => {
      // Input for the function
      const moduleId = 1;

      // Mock response
      const error = { status: 500, statusText: 'failed' };
      fetch.mockRejectOnce(error);

      // Expectations
      const expectedActions = [
        { type: types.MODULES_RELEASES_FETCHING },
        { type: types.MODULES_RELEASES_FAILED, ...error },
      ];

      const store = mockStore({});
      return store.dispatch(actions.fetchModuleReleases(moduleId))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});

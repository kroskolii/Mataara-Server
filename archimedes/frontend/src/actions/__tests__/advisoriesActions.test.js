import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../advisoriesActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('advisoriesActions', () => {
  afterEach(() => {
    fetch.resetMocks();
  });

  it('creates ADVISORIES_REQUEST and ADVISORIES_SUCCESS on success', () => {
    // Input for the function
    const filters = {};
    const page = 1;
    const sorting = {
      sortBy: 'date_posted',
      sortDir: 'desc'
    };

    // Mock response
    const init = { status: 200, statusText: 'success' };
    const body = { mockedApiResponse: 'completed' };
    fetch.mockResponseOnce(JSON.stringify(body), init);

    // Expectations
    const expectedActions = [
      { type: types.ADVISORIES_REQUEST, page },
      { type: types.ADVISORIES_SUCCESS, payload: body, page },
    ];

    const store = mockStore({});
    return store.dispatch(actions.fetchAdvisories(filters, page, sorting))
    .then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('creates ADVISORIES_REQUEST and ADVISORIES_FAILURE on failure', () => {
    // Input for the function
    const filters = {};
    const page = 1;
    const sorting = {
      sortBy: 'date_posted',
      sortDir: 'desc'
    };

    // Mock response
    const error = { status: 500, statusText: 'failed' };
    fetch.mockRejectOnce(error);

    // Expectations
    const expectedActions = [
      { type: types.ADVISORIES_REQUEST, page },
      { type: types.ADVISORIES_FAILURE, payload: error },
    ];

    const store = mockStore({});
    return store.dispatch(actions.fetchAdvisories(filters, page, sorting))
    .then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});

import * as api from '../api';
import {
  ADVISORIES_REQUEST,
  ADVISORIES_SUCCESS,
  ADVISORIES_FAILURE,
  STATS_FETCHING,
  STATS_FETCHED,
  STATS_FAILED,
  SITES_SUBSCRIBED_FETCHING,
  SITES_SUBSCRIBED_FETCHED,
  SITES_SUBSCRIBED_FAILED,
} from '../constants/ActionTypes';

function advisoriesFetching(page) {
  return {
    type: ADVISORIES_REQUEST,
    page
  };
}

function advisoriesFetched(payload, page) {
  return {
    type: ADVISORIES_SUCCESS,
    payload,
    page,
  };
}

function advisoriesFailed(err) {
  return {
    type: ADVISORIES_FAILURE,
    ...err
  };
}

function sitesSubscribedSitesFetching() {
  return {
    type: SITES_SUBSCRIBED_FETCHING
  };
}

function sitesSubscribedSitesFetched(payload) {
  return {
    type: SITES_SUBSCRIBED_FETCHED,
    payload
  };
}

function sitesSubscribedSitesFailed(err) {
  return {
    type: SITES_SUBSCRIBED_FAILED,
    ...err
  };
}

function drupalStatsFetching(page) {
  return {
    type: STATS_FETCHING,
    page
  };
}

function drupalStatsFetched(payload) {
  return {
    type: STATS_FETCHED,
    payload
  };
}

function drupalStatsFailed(err) {
  return {
    type: STATS_FAILED,
    ...err
  };
}

export function fetchAdvisories(page) {
  return (dispatch) => {
    dispatch(advisoriesFetching(page));

    return api.fetchAdvisories(page)
    .then(payload => dispatch(advisoriesFetched(payload, page)))
    .catch(err => dispatch(advisoriesFailed(err)));
  };
}

export function fetchDrupalStats(page) {
  return (dispatch) => {
    dispatch(drupalStatsFetching(page));

    return api.fetchDrupalStats(page)
    .then(payload => dispatch(drupalStatsFetched(payload)))
    .catch(err => dispatch(drupalStatsFailed(err)));
  };
}

export function fetchSubscribedSites() {
  return (dispatch) => {
    dispatch(sitesSubscribedSitesFetching());

    return api.fetchSubscribedSites()
    .then(payload => dispatch(sitesSubscribedSitesFetched(payload)))
    .catch(err =>dispatch(sitesSubscribedSitesFailed(err)));
  };
}

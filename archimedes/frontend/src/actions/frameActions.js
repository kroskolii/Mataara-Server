import {
  SET_PAGETITLE,
} from '../constants/ActionTypes';

export const setPageTitle = (pageTitle) => ({ type: SET_PAGETITLE, pageTitle });

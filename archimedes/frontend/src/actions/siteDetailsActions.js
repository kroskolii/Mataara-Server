import * as api from '../api';
import {
  SITES_DETAILS_FETCHING,
  SITES_DETAILS_FETCHED,
  REPORTS_DETAILS_FETCHING,
  REPORTS_DETAILS_FETCHED,
  SITES_DETAILS_FAILED,
  REPORTS_DETAILS_FAILED
} from '../constants/ActionTypes';

function sitesDetailsFetching() {
  return {
    type: SITES_DETAILS_FETCHING
  };
}

function sitesDetailsFetched(payload) {
  return {
    type: SITES_DETAILS_FETCHED,
    payload
  };
}

function sitesDetailsFailed(err) {
  return {
    type: SITES_DETAILS_FAILED,
    ...err
  };
}

function reportsDetailsFetching() {
  return {
    type: REPORTS_DETAILS_FETCHING
  };
}

function reportsDetailsFetched(payload) {
  return {
    type: REPORTS_DETAILS_FETCHED,
    payload
  };
}

function reportsDetailsFailed(err) {
  return {
    type: REPORTS_DETAILS_FAILED,
    ...err
  };
}

export function fetchSiteDetails(siteId) {
  return (dispatch) => {
    dispatch(sitesDetailsFetching(siteId));

    return api.fetchsitesDetails(siteId)
    .then(payload => dispatch(sitesDetailsFetched(payload)))
    .catch(err => dispatch(sitesDetailsFailed(err)));
  };
}

export function fetchReportDetails(siteId) {
  return (dispatch) => {
    dispatch(reportsDetailsFetching(siteId));

    return api.fetchreportsDetails(siteId)
    .then(payload => dispatch(reportsDetailsFetched(payload)))
    .catch(err => dispatch(reportsDetailsFailed(err)));
  };
}

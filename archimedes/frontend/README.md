# Mataara Frontend

A fork from [Simple Redux Boilerplate](https://github.com/tsaiDavid/simple-redux-boilerplate)

### Development

To run Mataara in development npm is run to server the JS bundle. This allows
the developer to make changes to frontend code and have it take effect
straight away.

From the Mataara repository run
```
fab runjs
```
Your JS bundle now will be served from http://mataara-server.local:3000/bundles/main-[hash].js

You will be able to access Mataara from http://mataara-server.local:8008

#### Start without Fabric

First you will need to bring up the vagrant box. This will start the
backend.

```
vagrant up
```

Start a node server to serve the JS bundle:
```
// Inside vagrant
$ vagrant ssh
$ cd /vagrant/archimedes/frontend
$ npm start
```


### Production build

```
$ cd PROJECT_DIR/archimedes/frontend
$ npm run build
```

### Test

We use [Jest](https://facebook.github.io/jest/) to write our test. Please note that we are using the convention TestName.test.js and we put those test file in \__tests__ directory. Jest will recognise this directory automatically.

To run all test you can do
```
fab test
```

#### Running tests without Fabric

To run all the tests just use `npm run test` and if you want to get a coverage report just run `npm run test -- --coverage`.
If you want to run a particular test just use `npm run test TestName`.

// On your local host
```
$ npm run test
$ npm run test -- --coverage
$ npm run test <TestName>
```

## Notes

### Downgrading / using old modules

Due to eslint-config-react-app dependencies, following modules are downgraded / kept old. Otherwise `npm install` fails and linting does not work.

* eslint-config-react-app@2.1.0 requires a peer of babel-eslint@^7.2.3
* eslint-config-react-app@2.1.0 requires a peer of eslint@^4.1.1
* eslint-config-react-app@2.1.0 requires a peer of eslint-plugin-jsx-a11y@^5.1.1

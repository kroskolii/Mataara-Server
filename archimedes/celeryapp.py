"""
Celery master configuration file.

This file is copied more-or-less verbatim from

   http://celery.readthedocs.org/en/latest/django/first-steps-with-django.html

All actual Archimedes-related tasks will be in apps.defined in Django
app directories in files called 'tasks.py'.
"""

import logging
import os

from celery import Celery
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.db.utils import ProgrammingError
from raven import Client
from raven.contrib.celery import register_signal

# From https://github.com/celery/django-celery/blob/master/djcelery/models.py#L87
PERIOD_DAYS = 'days'
PERIOD_HOURS = 'hours'
PERIOD_MINUTES = 'minutes'
PERIOD_SECONDS = 'seconds'
PERIOD_MICROSECONDS = 'microseconds'

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'archimedes.settings')

app = Celery('archimedes')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

logger = logging.getLogger(__name__)

# Integrate Raven for Celery tasks
if hasattr(settings, 'RAVEN_CONFIG'):
    # Celery signal registration
    register_signal(Client(dsn=settings.RAVEN_CONFIG['dsn']))
    # logger.debug("Registered signal for Sentry log in Celery.")


@app.task(bind=True)
def debug_task(self):
    logger.debug('Request: {0!r}'.format(self.request))


def schedule_task(description, task_name, every, period, enabled):
    """
    Add a task to the scheduler

    This should be called from the .ready() callback of an AppConfig for the app in which
    the task resides

    :param description: Task description
    :param task_name: Name of the task to run (eg archimedes.example_app.tasks.foo)
    :param every: Number of period units between runs
    :param period: Period of the task schedule. See the PERIOD_* constants
    :param enabled: Enable or disable the task
    """
    from djcelery import models as celery_models

    # IntervalSchedule does not enforce any uniqueness on objects, so its possible to get into a state
    # Where get_or_create gets multiple objects and blows up
    # Use a filter-or-create instead
    try:
        interval = celery_models.IntervalSchedule.objects.filter(every=every, period=period).first()
        if interval is None:
            interval = celery_models.IntervalSchedule.objects.create(every=every, period=period)
            logger.debug("Created interval object for {0} {1}".format(period, every))
        else:
            logger.debug("Found existing interval object for {0} {1}".format(period, every))

    except (ProgrammingError, ImproperlyConfigured):
        # See comment below as to why this is required
        return False

    try:
        task, created = celery_models.PeriodicTask.objects.get_or_create(name=description)
    except (ProgrammingError, ImproperlyConfigured):
        # This may get called during syncdb before the djcelery tables have been created, which causes
        # A ProgrammingError (relation does not exist). It may also get called when the app doesn't have
        # a full configuration file (eg, during a collectstatic while the .deb is being built)
        # Nothing we can do until the tables have been created, so just swallow the error and pretend
        # everything is OK
        return False

    task.task = task_name
    task.interval = interval
    task.enabled = enabled
    task.save()

    return task

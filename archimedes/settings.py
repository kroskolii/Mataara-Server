"""
Django settings for Archimedes.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""
import os
import sys
from datetime import timedelta
from tempfile import gettempdir

import djcelery

# Read our .ini configuration file
from .configuration import CONFIG


def conf_isset(section, option):
    if CONFIG.has_section(section) and CONFIG.has_option(section, option):
        if CONFIG.get(section, option) != "":
            return True
    return False


# DIRECTORIES
# ------------------------------------------------------------------------------
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
PROJECT_DIR = os.path.dirname(__file__)
BASE_DIR = os.path.dirname(PROJECT_DIR)
SITE_ROOT = BASE_DIR

# SECRET KEY
# ------------------------------------------------------------------------------
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = CONFIG['django']['secret_key']

# DEBUG
# ------------------------------------------------------------------------------
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = CONFIG.getboolean('django', 'debug')

# WSGI
# ------------------------------------------------------------------------------
WSGI_APPLICATION = 'archimedes.wsgi.application'

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'archimedes.urls'

# APPS CONFIGURATION (base config, is extended throughout config files)
# ------------------------------------------------------------------------------
INSTALLED_APPS = (
    # Leave this at the top so it's templates override the others
    'archimedes.core',

    # Django standard apps
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',

    # Third party apps
    'djcelery',
    'django_filters',
    'django_nose',
    'compressor',
    'django_extensions',
    'polymorphic',
    'db_mutex',
    'rest_framework',
    'rest_framework_swagger',
    'webpack_loader'
)
LOCAL_APPS = (
    # First party apps
    'archimedes.sites',
    'archimedes.reports',
    'archimedes.api',
    'archimedes.drupal',
    'archimedes.silverstripe',
    'archimedes.subscription',
)
INSTALLED_APPS += LOCAL_APPS

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MIDDLEWARE_CLASSES = (
    # Make sure djangosecure.middleware.SecurityMiddleware is listed first
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'archimedes.middleware.RequireLoginMiddleware',
)

# ALLOWED HOSTS
# ------------------------------------------------------------------------------
# A list of strings representing the host/domain names that this Django site can serve.
ALLOWED_HOSTS = CONFIG['django']['allowed_hosts'].strip().split()

# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# Set outbound email settings
if conf_isset('email', 'server'):
    EMAIL_HOST = CONFIG['email']['server']
    try:
        EMAIL_PORT = CONFIG.getint('email', 'port')
    except ValueError:
        print('Mail server hostname defined. Port number must be specified. Aborting.')
        os._exit(1)

    SERVER_EMAIL = CONFIG.get('archimedes', 'from_email', fallback='archimedes@localhost')
    EMAIL_SUBJECT_PREFIX = CONFIG.get('archimedes', 'email_subject_prefix', fallback='[Archimedes]')

# Set inbound mailbox
MAILBOX_URI = CONFIG['archimedes']['mailbox_uri']

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
if 'test' in sys.argv:
    DATABASES = {
        # Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': gettempdir() + '/archimedes',
            'OPTIONS': {
            },
        }
    }
else:
    DATABASES = {
        # Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': CONFIG['database']['name'],
            'PASSWORD': CONFIG['database']['password'] or None,
            'HOST': CONFIG['database']['host'] or None,
            'USER': CONFIG['database']['user'] or None,
            'OPTIONS': {
                'sslmode': 'require',
            },
        }
    }

# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
TIME_ZONE = 'Pacific/Auckland'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-nz'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Administrators to receive error emails
if conf_isset('django', 'admin_email'):
    ADMINS = (('Archimedes Admin', CONFIG['django']['admin_email']),)

# Login URL
# React frontend so if you are visiting non api side there is only
# the Django admin.
LOGIN_URL = r'/admin/'
LOGIN_URL = r'/login/'

# URL paths for which the user must be logged in
LOGIN_REQUIRED_URLS = (
    r'/(.*)$',
)

# URL publicly accessible
LOGIN_REQUIRED_URLS_EXCEPTIONS = (
    r'/admin(.*)$',
    r'/login(.*)$',
    r'/logout(.*)$',
    r'/reports/endpoint(.*)$',
    r'/reports/legacy(.*)$',
    r'/favicon.ico$',
)

LOGIN_REDIRECT_URL = r'/'

# STATIC FILES CONFIGURATION
# ------------------------------------------------------------------------------
STATIC_URL = '/static/'
STATIC_ROOT = CONFIG['django'].get('static_root', os.path.join(PROJECT_DIR, 'static'))
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'archimedes/frontend'),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder'
)
WEBPACK_DIR = os.path.join(BASE_DIR, 'archimedes/frontend/webpack-stats.json')
WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'bundles/',
        'STATS_FILE': WEBPACK_DIR,
    }
}

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
if conf_isset('django', 'media_root'):
    MEDIA_ROOT = CONFIG['django']['media_root']
else:
    MEDIA_ROOT = os.path.join(PROJECT_DIR, 'static/media')
MEDIA_URL = STATIC_URL + 'media/'

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': [
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# AUTHENTICATION CONFIGURATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

auth_type = CONFIG.get('archimedes', 'authentication', fallback='local').lower()

# LDAP
# ------------------------------------------------------------------------------
# See: https://pythonhosted.org/django-auth-ldap/index.html
if auth_type == 'ldap':
    import ldap

    # Add new LDAP authentication backend
    AUTHENTICATION_BACKENDS += ('django_auth_ldap.backend.LDAPBackend',)

    if not conf_isset('ldap', 'server'):
        print('LDAP authentication selected, but no LDAP server provided. Aborting')
        os._exit(1)
    AUTH_LDAP_SERVER_URI = CONFIG['ldap']['server']

    AUTH_LDAP_BIND_DN = CONFIG.get('ldap', 'bind_dn', fallback='')
    AUTH_LDAP_BIND_PASSWORD = CONFIG.get('ldap', 'bind_password', fallback='')

    # AUTH_LDAP_USER_SEARCH = LDAPSearch("ou=users,dc=example,dc=com",
    #   ldap.SCOPE_SUBTREE, "(uid=%(user)s)")

    # or for direct searches
    AUTH_LDAP_USER_DN_TEMPLATE = CONFIG['ldap']['search_dn']

    # Populate the Django user from the LDAP directory.
    AUTH_LDAP_USER_ATTR_MAP = {
        "first_name": "givenName",
        "last_name": "sn",
        "email": "mail"
    }

    AUTH_LDAP_START_TLS = CONFIG.getboolean('ldap', 'ldap_starttls', fallback=False)

    if CONFIG.getboolean('ldap', 'ldap_ignore_cert', fallback=False):
        AUTH_LDAP_GLOBAL_OPTIONS = {ldap.OPT_X_TLS_REQUIRE_CERT: ldap.OPT_X_TLS_NEVER}

# SAML CONFIGURATION
# ------------------------------------------------------------------------------
# See pysaml2 documenation at https://github.com/rohe/pysaml2/blob/master/doc/howto/config.rst
# Default text for below taken from https://github.com/knaperek/djangosaml2
elif auth_type == 'saml':
    AUTHENTICATION_BACKENDS += ('djangosaml2.backends.Saml2Backend',)
    INSTALLED_APPS += ('djangosaml2',)
    LOGIN_URL = r'/saml2/login/'

    LOGIN_REQUIRED_URLS_EXCEPTIONS += (r'/saml2/(.*)$',)

    import saml2
    import saml2.saml
    if not conf_isset('saml', 'entityid'):
        print('Error: For SAML authentication, an entityid must be specified for the Archimedes server')
        os._exit(1)

    # Determine server's hostname and setup SP endpoint bindings
    if conf_isset('saml', 'root_url'):
        root_url = CONFIG['saml']['root_url']
        if root_url[-1] != '/':
            root_url = root_url + '/'
    else:
        # extract root url component from entityid
        pos = CONFIG['saml']['entityid'].find('/', 9)
        root_url = CONFIG['saml']['entityid'][0:pos + 1]

    # Check that specified private and public key files exist
    if not conf_isset('saml', 'key_file'):
        print('Error: For SAML authentication, key_file must be specified.')
        os._exit(1)
    elif not os.path.isfile(CONFIG['saml']['key_file']):
        print('Error: Private key file specified for SAML does not exist:', CONFIG['saml']['key_file'])
        os._exit(1)
    if not conf_isset('saml', 'cert_file'):
        print('Error: For SAML authentication, cert_file must be specified.')
        os._exit(1)
    elif not os.path.isfile(CONFIG['saml']['cert_file']):
        print('Error: Public key certificate file specified for SAML does not exist -', CONFIG['saml']['cert_file'])
        os._exit(1)
    if conf_isset('saml', 'enc_key_file') and not os.path.isfile(CONFIG['saml']['enc_key_file']):
        print('Error: Private key encryption file specified for SAML does not exist:', CONFIG['saml']['enc_key_file'])
        os._exit(1)
    if conf_isset('saml', 'enc_cert_file') and not os.path.isfile(CONFIG['saml']['enc_cert_file']):
        print('Error: Public key encryption certificate file specified for SAML does not exist:',
              CONFIG['saml']['enc_cert_file'])
        os._exit(1)

    # Check that IdP server metadata file exists
    remote_metadata_file = CONFIG.get('saml', 'remote_metadata_file', fallback='remote_metadata.xml')
    if not os.path.isfile(remote_metadata_file):
        print('Error: Metadata file for remote IdP does not exist. Using ', remote_metadata_file)
        os._exit(1)

    SAML_CONFIG = {
        # full path to the xmlsec1 binary programm
        'xmlsec_binary': CONFIG.get('saml', 'xmlsec_binary', fallback='/usr/bin/xmlsec1'),

        # your entity id, usually your subdomain plus the url to the metadata view
        'entityid': CONFIG.get('saml', 'entityid'),

        # this block states what services we provide
        'service': {
            # we are just a lonely SP
            'sp': {
                'name': 'Archimedes SP',
                'name_id_format': saml2.saml.NAMEID_FORMAT_PERSISTENT,
                # Define empty list ready for endpoint values
                'endpoints': {
                    'assertion_consumer_service': [
                        ((root_url + 'saml2/acs/', saml2.BINDING_HTTP_POST))
                    ],
                    'single_logout_service': [
                        (root_url + 'saml2/ls/', saml2.BINDING_HTTP_REDIRECT),
                        (root_url + 'saml2/ls/post', saml2.BINDING_HTTP_POST)
                    ],
                },

                # Define empty list for the idp values
                'idp': {}
            },
        },

        # define an empty block ready for technical or administrative contacts, if specified
        'contact_person': [
        ],

        # where the remote metadata is stored
        'metadata': {
            'local': [remote_metadata_file],
        },

        # Define signing public/private cryptographic files
        'key_file': CONFIG['saml']['key_file'],
        'cert_file': CONFIG['saml']['cert_file'],

        # Define empty array/list ready for the encryption_keypairs values
        'encryption_keypairs': [{
            'key_file': CONFIG.get('saml', 'enc_key_file', fallback=CONFIG['saml']['key_file']),
            'cert_file': CONFIG.get('saml', 'enc_cert_file', fallback=CONFIG['saml']['cert_file']),
        }],

        # Allow unknown attributes in IdP user assertion response
        'allow_unknown_attributes': CONFIG.getboolean('saml', 'allow_unknown_attributes', fallback='false'),
    }

    # SAML_DJANGO_USER_MAIN_ATTRIBUTE = 'username'

    if conf_isset('saml', 'username'):
        SAML_ATTRIBUTE_MAPPING = {}
        SAML_ATTRIBUTE_MAPPING[CONFIG.get('saml', 'username')] = ('username',)
    else:
        # Use the djangosaml2 default
        SAML_ATTRIBUTE_MAPPING = {
            'uid': ('username', )
        }
    if conf_isset('saml', 'email'):
        SAML_ATTRIBUTE_MAPPING[CONFIG.get('saml', 'email')] = ('email',)
    if conf_isset('saml', 'first_name'):
        SAML_ATTRIBUTE_MAPPING[CONFIG.get('saml', 'first_name')] = ('first_name',)
    if conf_isset('saml', 'last_name'):
        SAML_ATTRIBUTE_MAPPING[CONFIG.get('saml', 'last_name')] = ('last_name',)

    if conf_isset('saml', 'required_attributes'):
        SAML_CONFIG['service']['sp']['required_attributes'] = CONFIG['saml']['required_attributes'].split(',')

    if conf_isset('saml', 'attribute_map_dir'):
        SAML_CONFIG['attribute_map_dir'] = CONFIG['saml']['attribute_map_dir']

    if conf_isset('saml', 'expires'):
        try:
            # We only want to set the valid_for parameter if it's a positive integer,
            # don't set if 0 for no expiry
            expires = CONFIG.getint('saml', 'expires')
            if expires > 0:
                SAML_CONFIG['valid_for'] = expires
            elif expires < 0:
                print('SAML expires value must be a positive integer. Found:', expires)
                os._exit(1)
        except ValueError:
            print('SAML expires value must be a positive integer. Found:', CONFIG.get('saml', 'expires'))
            os._exit(1)
    else:
        SAML_CONFIG['valid_for'] = 24

    if conf_isset('saml-technical', 'name'):
        SAML_CONFIG['contact_person'].append({
            'contact_type': 'technical',
            'company': CONFIG.get('saml-technical', 'company', fallback=''),
            'given_name': CONFIG.get('saml-technical', 'name', fallback=''),
            'sur_name': CONFIG.get('saml-technical', 'surname', fallback=''),
            'email': CONFIG.get('saml-technical', 'email', fallback=''),
        })

    if conf_isset('saml-administrative', 'name'):
        SAML_CONFIG['contact_person'].append({
            'contact_type': 'administrative',
            'company': CONFIG.get('saml-administrative', 'company', fallback=''),
            'given_name': CONFIG.get('saml-administrative', 'name', fallback=''),
            'sur_name': CONFIG.get('saml-administrative', 'surname', fallback=''),
            'email': CONFIG.get('saml-administrative', 'email', fallback=''),
        })

    if conf_isset('saml', 'idp_entityid'):
        idp_entityid = CONFIG['saml']['idp_entityid']
        SAML_CONFIG['service']['sp']['idp'][idp_entityid] = {}
        eid = SAML_CONFIG['service']['sp']['idp'][idp_entityid]

        if conf_isset('saml', 'idp_sso'):
            eid['single_sign_on_service'] = {saml2.BINDING_HTTP_REDIRECT: CONFIG['saml']['idp_sso']}

        if conf_isset('saml', 'idp_sls'):
            eid['single_logout_service'] = {saml2.BINDING_HTTP_REDIRECT: CONFIG['saml']['idp_sls']}

    if DEBUG:
        SAML_CONFIG['debug'] = True
elif auth_type != "local":
    print('Invalid authentication type specified in settings. Aborting.')
    os._exit(1)

# TEST CONFIGURATION
# ------------------------------------------------------------------------------
# The name of the class to use for starting the test suite
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = [
    '--cover-package=' + ','.join(('archimedes.core',) + LOCAL_APPS),
]

# Remove "Login Required" middleware during testing
if len(sys.argv) > 1 and sys.argv[1] == 'test':
    MIDDLEWARE_CLASSES = list(MIDDLEWARE_CLASSES)
    MIDDLEWARE_CLASSES.remove('archimedes.middleware.RequireLoginMiddleware')

# LOGGING
# ------------------------------------------------------------------------------
# Disabled - because it's handled instead in configuration.py
LOGGING_CONFIG = None

# CSS/JS COMPILATION & COMPRESSION
# ------------------------------------------------------------------------------
# See: https://django-compressor.readthedocs.org/en/latest/settings/
STATICFILES_FINDERS += ('compressor.finders.CompressorFinder',)
COMPRESS_JS_FILTERS = [
    'compressor.filters.jsmin.JSMinFilter'
]
COMPRESS_CSS_FILTERS = [
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSMinFilter'
]
COMPRESS_PRECOMPILERS = (
    # Needs to be fixed upstream?
    # ('text/x-scss', 'django_pyscss.compressor.DjangoScssFilter'),

    # Really quick!
    # See: https://github.com/torchbox/django-libsass
    ('text/x-scss', 'django_libsass.SassCompiler'),

    # Needs `gem install sass` or similar
    # See: http://sass-lang.com/documentation/file.SASS_REFERENCE.html
    # ('text/x-scss', 'sass --scss {infile} {outfile}'),
)

# CACHING
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': CONFIG['redis']['location'],
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient'
        }
    }
}

# CELERY CONFIGURATION
# ------------------------------------------------------------------------------
djcelery.setup_loader()

BROKER_URL = CONFIG['celery']['broker_url']

CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_BACKEND = 'djcelery.backends.database:DatabaseBackend'
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'
CELERYD_HIJACK_ROOT_LOGGER = False
CELERY_IGNORE_RESULT = True

# The periodic task schedule used by celerybeat
# See: http://docs.celeryproject.org/en/latest/configuration.html#std:setting-CELERYBEAT_SCHEDULE
CELERYBEAT_SCHEDULE = {
    'getmail': {
        'task': 'archimedes.reports.tasks.getmail',
        'schedule': timedelta(seconds=60),
    },
    'import_drupal_advisories': {
        'task': 'archimedes.drupal.tasks.import_api_advisories',
        'schedule': timedelta(hours=1),
    },
    'ssimportadvisories': {
        'task': 'archimedes.silverstripe.tasks.import_advisories',
        'schedule': timedelta(hours=1),
    },
    'ssparseadvisories': {
        'task': 'archimedes.silverstripe.tasks.parse_advisories',
        'schedule': timedelta(hours=1),
    }
}

# Special settings for redis
if BROKER_URL.startswith('redis:'):
    # These settings are recommended for redis in the celery documentation at
    # http://celery.readthedocs.org/en/latest/getting-started/brokers/redis.html#caveats
    BROKER_TRANSPORT_OPTIONS = {
        # Set the visibility timeout to really high (two days)
        'visibility_timeout': 60 * 60 * 24 * 2,
        'fanout_prefix': True,
        'fanout_patterns': True
    }

# MAILBOX CONFIGURATION
# ------------------------------------------------------------------------------
# See: http://django-mailbox.readthedocs.org/en/stable/topics/appendix/settings.html
INSTALLED_APPS += ('django_mailbox', )

# Strip unallowed mimetypes from messages prior to processing attachments
DJANGO_MAILBOX_STRIP_UNALLOWED_MIMETYPES = True
DJANGO_MAILBOX_MAX_MESSAGE_SIZE = 2 * 1024 * 1024  # 2M
DJANGO_MAILBOX_ALLOWED_MIMETYPES = [
    # Here we limit which mimetypes are saved, so we don't have to deal with
    'text/html',
    'text/plain',
    'application/ekey',
    'application/json',
    'application/xml',
]
DJANGO_MAILBOX_STORE_ORIGINAL_MESSAGE = True  # Must be enabled to support legacy reports

# API CONFIGURATION
# ------------------------------------------------------------------------------
# See: http://www.django-rest-framework.org/api-guide/settings/
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.DjangoModelPermissions',
    ),
    'URL_FIELD_NAME': 'api_url',
    'PAGE_SIZE': 25,
    # The DEFAULT_PAGINATION_CLASS may change to
    # rest_framework.pagination.LimitOffsetPagination in the future, in order
    # to accommodate user settings and increased API versatility (e.g.
    # different loading policies for mobile clients, etc.)
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
        'rest_framework.filters.SearchFilter',
        'rest_framework.filters.OrderingFilter',
    )
}

# SENTRY
# ------------------------------------------------------------------------------
if conf_isset('sentry', 'dsn'):
    INSTALLED_APPS += ('raven.contrib.django.raven_compat', )

    with open(os.path.join(os.path.dirname(__file__), 'RELEASE')) as f:
        release_num = f.read().rstrip()

    RAVEN_CONFIG = {
        'dsn': CONFIG['sentry']['dsn'],
        'release': release_num,
    }

# LOCAL DEBUGING CONFIGURATION & APPS
# ------------------------------------------------------------------------------
if DEBUG:
    # django-debug-toolbar
    try:
        __import__('debug_toolbar')
        INSTALLED_APPS += ('debug_toolbar',)
        MIDDLEWARE_CLASSES += (
            'debug_toolbar.middleware.DebugToolbarMiddleware',
        )
        INTERNAL_IPS = ('127.0.0.1',)
        DEBUG_TOOLBAR_CONFIG = {
            'DISABLE_PANELS': [
                'debug_toolbar.panels.redirects.RedirectsPanel',
            ],
            'SHOW_TEMPLATE_CONTEXT': True,
        }
    except ImportError:
        pass

    # django-devserver
    try:
        __import__('devserver')
        INSTALLED_APPS += ('devserver',)
        DEVSERVER_ARGS = ['--werkzeug']
    except ImportError:
        pass


# REPORTS CONFIGURATION
# ------------------------------------------------------------------------------

# Map the ReportType field to the model that will process it
REPORT_TYPE_MAPPING = {
    'drupal8': 'archimedes.drupal.models.DrupalSite',
    'drupal7': 'archimedes.drupal.models.DrupalSite',
    'drupal6': 'archimedes.drupal.models.DrupalSite',
    'drupalLegacy': 'archimedes.drupal.models.DrupalSite',
    'silverstripe3': 'archimedes.silverstripe.models.SilverStripeSite',
}

# Private key for decrypting reports
REPORTS_PRIVATE_KEYFILE = CONFIG['archimedes']['reports_keyfile']
REPORTS_PRIVATE_KEYFILE_PASSPHRASE = CONFIG.get('archimedes', 'reports_keyfile_passphrase', fallback='')

# Report processing
REPORT_PROCESSING_RETRY_DELAY = CONFIG.getint('archimedes', 'reports_processing_retry_delay', fallback=10)
REPORT_PROCESSING_MAX_RETRIES = CONFIG.getint('archimedes', 'reports_processing_max_retries', fallback=10)

# URL prefix to Drupal.org's XML update server
DRUPAL_UPDATE_SERVER = CONFIG.get('archimedes', 'drupal_update_server', fallback='https://updates.drupal.org/')

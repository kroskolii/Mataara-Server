from django.test import TestCase
from django.test.client import RequestFactory

from archimedes.drupal.models import DrupalSite

from ..api.views import SiteTypeFilterBackend
from ..models import Site


class ApiSiteTypeFilterBackendTestCase(TestCase):

    def test_site_knows_about_drupalSite(self):
        """Given a site and a drupalSite the drupalSite is included in the list of all the Site objects"""
        site = Site.objects.create()
        dsite = DrupalSite.objects.create()
        qs = Site.objects.all()
        self.assertCountEqual(qs.values_list('pk', flat=True), [site.pk, dsite.pk])

    def test_filtering_for_drupal_sites(self):
        """Given a site and a drupalSite if the type requested is drupal \
        return the drupalSite"""
        # site = Site.objects.create()
        dsite = DrupalSite.objects.create()
        rf = RequestFactory()
        fbe = SiteTypeFilterBackend()
        qs = Site.objects.all()
        # Test filtering for Drupal sites
        request_drupal = rf.get('/', {'type': 'drupal'})
        qs_filtered = fbe.filter_queryset(request_drupal, qs, None)
        self.assertCountEqual(qs_filtered.values_list('pk', flat=True), [dsite.pk])

    def test_without_filtering(self):
        """Given a site and a drupalSite if there is no type requested return all the sites"""
        # given
        # site = Site.objects.create()
        # dsite = DrupalSite.objects.create()
        rf = RequestFactory()
        fbe = SiteTypeFilterBackend()
        qs = Site.objects.all()
        # when
        request_normal = rf.get('/')
        qs_filtered = fbe.filter_queryset(request_normal, qs, None)
        # then
        self.assertEqual(qs_filtered, qs)

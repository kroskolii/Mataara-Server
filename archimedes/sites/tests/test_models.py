from django.test import TestCase
from django.utils import timezone

from archimedes.reports.models import Report

from ..models import Environment, Site, SiteKey, SiteGroup


class SiteKeyTestCase(TestCase):

    def test_the_value_set_properly(self):
        """Given a :SiteKey, the value field is set properly"""
        key = SiteKey(value='testkey')
        key_val = key.value
        self.assertEqual(key_val, 'testkey')

    def test_str(self):
        """Given a :SiteKey, the string representation is the value field"""
        key = SiteKey(value='testkey')
        self.assertEqual(str(key), 'testkey')


class EnvironmentTestCase(TestCase):

    def test_str(self):
        """Given an Environment the string representaion is the name field"""
        env = Environment(name='The Environment')
        self.assertEqual(str(env), 'The Environment')


class SiteTestCase(TestCase):

    def setUp(self):
        Site.objects.create(name='testsite')

    def test_str_named_site(self):
        """
        Given a :Site with a 'name', the string representation is the name
        """
        # Site with a name
        testsite = Site.objects.get(name='testsite')
        self.assertEqual(str(testsite), 'testsite')

    def test_str_unnamed_site(self):
        """
        Given a :Site with no 'name', the string representation is generated\
         using the 'title' and the 'hostname' fields
        """
        # Site with no name, or hostname or title (generated_name)
        site = Site()
        self.assertEqual(str(site), 'Site (on an unknown host)')

        # Site with no name, or hostname and title  is cats(generated_name)
        site.title = 'Cats'
        self.assertEqual(str(site), 'Cats (on an unknown host)')

        # Site with no name. hostname is server.cats.com title is cats(generated_name)
        site.hostname = 'server.cats.com'
        self.assertEqual(str(site), 'Cats (on server.cats.com)')

    def test_set_key(self):
        """Given a :Site set the value to the :SiteKeys collection"""
        site = Site.objects.get(name='testsite')

        # First key
        site.set_key('testkey1')
        self.assertEqual(SiteKey.objects.count(), 1)
        self.assertEqual(site.keys.all()[0].value, 'testkey1')

    def test_set_key_using_value_of_existing_key(self):
        """Given a :Site  with a collection of :SiteKeys\
        do not set_key if the value is the same as an existing key """
        site = Site.objects.get(name='testsite')
        # First key
        site.set_key('testkey1')

        # Same first key
        original_keys = site.keys.all()
        site.set_key('testkey1')
        self.assertEqual(SiteKey.objects.count(), 1)
        self.assertEqual(original_keys[0].pk, site.keys.all()[0].pk)

    def test_set_key_using_value_different_to_existing_key(self):
        """Given a :Site  with a collection of :SiteKeys add to the\
        keys collection if the value is different to an existing key """
        site = Site.objects.get(name='testsite')
        site.set_key('firstKey')
        site.set_key('secondKey')
        self.assertEqual(SiteKey.objects.count(), 2)
        self.assertEqual(
            [k.value for k in site.keys.all()],
            ['firstKey', 'secondKey']
        )

    def test_reports(self):
        """ Given a site:Site if a report:Report is created and given a reference\
        to site, report is added to the site.reports collection"""
        site = Site.objects.get(name='testsite')
        report = Report.objects.create(
            date_generated=timezone.now(),
            date_expires=timezone.now(),
            processed_site=site,
        )

        self.assertEqual(site.reports.count(), 1)
        self.assertEqual(site.reports[0].pk, report.pk)


class SiteGroupTestCase(TestCase):
    def setUp(self):
        dev = Environment.objects.create(name='Development', slug='dev')
        stage = Environment.objects.create(name='Staging', slug='stage')
        pre = Environment.objects.create(name='Preproduction', slug='pre')
        uat = Environment.objects.create(name='UAT', slug='uat')
        prod = Environment.objects.create(name='Production', slug='prod')
        sk1 = SiteKey.objects.create(value='test-sk1')
        sk2 = SiteKey.objects.create(value='test-sk2')
        Site.objects.create(name='Site 1-Development',   environment=dev).set_key(sk1)
        Site.objects.create(name='Site 1-Staging',       environment=stage).set_key(sk1)
        Site.objects.create(name='Site 1-UAT',           environment=uat).set_key(sk1)
        Site.objects.create(name='Site 1-Production',    environment=prod).set_key(sk1)
        Site.objects.create(name='Site 2-Development',   environment=dev).set_key(sk2)
        Site.objects.create(name='Site 2-Staging',       environment=stage).set_key(sk2)
        Site.objects.create(name='Site 2-Preproduction', environment=pre).set_key(sk2)
        Site.objects.create(name='Site 2-Production',    environment=prod).set_key(sk2)
        pass

    def test_get_sites(self):
        """Given a :SiteGroup, ensure :Sites can be attached directly or via :SiteKey"""
        sitegroup = SiteGroup.objects.create(name="Grouped sites")
        sitegroup.set_key('test-sk1')
        self.assertEqual(len(sitegroup.get_sites()), 4, 'Four sites added via test-sk1')

        sitegroup.set_key('test-sk2')
        self.assertEqual(sitegroup.keys.count(), 2, 'Two keys added.')
        self.assertEqual(len(sitegroup.get_sites()), 8, 'Another four sites added via test-sk2')

        mobile_site, created = Site.objects.get_or_create(name='Site 2-Mobile')
        mobile_site.groups.add(sitegroup)

        self.assertEqual(len(sitegroup.get_sites()), 9, 'No-key site added')
        self.assertEqual(
            len(sitegroup.get_sites(include_by_site_key=False)),
            1,
            'One site returned when filtering out site key inclusions')

        self.assertTrue(sitegroup in mobile_site.get_groups(), 'sitegroup registered for no-key site')
        self.assertTrue(
            sitegroup in Site.objects.get(name='Site 2-Staging').get_groups(),
            'sitegroup registered for a key-included site'
        )

    def test_set_key(self):
        """Given a :SiteGroup set the values of the :SiteKeys collection"""
        sitegroup, created = SiteGroup.objects.get_or_create(name='testsitegroup')

        sitegroup.set_key('testkey1')
        self.assertEqual(sitegroup.keys.count(), 1, 'First key added')

        sitegroup.set_key('testkey2')
        self.assertEqual(sitegroup.keys.count(), 2, 'Second key added')

        sitegroup.set_key('testkey1')
        self.assertEqual(sitegroup.keys.count(), 2, 'Re-adding first key does not create another')

        self.assertTrue(SiteKey.objects.get(value='testkey1') in sitegroup.keys.all(), 'testkey1 in testsitegroup')
        self.assertTrue(SiteKey.objects.get(value='testkey2') in sitegroup.keys.all(), 'testkey2 in testsitegroup')

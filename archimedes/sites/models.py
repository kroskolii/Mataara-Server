from abc import abstractproperty, abstractmethod
from datetime import timedelta

from django.db import models
from django.utils import timezone
from polymorphic.manager import PolymorphicManager
from polymorphic.models import PolymorphicModel

from archimedes.reports.models import Report


class SiteKey(models.Model):
    value = models.CharField(max_length=200, unique=True)
    enabled = models.BooleanField(default=True)
    description = models.CharField(max_length=200, blank=True, help_text="Short description of intended key use")

    def __str__(self):
        return self.value

    class Meta:
        ordering = ['value']


class SiteGroup(models.Model):
    """
    A grouping of sites. Sites may be in multiple groups.

    Sites can be associated individually with a group, or via SiteKey.
    """
    name = models.CharField(max_length=200, help_text="Group name")
    description = models.CharField(blank=True, max_length=200, help_text="Short description of the group")
    keys = models.ManyToManyField(SiteKey, blank=True, help_text="Site keys associated with the group")
    notes = models.TextField(blank=True, help_text="Notes and external references")

    def __str__(self):
        return self.name

    def get_sites(self, include_by_site_key=True):
        """
        Return a set of all sites associated with this group.

        By default, this collects sites via site key as well as via explicit grouping.
        """
        sites = set()
        if include_by_site_key:
            sites = sites.union(Site.objects.filter(keys__in=self.keys.all()))
        sites = sites.union(Site.objects.filter(groups__pk=self.pk))

        return sites

    def set_key(self, key):
        """Set the site key, creating the key if necessary"""
        if self.keys.filter(value=key).count() == 0:
            new, created = SiteKey.objects.get_or_create(value=key)
            self.keys.add(new)


class Environment(models.Model):
    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=200, unique=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class SiteManager(PolymorphicManager):
    pass


class Site(PolymorphicModel):
    label = 'Site'
    objects = SiteManager()

    name = models.CharField(max_length=200, blank=True)
    hostname = models.CharField(max_length=200, blank=True)
    url = models.URLField(blank=True)
    root = models.CharField(max_length=200, blank=True)
    title = models.CharField(max_length=200, blank=True)
    environment = models.ForeignKey('Environment', null=True, blank=True,
                                    on_delete=models.SET_NULL, related_name='sites')
    keys = models.ManyToManyField(SiteKey, blank=True)
    groups = models.ManyToManyField(SiteGroup, blank=True)
    decommissioned = models.BooleanField(default=False)

    class Meta:
        ordering = ['title', 'environment']

    def __str__(self):
        generated_name = '{} (on {})'.format(
            (self.title or 'Site'),
            (self.hostname or 'an unknown host')
        )
        return self.name or generated_name

    @abstractmethod
    def compare_against_latest_report(self, report_json, field_paths=None):
        """
        Check whether report has unexpected values based on last report from this site.
        Return a dict of changed fields and their old values. If no field_paths argument
        is specified, a default_fields set may be used.
        """
        return

    def get_groups(self, include_by_site_key=True):
        """
        Return a set of all groups associated with this site.

        By default, this collects sites via site key as well as via explicit grouping.
        """
        groups = set(self.groups.all())

        if include_by_site_key:
            groups = groups.union(SiteGroup.objects.filter(keys__in=self.keys.all()))

        return groups

    def get_site_instance_values(self):
        """
        Return a dict of uniquely identifying values for a particular site instance.

        This may be overridden for subclasses.
        """
        return {
            "Label": self.label,
            "SiteKeys": [k.value for k in self.keys.all()],
            "Hostname": self.hostname,
            "Environment": self.environment.slug
        }

    @abstractproperty
    def vulnerability_count(self):
        """Return total count of vulnerable components across core, modules and themes."""
        return

    @abstractproperty
    def subscribed(self):
        """This defaults to False. The value can be updated in the ViewSet."""
        return False

    @property
    def date_last_received(self):
        """Get the latest date on which a report was received."""
        try:
            date_received = self.reports.latest('date_received')
        except Report.DoesNotExist:
            date_received = None
        if date_received is None:
            return None
        else:
            return self.reports.latest('date_received').date_received

    @property
    def reports(self):
        """Fetch all Reports about this Site."""
        return Report.objects.filter(processed_site_id=self.pk)

    def set_key(self, key):
        """Set the site key, creating the key if necessary"""
        if self.keys.filter(value=key).count() == 0:
            new, created = SiteKey.objects.get_or_create(value=key)
            self.keys.add(new)

    @property
    def active(self):
        """Has this site received a report in the last day_interval days?"""
        last_month = timezone.now() - timedelta(days=30)
        last_report_received = self.date_last_received
        return (last_report_received is not None and last_report_received > last_month)

# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0002_auto_20151125_1615'),
    ]

    operations = [
        migrations.AddField(
            model_name='environment',
            name='slug',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='environment',
            name='description',
            field=models.TextField(blank=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0012_site_decommissioned'),
    ]

    operations = [
        migrations.CreateModel(
            name='SiteGroup',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(help_text='Group name', max_length=200)),
                ('description', models.CharField(help_text='Short description of the group', blank=True, max_length=200)),
                ('notes', models.TextField(help_text='Notes and external references', blank=True)),
                ('keys', models.ManyToManyField(to='sites.SiteKey', help_text='Site keys associated with the group', blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='site',
            name='groups',
            field=models.ManyToManyField(to='sites.SiteGroup', blank=True),
        ),
    ]

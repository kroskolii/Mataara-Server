# -*- coding: utf-8 -*-


import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0008_auto_20160127_1427'),
    ]

    operations = [
        migrations.AlterField(
            model_name='site',
            name='environment',
            field=models.ForeignKey(related_name='sites', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='sites.Environment', null=True),
        ),
    ]

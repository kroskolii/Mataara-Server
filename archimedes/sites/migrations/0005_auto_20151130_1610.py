# -*- coding: utf-8 -*-


import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0004_auto_20151126_1205'),
    ]

    operations = [
        migrations.AlterField(
            model_name='site',
            name='environment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='sites.Environment', null=True),
        ),
    ]

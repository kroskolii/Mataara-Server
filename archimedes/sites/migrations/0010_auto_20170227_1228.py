# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0009_auto_20160128_1724'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='site',
            options={'ordering': ['name']},
        ),
    ]

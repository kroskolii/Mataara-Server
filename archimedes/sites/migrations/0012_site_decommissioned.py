# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0011_auto_20170227_1241'),
    ]

    operations = [
        migrations.AddField(
            model_name='site',
            name='decommissioned',
            field=models.BooleanField(default=False),
        ),
    ]

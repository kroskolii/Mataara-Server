# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('sites', '0006_auto_20151214_1634'),
    ]

    operations = [
        migrations.AddField(
            model_name='site',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='polymorphic_sites.site_set+', editable=False, to='contenttypes.ContentType', null=True),
        ),
    ]

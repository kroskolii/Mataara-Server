# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0007_site_polymorphic_ctype'),
    ]

    operations = [
        migrations.AlterField(
            model_name='environment',
            name='slug',
            field=models.CharField(unique=True, max_length=200),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0010_auto_20170227_1228'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='site',
            options={'ordering': ['title', 'environment']},
        ),
    ]

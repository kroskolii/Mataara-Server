# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0003_auto_20151126_1143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='site',
            name='content_type',
            field=models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True),
        ),
        migrations.AlterField(
            model_name='site',
            name='environment',
            field=models.ForeignKey(blank=True, to='sites.Environment', null=True),
        ),
        migrations.AlterField(
            model_name='site',
            name='object_id',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
    ]

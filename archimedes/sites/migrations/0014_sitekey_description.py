# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0013_auto_20181012_1057'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitekey',
            name='description',
            field=models.CharField(help_text='Short description of intended key use', max_length=200, blank=True),
        ),
    ]

from django.contrib import admin
from polymorphic.admin import PolymorphicParentModelAdmin

from archimedes.drupal.admin import DrupalSiteAdmin
from archimedes.drupal.models import DrupalSite
from archimedes.silverstripe.admin import SilverStripeSiteAdmin
from archimedes.silverstripe.models import SilverStripeSite

from . import models


class SiteKeyAdmin(admin.ModelAdmin):
    list_display = ('value', 'enabled', 'description')
    search_fields = ('value', 'description')

    def get_queryset(self, request):
        return super(SiteKeyAdmin, self).get_queryset(request)  # pragma: no cover


class EnvironmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'description')


class SiteAdmin(PolymorphicParentModelAdmin):
    base_model = models.Site
    child_models = (
        (DrupalSite, DrupalSiteAdmin),
        (SilverStripeSite, SilverStripeSiteAdmin),
    )
    list_display = ('__str__', 'environment', 'url', 'hostname', 'root', 'title')
    search_fields = ('environment__name', 'url', 'hostname', 'root', 'title')

    def get_queryset(self, request):
        return super(SiteAdmin, self).get_queryset(request)  # pragma: no cover


class SiteGroupAdmin(admin.ModelAdmin):
    base_model = models.SiteGroup
    list_display = ('name', 'description', 'notes')
    search_fields = ('name', 'description', 'notes', 'keys__value')

    def get_queryset(self, request):
        return super(SiteGroupAdmin, self).get_queryset(request).prefetch_related('keys')  # pragma: no cover


admin.site.register(models.SiteKey, SiteKeyAdmin)
admin.site.register(models.Environment, EnvironmentAdmin)
admin.site.register(models.Site, SiteAdmin)
admin.site.register(models.SiteGroup, SiteGroupAdmin)

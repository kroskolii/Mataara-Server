from rest_framework import serializers
from archimedes.subscription.models import Subscription

from .. import models


class SiteSerializer(serializers.HyperlinkedModelSerializer):
    environment = serializers.SlugRelatedField(
        read_only=True,
        slug_field='name'
    )
    keys = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='value'
    )
    string = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    core = serializers.SerializerMethodField()
    pk = serializers.IntegerField(read_only=True)
    subscribed = serializers.SerializerMethodField()

    class Meta:
        model = models.Site
        fields = (
            'api_url',
            'pk',
            'string',
            'name',
            'hostname',
            'url',
            'root',
            'title',
            'environment',
            'keys',
            'type',
            'core',
            'date_last_received',
            'vulnerability_count',
            'decommissioned',
            'subscribed',
        )

    def get_string(self, obj):
        return '%s' % obj

    def get_type(self, obj):
        return obj.label

    def get_core(self, obj):
        if obj.label == 'Drupal':
            return obj.site_ptr.drupalsite.core_version.version
        else:
            return 'not a drupal site'

    def get_subscribed(self, obj):
        request = self.context['request']
        user = request.user
        return Subscription.is_subscribed(user, obj)


class SiteIndexSerializer(SiteSerializer):
    """
    A cut-down version of the SiteSerializer, for stubbing in other models.

    This provides just enough information to fetch further data, without running
    into cyclic loops.
    """
    class Meta:
        model = models.Site
        fields = (
            'api_url',
            'pk',
            'string',
            'decommissioned',
        )


class SiteKeySerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer for SiteKey model.
    """
    class Meta:
        model = models.SiteKey
        fields = (
            'value',
            'enabled',
            'description',
        )


class SiteGroupSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer for information about a SiteGroup.
    """
    pk = serializers.IntegerField(read_only=True)
    sites = SiteIndexSerializer(many=True, read_only=True, source='get_sites')
    keys = SiteKeySerializer(many=True)

    class Meta:
        model = models.SiteGroup
        fields = (
            'api_url',
            'pk',
            'name',
            'description',
            'notes',
            'sites',
            'keys',
        )

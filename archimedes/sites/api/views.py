from rest_framework import viewsets
from rest_framework.filters import BaseFilterBackend
from rest_framework.settings import api_settings

from archimedes.drupal.models import DrupalSite

from . import serializers
from .. import models


class SiteTypeFilterBackend(BaseFilterBackend):
    """
    Filter that limits the queryset to a given polymorphic model instance.
    """
    def filter_queryset(self, request, queryset, view):
        if request.GET.get('type') == 'drupal':
            return queryset.instance_of(DrupalSite)
        else:
            return queryset


class SiteViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows sites to be viewed or edited.
    """
    queryset = models.Site.objects.select_related('environment').prefetch_related('keys')
    serializer_class = serializers.SiteSerializer
    filter_backends = api_settings.DEFAULT_FILTER_BACKENDS + [SiteTypeFilterBackend]
    filter_fields = ('name', 'hostname', 'title', 'decommissioned')
    search_fields = ('name', 'hostname', 'title', 'decommissioned')


class SiteGroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows site groups to be viewed or edited.
    """
    queryset = models.SiteGroup.objects.prefetch_related('keys')
    serializer_class = serializers.SiteGroupSerializer
    search_fields = ('name', 'description', 'notes',)

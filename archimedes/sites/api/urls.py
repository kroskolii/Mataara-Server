# from rest_framework import routers

from archimedes.api.urls import router

from ..api import views

# Register viewsets witht the router created in api.urls
router.register(r'sites', views.SiteViewSet)
router.register(r'sitegroups', views.SiteGroupViewSet)

# No URL patterns needed - they're done by api.urls.router
urlpatterns = []

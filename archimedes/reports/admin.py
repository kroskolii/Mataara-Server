

from django.contrib import admin

from .models import Report


class ReportAdmin(admin.ModelAdmin):
    search_fields = ('site_key', 'type', 'endpoint', 'processed_site__title', 'processed_site__url')
    list_display = ('__str__', 'type', 'endpoint', 'date_received', 'date_generated',
                    'site_key', 'processed', 'processed_site')

    def get_queryset(self, request):
        return super(ReportAdmin, self).get_queryset(request).select_related('processed_site')  # pragma: no cover


admin.site.register(Report, ReportAdmin)

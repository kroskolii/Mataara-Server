class ReportsException(Exception):
    """An exception occured in the Drupal app"""
    pass


class ReportDecodeError(ReportsException):
    """Unable to decode the report"""
    pass


class ReportUnknownEncoding(ReportDecodeError):
    """Unknown encoding type"""
    pass


class ReportDecryptError(ReportsException):
    """Unable to decrypt the report"""
    pass


class ReportUnknownCipher(ReportDecryptError):
    """Unknown cipher suite"""
    pass


class ReportAlreadyReceivedError(ReportsException):
    """The report has already been received"""
    pass


class ReportTypeNotConfiguredError(ReportsException):
    """The report type can not be processed by this app"""
    pass

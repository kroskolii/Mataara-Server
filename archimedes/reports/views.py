import json

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import permissions
from rest_framework.status import (HTTP_400_BAD_REQUEST, HTTP_409_CONFLICT,
                                   HTTP_500_INTERNAL_SERVER_ERROR)
from rest_framework.decorators import api_view, permission_classes

from .exceptions import (ReportAlreadyReceivedError, ReportDecodeError,
                         ReportDecryptError)
from .utils import decrypt, decode, legacy_decode, save_report


# Required keys for each report version
REQUIRED_KEYS = {
    "legacy": ["key", "data"],
    "1.0.0": ["ek", "enc"],
    "1.1.0": ["version", "cipher", "encoding", "report"]
}


@api_view(http_method_names=['POST'])
@permission_classes((permissions.AllowAny,))
@csrf_exempt
def endpoint(request):
    """
    HTTP endpoint for receiving Archimedes reports. This endpoint expects a POST
    request with either the value 'cipher' set to either a recognised cipher or 'none'.

    If the cipher key is not found it is assumed to be encrypted with RC4.

    - enc : encrypted data
    - ek  : encapsulation key
    """

    # Get the cipher, encoding and report version
    cipher = request.POST['cipher'] if 'cipher' in request.POST else "RC4"
    encoding = request.POST['encoding'] if 'encoding' in request.POST else "base64"

    if request.resolver_match.url_name == "legacy_endpoint":
        report_version = "legacy"
    elif request.resolver_match.url_name == "endpoint" and 'version' not in request.POST:
        report_version = "1.0.0"
    else:
        report_version = request.POST['version']

    # Check we have all required keys for the report version
    if not all(key in request.POST for key in REQUIRED_KEYS[report_version]):
        return HttpResponse(json.dumps({
            'success': False,
            'error': 'Missing required value(s): ' + ' '.join(
                key for key in REQUIRED_KEYS[report_version]
                if key not in request.POST)
        }), status=HTTP_400_BAD_REQUEST)
    # Check for conditional keys
    if report_version == "1.1.0" and cipher != "none" and 'ek' not in request.POST:
        return HttpResponse(json.dumps({
            'success': False,
            'error': 'Missing required value: ' + 'ek'
        }), status=HTTP_400_BAD_REQUEST)

    # Get the key and data
    if report_version == "legacy":
        key = request.POST['key']
        data = request.POST['data']
    elif report_version == "1.0.0":
        key = request.POST['ek']
        data = request.POST['enc']
    elif report_version == "1.1.0":
        key = request.POST['ek'] if 'ek' in request.POST else None
        data = request.POST['report']

    # Decode report
    if encoding != 'plain':
        try:
            key = decode(encoding, key)
            data = decode(encoding, data)
        except ReportDecodeError as e:
            return HttpResponse(json.dumps({
                'success': False,
                'error': str(e)
            }), status=HTTP_400_BAD_REQUEST)

    # Decrypt report
    if cipher != 'none':
        try:
            data = decrypt(cipher, key, data)
        except ReportDecryptError as e:
            return HttpResponse(json.dumps({
                'success': False,
                'error': str(e)
            }), status=HTTP_500_INTERNAL_SERVER_ERROR)

    # Convert legacy reports into the new format
    if report_version == "legacy":
        data = legacy_decode(data)

    # Decode the inner json
    try:
        data = json.loads(data.decode('utf-8'))
    except ValueError:
        return HttpResponse(json.dumps({
            'success': False,
            'error': 'Unable to decode json report',
        }), HTTP_500_INTERNAL_SERVER_ERROR)

    # Store the report version with the report
    data['ReportVersion'] = report_version

    # Save report
    try:
        save_report(data, 'http')
    except (ReportDecodeError, ReportAlreadyReceivedError) as e:
        return HttpResponse(json.dumps({
            'success': False,
            'error': str(e)
        }), status=HTTP_409_CONFLICT)

    return HttpResponse(json.dumps({
        'success': True,
    }))

import binascii
import datetime
import importlib
import json
import logging
import time
import urllib.error
from base64 import b64decode, b64encode
from urllib.parse import urlparse

import pytz
from bs4 import BeautifulSoup
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from django.conf import settings
from django.utils.dateparse import parse_datetime
from django.utils.text import slugify
from raven.contrib.django.raven_compat.models import client as raven_client

from archimedes.sites.models import Environment, Site

from .exceptions import (ReportAlreadyReceivedError, ReportDecodeError,
                         ReportDecryptError, ReportTypeNotConfiguredError,
                         ReportUnknownCipher, ReportUnknownEncoding)
from .models import Report
from .tasks import process

# Is Raven enabled? Assume it is if there is a RAVEN_CONFIG setting.
has_raven = hasattr(settings, 'RAVEN_CONFIG')

logger = logging.getLogger(__name__)


def decode(encoding, data):
    """Decode the data encoded in e.g. base64"""
    if encoding == "base64":
        try:
            return b64decode(data)
        except binascii.Error as e:
            raise ReportDecodeError("Unable to decode data: %s" % str(e)) from e

    else:
        raise ReportUnknownEncoding("Unknown encoding: %s" % encoding)


def decrypt(cipher_name, encrypted_envelope_key, encrypted_data):
    """Decrypt envelope encrypted report data from clients"""

    if cipher_name == "RC4":
        # Get the private key
        with open(settings.REPORTS_PRIVATE_KEYFILE, 'rb') as f:
            pem_data = f.read()

        # Check that the passphrase setting has been configured
        if not hasattr(settings, 'REPORTS_PRIVATE_KEYFILE_PASSPHRASE'):
            raise ReportDecryptError('Unable to decrypt report (passphrase required)')

        # Define the passphrase callback
        def get_passphrase(*args):
            try:
                return settings.REPORTS_PRIVATE_KEYFILE_PASSPHRASE
            except AttributeError:
                return None

        # Decrypt the envelope key
        key = load_pem_private_key(pem_data, password=None, backend=default_backend())
        try:
            envelope_key = key.decrypt(encrypted_envelope_key, padding.PKCS1v15())
        except ValueError as e:
            raise ReportDecryptError('Unable to decrypt the envelope key: ' + str(e)) from e

        # Decrypt the data using the envelope key
        # yes, we are using RC4 here for compatibility with our legacy Drupal client modules
        cipher = Cipher(algorithms.ARC4(envelope_key), mode=None, backend=default_backend())  # nosec
        decryptor = cipher.decryptor()
        data = decryptor.update(encrypted_data)
    else:
        raise ReportUnknownCipher("Unknown cipher: %s" % cipher_name)

    return data


def isBase64(s):
    """
    Check whether a string is a valid base64 encoding.

    This is done by attempting to decode the string, re-encode it, and comparing
    against the original (with linefeed characters stripped out).
    """
    try:
        lines_merged = s.translate(''.maketrans('', '', '\r\n'))

        if b64encode(b64decode(s)).decode() == lines_merged:
            return True
        else:
            return False
    except Exception:
        return False


def legacy_decode(raw_data):
    """
    Decode a legacy report, and throw it into the JSON format that
    the save_report function can handle.
    """

    soup = BeautifulSoup(raw_data, 'lxml')
    node = soup.find('node')
    if node is None:
        raise ReportDecodeError('Unable to parse the legacy XML')

    # Date generated/expiry
    if not node['datetime']:
        raise ReportDecodeError('Legacy report missing datetime attribute')
    timestamp = time.mktime(parse_datetime(node['datetime']).timetuple())

    # Server hostname
    servername = get_legacy_field(node, 'field_servername')
    try:
        o = urlparse(servername)
        hostname = o.hostname
    except urllib.error.URLError as e:
        raise ReportDecodeError('Legacy report servername could not be parsed to get the hostname') from e

    # Build up a dictionary of expected data
    data = {
        'ReportGenerated': timestamp,
        'ReportExpiry': timestamp + 30,
        'SiteKey': get_legacy_field(node, 'field_common_hash'),
        'ReportType': 'drupalLegacy',
        'ServerName': servername,
        'ServerHostname': hostname,
        'SiteRoot': get_legacy_field(node, 'field_webroot'),
        'SiteTitle': get_legacy_field(node, 'title'),
        'Environment': {
            'Name': slugify(get_legacy_field(node, 'field_site_env')).replace('-', '_'),
            'Label': get_legacy_field(node, 'field_site_env'),
        },
    }

    # "Extra" fields - optional
    data['SiteSlogan'] = get_legacy_field(node, 'body', required=False)
    data['DirectoryHash'] = get_legacy_field(node, 'field_directory_hash', required=False) or ''
    data['DrupalVersion'] = get_legacy_field(node, 'field_drupal_version', required=False) or ''

    # Database
    dbhosts = get_legacy_field(node, 'field_dbhost', required=False, values=True)
    if dbhosts and len(dbhosts) > 0:
        data['Databases'] = {
            'default': {
                'Driver': 'unknown',
                'Database': get_legacy_field(node, 'field_dbname', required=False) or '',
                'Host': dbhosts[0]['node:title'],
            }
        }

    # Nodes, Revisions, Users
    datasets = get_legacy_field(node, 'field_c_dataset', required=False, values=True)
    if datasets:
        data['Nodes'] = {}
        for dset in datasets:
            if dset['dataset:title'] == 'Nodes':
                data['Nodes']['Nodes'] = dset.get_text()
            if dset['dataset:title'] == 'Revisions':
                data['Nodes']['Revisions'] = dset.get_text()
            if dset['dataset:title'] == 'Users':
                data['Users'] = dset.get_text()

    # Modules
    modules = get_legacy_field(node, 'field_drupal_mod', required=False, values=True)
    if modules:
        data['Modules'] = []
        for module in modules:
            data['Modules'].append({
                'Module': module['node:field_name'],
                'Name': module['node:title'],
                'Description': module['node:body'],
                'Package': module['node:field_dru_pkg'],
                'Project': module['node:field_dru_proj'],
                'Version': module['node:field_mod_version'],
                'Url': module['node:field_mod_url'],
            })

    # Themes
    themes = get_legacy_field(node, 'field_drupal_theme', required=False, values=True)
    if themes:
        data['Themes'] = []
        for module in themes:
            data['Themes'].append({
                'Theme': module['node:field_name'],
                'Name': module['node:title'],
                'Description': module['node:body'],
                'Project': module['node:field_dru_proj'],
                'Version': module['node:field_mod_version'],
                'Url': module['node:field_mod_url'],
            })

    # Git repo
    git_repo = get_legacy_field(node, 'field_git_repo', required=False, values=True)
    if git_repo and len(git_repo) > 0:
        if 'Git' not in data:
            data['Git'] = {}
        git_repo = git_repo[0]
        data['Git']['Remote'] = git_repo['git:remote']
        data['Git']['Repo'] = git_repo.get_text()

    # Git hash
    git_hash = get_legacy_field(node, 'field_git_hash', required=False)
    if git_hash:
        if 'Git' not in data:
            data['Git'] = {}
        data['Git']['Hash'] = git_hash

    # Mark the report as legacy
    data['Legacy'] = True
    return data


def get_legacy_field(node_soup, field_id, required=False, values=False):
    """Get the string contents of a field from a legacy XML report"""
    try:
        data = node_soup.find('field', id=field_id)
        if values:
            return data.find_all('value')
        else:
            return data.get_text().replace('\n', '')
    except AttributeError:
        if required:
            raise ReportDecodeError('Legacy report missing %s field' % field_id)
        else:
            return None


def mail_decode_and_save(ekey, encdata, legacy=False):
    """Decode and save report data as a Report object"""

    # For reports received by email, we currently only support RC4 ciphers with base64 encoding.
    # Encryption is currently non-optional for email reports
    # Report format version 1.1.0 not supported by email

    # Decode report. We can receive either a base64 encoding or a byte stream.
    # Other inputs should throw an error.

    # Decode envelope key.
    key = decode('base64', ekey) if isBase64(ekey) else ekey
    if type(key) != bytes:
        extra = {'type': type(ekey), 'payload': ekey}
        msg = "Envelope key is not a byte stream or base64 encoded. (received type: {}).".format(
            extra['type']
        )
        if has_raven:
            ident = raven_client.get_ident(raven_client.captureMessage(
                msg,
                extra=extra,
                level='error'))
            logger.warning("Sentry ident: %s" % ident)
        raise ReportDecodeError(msg)

    # Decode encrypted data payload.
    data = decode('base64', encdata) if isBase64(encdata) else encdata
    if type(key) != bytes:
        extra = {'type': type(encdata), 'payload': encdata}
        msg = "Encrypted data is not a byte stream or base64 encoded (received type: {}).".format(
            extra['type']
        )
        if has_raven:
            ident = raven_client.get_ident(raven_client.captureMessage(
                msg,
                extra=extra,
                level='error'))
            logger.warning("Sentry ident: %s" % ident)
        raise ReportDecodeError(msg)

    if legacy:
        # Ensure the 'EKEY: ' prefix has been stripped from the envelope key.
        key = key.replace(b'EKEY: ', b'')

    # Decrypt report
    data = decrypt("RC4", key, data)

    # Convert legacy reports into the new format
    if legacy:
        data = legacy_decode(data)
        data['ReportVersion'] = 'legacy'
    else:
        # Coerce bytes into string.
        data = data.decode('UTF-8')

        # Decode the inner json
        try:
            data = json.loads(data)
            # Add default report version for non-legacy reports, if needed.
            if 'ReportVersion' not in data:
                data['ReportVersion'] = '1.0.0'
        except ValueError:
            raise ReportDecodeError

    pk = save_report(data, 'mail')

    return pk


def save_report(data, endpoint):
    """Save report data as a Report object"""

    # Check the expected keys are available
    expected = [
        'ReportGenerated',
        'ReportExpiry',
        'SiteKey',
        'ReportType',
        # The root url of the server
        'ServerName',
        'ServerHostname',
        'SiteRoot',
        'SiteTitle',
        'Environment',
    ]
    for e in expected:
        if e not in list(data.keys()) or not data[e]:
            raise ReportDecodeError('Missing the required "%s" report item' % e)

    # Convert timestamps
    dt_generated = datetime.datetime.fromtimestamp(data['ReportGenerated'], pytz.utc)
    dt_expires = datetime.datetime.fromtimestamp(data['ReportExpiry'], pytz.utc)

    # Check we haven't received this before
    matching = Report.objects.filter(
        site_key=data['SiteKey'],
        date_generated=dt_generated
    )
    if matching:
        raise ReportAlreadyReceivedError('This report has already been received')

    # TODO: Swap out for Notifications, when we have a better system implemented.
    if has_raven:
        # Fetch latest report for site, to check for mismatches.
        site_list = Site.objects.filter(
            keys__value=data['SiteKey'],
            environment__slug=data['Environment']['Name'],
            hostname=data['ServerHostname']
        )

        if len(site_list) > 1:
            # More than one site object with the same SiteKey / Environment pair.
            msg = "More than one site '{}' found with key '{}' for environment '{}'".format(
                data['ServerHostname'],
                data['SiteKey'],
                data['Environment']['Name']
            )

            ident = raven_client.get_ident(raven_client.captureMessage(
                msg,
                extra={s.pk: s.get_site_instance_values() for s in site_list},
                level='error'))
            logger.warning("Sentry ident: %s" % ident)

        elif len(site_list) == 1:
            # Found an existing site to compare.
            site = site_list[0]

            # Check within expected values for mismatches.
            mismatches = site.compare_json_against_latest_report(data)

            if len(mismatches) > 0:
                msg = "SiteKey {}: Expected field value mismatches found.".format(
                    data['SiteKey']
                )

                ident = raven_client.get_ident(raven_client.captureMessage(
                    msg,
                    extra=mismatches,
                    level='warning'))
                logger.warning("Sentry ident: %s" % ident)

    # Create report object, with basic info & blob of JSON for deferred processing
    r = Report(
        site_key=data['SiteKey'],
        date_generated=dt_generated,
        date_expires=dt_expires,
        type=data['ReportType'],
        endpoint=endpoint,
        json=json.dumps(data),
    )

    # If ReportVersion is present, add it in.
    if 'ReportVersion' in list(data.keys()):
        r.version = data['ReportVersion']

    r.save()

    # Process the report
    process.delay(r.pk)

    return r


def process_report(report):
    """Processes each item in an incoming report, into the relevant site object"""

    try:
        # Figure out which app will handle this site
        module_path = settings.REPORT_TYPE_MAPPING[report.dict['ReportType']]

    except KeyError as e:
        # Unable to find the mapping for this report type
        raise ReportTypeNotConfiguredError(
            'Could not find a mapping for report of type "%s"' % report.dict['ReportType']
        ) from e

    try:
        # Import the module's process function to be later invoked
        parts = module_path.split('.')
        submodule_models = importlib.import_module('.'.join(parts[:-1]))
        SubSite = getattr(submodule_models, parts[-1])
    except (ImportError, AttributeError) as e:
        # Unable to import the sub site class
        raise ReportTypeNotConfiguredError(
            'Could not import the subsite model "{}" for report type "{}"'.format(
                module_path,
                report.dict['ReportType']
            )
        ) from e

    # Get the site environment
    # If the report is passing through nulls for environment name/label, default to 'None'
    env, created = Environment.objects.get_or_create(
        slug=report.dict['Environment']['Name'] or 'none',
        defaults={'name': report.dict['Environment']['Label'] or 'None'}
    )

    # Try to find the site by key/environment, otherwise create a site
    s, created = SubSite.objects.get_or_create(
        keys__value=report.site_key,
        environment=env,
        hostname=report.dict['ServerHostname']
    )
    s.set_key(report.site_key)

    # Save the basic site data
    s.url = report.dict['ServerName']
    s.root = report.dict['SiteRoot']
    s.title = report.dict['SiteTitle']
    s.environment = env

    # Pass over to the subsite handler
    utils_module = '.'.join(parts[:-2]) + '.utils'

    try:
        submodule_utils = importlib.import_module(utils_module)
    except ImportError as e:
        # Unable to import the utils.process_report function
        raise ReportTypeNotConfiguredError(
            'Could not import the subsite utils module "{}" for report type "{}"'.format(
                utils_module,
                report.dict['ReportType']
            )
        ) from e

    submodule_utils.process_report(report, s)

    # Mark the site as processed
    s.save()

    report.processed = True
    report.processed_site = s
    report.save()

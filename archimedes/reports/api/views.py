from rest_framework import viewsets

from . import serializers
from .. import models


class ReportViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Reports to be viewed or edited.
    """
    queryset = models.Report.objects.all()
    serializer_class = serializers.ReportSerializer
    search_fields = ('name', 'site_key')
    filter_fields = ('type', 'site_key', 'processed_site')

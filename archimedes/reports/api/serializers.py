from rest_framework import serializers

from .. import models


class ReportSerializer(serializers.HyperlinkedModelSerializer):
    pk = serializers.IntegerField(read_only=True)

    class Meta:
        model = models.Report
        fields = (
            'api_url',
            'pk',
            'site_key',
            'type',
            'endpoint',
            'json',
            'date_generated',
            'date_received',
            'date_expires',
            'processed',
            'processed_site',
            'version',
        )

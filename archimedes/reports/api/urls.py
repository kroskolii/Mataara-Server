from archimedes.api.urls import router

from ..api import views

# Register viewsets witht the router created in api.urls
router.register(r'reports', views.ReportViewSet)

# No URL patterns needed - they're done by api.urls.router
urlpatterns = []

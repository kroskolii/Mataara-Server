import json
from base64 import b64encode

from django.core.urlresolvers import reverse
from django.test import TestCase


class EndpointViewTestCase(TestCase):

    def is_json(self, string):
        try:
            json.loads(string)
        except ValueError:
            return False
        return True

    def test_endpoint_requires_post(self):
        """Return an HTTP 405 error when endpoint receives a GET request."""
        response = self.client.get(reverse('reports:endpoint'))
        self.assertEquals(response.status_code, 405,
                          "Endpoint GET does not return 405 (Method Not Allowed) status code.")
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        decoded = json.loads(response.content.decode())
        self.assertIn('detail', decoded, "JSON does not contain a 'detail' message key.")
        self.assertEquals(decoded['detail'], 'Method "GET" not allowed.',
                          "Detail does not match expected error message.")

    def test_endpoint_requires_post_data(self):
        """Return an HTTP 400 error when endpoint receives an empty POST request."""
        response = self.client.post(reverse('reports:endpoint'))
        self.assertEquals(response.status_code, 400,
                          "Endpoint empty POST does not return 400 (Bad Request) status code.")
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        decoded = json.loads(response.content.decode())
        self.assertIn('success', decoded, "JSON does not contain an 'success' key.")
        self.assertEquals(decoded['success'], False, "Success flag is not false on error.")
        self.assertIn('error', decoded, "JSON does not contain an 'error' key.")
        self.assertEquals(decoded['error'], 'Missing required value(s): ek enc', "Error message mismatch.")

    def test_endpoint_post_nonb64(self):
        """Return an HTTP 400 error given a POST request with non-base64 encoded json data."""
        response = self.client.post(reverse('reports:endpoint'), {'ek': 'somekey', 'enc': 'somedata'})
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        self.assertContains(response, 'Incorrect padding', status_code=400)

    def test_endpoint_post_error(self):
        """Return an HTTP 500 error given b64encoded json data and key, if decryption fails."""
        response = self.client.post(reverse('reports:endpoint'),
                                    {'ek': b64encode(b'somekey').decode(), 'enc': b64encode(b'somedata').decode()})
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        self.assertContains(response, 'Unable to decrypt', status_code=500)


class LegacyEndpointViewTestCase(TestCase):

    def is_json(self, string):
        try:
            json.loads(string)
        except ValueError:
            return False
        return True

    def test_legacy_endpoint_requires_post(self):
        """Return an HTTP 405 error when endpoint receives a GET request."""
        response = self.client.get(reverse('reports:legacy_endpoint'))
        self.assertEquals(response.status_code, 405,
                          "Endpoint GET does not return 405 (Method Not Allowed) status code.")
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        decoded = json.loads(response.content.decode())
        self.assertIn('detail', decoded, "JSON does not contain a 'detail' message key.")
        self.assertEquals(decoded['detail'], 'Method "GET" not allowed.',
                          "Detail does not match expected error message.")

    def test_legacy_endpoint_requires_post_data(self):
        """Return an HTTP 400 error when endpoint receives an empty POST request."""
        response = self.client.post(reverse('reports:legacy_endpoint'))
        self.assertEquals(response.status_code, 400,
                          "Endpoint empty POST does not return 400 (Bad Request) status code.")
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        decoded = json.loads(response.content.decode())
        self.assertIn('success', decoded, "JSON does not contain an 'success' key.")
        self.assertEquals(decoded['success'], False, "Success flag is not false on error.")
        self.assertIn('error', decoded, "JSON does not contain an 'error' key.")
        self.assertEquals(decoded['error'], 'Missing required value(s): key data', "Error message mismatch.")

    def test_legacy_endpoint_post_nonb64(self):
        """Return an HTTP 400 error given a POST request with non-base64 encoded json data."""
        response = self.client.post(reverse('reports:legacy_endpoint'), {'key': 'somekey', 'data': 'somedata'})
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        self.assertContains(response, 'Incorrect padding', status_code=400)

    def test_legacy_endpoint_post_error(self):
        """Return an HTTP 500 error given b64encoded json data and key, if decryption fails."""
        response = self.client.post(reverse('reports:legacy_endpoint'),
                                    {'key': b64encode(b'somekey').decode(), 'data': b64encode(b'somedata').decode()})
        self.assertTrue(self.is_json(response.content.decode()), "Endpoint response is not valid JSON.")
        self.assertContains(response, 'Unable to decrypt', status_code=500)

import os
import six
import email
import django_mailbox
import re
from unittest.mock import patch

from base64 import b64encode, b64decode

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import load_pem_private_key, load_pem_public_key
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.ciphers import Cipher
from cryptography.hazmat.primitives.ciphers.algorithms import ARC4

from django.test import TestCase, override_settings
from django_mailbox.models import Mailbox, Message

from archimedes.reports import utils


def _get_test_message_path(msg):
    """Return path for message file."""
    return os.path.join(os.path.dirname(__file__),
                        'messages',
                        msg
                        )


def _get_test_privkey():
    """Return private keyfile for tests."""
    return os.path.join(os.path.dirname(__file__),
                        'keys',
                        'private.pem'
                        )


def _get_test_pubkey():
    """Return public keyfile for tests."""
    return os.path.join(os.path.dirname(__file__),
                        'keys',
                        'public.pem'
                        )


class MailReportTaskTestCase(TestCase):

    def _get_email_as_text(self, name):
        """Fetch sample email bytestream from file system.

        Adapted from django_mailbox test helper.
        """
        with open(
            os.path.join(
                os.path.dirname(__file__),
                'messages',
                name,
            ),
            'rb'
        ) as f:
            return f.read()

    def _get_email_object(self, name):
        """Fetch sample email object from file system.

        Adapted from django_mailbox test helper.
        """
        copy = self._get_email_as_text(name)
        if six.PY3:
            return email.message_from_bytes(copy)
        else:
            return email.message_from_string(copy)

    def test_encryption_pipeline(self):
        """Ensure encrypt/decrypt pipeline functions with test keys."""
        message = self._get_email_as_text('legacy-decoded.xml')
        public_key_data = open(_get_test_pubkey()).read().encode('utf-8')
        public_key = load_pem_public_key(public_key_data, backend=default_backend())

        # Encryption
        random_key = os.urandom(16)
        enc_algorithm = ARC4(random_key)  # nosec : we know RC4 is a bad idea.
        encrypted_random_key = public_key.encrypt(plaintext=random_key, padding=padding.PKCS1v15())

        enc_cipher = Cipher(algorithm=enc_algorithm, mode=None, backend=default_backend())
        encryptor = enc_cipher.encryptor()
        encrypted_message = encryptor.update(message)

        # EKEY prepend for legacy format.
        ekey = b64encode(b'EKEY: ' + encrypted_random_key)
        enc = b64encode(encrypted_message)

        # For generating sample report from cleartext:
        #
        # print("EKEY:\n{}\nENC:\n{}".format(ekey, enc))
        # f = open('ekey.tmp', 'wb')
        # f.write(ekey)
        # f.close()
        # f = open('enc.tmp', 'wb')
        # f.write(enc)
        # f.close()

        # Decryption
        private_key_data = open(_get_test_privkey()).read().encode('utf-8')
        private_key = load_pem_private_key(private_key_data, password=None, backend=default_backend())
        envelope_key = private_key.decrypt(b64decode(ekey).replace(b'EKEY: ', b''), padding.PKCS1v15())
        self.assertEqual(envelope_key, random_key, 'Envelope key match')

        dec_algorithm = ARC4(envelope_key)  # nosec : we know RC4 is a bad idea.
        dec_cipher = Cipher(algorithm=dec_algorithm, mode=None, backend=default_backend())
        decryptor = dec_cipher.decryptor()
        decrypted_message = decryptor.update(b64decode(enc))
        self.assertEqual(message, decrypted_message, 'Message encrypted and decrypted successfully.')

    def test_create_sample_mail(self):
        """Create sample mail message, recruit into Django Message object."""

        raw_message = self._get_email_as_text('legacy-test.eml')
        self.assertIsNotNone(raw_message, 'Found sample message.')

        # Convert to email.message.Message object.
        object_message = self._get_email_object('legacy-test.eml')
        self.assertEqual(type(object_message), email.message.Message,
                         'Converted to email.message.Message.')

        # Process into Django mailbox message.
        mailbox = Mailbox.objects.create()
        with patch('django_mailbox.models.logger') as mail_log:
            msg = mailbox.process_incoming_message(object_message)
            self.assertFalse(mail_log.warning.called, 'No warnings on mailbox processing.')

        self.assertEqual(type(msg), django_mailbox.models.Message,
                         'Processed as django_mailbox.models.Message.')

        # Ensure raw message matches after processing.
        msg.eml.open()
        raw_eml = msg.eml.file.read()
        msg.eml.close()
        self.assertEqual(raw_message, raw_eml, 'Raw message match.')

    @override_settings(REPORTS_PRIVATE_KEYFILE=_get_test_privkey())
    def test_legacy_email_report(self):
        """Process incoming legacy format email."""
        # Attempt to process legacy email.
        object_message = self._get_email_object('legacy-test.eml')
        decoded_message = message = self._get_email_as_text('legacy-decoded.xml')
        mailbox = Mailbox.objects.create()
        message = mailbox.process_incoming_message(object_message)
        attachment_filenames = [f.get_filename() for f in message.attachments.all()]
        # Check email does not have non-legacy attachments.
        self.assertFalse('data.enc' in attachment_filenames, 'No data.enc found.')
        self.assertFalse('ekey.enc' in attachment_filenames, 'No ekey.enc found.')
        # Check email does have legacy attachment.
        self.assertTrue('data.xml' in attachment_filenames, 'data.xml found.')
        # encdata = a.document.read()
        # Check XML data in expected form.
        message.eml.open()
        raw = message.eml.read().decode()
        message.eml.close()
        ekey_b64 = re.split('\n\n', raw)[3].split('\n-------')[0]
        ekey_b64 = ekey_b64.translate(''.maketrans('', '', '\r\n'))
        encdata_b64 = re.split('\n\n', raw)[4]
        encdata_b64 = encdata_b64.translate(''.maketrans('', '', '\r\n'))

        self.assertTrue(utils.isBase64(ekey_b64), 'envelope key in email is base64 encoded.')
        self.assertTrue(utils.isBase64(encdata_b64), 'data.xml in email is base64 encoded.')

        ekey = b64decode(ekey_b64).replace(b'EKEY: ', b'')
        encdata = b64decode(encdata_b64)
        data = utils.decrypt("RC4", ekey, encdata)
        self.assertEqual(data, decoded_message, 'Decrypted report matches expected decoded XML.')

        # Ensure it does this without throwing an exception.
        data = utils.legacy_decode(data)

        report = utils.mail_decode_and_save(ekey_b64, encdata_b64, legacy=True)

        # Make sure a few pieces of data are as expected from source.
        self.assertEqual(report.dict['DrupalVersion'], '7.52')
        self.assertEqual(report.dict['Users'], '1138')
        self.assertEqual(report.dict['ReportType'], 'drupalLegacy')
        self.assertEqual(report.dict['ServerHostname'], 'customer-d7.local')

    def tearDown(self):
        """Clean up any saved message files and their attachments."""
        for msg in Message.objects.all():
            msg_filename = msg.eml.file.name
            # Removes message attachments from static/media/mailbox_attachments.
            msg.delete()
            # Remove email copy from static/media/messages.
            if os.path.isfile(msg_filename):
                os.remove(msg_filename)

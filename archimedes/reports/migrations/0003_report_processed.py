# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0002_auto_20151125_1314'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='processed',
            field=models.BooleanField(default=False),
        ),
    ]

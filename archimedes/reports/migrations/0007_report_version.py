# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0006_auto_20170227_1228'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='version',
            field=models.CharField(max_length=20, blank=True),
        ),
    ]

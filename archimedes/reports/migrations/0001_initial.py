# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('site_key', models.CharField(unique=True, max_length=50)),
                ('date_received', models.DateTimeField(auto_now_add=True)),
                ('date_generated', models.DateTimeField()),
                ('date_expires', models.DateTimeField()),
                ('type', models.CharField(max_length=50)),
                ('endpoint', models.CharField(max_length=50)),
                ('json', models.TextField()),
            ],
            options={
                'ordering': ['-date_received'],
            },
        ),
    ]

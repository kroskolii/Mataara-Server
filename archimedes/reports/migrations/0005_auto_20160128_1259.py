# -*- coding: utf-8 -*-


import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0004_report_processed_site'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='processed_site',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='sites.Site', null=True),
        ),
    ]

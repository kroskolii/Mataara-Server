from django.conf.urls import include, patterns, url

from . import views

urlpatterns = patterns(
    '',
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^sites', views.IndexView.as_view(), name='index'),
    url(r'^modules', views.IndexView.as_view(), name='index'),
    url(r'^advisories', views.IndexView.as_view(), name='index'),
    url(r'^drupal', views.IndexView.as_view(), name='index'),
    url(r'^api/', include('archimedes.api.urls')),
    url(r'^reports/', include('archimedes.reports.urls', namespace='reports')),
)

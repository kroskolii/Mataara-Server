from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
from django.core.validators import URLValidator
from django.db.utils import IntegrityError

from archimedes.drupal.models import AdvisorySource as DrupalAdvisorySource
from archimedes.silverstripe.models import AdvisorySource as SilverStripeAdvisorySource


class Command(BaseCommand):
    """
    Add an advisory source.

    Currently only supports Drupal RSS feeds and SilverStripe core.

    :param module: Type of feed to import
    :param title: Title for the advisory source
    :param url: URL of rss feed
    :type module: str
    :type title: str
    :type url: str

    :Examples:

    "drupal","Drupal Contrib","https://www.drupal.org/security/contrib/rss.xml"
    "drupal","Drupal Core","https://www.drupal.org/security/rss.xml"
    "silverstripe","SilverStripe","https://www.silverstripe.org/download/security-releases/rss"

    """

    # TODO - work out how to put line breaks into the help text

    help = '''Add an advisory source.

           It is necessary to provide the module type, title and URL.'''

    def add_arguments(self, parser):
        parser.add_argument('-t', '--title',
                            action='store',
                            dest='title',
                            help='Title for the advisory source')
        parser.add_argument('-u', '--url',
                            action='store',
                            dest='url',
                            help='URL of advisory source')
        parser.add_argument('-m', '--module',
                            action='store',
                            dest='module',
                            help='The module for which to register the advisory')

    def handle(self, *args, **options):
        parsing_failed = False
        if not options['title']:
            self.stderr.write('Error: title must be specified')
            parsing_failed = True
        if not options['module']:
            self.stderr.write('Error: module must be specified')
            parsing_failed = True

        if not options['url']:
            self.stderr.write('Error: url must be specified')
            parsing_failed = True
        else:
            val = URLValidator()
            try:
                # This is validating against the default schemas of ['http', 'https', 'ftp', 'ftps']
                val(options['url'])
            except ValidationError:
                parsing_failed = True
                self.stderr.write('Error processing URL. It looks invalid: {url}'.format(url=options['url']))

        if not parsing_failed:
            if options['module'] == 'drupal':
                src = DrupalAdvisorySource(title=options['title'], url=options['url'])
            elif options['module'] == 'silverstripe':
                src = SilverStripeAdvisorySource(title=options['title'], url=options['url'])
            else:
                self.stderr.write("Error adding advisory source: Unknown module '{}'.".format(options['module']))
                exit()
            try:
                src.save()
            except IntegrityError:
                self.stderr.write("Error adding advisory source. It probably already exists.")
            else:
                self.stdout.write('Added advisory source. You will still need to run "importadvisories"')

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django_mailbox.models import Mailbox


class Command(BaseCommand):
    help = 'Generates a mailbox'

    def handle(self, *args, **options):
            try:
                Mailbox.objects.get_or_create(name="archimedes", uri=settings.MAILBOX_URI,
                                              from_email=settings.SERVER_EMAIL)
            except Mailbox.MultipleObjectsReturned:
                raise CommandError('Multiple mailboxes. Please check.')
            self.stdout.write('Successfully found or created mailbox')

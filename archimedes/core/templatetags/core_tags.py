import re

from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def active(context, urlname, class_name='active'):
    current_urlname = context['request'].resolver_match.url_name
    current_path = context['request'].path

    # Build a list of potential namespace options
    namespace_options = []
    for namespace in context['request'].resolver_match.namespaces or []:
        namespace_options.append('%s:%s' % (namespace, current_urlname))

    # Try to match the namespace/name
    if urlname in namespace_options:
        return class_name

    # Try to match the URL name
    elif current_urlname == urlname:
        return class_name

    # Try to match the current URL using regex
    elif re.search(urlname, current_path):
        return class_name

    # No match
    return ''

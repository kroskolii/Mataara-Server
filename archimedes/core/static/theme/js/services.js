'use strict';

var api_base = "/api/";

angular.module('archimedesServices', ['ngResource'])

    .config(function($resourceProvider, $httpProvider) {
        $resourceProvider.defaults.stripTrailingSlashes = false;
        $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';
    })

    .factory("Site", function ($resource) {
        return $resource(api_base + "sites/:id", {id: '@id'});
    })

    .factory("DrupalSite", function ($resource) {
        return $resource(api_base + "drupal/sites/:id", {id: '@id'});
    })

    .factory("DrupalAdvisory", function ($resource) {
        return $resource(api_base + "drupal/advisories/:id", {id: '@id'});
    })

    .factory("DrupalProject", function ($resource) {
        return $resource(api_base + "drupal/projects/:id", {id: '@id'});
    })

    .factory("DrupalRelease", function ($resource) {
        return $resource(api_base + "drupal/releases/:id", {id: '@id'});
    })

    .factory("SilverStripeSite", function($resource) {
        return $resource(api_base, + "silverstripe/advisories/:id", {id: '@id'});
    })

    .factory("SilverStripeAdvisory", function ($resource) {
        return $resource(api_base + "silverstripe/advisories/:id", {id: '@id'});
    })

    .factory("SilverStripeProject", function($resource) {
        return $resource(api_base + "silverstripe/projects/:id", {id: '@id'});
    })

    .factory("SilverStripeRelease", function ($resource) {
        return $resource(api_base + "silverstripe/releases/:id", {id: '@id'});
    });

'use strict';

/*
 * Angular controller for the site list
 */
angular.module('archimedes').controller('SiteListController', [
    '$scope', 'Site',
    function($scope, Site) {

    $scope.page = 1;
    $scope.next = true;
    $scope.loading = false;
    $scope.sites = [];
    $scope.search = '';
    $scope.filter = '';

    $scope.loadSites = function () {
        if (!$scope.loading) {
            if ($scope.next)
            {
                $scope.loading = true;
                Site.get({
                    page: $scope.page,
                    search: $scope.search,
                    type: $scope.filter,
                }, function(data) {
                    // If we are still loading (i.e. haven't been cancelled by a filter)
                    if ($scope.loading)
                    {
                        $scope.next = data.next;
                        $scope.page += 1;
                        for (let site of data.results) {
                            $scope.sites.push(site);
                        }
                        $scope.loading = false;
                        $scope.$emit('sites:updated');
                    }
                });
            }
        }
    }

    var clearSites = function () {
        $scope.page = 1;
        $scope.next = true;
        $scope.sites = [];
        $scope.loading = false;
    }

    $scope.$watch("search", function (newValue, oldValue) {
        if (newValue !== oldValue) {
            clearSites();
            $scope.loadSites();
        }
    });

    $scope.$watch("filter", function (newValue, oldValue) {
        if (newValue !== oldValue) {
            clearSites();
            $scope.loadSites();
        }
    });

}]);

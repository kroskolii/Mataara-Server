'use strict';

/*
 * Angular controller for the module list
 */
angular.module('archimedes').controller('ModuleListController', [
    '$scope', 'DrupalProject',
    function($scope, DrupalProject) {

    $scope.page = 1;
    $scope.next = true;
    $scope.loading = false;
    $scope.modules = [];
    $scope.search = '';
    $scope.filter = '';

    $scope.loadModules = function () {
        if (!$scope.loading) {
            if ($scope.next)
            {
                $scope.loading = true;
                DrupalProject.get({
                    page: $scope.page,
                    search: $scope.search,
                    type: $scope.filter,
                }, function(data) {
                    // If we are still loading (i.e. haven't been cancelled by a filter)
                    if ($scope.loading)
                    {
                        $scope.next = data.next;
                        $scope.page += 1;
                        for (let module of data.results) {
                            $scope.modules.push(module);
                        }
                        $scope.loading = false;
                        $scope.$emit('modules:updated');
                    }
                });
            }
        }
    }

    var clearModules = function () {
        $scope.page = 1;
        $scope.next = true;
        $scope.modules = [];
        $scope.loading = false;
    }

    $scope.$watch("search", function (newValue, oldValue) {
        if (newValue !== oldValue) {
            clearModules();
            $scope.loadModules();
        }
    });

    $scope.$watch("filter", function (newValue, oldValue) {
        if (newValue !== oldValue) {
            clearModules();
            $scope.loadModules();
        }
    });

}]);

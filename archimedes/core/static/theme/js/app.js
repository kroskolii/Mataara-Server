angular.module('archimedes', [
    'archimedesServices',
    'ngResource',
    'infinite-scroll',
    'angularSpinner',
    'angularMoment',
]);

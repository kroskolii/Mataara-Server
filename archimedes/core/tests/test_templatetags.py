from django.template import Context, Template
from django.test import RequestFactory, TestCase


# from django.core.urlresolvers import ResolverMatch

# from ..templatetags.core_tags import active


class CoreTags(TestCase):

    def setUp(self):
        self.factory = RequestFactory()

    def render_template(self, string, request_url='/', request_namespaces=[], request_url_name=None):
        """
        Render a template string using a faked context

        The fake context contains the namespace list and url name as used by the active()
        template tag. The request_url is only relevant when testing the active() tag's ability
        to match urls by regex.
        """
        class FakeResolverMatch:
            namespaces = request_namespaces
            url_name = request_url_name
        request = self.factory.get(request_url)
        request.resolver_match = FakeResolverMatch
        return Template(string).render(Context({'request': request}))

    def test_active_class_default(self):
        """'active' tag return value has the correct default"""
        self.assertEqual(
            self.render_template(
                "{% load core_tags %}{% active '' %}",
                request_url='/',
            ),
            'active'
        )

    def test_active_class_given(self):
        """'active' tag return value can be specified"""
        self.assertEqual(
            self.render_template(
                "{% load core_tags %}{% active '' 'anotherclass' %}",
                request_url='/',
            ),
            'anotherclass'
        )

    def test_active_url_regex(self):
        """'active' tag matches correctly using regex"""
        self.assertEqual(
            self.render_template(
                "{% load core_tags %}{% active 'dont/match/this/url' %}",
                request_url='/',
            ),
            ''
        )

    def test_active_url_name(self):
        """'active' tag matches correctly using URL name"""
        self.assertEqual(
            self.render_template(
                "{% load core_tags %}{% active 'matchbyurlname' %}",
                request_url_name='matchbyurlname',
            ),
            'active'
        )

    def test_active_url_namespace(self):
        """'active' tag matches correctly using URL name and namespace"""
        self.assertEqual(
            self.render_template(
                "{% load core_tags %}{% active 'matchby:namespace' %}",
                request_namespaces=['matchby'],
                request_url_name='namespace',
            ),
            'active'
        )

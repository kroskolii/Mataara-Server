from collections import Counter

from django.views.generic import TemplateView
from packaging import version

from archimedes.drupal.models import Advisory, DrupalSite


class IndexView(TemplateView):
    """The 'Dashboard view for Drupal sites (shows sites by version/ sites by major version)"""
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)

        # Obtain a list of active sites
        active_sites = [site for site in DrupalSite.objects.all() if site.active]

        # Collate Drupal sites by major core version
        core_pks = [c.core for c in active_sites]
        context['core_version_data'] = Counter(core_pks).most_common()

        # Collate Drupal sites by core version number
        drupal_pks = [c.core_version.version for c in active_sites]
        drupal_counted = Counter(drupal_pks).most_common()

        # Convert version string to a sortable Version object.
        # packaging.version follows PEP 440 for version comparisons.
        # Documentation: https://packaging.pypa.io/en/latest/version/
        def version_key(x):
            return version.parse(x[0])

        drupal_counted = sorted(drupal_counted, key=version_key, reverse=True)
        context['drupal_version_labels'] = ["'%s'" % x[0] for x in drupal_counted]
        context['drupal_version_data'] = [x[1] for x in drupal_counted]

        # Collate latest Drupal security advisories
        latest_advisories = Advisory.objects.all()[:10]

        advisory_dicts = []
        for advisory in latest_advisories:
            if (advisory.project and advisory.project.slug == 'drupal'):
                release_versions = [v.version for v in advisory.releases.all()]
                affected = DrupalSite.objects.filter(core_version__version__in=release_versions)
            else:
                affected = advisory.affected_sites.all()
            advisory_dicts.append({'object': advisory, 'affected': affected})

        context['drupal_latest_advisories'] = advisory_dicts

        return context

from rest_framework import viewsets

from . import serializers
from .. import models


class SilverStripeSiteViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows SilverStripe sites to be viewed or edited.
    """
    queryset = models.SilverStripeSite.objects.select_related('environment').prefetch_related('keys')
    serializer_class = serializers.SilverStripeSiteSerializer

    def get_view_name(self):
        """
        Override automagical view name generation, so it doesn't call our views "Silver Stripe".
        """
        # Suffix is for individual operations, e.g. "List" and "Instance".
        return "SilverStripe {}".format(
            getattr(self, 'suffix', None)
        )


class SilverStripeAdvisorySourceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows SilverStripe advisory sources to be viewed or edited.
    """
    queryset = models.AdvisorySource.objects.all()
    serializer_class = serializers.AdvisorySourceSerializer


class SilverStripeAdvisoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows SilverStripe advisories to be viewed or edited.
    """
    queryset = models.Advisory.objects.all()
    serializer_class = serializers.AdvisorySerializer
    search_fields = ('title', 'advisory_id', 'project__slug', )
    filter_fields = ('source__slug',)

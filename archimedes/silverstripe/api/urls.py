from archimedes.api.urls import router

from ..api import views

# Register viewsets with the router created in api.urls
router.register(r'silverstripe/sites', views.SilverStripeSiteViewSet)
router.register(
    r'silverstripe/advisory_sources',
    views.SilverStripeAdvisorySourceViewSet,
    base_name='silverstripe/advisory_sources'
)
router.register(
    r'silverstripe/advisories',
    views.SilverStripeAdvisoryViewSet,
    base_name='silverstripe/advisories'
)

# No URL patterns needed - they're done by api.urls.router
urlpatterns = []

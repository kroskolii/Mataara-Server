# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('silverstripe', '0004_project_custom'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='silverstripesite',
            name='core',
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('silverstripe', '0001_squashed_0009_auto_20170712_1457'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advisory',
            name='raw_summary',
            field=models.TextField(),
        ),
    ]

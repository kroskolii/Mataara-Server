# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('silverstripe', '0003_auto_20170719_1527'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='custom',
            field=models.BooleanField(default=False),
        ),
    ]

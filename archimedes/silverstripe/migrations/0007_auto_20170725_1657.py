# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('silverstripe', '0006_auto_20170721_1508'),
    ]

    operations = [
        migrations.AlterField(
            model_name='silverstripesite',
            name='modules',
            field=models.ManyToManyField(related_query_name='module_site', blank=True, related_name='site_modules', to='silverstripe.Release'),
        ),
        migrations.AlterField(
            model_name='silverstripesite',
            name='themes',
            field=models.ManyToManyField(related_query_name='theme_site', blank=True, related_name='site_themes', to='silverstripe.Release'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('silverstripe', '0005_remove_silverstripesite_core'),
    ]

    operations = [
        migrations.AlterField(
            model_name='version',
            name='extra',
            field=models.CharField(max_length=20, null=True, blank=True, default=''),
        ),
    ]

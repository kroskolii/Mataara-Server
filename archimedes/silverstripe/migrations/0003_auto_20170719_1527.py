# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('silverstripe', '0002_auto_20170725_0948'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('slug', models.CharField(max_length=200, unique=True)),
                ('url', models.URLField(blank=True)),
                ('description', models.TextField(blank=True)),
                ('date_updated', models.DateTimeField(auto_now=True)),
                ('type', models.CharField(max_length=50)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Release',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('url', models.URLField()),
                ('date', models.DateTimeField(blank=True, null=True)),
                ('date_updated', models.DateTimeField(auto_now=True)),
                ('project', models.ForeignKey(related_name='releases', to='silverstripe.Project', related_query_name='release')),
            ],
            options={
                'ordering': ['-version', '-date'],
            },
        ),
        migrations.CreateModel(
            name='Version',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('major', models.PositiveIntegerField()),
                ('minor', models.PositiveIntegerField(default=0, blank=True)),
                ('patch', models.PositiveIntegerField(default=0, blank=True)),
                ('extra', models.CharField(max_length=10, default='', blank=True, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='silverstripesite',
            name='revisions',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='silverstripesite',
            name='slogan',
            field=models.CharField(max_length=200, blank=True),
        ),
        migrations.AddField(
            model_name='release',
            name='version',
            field=models.ForeignKey(to='silverstripe.Version'),
        ),
        migrations.AddField(
            model_name='silverstripesite',
            name='modules',
            field=models.ManyToManyField(related_name='site_modules', to='silverstripe.Release', related_query_name='module_site'),
        ),
        migrations.AddField(
            model_name='silverstripesite',
            name='php_version',
            field=models.ForeignKey(related_name='php_version_site', to='silverstripe.Version', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='silverstripesite',
            name='silverstripe_version',
            field=models.ForeignKey(related_name='silverstripe_version_site', to='silverstripe.Version', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='silverstripesite',
            name='themes',
            field=models.ManyToManyField(related_name='site_themes', to='silverstripe.Release', related_query_name='theme_site'),
        ),
    ]

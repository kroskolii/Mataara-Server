import re

from celery.utils.log import get_task_logger
from django.utils import timezone

logger = get_task_logger(__name__)

# TODO Inconsistent performance passing advisories, between .5 and 4.5 seconds
# It might make sense to optimise these regexes a bit
REGEXES = {
    "risk": re.compile(r"Severity:.+(Low|Moderate|Important|Critical)", re.MULTILINE | re.DOTALL),
    "advisory_id": re.compile(r"Identifier:.+((?:SS|ss)(?:-[0-9]+)+)", re.MULTILINE | re.DOTALL),
    "versions_affected": re.compile(r"Versions Affected.+>(.+)<\/dd>"),
    "versions_fixed": re.compile(r"Versions Fixed.+>(.+)<\/dd>"),
    # TODO This still contains HTML entities (&amp;)
    # We might have to tell django templates to parse them after sanitizing them
    "description": re.compile(r"\/dl>(.+)$", re.MULTILINE | re.DOTALL)
}


class AdvisoryParser(object):

    def parse(self, advisory, forcedownload=False):
        """
        Pass the summary of an advisory
        """
        # Get data out of the summary
        for key, regex in REGEXES.items():
            match = regex.search(advisory.raw_summary)
            if match:
                setattr(advisory, key, match.group(1))
            else:
                logger.error("Couldn't find match for key: %s" % key)
                return False

        # Description is HTML text, replace closing p tags with a newline and
        # strip everything else
        advisory.description = advisory.description.replace('</p>', '\n')
        advisory.description = re.sub('<[^<]+?>', '', advisory.description)

        advisory.risk = advisory.risk.lower()
        advisory.date_parsed = timezone.now()
        advisory.failed = False
        advisory.save()

        return True

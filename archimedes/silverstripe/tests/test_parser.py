import os

from django.test import TestCase

from ..models import Advisory, AdvisorySource
from ..parser import AdvisoryParser


def get_advisory_summary():
    """Get summaries of advisories"""
    with open(os.path.join(os.path.dirname(__file__), 'sample/advisory_SS-2017-004.txt'), 'r') as summary_file:
        summary = summary_file.read()
    return summary


class AdvisoryParserCoreTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        advisory_source = AdvisorySource(
            title="SilverStripe",
            url="https://www.example.com"
        )
        advisory_source.save()
        cls.advisory = Advisory(
            raw_summary=get_advisory_summary(),
            # Following used to satisfy NOT NULL contraints
            title="Advisory",
            url="https://www.example.com",
            source=advisory_source,
        )
        cls.advisory.save()

    def setUp(self):
        parser = AdvisoryParser()
        self.assertEqual(parser.parse(self.advisory), True)

    def test_id(self):
        self.assertEqual(self.advisory.advisory_id, "SS-2017-004")

    def test_risk(self):
        self.assertEqual(self.advisory.risk, "low")

    def test_versions_affected(self):
        self.assertEqual(self.advisory.versions_affected, "3.4.5 and below, 3.5.0 to 3.5.3")

    def test_versions_fixed(self):
        self.assertEqual(self.advisory.versions_fixed, "3.4.6, 3.5.4, 3.6.0")

    def test_description(self):
        pass
        # TODO This test doesn't work even with identical strings
        #  self.assertEqual(self.advisory.description, "description...")

    def test_affected(self):
        # TODO Once we have reports from SilverStripe sites write this test
        pass

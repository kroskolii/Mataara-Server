from django.test import TestCase
from django.contrib.auth.models import User
from django.utils import timezone

from ..models import (SilverStripeSite, Advisory, AdvisorySource)


class AdvisoryTestCase(TestCase):

    def test_str(self):
        """Given an :Advisory, the string representaion is the 'title' field"""
        adv = Advisory(title='Test Title')
        self.assertEqual(str(adv), 'Test Title')

    def test_save(self):
        """Given an :Advisory, update the status on saving"""
        src = AdvisorySource.objects.create(slug='test')
        usr = User.objects.create(username='testuser')

        adv = Advisory.objects.create(
            ignored=False,
            date_parsed=None,
            reviewer=None,
            date_posted=timezone.now(),
            source=src
        )
        self.assertEqual(adv.status, 'downloaded')

        adv.date_parsed = timezone.now()
        self.assertEqual(adv.status, 'downloaded')

        adv.save()  # Status is updated upon save()
        self.assertEqual(adv.status, 'parsed')

        adv.reviewer = usr
        adv.save()
        self.assertEqual(adv.status, 'reviewed')

        adv.ignored = True
        adv.save()
        self.assertEqual(adv.status, 'ignored')

    def test_review_multiple_advisories_with_a_single_reviewer(self):
        """Given multiple :Advisories, allow reviewer field to be a single :User"""
        user = User.objects.create(username='testuser')
        src = AdvisorySource.objects.create(slug='test')
        adv1 = Advisory.objects.create(
            ignored=False,
            date_parsed=None,
            reviewer=None,
            date_posted=timezone.now(),
            guid='test-adv1',
            source_id=src.pk
        )

        adv2 = Advisory.objects.create(
            ignored=False,
            date_parsed=None,
            reviewer=None,
            date_posted=timezone.now(),
            guid='test-adv2',
            source_id=src.pk
        )

        adv1.reviewer = user
        adv1.save()
        testUser_reviewed = Advisory.objects.filter(reviewer=user)
        self.assertCountEqual(testUser_reviewed.values_list('pk', flat=True), [adv1.pk])

        adv2.reviewer = user
        adv2.save()
        testUser_reviewed = Advisory.objects.filter(reviewer=user)
        self.assertCountEqual(testUser_reviewed.values_list('pk', flat=True), [adv1.pk, adv2.pk])

    def test_affected_sites(self):
        pass


class AdvisorySourceTestCase(TestCase):

    def test_str(self):
        """Given an :AdvisorySource, the string representaion is the 'title' field"""
        src = AdvisorySource(title='Test Title')
        self.assertEqual(str(src), 'Test Title')


class SilverStripeSiteTestCase(TestCase):

    def test_build_silverstripe_site(self):
        """
        Build a :SilverStripeSite specifying number of users, then save and modify.
        """
        site = SilverStripeSite(users=5)
        self.assertEqual(site.id, None, 'Unsaved SilverStripe site has no ID.')
        self.assertEqual(site.users, 5, 'Users field from constructor works.')
        site.save()
        saved_site_id = site.id
        self.assertNotEqual(site.id, None, 'Saved SilverStripe site has ID.')

        fetch_site = SilverStripeSite.objects.get(pk=saved_site_id)
        self.assertEqual(fetch_site.users, 5, 'Saved users field retrieved.')

        site.users = 10
        site.save()

        fetch_site = SilverStripeSite.objects.get(pk=saved_site_id)
        self.assertEqual(fetch_site.users, 10, 'Updated users field retrieved.')

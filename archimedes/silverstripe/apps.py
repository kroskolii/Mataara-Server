from django.apps import AppConfig


class SilverStripeConfig(AppConfig):
    name = "archimedes.silverstripe"

from django.views.generic import DetailView, TemplateView

from .models import Advisory, AdvisorySource, Project

# The views that were here will need to be replaced by
# API end points for React.  They are just left here as an example
# of the information that could be needed.


class AdvisoryListView(TemplateView):
    template_name = "silverstripe/advisory_list.html"

    def get_context_data(self, **kwargs):
        context = super(AdvisoryListView, self).get_context_data(**kwargs)
        context['advisory_sources'] = AdvisorySource.objects.all()
        return context


class AdvisoryDetailView(DetailView):
    template_name = "silverstripe/advisory_detail.html"
    model = Advisory
    context_object_name = 'advisory'

    def get_context_data(self, **kwargs):
        context = super(AdvisoryDetailView, self).get_context_data(**kwargs)

        context['affected'] = self.object.affected_sites.all()
        return context


class ModuleListView(TemplateView):
    template_name = "silverstripe/module_list.html"


class ModuleDetailView(DetailView):
    template_name = "silverstripe/module_detail.html"
    model = Project
    context_object_name = 'project'

    def get_queryset(self):
        return super(ModuleDetailView, self).get_queryset().prefetch_related('releases', 'releases__advisories')

    def get_context_data(self, **kwargs):
        context = super(ModuleDetailView, self).get_context_data(**kwargs)
        context['sites_vulnerable'] = self.object.sites_vulnerable.all()
        context['sites_using'] = self.object.sites_using.all()
        return context

from django.core.management.base import BaseCommand

from ...models import Advisory
from ...tasks import parse_advisory


class Command(BaseCommand):
    help = 'Parse security advisories'

    def handler(self, *args, **options):
        # Find unparsed advisories
        advisories = Advisory.objects.filter(status='downloaded')

        # Queue the advisory for parsing
        for advisory in advisories:
            parse_advisory.delay(advisory.pk)

        if len(advisories):
            self.stdout.write('Submitted %d advisory(ies) for processing' % len(advisories))
        else:
            self.stdout.write('No advisories are awaiting processing at the moment')

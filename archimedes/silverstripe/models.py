import re
from functools import total_ordering
from unidecode import unidecode

# Built in but undocumented
# from https://stackoverflow.com/questions/11887762/compare-version-strings-in-python
from distutils.version import LooseVersion

from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify

from archimedes.sites.models import Site, SiteManager

# Number (. or -) number (. or -) number (. or -) lettersAnd342
VERSION_REGEX = re.compile(r"^v?(\d+)(?:[\.-](\d+))?(?:[\.-](\d+))?(?:[\.-]([\d\w.-]+))?$")


class VersionManager(models.Manager):
    """Used to create a version object from a version string"""

    def parse_version(self, version):
        """Parse a version string into the a Version object"""

        match = VERSION_REGEX.match(version)
        if not match:
            raise ValueError("Not a version string")

        return self.create(
            major=match.group(1),
            minor=match.group(2) if match.lastindex > 1 else 0,
            patch=match.group(3) if match.lastindex > 2 else 0,
            extra=match.group(4) if match.lastindex > 3 else ''
        )


# Total ordering means we only need to implement two comparison functions
# to use them all
@total_ordering
class Version(models.Model):
    """A normalized version object"""
    objects = VersionManager()

    major = models.PositiveIntegerField()
    minor = models.PositiveIntegerField(blank=True, default=0)
    patch = models.PositiveIntegerField(blank=True, default=0)
    extra = models.CharField(max_length=20, blank=True, null=True, default='')

    def __str__(self):
        return "%d.%d.%d-%s" % (self.major, self.minor, self.patch, self.extra)

    def __gt__(self, version2):
        # Would love to know if there's a version of this which doesn't fall
        # down stairs
        if self.major != version2.major:
            if self.minor != version2.minor:
                if self.patch != version2.patch:
                    if self.extra != version2.extra:
                        return self.extra > version2.extra

                    return False
                return self.extra > version2.extra
            return self.minor > version2.minor
        return self.major > version2.major

    def __eq__(self, version2):
        try:
            return self.major == version2.major and \
                self.minor == version2.minor and \
                self.patch == version2.patch and \
                self.extra.lower().strip() == version2.lower().strip()
        except AttributeError:
            return isinstance(self, None) and isinstance(version2, None)


class Project(models.Model):
    """A module or theme in a site"""
    # In future we could include information from
    # http://addons.silverstripe.org/add-ons
    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=200, unique=True)
    url = models.URLField(blank=True)
    description = models.TextField(blank=True)
    date_updated = models.DateTimeField(auto_now=True)
    custom = models.BooleanField(default=False)
    type = models.CharField(max_length=50)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.slug

    @property
    def type_name(self):
        if self.type == 'project_module':
            return 'Module'
        elif self.type == 'project_theme':
            return 'Theme'
        return 'Project'

    @property
    def sites_using(self):
        return SilverStripeSite.objects.filter(modules__project__pk=self.pk)

    @property
    def sites_vulnerable(self):
        """All sites that have a release of this project that contain an advisory"""
        return SilverStripeSite.objects.filter(modules__project__pk=self.pk, modules__advisories__isnull=False)


class ReleaseManager(models.Manager):
    use_for_related_fields = True

    def get_queryset(self):
        return super(ReleaseManager, self).get_queryset().select_related('project')


class Release(models.Model):
    """A release of a project, such as a theme or model"""
    objects = ReleaseManager()

    project = models.ForeignKey('Project', related_name='releases', related_query_name='release')
    url = models.URLField()
    date = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    version = models.ForeignKey('Version')

    class Meta:
        ordering = ['-version', '-date']

    @property
    def slug(self):
        return self.project.slug + ':' + self.version

    def __str__(self):
        return self.slug


class SilverStripeSite(Site):
    label = 'SilverStripe'
    objects = SiteManager()

    php_version = models.ForeignKey('Version', blank=True, null=True, related_name="php_version_site")
    silverstripe_version = models.ForeignKey(
        'Version', blank=True, null=True,
        related_name="silverstripe_version_site"
    )

    slogan = models.CharField(max_length=200, blank=True)
    revisions = models.PositiveIntegerField(blank=True, null=True)
    users = models.PositiveIntegerField(blank=True, null=True)

    modules = models.ManyToManyField(
        Release, related_name='site_modules', related_query_name='module_site', blank=True
    )
    themes = models.ManyToManyField(
        Release, related_name='site_themes',
        related_query_name='theme_site', blank=True
    )

    # A count of users in the system.
    users = models.PositiveIntegerField(blank=True, null=True, help_text='Number of users on site')

    def compare_json_against_latest_report(self, report_json, field_paths=None):
        """
        Check whether report has unexpected values based on last report from this site.
        Return a dict of changed fields and their old values. If no field_paths argument
        is specified, a default_fields set may be used.
        """
        default_field_paths = ['ReportType', ['Environment', 'Name'], ['Environment', 'Label'], 'ServerHostname']
        if field_paths is None:
            field_paths = default_field_paths

        latest = self.reports.latest('date_received')
        return latest.compare_json(report_json, field_paths=field_paths)

    @property
    def core_vulnerabilities(self):
        """Returns any vulnerabilities for this core (Framework/CMS) version"""
        # TODO return Advisory.objects.filter(affected_sites__in=self)
        return 0

    @property
    def vulnerability_count(self):
        """Return total count of vulnerable components."""
        count = (1 if self.core_vulnerabilities > 0 else 0)
        count += len(self.vulnerable_modules)
        count += len(self.vulnerable_themes)
        return count

    @property
    def vulnerable_modules(self):
        """All releases in self.modules that have an advisory"""
        return Release.objects.filter(module_site=self, advisories__pk__isnull=False).distinct()

    @property
    def vulnerable_themes(self):
        """All releases in self.themes that have an advisory"""
        return Release.objects.filter(theme_site=self, advisories__pk__isnull=False).distinct()


class Advisory(models.Model):
    STATUS = (
        ('downloaded', 'Downloaded'),
        ('parsed', 'Needs Review'),
        ('reviewed', 'Reviewed'),
        ('ignored', 'Ignored'),
        ('failed', 'Parsing Failed'),
    )

    guid = models.CharField(max_length=200, unique=True)
    title = models.CharField(max_length=200)
    url = models.URLField()
    source = models.ForeignKey('AdvisorySource', default=None)

    description = models.TextField()
    # Unparsed summary element
    raw_summary = models.TextField()

    date_posted = models.DateTimeField(null=True, blank=True)
    date_parsed = models.DateTimeField(null=True, blank=True)

    ignored = models.BooleanField(default=False)
    failed = models.BooleanField(default=False)
    reviewer = models.ForeignKey(User, null=True, blank=True, related_name='ss_advisory')
    status = models.CharField(max_length=50, choices=STATUS, default='downloaded')

    advisory_id = models.CharField(max_length=200, blank=True)

    # TODO This is limited to SilverStripe's security feed
    risk = models.CharField(max_length=50, choices=(
        ('low', 'Low'),
        ('moderate', 'Moderate'),
        ('important', 'Important'),
        ('critical', 'Critical')
    ))

    versions_affected = models.CharField(max_length=50, null=True, blank=True)
    versions_fixed = models.CharField(max_length=50, null=True, blank=True)

    affected_sites = models.ManyToManyField(SilverStripeSite)

    class Meta:
        ordering = ['-date_posted']
        verbose_name_plural = 'Advisories'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        """Set the status before saving"""
        if self.ignored:
            self.status = 'ignored'
        elif self.failed:
            self.status = 'failed'
        elif not self.date_parsed:
            self.status = 'downloaded'
        elif self.reviewer:
            self.status = 'reviewed'
        else:
            self.status = 'parsed'
        super(Advisory, self).save(*args, **kwargs)

    def affected(self, version):
        """Check if version is affected by this advisory"""
        # TODO Update this to work with the Version model rather than version strings.
        # Should be a simpler than the current method (I think)
        # Match a single version number
        singlereg = r"([1-9](?:(?:\.d)|(?:-[a-zA-Z0-9]+))*)"

        # Build a list of conditions
        single = []
        between = []
        less = []

        def match_ver_str(version, version_string):
            for part in version_string.split(','):
                # If the part is a range of versions
                m = re.match(r"%s\s+to|-\s+%s" % (singlereg, singlereg))
                if m:
                    between.append((m.group(1), m.group(2)))

                # If the part is less than a version
                m = re.match("%s and below|lower" % singlereg)
                if m:
                    less.append(m.group(1))
                # If the part is a single version number
                m = re.match(singlereg)
                if m:
                    single.append(m.group(1))

            # Do the comparison
            for v in single:
                if LooseVersion(single) == version:
                    return True
            for start, end in between:
                if LooseVersion(start) <= version and LooseVersion(end) >= version:
                    return True
            for v in less:
                if LooseVersion(less) >= version:
                    return True

        # Check id is affected and hasn't been fixed
        # TODO We might want to handle this differently
        if match_ver_str(version, self.affected_versions) and not match_ver_str(version, self.fixed_versions):
            return True

        return [site for site in SilverStripeSite.objects.all() if self.affected(site.core)]


class AdvisorySource(models.Model):
    title = models.CharField(max_length=200, unique=True,
                             help_text='Enter the name for this advisory source.')
    slug = models.SlugField(unique=True, editable=False)
    # URL validation checks enforced through admin web interface
    url = models.URLField(unique=True,
                          help_text='Enter URL for the advisory source. Only http(s):// or ftp(s):// are supported.')

    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.slug = slugify(unidecode(self.title))

        return super(AdvisorySource, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

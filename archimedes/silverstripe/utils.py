import json
import urllib.parse
import urllib.error
import urllib.request
# import re - only used for parsing timezone from advisory publication date

# from dateutil.tz import tzoffset - only used for parsing timezone from advisory publication date
from datetime import datetime
from time import mktime
from celery.utils.log import get_task_logger
from django.core.exceptions import ObjectDoesNotExist

from feedparser import parse

from .models import Advisory, Project, Version, Release

logger = get_task_logger(__name__)


def import_advisories(source, xml=None):
    """Download the RSS feed for the given advisory source"""

    # Note URLs have been filtered through admin interface model and in 'addadvisorysource command'
    if xml is None:
        # The SS endpoint currently returns 403 for the Python-urllib user-agent
        if source.url == "https://www.silverstripe.org/download/security-releases/rss":
            response = urllib.request.urlopen(urllib.request.Request(  # nosec
                source.url,
                headers={'User-Agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36" +
                         "(KHTML, like Gecko) Ubuntu Chromium/59.0.3071.109 Chrome/59.0.3071.109" +
                         " Safari/537.36"}
            ))
        else:
            response = urllib.request.urlopen(source.url)  # nosec
        xml = response.read()

    rss = parse(xml)

    existing = Advisory.objects.all()

    for entry in rss.entries:
        # Check if the advisory has already been imported
        if existing.filter(guid=entry.id):
            continue

        advisory = Advisory()
        advisory.guid = entry.id
        advisory.title = entry.title
        advisory.url = entry.link
        advisory.source = source
        advisory.raw_summary = entry.summary

        # TODO This doesn't support daylight savings
        # TODO Finish sorting out timezone adjustment from GMT
        # tzmatch = re.match(r"\+([0-9]{2})\1", entry.published)
        # tz = int(tzmatch.group(1)) if tzmatch else 12
        advisory.date_posted = datetime.fromtimestamp(
            mktime(entry.published_parsed)
            # + tzoffset('NZST', tz*60)
        )

        advisory.save()


def get_or_download_projects(names):
    for name, version in names.items():
        # TODO Remove unnecessary variables here
        r = urllib.request.urlopen("https://packagist.org/packages/%s.json" % name)  # nosec
        response = r.read().decode("utf-8")

        # For the values in the response see
        # https://getcomposer.org/doc/04-schema.md

        package_info = json.loads(response)
        package = package_info['package']

        # Check if we already have the project
        try:
            project = Project.objects.get(name=name)
        except ObjectDoesNotExist:
            # If not create a new one
            project = Project(
                name=package['name'],
                slug=name,
                description=package['description']
            )

        # This information could have changed since we last updated the project

        # Project type shouldn't change between releases
        project.type = package['versions']['dev-master']['type']
        try:
            project.url = package['versions']['dev-master']['homepage']
        except IndexError:
            project.url = ''
        # TODO This should maybe be the latest tagged release
        try:
            project.date_updated = package['versions']['dev-master']['time']
        except IndexError:
            pass

        project.save()

        releases = []
        # If the reported version is in compoesr's registry
        if version in package['versions'].keys():
            version_obj = Version.objects.parse_version(version)

            # Check if we already have the release object
            try:
                release = Release.objects.get(
                    project=project,
                    version__major=version_obj.major,
                    version__minor=version_obj.minor,
                    version__patch=version_obj.patch,
                    version__extra=version_obj.extra
                )
            except ObjectDoesNotExist:
                pass

            else:
                releases.append(release)
                continue

            release = Release(
                project=project,
                version=version_obj,
                date=package['versions'][version]['time'],
                url=package['versions'][version]['homepage']
            )

        # Version isn't in the registry but the project is using composer...
        else:
            # Custom release
            release = Release(project=project, version=version_obj, custom=True)

        for release in releases:
            release.save()

        return releases


def process_report(report, site):
    """Process a report from a site"""
    site.slogan = report.dict.get('SiteSlogan', '')
    site.users = report.dict.get('Users', None)

    # Try to parse the version strings
    try:
        site.phpversion = Version.objects.parse_version(
            report.dict.get('PHPVersion', ''))
    except ValueError:
        logger.warning("Failed to parse version string: " +
                       report.dict.get('PHPVersion', ''))
    try:
        site.version = Version.objects.parse_version(
            report.dict.get('SilverStripeVersion', ''))
    except ValueError:
        logger.warning("Failed to parse version string: " +
                       report.dict.get('SilverStripeVersion', ''))

    # Update the modules
    module_list = report.dict.get('Modules', {})
    module_objects = []
    # Create unmanaged projects
    for module, version in module_list.items():
        if version == "unmanaged":
            obj = Project(name=module, type="module")
            obj.save()
            module_objects.append(obj)

    # Get the rest of them
    obj = get_or_download_projects(
        {module: version for module, version in module_list.items()
         if version != "unmanaged"}
    )
    if obj:
        module_objects.append(obj)

    site.modules.clear()
    site.modules = module_objects

    # Update the themes
    theme_list = report.dict.get('Themes', {})
    theme_objects = []
    # Create unmanaged projects
    for theme, version in theme_list.items():
        if version == "unmanaged":
            obj = Project(name=theme, type="theme")
            obj.save()
            theme_objects.append(obj)

    # Get the rest of them
    obj = get_or_download_projects(
        {theme: version for theme, version in theme_list.items()
         if version != "unmanaged"}
    )
    if obj:
        theme_objects.append(obj)
    site.themes.clear()
    site.themes = theme_objects

# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import include, patterns, url
from django.contrib import admin
from django.contrib.auth.views import login, logout
from django.views.generic import RedirectView

urlpatterns = patterns(
    '',
    url(r'^favicon\.ico$', RedirectView.as_view(url=settings.STATIC_URL + 'favicon.ico', permanent=True)),
    url(r'admin/', include(admin.site.urls)),
    url('', include('archimedes.core.urls')),
)

if 'djangosaml2' in settings.INSTALLED_APPS:
    urlpatterns.append(url(r'^saml2/', include('djangosaml2.urls')))
    urlpatterns.append(url(r'logout/$', logout, {'next_page': '/logout.html'}, name='logout'))
    urlpatterns.append(url(r'^logout\.html$',
                       RedirectView.as_view(url=settings.STATIC_URL + 'logout.html', permanent=True)))
else:
    urlpatterns.append(url(r'login/$', login, name='login'))
    urlpatterns.append(url(r'logout/$', logout, {'next_page': '/'}, name='logout'))

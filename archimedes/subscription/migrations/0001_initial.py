# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0012_site_decommissioned'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('site', models.ForeignKey(blank=True, to='sites.Site', related_name='site', null=True)),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, related_name='user', null=True)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='subscription',
            unique_together=set([('site', 'user')]),
        ),
    ]

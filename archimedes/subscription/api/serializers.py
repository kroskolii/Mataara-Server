from rest_framework import serializers

from .. import models


class SubscriptionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Subscription
        fields = (
            'site',
            'user',
        )

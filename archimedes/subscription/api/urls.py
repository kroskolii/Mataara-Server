from archimedes.api.urls import router

from ..api import views

# Register viewsets with the router created in api.urls
router.register(r'subscription', views.SubscriptionViewSet)

# No URL patterns needed - they're done by api.urls.router
urlpatterns = []

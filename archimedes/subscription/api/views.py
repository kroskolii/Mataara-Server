from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from . import serializers
from .. import models
from archimedes.sites.models import Site
from archimedes.sites.api.serializers import SiteSerializer
from ..models import Subscription

# Create your views here.


class SubscriptionViewSet(viewsets.ModelViewSet):
    """
    API endpoint for User subscriptions to Sites.
    """
    queryset = models.Subscription.objects.all()
    serializer_class = serializers.SubscriptionSerializer

    def list(self, request):
        if not request.user.is_authenticated:
            # This should never get hit, but just in case...
            return Response('API only available to authenticated users.', status=status.HTTP_401_UNAUTHORIZED)

        # Return sites the logged in user has subscribed to.
        user = request.user

        response_code = status.HTTP_200_OK

        # What are we doing?
        if 'toggle' in request.query_params:
            site = Site.objects.filter(pk=request.query_params['toggle']).get()
            if Subscription.toggle_subscription(user, site):
                # We toggled. What is our current state?
                ret = {
                    'subscribed': Subscription.is_subscribed(user, site),
                }
            else:
                ret = 'error'
                response_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        elif 'status' in request.query_params:
            site = Site.objects.filter(pk=request.query_params['status']).get()
            ret = {
                'subscribed': Subscription.is_subscribed(user, site),
            }
        elif 'mine' in request.query_params:
            site_ids = Subscription.objects.values_list('site', flat=True).filter(user=user.pk)
            sites = Site.objects.filter(pk__in=site_ids).all()
            serialized = SiteSerializer(sites, context={'request': request}, many=True)
            ret = serialized.data
        else:
            ret = 'Action not set'
            response_code = status.HTTP_400_BAD_REQUEST

        # return that list of sites.
        return Response(ret, status=response_code)

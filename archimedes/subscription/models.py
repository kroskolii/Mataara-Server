import sys

from django.contrib.auth.models import User
from django.db import models


# from archimedes.sites.models import Site

# Create your models here.
class Subscription(models.Model):
    site = models.ForeignKey('sites.Site', null=True, blank=True,
                             on_delete=models.CASCADE, related_name='site')
    user = models.ForeignKey(User, null=True, blank=True,
                             on_delete=models.CASCADE, related_name='user')

    class Meta:
        unique_together = (("site", "user"),)

    def __str__(self):
        return self.site.title

    def get_sites(self, user_key):
        """Return a list of sites for a user."""
        try:
            sites = type(self).objects.filter(user=user_key)
        except sites.DoesNotExist:
            sites = None
        if sites is None:
            return None
        else:
            return sites

    @staticmethod
    def subscribe_to_site(user, site):
        """Save a users subscription to a site."""
        try:
            if not Subscription.is_subscribed(user, site):
                s = Subscription()
                s.user = user
                s.site = site
                s.save()
                return True
            else:
                """We are already subscribed."""
                return True
        except Exception:
            print("Unexpected error:", sys.exc_info()[0])
            raise
            return False

    @staticmethod
    def is_subscribed(user, site):
        """Return True if user is subscribed to the site."""
        return Subscription.objects.filter(user=user, site=site).exists()

    @staticmethod
    def unsubscribe_from_site(user, site):
        """Unsubscribe a site from a user."""
        Subscription.objects.filter(user=user, site=site).delete()
        return not Subscription.is_subscribed(user, site)

    @staticmethod
    def toggle_subscription(user, site):
        if Subscription.is_subscribed(user, site):
            return Subscription.unsubscribe_from_site(user, site)
        else:
            return Subscription.subscribe_to_site(user, site)

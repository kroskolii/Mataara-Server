# from fabric.contrib.files import append, exists, sed
import os
from datetime import datetime
from sys import exit

from fabric.api import env, local, task, warn_only
from fabric.colors import blue, magenta, red

# Define useful constants
app = "archimedes"
root_dir = "/vagrant"
wheelhouse = os.path.join(root_dir, "wheels")
venv_bin = os.path.join(root_dir, "venv/bin")
frontend = os.path.join(root_dir, "archimedes/frontend")

local_root = os.path.dirname(os.path.abspath(__file__))
local_venv_bin = local_root + "/venv/bin"
vm_up = False
vm_uptodate = False


# Check if local commands exist and are on the search path
# Taken from http://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
def cmd_exists(cmd):
    return any(
        os.access(os.path.join(path, cmd), os.X_OK)
        for path in os.environ["PATH"].split(os.pathsep)
    )
# Would be good to switch to the following Python 3.3+ version in Fabric 2:
# import shutil
# def cmd_exists(cmd):
#    return shutil.which(cmd) is not None


# short-cut function for when we are SSHing into a vagrant VM to run something
def vagrant(action, error_exit=True):
    if error_exit:
        local("vagrant ssh -c '{cmd}'".format(cmd=action))
    else:
        with warn_only():
            local("vagrant ssh -c '{cmd}'".format(cmd=action))


def vagrant_capture(action):
    return local("vagrant ssh -c '{cmd}'".format(cmd=action), capture=True)


def django_selfcheck():
    """Check that the application itself runs successfully without errors"""
    print(blue("Run sanity self-check"))
    vagrant("{bin}/{app} check".format(bin=venv_bin, app=app))


def make_migrations_check():
    """Check whether there have been model changes that need a migration to be created"""
    print(blue("Checking for migrations that need to be created"))
    migrations = vagrant_capture("{bin}/{app} makemigrations --dry-run".format(bin=venv_bin, app=app))
    if ('No changes detected' not in migrations):
        print(red("Database/model changes have been detected that do not have a corresponding migration"))
        print(red("Please run '{app} makemigrations' in vagrant and test".format(app=app)))
        exit(1)


def update_dependencies():
    """Install/update all python dependencies required for development"""
    print(blue("Updating Python dependencies"))
    vagrant("{bin}/pip install -r {root}/requirements/dev.txt -q".format(bin=venv_bin, root=root_dir))


def reinit_dev_environment():
    """Reinitialise development environment including updating pip dependencies"""
    update_dependencies()
    print(blue("Updating Python build environment"))
    vagrant("cd {root} && {bin}/python3 {root}/setup.py develop".format(bin=venv_bin, root=root_dir))
    print(blue("Updating ReactJS build environment"))
    vagrant("cd {frontend_dir} && /usr/bin/npm rebuild".format(bin=venv_bin, frontend_dir=frontend))


def ensure_vm_up():
    """Simple check to confirm that vagrant has launched the virtual machine before we do anything else"""
    global vm_up
    if vm_up is False:
        env.warn_only = True
        vagrant_status = local("vagrant status", capture=True)
        env.warn_only = False
        if 'No Puppet settings file found' in vagrant_status.stderr:
            config_filename = 'conf/hiera/config.yaml'
            print('Creating puppet settings file for vagrant at ' + config_filename)
            print('You may want to customise some of the values.')
            local('cp {filename}.example {filename}'.format(filename=config_filename))
            vagrant_status = local("vagrant status", capture=True)
        if 'running' not in vagrant_status:
            local('vagrant up')
        vm_up = True


def ensure_vm_up_to_date():
    """Ensure that the build environment is ready to run the app in development mode"""
    global vm_uptodate
    if vm_uptodate is False:
        ensure_vm_up()
        reinit_dev_environment()
        django_selfcheck()
        make_migrations_check()
        migrate()
        vm_uptodate = True


@task
def migrate():
    """Runs the Django migrate"""
    print(blue("Synchronising database with migrations"))
    ensure_vm_up()
    vagrant("{bin}/{app} migrate".format(bin=venv_bin, app=app))


@task
def run_checks():
    """Run all checks; should be used before building wheels and new tags are created"""
    # Ensure VM is running and up-to-date, i.e. we've checked for unapplied migrations
    print(blue("Running all checks"))
    ensure_vm_up_to_date()

    # run the actual checks
    jslint()
    flake8()
    bandit()
    test()


@task
def build_wheels():
    """Build wheels for the application and its dependencies"""
    # Run checks and tests before we compile up the wheels
    run_checks()

    # Create JavaScript bundle with the front-end code
    # vagrant("cd {root}/archimedes/front-end && npm run build".format(root=root_dir))

    # reset the wheelhouse, if it exists
    vagrant("if [ -d {house} ] ; then rm -f {house}/*; else mkdir -p {house}; fi".format(house=wheelhouse))
    # install build dependencies
    vagrant("{bin}/pip3 install -r {root}/requirements/build.txt".format(bin=venv_bin, root=root_dir))
    # build wheels (and JavaScript bundle)
    vagrant("{bin}/pip3 wheel --wheel-dir={house} .".
            format(bin=venv_bin, house=wheelhouse, root=root_dir))


@task
def prepare_build():
    """run build pre-checks and create tag"""
    # check if we're on the master branch
    existing_branch = local('git name-rev --name-only HEAD', capture=True)
    if (existing_branch == 'master'):
        # Run checks
        run_checks()

        # Obtain new tag, including updating release number in RELEASE file
        # Then upload new tag to git
        new_tag()
    else:
        print(red('Error, you are in branch "{existing}" instead of "master". Aborting'.
                  format(existing=existing_branch)))


def calculate_new_tag_suffix():
    """Calculate tag suffix - date + relase number"""
    print(blue("Determining what the next tag number will be"))
    # Update local git repo so that we know about all existing tags
    local('git fetch --tags')
    # Create new tag using format %Y%m%d-%N
    today = datetime.now().strftime('%Y%m%d')
    tag_num = local('echo $(expr $(git tag | grep {} | wc -l) + 1)'.format(today), capture=True)
    return '{day}.{num}'.format(day=today, num=tag_num)


@task
def new_tag():
    """Create new tag for release and upload to git server"""
    # Check if we permissions to push
    local('git push --dry-run')

    tag_suffix = calculate_new_tag_suffix()

    # Read application version from RELEASE file
    version = local("grep -o \"^[0-9]\+.[0-9]\+\" {root}/{app}/RELEASE".  # noqa: W605
                    format(root=local_root, app=app), capture=True)
    # Calculate full tag number
    full_new_tag = version + '.' + tag_suffix

    # Get existing version number and update to include tag number
    old_tag = local("cat {root}/{app}/RELEASE".format(root=local_root, app=app), capture=True)
    if old_tag != full_new_tag:
        local("echo {newrelease} > {root}/{app}/RELEASE".
              format(newrelease=full_new_tag, root=local_root, app=app))
        local('git add -- {app}/RELEASE'.format(app=app))

    file_changed = local('cd {root} && git status -s {app}/RELEASE'.format(root=local_root, app=app), capture=True)
    if file_changed[0] == 'M':
        local('git commit -m "Update release number for tag {}"'.format(full_new_tag))
    local('git push')

    # Refresh tag to point to post-commit with new release file
    local('git tag -a {newtag} -m "tag for new deployment"'.format(newtag=full_new_tag))
    local('git push origin {newtag}'.format(newtag=full_new_tag))


@task
def flake8():
    """Run flake8 linter and code checker"""
    print(blue("Running flake8 code checker"))
    if cmd_exists("flake8"):
        local("cd {root} && flake8".format(root=local_root))
    else:
        print(red("Missing command 'flake8'. Try running 'fab 'setup_dev_env'"))


@task
def jslint():
    """Run JS lint"""
    print(blue("Running js lint"))
    ensure_vm_up()
    vagrant("cd {frontend_dir} && /usr/bin/npm run lint".
            format(bin=venv_bin, frontend_dir=frontend))


@task
def bandit():
    """Run bandit security checker across source code"""
    print(blue("Running bandit security checker"))
    if cmd_exists("bandit"):
        local("bandit -r {root}/{app} -x {root}/*/migrations,{root}/{app}/frontend".
              format(root=local_root, app=app))
    else:
        print(red("Missing command 'bandit'. Try running 'fab 'setup_dev_env'"))


@task
def test(speed='slow', server_tests=True, frontend_tests=True):
    """
    Run built-in tests (use test:fast to bypass system checks)

    Defaults to running the server and frontend tests.
    """
    print(blue("\nRunning tests"))
    if speed.lower() != 'fast':
        ensure_vm_up_to_date()
    else:
        ensure_vm_up()
    if server_tests:
        print(blue("\nRunning server tests"))
        vagrant("cd {root} && {bin}/{app} test".format(bin=venv_bin, root=root_dir, app=app))
    if frontend_tests:
        print(blue("\nRunning npm to test React code"))
        vagrant("cd {frontend_dir} && /usr/bin/npm run test".
                format(bin=venv_bin, frontend_dir=frontend))


@task
def run(speed='slow'):
    """Run the web application. Access using http://[vagrant_ip]:8000/ (use run:fast to bypass system checks)"""
    print(magenta('This task is being  deprecated as the Vagrant setup runs the server. It is on port 8008.'))
    if speed.lower() != 'fast':
        ensure_vm_up_to_date()
    vagrant("{bin}/{app} runserver 0.0.0.0:8000".format(bin=venv_bin, app=app))


@task
def runjs():
    """
    Run npm to serve the react frontend

    It is run this way so that developers can make changes to the code
    This neeeds to be running for Mataara to work
    """
    print(blue("Running npm to serve React code"))
    ensure_vm_up()
    vagrant("cd {frontend_dir} && /usr/bin/npm start".
            format(bin=venv_bin, frontend_dir=frontend))


@task
def reload_server(speed='slow'):
    """Runs migrations and restarts supervisor"""
    print(blue("Reloading dev server"))
    if speed.lower() != 'fast':
        ensure_vm_up_to_date()
    else:
        ensure_vm_up()
    migrate()
    vagrant('/usr/bin/supervisorctl restart all')
    print(magenta("Logs are in /var/log/supervisor"))


@task
def show_logs():
    """Tails the Mataara Server Django logs"""
    print(blue("Tailing the Django supervisor logs /var/log/supervisor/*.log"))
    ensure_vm_up()
    vagrant('sudo tail -f /var/log/supervisor/*.log')


def add_advisory_source(module, title, url):
    print(blue("Adding advisory source"))
    vagrant('{bin}/{app} addadvisorysource --module "{module}" --title "{title}" --url "{url}"'.
            format(bin=venv_bin, app=app, module=module, title=title, url=url))


@task
def setup_feeds():
    """Subscribe to advisory feeds"""
    ensure_vm_up_to_date()
    print(blue("Adding advisory sources"))
    # DEPRECATED: Old Drupal Core/Contrib feeds will be retired in future releases.
    add_advisory_source("drupal", "Drupal Core", "https://www.drupal.org/security/rss.xml")
    add_advisory_source("drupal", "Drupal Contrib", "https://www.drupal.org/security/contrib/rss.xml")

    add_advisory_source("silverstripe", "SilverStripe", "https://www.silverstripe.org/download/security-releases/rss")
    add_advisory_source("drupal", "Drupal SA API", "https://www.drupal.org/api-d7/node.json?type=sa&status=1")
    add_advisory_source("drupal", "Drupal Core API", "https://www.drupal.org/api-d7/node.json?taxonomy_forums=1852")
    add_advisory_source("drupal", "Drupal Contrib API", "https://www.drupal.org/api-d7/node.json?taxonomy_forums=44")
    print("Initiating import of advisory source data")
    vagrant('{bin}/{app} importadvisories'.format(bin=venv_bin, app=app))
    print("Initiating parsing of advisories. Warning, it may take time if there are lots.")
    vagrant('{bin}/{app} parseadvisories'.format(bin=venv_bin, app=app))


@task
def pip_update():
    """Update package requirements and pin new versions"""
    print(blue("Update pip"))
    ensure_vm_up()
    vagrant("{bin}/pip-compile --upgrade -o {root}/requirements/common.txt {root}/requirements/requirements.in".
            format(bin=venv_bin, root=root_dir))
    reinit_dev_environment()


def check_host_package(package):
    """Install a Python package on the host operating system"""
    if not cmd_exists(package):
        if cmd_exists('pip3'):
            if cmd_exists('easy_install3'):
                local("sudo -H pip3 install --upgrade {package}".format(package=package))
            else:
                print("You appear to be missing Python setuptools packages. " +
                      "Please try installing the package 'python3-setuptools' or similar for your distribution")
        else:
            print("Missing command 'pip3'; unable to install '{package}' using Python. ".format(package=package) +
                  "Trying installing the package 'python3-pip' for Python support or installing the package directly.")


@task
def setup_dev_env():
    """Setup local development environment, including installation of flake8 and bandit"""

    # Setup pre-commit link
    if not os.path.exists(".git/hooks/pre-commit"):
        local("ln -s ../../bin/pre-commit.py .git/hooks/pre-commit")

    # Check we have flake8 and bandit installed
    check_host_package('flake8')
    check_host_package('bandit')


@task
def make_docs():
    """Run Sphinx to compile up the documentation"""
    print(blue("Compiling the docs"))
    ensure_vm_up()
    update_dependencies()
    vagrant("{venv_bin}/sphinx-apidoc -f -o {root}/docs/source/server/api {root}/{app} migrations".
            format(root=root_dir, venv_bin=venv_bin, app=app))
    vagrant("/usr/bin/make --directory {src} SPHINXBUILD={venv_bin}/sphinx-build html".
            format(src=root_dir + '/docs', venv_bin=venv_bin))

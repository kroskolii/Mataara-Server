.. _client_drush:

Drush Commands
==================================

The client module implements a number of Drush commands that can be used to send
or display reports at leisure for debugging purposes.

.. _client_drush_arch_report:

Generate a report (``arch-report``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generate and display a Mataara client report.

+--------------+----------------------------------------------------------------------------------+
| **Options**  | ``format``: The format in which the report is displayed.                         |
|              |                                                                                  |
|              | * ``rendered``: Rendered as a table (default)                                    |
|              | * ``json``: JSON                                                                 |
|              | * ``json-pretty``: JSON (pretty-printed)                                         |
|              | * ``encrypted``: Encrypted JSON                                                  |
+--------------+----------------------------------------------------------------------------------+
| **Aliases**  | ``ar``                                                                           |
+--------------+----------------------------------------------------------------------------------+
| **Examples** | * ``drush arch-report``: Display a report                                        |
|              | * ``drush arch-report json``: Display a report in JSON format                    |
|              | * ``drush arch-report encrypted > report.enc``: Save an encrypted report to file |
+--------------+----------------------------------------------------------------------------------+

.. _client_drush_arch_send:

Send a report (``arch-send``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Send a Mataara Client report to the default, or a desired server.

+--------------+-----------------------------------------------------------------------------------------+
| **Options**  | ``method``: How the report is sent. Defaults is the site's configured method.           |
|              |                                                                                         |
|              | * ``email``: via email attachment                                                       |
|              | * ``http``: via HTTP(s) POST                                                            |
|              |                                                                                         |
|              | ``location``: Email address or URL to send to (depending on the chosen method)          |
|              |                                                                                         |
+--------------+-----------------------------------------------------------------------------------------+
| **Aliases**  | ``as``                                                                                  |
+--------------+-----------------------------------------------------------------------------------------+
| **Examples** | * ``drush arch-send``: Send a report using the configured method & location             |
|              | * ``drush arch-send http``: Send a report using HTTP (using the configured URL)         |
|              | * ``drush arch-send http http://server.com/endpoint``: Send an HTTP report to this URL  |
+--------------+-----------------------------------------------------------------------------------------+

The ``arch-send`` command may fail with the message "Cannot send report: could
not find a valid hostname for this site." This means that drush cannot determine
the base URL to use, so Mataara cannot identify the site that it is sending
from.

There are multiple ways to address this, including:

* Fixing the underlying hostname issue, if you have the system permissions to do so
* Using an alias in a ``drushrc.php`` file
* Specifying the base URL in the command line, e.g. ``drush -l https://example.com arch-send``
.. _client_configuration:

Configuration
==================================

The Mataara client has some configuration options that allow you to change how
often and it reports to the server, and what transport method it uses.

Changing client settings
-----------------------------------

There are a number of ways to configure Mataara - using Drush or the web interface
as described, or more advanced users may use Strongarm to preset the configuration as
part of a site profile.

Using Drush
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When changing settings using Drush, you must know the exact value that the
Drupal variables need to be set to otherwise the client may not operate correctly.

::

  cd {drupal web root directory}
  drush vset archimedes_client_SETTING_NAME SETTING_VALUE

Using the web interface
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Navigate to http://your-client-site.com/admin/config/archimedes_client/settings
* Change the settings to the desired values
* Click `Save configuration` at the bottom of the page


Using ``settings.php``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When creating a base install for Drupal, it may be desirable to preset the configuration
of the client - such as the server address and public key. This can be achieved by
using `Strongarm <https://www.drupal.org/project/strongarm>`_ in a profile or
`Features <https://www.drupal.org/project/features>`_ module, or by setting
variables in the site's ``settings.php`` file as follows::

  <?php

  // Archimedes server public key
  $conf['archimedes_client_pubkey'] =
  "-----BEGIN PUBLIC KEY-----
  1234567890ABCDEFG1234567890ABCDEFG
  ABCDEFG1234567890ABCDEFGH123456790
  1234567890ABCDEFG1234567890ABCDEFG
  ABCDEFG1234567890ABCDEFGH123456790
  -----END PUBLIC KEY-----";

  // Archimedes server URL
  $conf['archimedes_client_server_url'] = "https://your-server-site.com/archimedes/endpoint";

  // Archimedes server email address
  $conf['archimedes_client_server_email'] = 'archimedes@your-server-site.com';

  // Archimedes report sending method - "email" or "http"
  $conf['archimedes_client_server_method'] = 'http';

  ?>

Client settings reference
-----------------------------------

The following settings are available in the Mataara Client:

Reporting Method
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Mataara can be configured to send reports to the server over different channels:

* ``email``: Reports are emailed as attachments to a mailbox monitored by the server.
* ``http``: Reports are posted directly to the server over HTTP(s) using POST.

All reporting methods are encrypted.

+--------------+-------------------------------------+
| **Variable** | ``archimedes_client_server_method`` |
+--------------+-------------------------------------+
| **Default**  | ``email``                           |
+--------------+-------------------------------------+

Reporting URL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

URL of the Mataara server's HTTP(s) POST endpoint.

+--------------+----------------------------------+
| **Variable** | ``archimedes_client_server_url`` |
+--------------+----------------------------------+
| **Default**  | (not set)                        |
+--------------+----------------------------------+

Reporting Email
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Email address of a mailbox monitored by the Mataara server.

+--------------+------------------------------------+
| **Variable** | ``archimedes_client_server_email`` |
+--------------+------------------------------------+
| **Default**  | (not set)                          |
+--------------+------------------------------------+

Reporting Interval
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Minimum time in seconds between Mataara reports being sent. This time may be
longer depending on the frequency with which Drupal's cron is run.

+--------------+-------------------------------------+
| **Variable** | ``archimedes_client_cron_interval`` |
+--------------+-------------------------------------+
| **Default**  | ``86400``                           |
+--------------+-------------------------------------+

Public Key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Mataara uses a public/private key pair to encrypt data during transmission
(to prevent eavesdropping). Encrypted reports include an expiry time in order to
minimise the effect of a reply attack. This variable holds the public key used
to encrypt the reports sent to the server.

+--------------+------------------------------+
| **Variable** | ``archimedes_client_pubkey`` |
+--------------+------------------------------+
| **Default**  | (not set)                    |
+--------------+------------------------------+

Report Expiry Time
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Number of seconds after sending that a report becomes invalid. The server must
have enough time to receive the report before it expires, however the expiry
should be set to a minimal value to minimise the effect of a replay attack.

+--------------+-------------------------------------+
| **Variable** | ``archimedes_client_report_expiry`` |
+--------------+-------------------------------------+
| **Default**  | ``1800``                            |
+--------------+-------------------------------------+

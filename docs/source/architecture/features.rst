.. _architecture_features:

Features
==================================

The following is a description of the Mataara system and some of its
features. For a better and more complete understanding of the software, it would
be best to download the source code of the client and server modules and
experiment with the system in a test environment.

Client Reporting
----------------------------------

* Each client periodically sends reports to a Mataara server (daily by default)
* Reports contain information about the modules and themes installed on the client website
* Data is encoded in JSON and public-key encrypted during transport
* Reports can be sent to the server using HTTP(s) POST, or email attachment
* Custom report data can be added by extending a base "ReportItem" class in the client module
* Drush commands are available to send reports, or to generate a report in various formats
* An admin page is available for the server, reporting, and encryption settings to be changed
* The client is compatible with Drupal 6, 7, and Drupal 8

Server Structure
----------------------------------

* Reports are recieved at either an HTTP or Email/Mailbox endpoint and placed in a queue
* Periodically, the server saves queued reports and updates "website" nodes with any new information
* Each client reports with a random "site key", which is used to distinguish between sites
* Websites are grouped together into products (e.g. a product could include the dev/staging/production sites of a project)
* Drush commands are availble to import security advisories at leisure, or for debugging
* Raw report JSON is always stored and could (by extension) be piped into a No-SQL database for alternative querying and processing

Security Advisories
----------------------------------

* The server periodically checks in with Drupal.org looking for any newly posted security advisories (via RSS)
* When received, security advisories are parsed and imported into Mataara and the metadata of "affected" projects & releases are fetched from the Drupal update server
* When a site is viewed on the Mataara server, its installed module releases are compared to those linked to security advisories and a list of vulnerable modules is created.


Miscellaneous
----------------------------------

* A unit testing library is being developed to verify the operation of the client and server modules (specifically the Advisory importer)
* The server module utilises Features and Strongarm to configure a Drupal system for receiving Mataara reports

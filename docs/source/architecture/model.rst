.. _architecture_model:

Client/Server Model
==================================

Mataara uses a client/server model in order to completely separate the report
generation from the parsing and advisory-matching functionalities. Although this
could be implemented in a single module (with the benefit that individual sites
would be aware of their vulnerabilities), that would defeat the purpose of a
centralised tool.

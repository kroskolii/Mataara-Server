.. _architecture_front_end:


Introduction
==================================
We are using react JS, a JavaScript library for building user interfaces, to build
our front end for Mataara. For more details please visit  `ReactJs <https://facebook.github.io/react/>`_
We are also combining react+redux to control the state of the app in one store.

What does Redux do, and how does it work?
-----------------------------------------
Redux is a predictable state container for JavaScript apps. (`Redux <http://redux.js.org/>`_)
It helps you write applications that behave consistently, run in different environments (client, server, and native), and are easy to test.

Redux only has a single store that holds no logic by itself. Actions are dispatched and handled directly by the store, eliminating the need for a standalone dispatcher. In turn, the store passes the actions to state-changing functions called reducers. Another layer may be included — i.e, a middleware, it could be an API middleware or client-side middleware which every action passed through. Middleware can stop, modify or add more actions or authenticate.

Base of Redux
----------------------------------

    * Store: Control the state.
    * Actions: Describe the changes we want to make.
    * Reducers: Services that change the state based on requested action.
    * Middleware: Handles housekeeping tasks of the app.

N/B: Reducers, never change their state, it is immutable. An entire application state is kept in a single location, the store- This provides enormous benefits during debugging, serialization and development.

Principles of Redux
----------------------------------

    * The State Tree Principle: The state of an application is stored in an object tree within a single store, meaning everything that changes in an app is contained in a single object called state or state tree.

    * The Action Tree Principle: A state tree cannot be modified or written to. To change a state, an action has to be dispatched. An action ensures that views nor the network callbacks will ever write directly to the state. Actions must be plain JS objects, that can be logged, serialized, stored and later replayed for debugging or testing purposes.

    * The Reducer Tree Principle: Reducers exists to specify how the state tree is transformed by actions. Reducers are pure functions that takes the previous state and an action then return the next state. Using the default switch, it must return current state for undefined actions.



Basic
==================================
  - React + Redux
  - Babel 6
  - Webpack
  - Eslint
  - Redux DevTools + Logger middleware - easily removable/replaceable based on your needs
  - Jest for unit test

Requirements
----------------------------------
  - `nodeJS 5.0.0` and higher plus npm!

Directory structure
==================================

.. code-block:: text

├── devServer.js
├── README.md
├── webpack-stats.json
├── src
│   ├── index.js
│   ├── actions  
│   ├── store
│   ├── components
│   ├── constants
│   ├── containers
│   ├── api.js
│   └── reducers
├── setupJest.js
├── webpack.config.prod.js
├── index_frontend.html
├── package.json
└── webpack.config.dev.js


Actions Directory
==================================
Actions are payloads of information that send data from the application to the store. They are the only source of information for the store.
We put all our actions files in this directory.
The Action Principle: A state tree cannot be modified or written to. To change a state, an action has to be dispatched. An action ensures that views nor the network callbacks will ever write directly to the state. Actions must be plain JS objects, that can be logged, serialized, stored and later replayed for debugging or testing purposes.

Contants Directory
==================================
Constants that describe the type of action that is performed within your app. We defined all the contants in the ActionsTypes file.

Reducers Directory
==================================
Actions describe the fact that something happened, but don't specify how the application's state changes in response. This is the job of reducers.
Because we are using redux, we combined reducers with redux tool combineReducers.
The Reducer Principle: Reducers exists to specify how the state tree is transformed by actions. Reducers are pure functions that takes the previous state and an action then return the next state. Using the default switch, it must return current state for undefined actions.

Containers Directory
==================================
Containers components are a React pattern used for separate data fetching from rendering concerns.
The idea is simple: A container does data fetching and then renders its corresponding sub-component. That’s it.

Store Directory
==================================
The Store is the object that brings actions and reducers together. We only have a single store in our redux application.
The store has the following responsibilities:

  * Holds application state;
  * Allows access to state via getState();
  * Allows state to be updated via dispatch(action);
  * Registers listeners via subscribe(listener);
  * Handles unregistering of listeners via the function returned by subscribe(listener).

Api File
==================================
This file is responsible for building the query string, fetch data from Django Rest Api
and parse the response (json). Every actions use api file functions to fecth data.

Components Directory
==================================
Components in React JS are like lego pieces. We could defined it as “every part of applications visuals would be wrapped inside a self-contained module known as a component”.
Our components use Material-UI, a set of react components that implement Google's material design (http://www.material-ui.com)

Unit Test folder
==================================
We use jest to create unit test ( `Jest <https://facebook.github.io/jest/>`_). We place or test files into a folder called __tests__. Jest it will look automatically for test files inside that folder.

Eslint
==================================
We use this JavaScript linting utility to find problematic patterns or code that doesn’t adhere to certain style guidelines. We define lint rules in the file .eslintrc. You can find more rules at `Eslint <https://eslint.org/docs/rules/>`_

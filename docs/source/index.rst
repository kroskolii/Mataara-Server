Mataara Documentation
======================================

Mataara is a client/server reporting system for websites with Client
modules/plugins that provides a means of monitoring module vulnerabilities
and site configuration from a central location.

The documentation for Mataara is organised into a number of sections:

* :ref:`Architecture <architecture_features>`
* :ref:`Drupal Client <client_getting_started>`
* :ref:`Server <server_getting_started>`
* :ref:`Development <development_getting_started>`
* :ref:`Development Quickstart Guide <development_quickstart>`

The `Mataara <https://gitlab.com/mataara>`_ project was created at `Catalyst IT <https://catalyst.net.nz>`_.

.. _architecture-docs:
.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Architecture

   architecture/features
   architecture/model
   architecture/frontend

.. _client-docs:
.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Mataara Client

   client/getting_started
   client/installation
   client/configuration
   client/drush

.. _server-docs:
.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Server

  server/getting_started
  server/installation
  server/generating_keypair
  server/configuration
  server/saml

.. _development:
.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Development

  development/getting_started
  development/quickstart
  development/setup
  development/fabric
  development/django_commands
  development/database
  development/reactjs
  development/server_modules
  development/report_format
  development/report_format_legacy
  development/technologies
  development/repo_structure

.. _server-api:
.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Server API

  server/api/modules
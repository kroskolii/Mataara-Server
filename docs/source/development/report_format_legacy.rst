.. _report_format_legacy:

.. _legacy:

Archimedes report format (legacy XML)
======================================

This file documents the legacy report format based on XML. This format is only supported by the Drupal 7 client.
New installations should be using the new JSON based report format.

Transport Mechanism
-----------------------------------

Encryption uses OpenSSL with an RC4 stream cipher. The HTTP endpoint receives a
JSON blob with two fields containing base64-encoded values:

key
  Envelope key for the public key specified
data
  The encrypted XML data payload

The HTTP User-Agent is: **Archimedes Client**

The ``reports.utils.legacy_decode()`` function fills in a few fields that the old
report format is missing, or creates mappings to the same fields as the newer
Drupal 7 reports, with a ReportType of ``drupalLegacy``.

XML Format
-----------------------------------

The XML report format starts with a standard XML definition:

.. code::

  <?xml version="1.0" encoding="UTF-8"?>

Everything is contained within a ``node`` tag. Node attributes are:

  xmlns
    ``monitor:node``
  type
    The only supported type for this legacy format is ``drupal``
  id
    The Drupal site's ``SiteKey`` value
  datetime
    Report generation timestamp in ISO format, e.g. ``2016-11-29T10:32:09+13:00``
  author
    Mailto link to the registered Drupal site email, e.g. ``mailto:noreply@drupal.example.com``

For example:

.. code::

  <node xmlns="monitor:node" type="drupal" id="0mVH53BTsmkowNepLJ3V6pgWmLa7xHZZffhz-DZRDGQ" datetime="2016-11-29T10:32:09+13:00" author="mailto:noreply@drupal.example.com">

All fields inside ``node`` are defined in the form ``<field id="foo"><value>bar</value></field>``.

title
  Site title.
field_drupal_version
  Drupal core version.
field_common_hash
  Site key.
field_body
  Site slogan / description.
field_servername
  Server's hostname.
field_webroot
  Drupal site root directory.
field_dbname
  Database name (assumes only one?).
field_dbhost
  XML definition : ``xmlns:node="monitor-plugin:node", node:title="$host", node:type="host"``

  facet
    Contains the database hostname. Should match the title specified in the definition for the database host.

field_users[]
  List of admin users. XML definition : ``xmlns:user="monitor-plugin:user", user:type="mail"``

  value
    Specify user's email address using the format ``mailto:$email``

field_c_dataset
  Node and user counts. XML definition for the following fields: ``xmlns:dataset="monitor-plugin:dataset", dataset:title="$title"``

  Nodes
    Number of content nodes.
  Revisions
    Number of content node revisions.
  Users
    Number of users.

field_drupal_mod[]
  List of Drupal modules. There are lots of attributes for modules.

  xmlns:node
    ``node="monitor-plugin:drupal-module"``
  node:title
    Display name.
  node:body
    Description.
  node:field_name
    Module machine name.
  node:field_dru_pkg
    Package name.
  node:field_dru_proj
    Project name.
  node:field_mod_version
    Module version.
  node:field_mod_url
    Module URL, if supplied.
  node:type
    ``drupal_module``

  facet
    Title of modules

field_drupal_theme[]
  List of Drupal themes (identical format to `_mod`)
field_site_env
  Drupal environment name.
field_directory_hash
  Directory hash

Example report
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: report_format_example_xml.txt
  :literal:

.. _development_reactjs:

Setting up your development environment for ReactJS
===================================================

The ReactJS environment can be installed and started with the following fabric
task:

  # On the host
  fab runjs

This may take a while depending on the speed of your machine and Internet 
connection and when completed it will keep the terminal session and
automatically recompile as you make edits/updates.

Installed packages will be in ``archimedes/frontend/node_modules``.

We're using `Webpack <https://webpack.js.org/api/node/>`_ to bundle up the
JavaScript files.

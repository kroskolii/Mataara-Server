.. _report_format:

Mataara report format (JSON)
======================================

This file documents the format used for reports in the modern JSON format,
with the intent of moving to a standardised, versioned API in future.

.. seealso::

  There is also a `legacy XML-based report format <report_format_legacy.html>`_ that is only supported for Drupal 7.


Encryption
-----------------------------------

Encryption uses OpenSSL with an RC4 stream cipher. The HTTP endpoint receives a
JSON blob with two fields containing BASE64-encoded values:

ek
  Envelope key for the public key specified

enc
  The encrypted data payload

JSON Format
-----------------------------------

All timestamps are expected to be in *UNIX time*, i.e. the number of seconds since
1 January 1970 UTC.

AdminRole{}
  Details of the Drupal website administration role.

  RoleName
    Name of role (e.g. ``administrator``).
  RoleID
    Drupal rid (role identifier)
  RoleUsers[]
    Set of users with this role.

    uid{}
      User's unique identifier

      Name
        User's Drupal site username.
      Mail
        User's email address, if defined.
      Status
        Is the account marked as active inside Drupal? (0 = blocked, 1 = active).
      LastLogin
        Timestamp of the user's last login to the Drupal site.

Databases{}
  Set of databases that the Drupal site is connected to.

  key{}
    Database key (usually default).

    target{}
      Database target (usually default).

      Driver
        Database driver (e.g. mysql, pgsql).
      Database
        Name of the database to connect to.
      Host
        Hostname of the database server.

DirectoryHash
  MD5 hash of the ``DRUPAL_ROOT`` directory and files inside to allow for detection of code changes.

DrupalVersion
  Version number for the Drupal core, e.g. 7.34

Environment{}
  Drupal environment in use.

  Label
    user-friendly display name for the environment.
  Name
    Internal machine name for the environment.

Modules[{}]
  List of installed Drupal modules.

  Module
    Internal machine name of module.
  Name
    User-friendly display name of module.
  Description
    Typically one-line description of module behaviour.
  Package
    Package name (used to group projects, e.g. ``Core`` for Drupal Core related modules).
  Project
    Machine name of project (used to group modules).
  Version
    Module version string (e.g. ``7.x-2.3``, ``7.x-dev``, ``7.x-1.0-beta3``, ``7.x-1.0-alpha4+1-dev``).
  Url
    External URL for module, if provided.

Nodes{}
  Summary of the Drupal content nodes in the site.

  Nodes
    Number of nodes in the site.
  Revisions
    Number of node revisions in the site.

ReportExpiry
  Timestamp at which point the report should be considered stale and ignored if received after this time.
ReportGenerated
  Timestamp when the report was generated.
ReportType
  Type of report (e.g. ``drupal7``).
ServerHostName
  Server's hostname.
SiteKey
  Unique identifier for site.
SiteSlogan
  An optional site description that may be converted in Drupal.
SiteTitle
  Drupal site title.

Themes[{}]
  List of installed Drupal front-end themes.

  Theme properties are the same as for *Modules*.

Users
  Number of users on site.

Example report
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: report_format_example_json.txt
  :literal:

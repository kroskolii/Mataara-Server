.. _development_setup:

Environment setup
======================================

.. note::

  There is a `Quickstart Guide <quickstart.html>`_ if you are wanting to dive
  in or are familiar with development and just want a handy reference to
  stand up your dev environment. 

For ease of development. Mataara supports the use of `Vagrant <https://www.vagrantup.com/>`_ to manage a virtual machine containing the server.
The instructions below also assume the use of an Ubuntu-based host system.



.. note::

  **TODO - Add non-vagrant manual development environment setup steps**

.. _cloning_from_git:

Cloning from Git
-----------------------------------

Clone the Mataara Server project from git using::

  git clone git@gitlab.com:mataara/Mataara-Server.git mataara

Shared folder into vagrant
-----------------------------------

If you follow the instructions below, use Linux on your host computer and Vagrant, the ``mataara`` folder
containing your git clone which be accessible from within the vagrant virtual machine at the path ``/vagrant``.

.. _install_dependencies:

Install dependencies
-----------------------------------

VirtualBox installation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The vagrant file is expecting to use VirtualBox to provide the virtualisation layer. Other alternatives, such as LXC are possible.
(The VirtualBox Oracle VM VirtualBox Extension Pack is not required.)

.. note::

  **TODO - Add alternatives to VirtualBox to the Vagrantfile**

To install your distribution provided version of VirtualBox use::

  sudo apt-get install virtualbox

Information on installing the latest upstream Oracle managed version of VirtualBox for a variety of Linux systems can be read at
https://www.virtualbox.org/wiki/Linux_Downloads

Other packages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

On Ubuntu install the dependencies using::

  sudo apt-get install vagrant fabric python3-pip python3-setuptools

.. _fab_setup_dev_env:

Configure the development environment
-------------------------------------

Install git pre-commit hook and any necessary local dependencies::

  fab setup_dev_env

Note: Fabric (``fab``) commands are run from the host.

.. _config_mataara_server:

Configure the Mataara server
-----------------------------------

The Mataara server uses a configuration settings file which is managed by puppet inside vagrant.
When using vagrant this file is ``conf/archimedes.ini.vagrant`` and is accessible outside of the vagrant image for customising.

Puppet uses hiera to load the settings that it manages from ``conf/hiera/config.yaml``.
Copy the example template::

  cp conf/hiera/config.yaml.example conf/hiera/config.yaml

Now configure your site specific settings by editing the appropriate entries. Full details on all the options can be found in on the :ref:`Server Configuration Settings <server_configuration>` page.

The development environment comes with a single preconfigured user (|default_account_user|:|default_account_password|). If you are wanting to use an existing system for users Mataara currently supports :ref:`LDAP <ldap_configuration>` and :ref:`SAML <saml_configuration>` for external authentication.

.. _stand_up_the_vm:

Install Mataara
-----------------------------------

Once the Mataara config file that will be used by puppet has been configured, the virtual machine itself can be setup.
This may take awhile, particularly if you need to download an operating system image.

To build and provision the initial vagrant virtual machine::

  vagrant up

If the initial fails, or some component times out, it is probably sufficient to just tell vagrant
to reprovision the virtual machine using::

  vagrant provision

Once Vagrant is running the Mataara server should be available on the default
IP at http://192.168.33.5:8000

.. _post_install_config:

Post-install configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    If you have an existing Mataara database, you might want to copy the data across to kick-start your development. See :ref:`Database backup and restore <development_database>`.

At a minimum you will want to configure the :ref:`default security advisory sources <development_fabric_setup_feeds>`.::

  fab setup_feeds

You will also want to setup at least one client. This will depend on a preferred site(or sites) that you are monitoring. See :ref:`Getting Started <client_getting_started>` in the Client section for options.

.. seealso::

  **TODO - cross-link to client documentation**

Setting up a superuser
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To access the admin web interface, which can be useful in development, you'll need to convert a standard user to a superuser.
First login through the web interface as normal using whatever authentication source you have configured.

Now grant yourself superuser rights::

  vagrant ssh -c "archimedes setsuperuser --username=<username>"

Stand up your development server
-----------------------------------

You should now able to run the server. ::

  fab run

Note: ``fab run`` will not exit.  This starts the process in the VM and watches the webserver logs.

You should now be able to log into the Mataara server at http://192.168.33.5:8000 (or the IP address you specified in the config.yml file).

By default there is a single user called |default_account_user| with the password |default_account_password|.

* To explore more fabric commands see :ref:`Fabric tasks <development_fabric>`.
* To explore the server configuration settings in more detail see :ref:`Server Configuration Settings <server_configuration>`.

.. |default_account_user| replace:: ``vagrant``
.. |default_account_password| replace:: ``vagrant``

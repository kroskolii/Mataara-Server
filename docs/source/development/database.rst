.. _development_database:

Database Backup and Restore
==================================

During development it is often useful to have a database that contains real information. 
If you have an existing server with data it can easily be transferred to your development environment.

Create Database Dump
---------------------------------

We'll be using ``pg_dump`` to create the database dump into a plain-text SQL script file format. 
However, as this may be quite large we will also be compressing it. We will also create the backup 
such that when it is restored it will clean out any existing content and refresh the database tables.

Database backup in vagrant
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Login to vagrant and create the backup in the vagrant directory that is shared with the host computer::

  vagrant ssh
  pg_dump --clean --compress=9 > /vagrant/mataara.sql.gz

Database backup on a server
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Login to your server as the user that is running Mataara. Create the file and then transfer it off the server::

  pg_dump --clean --compress=9 > mataara.sql.gz

Restoring a Database Dump in Vagrant
------------------------------------

Obtain your database backup and place it into the Mataara source code folder that is shared into the vagrant virtual machine.

Before restoring the database it is important to shutdown any applications using it. To stop the automatic background processes run::

  vagrant ssh
  supervisorctl stop all

Restore the database dump::

  zcat /vagrant/<database_dump_file> | psql

If there are errors during the import process it may be that the database backup was not created with the ``--clean`` parameter. In which case delete and recreate the database using::

  dropdb vagrant
  createdb vagrant

Apply any missing migrations to the database models in Django so that it matching the restored database schema by running::

  archimedes migrate

Restart the background tasks in Vagrant using::

  supervisorctl start all


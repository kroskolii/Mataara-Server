.. _saml_configuration:

Configuring SAML authentication
===============================================

Dependencies
-----------------------------------------------

The following instructions use the ``djangosaml2`` package. This requires the ``xmlsec`` binary to be installed.
On Ubuntu systems it can be installed using::

  sudo apt-get install xmlsec1

The ``djangosaml2`` package can be manually installed using::

  pip install djangosaml2

On a server, it should be added to the requirements file `requirements/common.txt`.

Enable SAML in config file
-----------------------------------------------

Define a unique SAML entity identifier URL for the website in the ``archimedes.ini`` configuration file.
Defining a SAML entity identifier will cause Mataara to automatically load the SAML modules.
Although the entity identifier does not have to map to a real-world resolvable web page, by convention
it is often desirable to specify the URL that advertises the server's SAML metadata.

.. code::

  [saml]
  entityid = http://mataara.company.com/saml2/metadata

.. note::

  Use an EntityID that describes the service, not the server currently hosting it, so that
  the same value can persist over time.

An optional root URL can be set for the Mataara server that will be used by the IdP to contact
it. The default is to extract the relevant value from the entityid.

.. code::

  [saml]
  root_url = http://mataara.company.com/

xmlsec1 binary location
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Mataara expects the *xmlsec1* binary to be installed to ``/usr/bin/xmlsec1``.
This value can be overwritten in the ``archimedes.ini`` file::

  [saml]
  xmlsec_binary = /path/to/binary

Define IdP user attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Mataara will default to only checking for the existing of the attribute ``uid`` in the
assertion that the IdP provides. Alternative, or additional, required attributes can be defined
in the configuration file. These are specified as a command separated set of strings. For example::

  [saml]
  required_attributes = uid,email

Mataara will also default to using the ``uid`` for the ``username`` value. You can also optionally
tell Mataara which SAML IdP user attributes should be used for the user's name and email address.
For example::

  [saml]
  username = uid
  email = mail
  first_name = givenName
  last_name = sn

The above values were taken from an IdP that uses LDAP as its back-end authentication source.
Other sources are likely to have different attributes that need to be configured.

Configure Mataara to accept unknown attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Mataara will attempt to identify the user's attributes using standard unique identifiers.
This configuration setting allows Mataara to accept attributes that are not recognised.
It will rely on being able to identify an attribute whose name matches that defined in the
``primary_attribute`` value::

  [saml]
  allow_unknown_attributes = True

This bug is known to affect SimpleSamlPHP version 1.8.2 (see djangosaml2 documentation).

Define metadata expiry
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The generated metadata has a default expiry of 24 hours. This is normally sufficient for most
Identity Provider's that dynamically refresh. If you want to set a longer expiry time there is
an optional configuration option for the number of hours before expiry::

  [saml]
  expiry =

If you want the metadata to never expire, set a value of ``0``.

Create SAML crypto keys
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You'll need to create a public/private key pair for SAML to use when protecting communications
from the Mataara server to the IdP server.
See :ref:`Generating an OpenSSL key-pair <server_generating_keypair>` for more information.
Specify these in the ``archimdes.ini`` configuration file using::

  [saml]
  key_file = /path/to/private.key
  cert_file = /path/to/public.cer

.. note::

  The certificate file, containing the public key, needs to be a full X509 certificate file and not just a public key.
  You can verify this using ``openssl x509 -noout -text -in public.cer``

If you need to use separate key pairs for signing and encryption these can be specified using::

  [saml]
  enc_key_file = /path/to/private.key
  enc_cert_file = /path/to/public.cer

Install IdP server metadata
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Obtain the IdP's server metadata and store it in a file. Mataara uses the default filename ``remote_metadata.xml``.
An alternative filename can be defined in ``archimedes.ini``::

  [saml]
  remote_metadata_file = /path/to/metadata.xml

The IdP entity identifier and endpoint information should be automatically determined from the metadata. However,
if it is necessary to specify the specific IdP entity identifier to use, for example if the metadata contains more than one,
it can be manually specified in ``archimedes.ini``. Additionally, the binding URLs for the IdP's *single sign-on service*
and *single logout service* can be manually specified::

  idp_entityid =
  [saml]
  idp_sso =
  idp_sls =

Contact details
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Optional technical and/or administrative contact details can be specified in ``archimedes.ini``
which will appear in the SAML metadata. Specify suitable values using::

  [saml-technical]
  company =
  name =
  surname =
  email =

  [saml-administrative]
  company =
  name =
  surname =
  email =

SAML Attribute mapping files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Depending on the IdP software, it may be necessary to install some attribute mapping files
to translate between the SAML short attribute names and global oid values.

Create a directory and install some attribute maps. You will probably want to download
these from the pysaml2 source code at https://github.com/rohe/pysaml2/tree/master/example/attributemaps

Specify the directory in the configuration file using::

  [saml]
  attribute_map_dir =

Configuring Mataara in IdP
-------------------------------------------

For Mataara to successfully talk SAML to the IdP, the SAML metadata for
Mataara must be configured in the IdP. The local SAML metadata can be obtained
by visiting::

  http://[hostname]/saml2/metadata

Upload this metadata file to your IdP. How to do this is beyond the scope of this document
as it depends on the software involved.

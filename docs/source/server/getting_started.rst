.. _server_getting_started:

Getting Started
==================================

Here's how to get a Mataara Server configured and some notes on basic usage
- such as generating OpenSSL keys and setting up products from sets of websites.

Requirements
----------------------------------

The Mataara server uses Django and thus requires Python to be installed. Other requirements:

* web server
* SSL certificate (if you want to access the site over HTTPS)
* PostgreSQL Database
* a user authentication source (such as LDAP)

The instructions here describe installing Mataara in a single-server, non-high availability manner.
The components are all standard and the stack can easily be expanded.

Instructions have only been tested on Ubuntu Trusty (14.04 LTS)

The server can optionally integrate with a Sentry server for error reporting.

Network Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following firewall rules will be required for the application in addition to any used for
standard system management and monitoring.

Inbound:
  * HTTP(S) Access

Outbound:
  * HTTP(S) to advisories websites
  * IMAP(S) or POP3(S) for polling a mail server for reports
  * LDAP(S) if LDAP will be used for authentication
  * SMTP for sending error emails out

A proxy server may optionally be configured for outbound HTTP(S).

Key Directories and Files
----------------------------------

The following default directories and files are used in these installation instructions.
If you choose other options, please update accordingly as you install Mataara.

/etc/archimedes/archimedes.ini
  Configuration file for Mataara itself.

/etc/nginx/sites-available/default
  Configuration file for nginx that will define the external Mataara site and internal gunicorn site.

/etc/supervisor/conf.d/archimedes.conf
  Mataara specific configuration settings for supervisor.

/usr/share/archimedes/static
  Directory that will contain the static files to be served up directly by the web server.

/var/www/archimedes
  Directory to contain the Mataara virtual environment and the actual program

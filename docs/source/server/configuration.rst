.. _server_configuration:

Configuration
==================================

The Mataara server has some configuration options that allow you to set up
encryption and other features.

The settings are all stored in the file ``/etc/archimedes/archimedes.ini``.

The settings file is structured into sections introduced by a header in square brackets.
For example, the following extract defines the ``django`` section and the first configuration option::

  [django]
  debug = false

Django standard configuration options
-------------------------------------

Further information on all the Django standard options can be found at https://docs.djangoproject.com/en/1.11/ref/settings/

This section summarises some of the key ones:

admin_email
  Specify email address(es) used to receive error reports. Requires you to have defined an outbound email server.

allowed_hosts
  Define which host(s) this website can appear to be.

  For development environments where we're less concerned, permit all hosts using ``*`` -
  we'll probably be referencing it by IP address anyway.

  For production environments, should configure localhost and the FQDN, e.g. ``allowed_hosts = local archimedes.example.com``

configuration_files
  Can be used to split out configuration to additional files.

debug
  Set to *True* in development environments, otherwise *False*.

secret_key
  Must be set. A default in used in vagrant, but a unique value must be set for production servers.

  To generate a new key of a suitable length, you can try this website: http://www.miniwebtool.com/django-secret-key-generator/

media_root
  Directory used to define a specific writeable location for the storage of uploaded files and processed emails

static_root
  Define the directory that will contain your static assets, e.g. ``/usr/share/archimedes/static``

Configuration options for other modules
---------------------------------------

Celery configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

broker_url
  URL for celery to use when talking to redis.

  Unless you are running the redis server on non-standard port, or another machine, this should be safe to leave at the default.

  Default: ``broker_url = redis://localhost:6379/0``

Database configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

host
  Hostname of the database. To connect using UNIX sockets to PostgreSQL running
  on the same server leave the value blank.

name
  Name of the Mataara database.

  In the vagrant development environment this defaults to ``vagrant``.

pass
  Password for the databae user, if defined.

user
  Name of the database user that should be used to connect to the database.

  In the vagrant development environment this defaults to ``vagrant``.

.. _ldap_configuration:

LDAP configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Only specify settings in this section if you are using LDAP for authentication.
Further information on the settings can be found at https://pythonhosted.org/django-auth-ldap/index.html

bind_dn
  LDAP bind DN to use.

  Leave blank for anonymous bind connections.

bind_password
  Password to use with the LDAP bind DN.

  Leave blank for anonymous bind connections.

ldap_starttls
  Set to ``True`` if the LDAP server URI does not use ldaps but the server support STARTTLS and you want, or need, to use it.

ldap_ignore_cert
  Set to ``True`` to ignore any SSL certificate errors from the LDAP server.

  Useful in development environments.

search_dn
  LDAP search string to be used when searching for users. Typical form is ``uid=%(user)s,ou=users,dc=example,dc=com``

server
  LDAP server URI of the form ``ldap://hostname/`` or ``ldaps://hostname/``.
  A comma separated list of URIs can be provided if you wish to specify backup LDAP servers.

Redis configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

location
  URL for the redis service.

  Unless you are running the redis server on non-standard port, or another machine, this should be safe to leave at the default.

  Default: ``location = redis://localhost:6379/0``

Mail server configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

server
  Hostname of a mail server to be used for outbound email

port
  Port to be used with the mail server (probably ``25``).

Sentry configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Only specify settings in this section if you are using integration with Sentry.

dsn
  Sentry DSN value to be used in reporting errors.

Web proxy configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Often, but not always, a web proxy will handle both HTTP and HTTPS on the same port.

http
  URL of a web proxy that will handle HTTP requests.

https
  URL of a web proxy that will handle HTTPS requests.

Mataara configuration options
-------------------------------------

Encryption for reports
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If a public-private keypair has been generated and is being used by clients to encrypt reports,
it can be defined here.

reports_keyfile
  Path to the file with the private key.

  Ensure that suitable file permissions are applied unless a passphrase is specified.

reports_keyfile_passphrase
  Optional passphrase to use if the private key has been protected by a passphrase.

  If a passphrase is specified, ensure that suitable file permissions are applied to
  the settings file.

Inbound email reports
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

mailbox_uri
  URI connection string for the mailbox that will be used to receive email reports.

  Typical form is ``imap+ssl//myusername:mypassword@somewhere.com``.

Note, the password will be stored in plain-text on the server. Ensure that you have suitable
file permissions applied to protect the settings file.

Outbound email
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
email_subject_prefix
  Used with outbound emails to indicate that they are from the Mataara server. Typical value is ``[Archimedes]``.

from_email
  Specify the from email address to be used for outbound emails.

Report processing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

report_processing_retry_delay
  Delay in automatic processing of a report in scheduled tasks.

reports_processing_max_retries
  Maximum number of time automatic processing of a report is attempted during a scheduled task.

Updates
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

drupal_update_server
  Default value is ``https://updates.drupal.org/```

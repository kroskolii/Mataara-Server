.. _server_installation:

Installing the Server
==================================

Currently the Mataara Server is only available for download from its git repository.

Download wheel
-----------------------------------

.. note::

  **TODO - Package up and publish wheels somewhere**

Cloning from Git
-----------------------------------

Instead of downloading and installing the wheel directly, we can grab the source from git using:

  git clone git@gitlab.com:mataara/Mataara-Server.git mataara

Add Node.js repository
-----------------------------------

`Node.js <https://nodejs.org/en/about/>`_ is used in the build process. It is not required on a production server if Mataara
is installed by wheel.

Mataara has been tested using the NodeJS v6 LTS. It may work without any problems using later versions.
LTS versions are recommended.

If your operating system provides a suitable LTS it is recommended to install that version so that subsequent updates
are automatically detected and can be applied.

An individual version can be downloaded from the `Node.js download <https://nodejs.org/en/download/>`_ page.
If not available directly from the operating system provider, it is recommended that an LTS version is
installed through a suitable third-party package manager.
Instructions are available on the `Installing Node.js via package manager <https://nodejs.org/en/download/package-manager>`_ page.

Ubuntu installation
-----------------------------------

Install JavaScript build dependencies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We need to install `npm <https://www.npmjs.com>`_ so that we can compile the JavaScript front-end bundle.

Install nodejs::

  sudo apt-get install -y nodejs

Create JavaScript bundle
^^^^^^^^^^^^^^^^^^^^^^^^

The JavaScript bundle can be created live on a production server, or on a build server prior to
wheels being built::

  cd archimedes/frontend
  npm run build

Install redis
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Redis <https://redis.io/>`_ is used internally by Mataara.
There should be specific configuration required if it can run on its default TCP port of 10448.
Install it using::

  sudo apt-get install -y redis-server

Database setup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Mataara has been developed with `PostgreSQL <https://www.postgresql.org/>`_` as the primary database. Other options should work but have not been tested.

To install PostgreSQL, setup a Mataara user and associated database, run::

  sudo apt-get install -y postgresql
  sudo -i -u postgres
  createuser archimedes -d

Install web server
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Mataara will work with any web server that supports WSGI. These instructions are for nginx.
Install using::

  sudo apt-get install -y nginx

Assuming this site will be the default one for nginx, configure ``/etc/nginx-sites-available/default`` to contain::

  upstream gunicorn {
      server localhost:4005;
  }

  server {
      listen 80;
      client_max_body_size 20M;
      location / {
          proxy_pass http://gunicorn;
          proxy_set_header X-Request-Id $pid-$msec-$remote_addr-$request_length;
      }

      location /static/ {
          alias /usr/share/archimedes/static/;
      }
  }

.. warning::

  This won't work until we've installed gunicorn and archimedes

.. note::

  **TODO - add the extra commands required for this to run on HTTPS**

Install other packages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To install the core packages required to run Mataara run::

  sudo apt-get install -y python3-all libxml2 libxslt1.1

To install the additional packages required to build Mataara (required if installing from source) run::

  sudo apt-get install -y python3-dev, python3-setuptools, libpq-dev, libldap2-dev, libsasl2-dev, libssl-dev, libffi-dev

Install Python packages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pip is used to install all the Python packages. Unfortunately, the version of pip distributed by many systems is old.
Hence, the script ``bin/get-pip.py`` which will bootstrap us a sufficiently up-to-date version.

First we'll create the venv, install our pip and check if there are any updates,
then use it to install the required packages::

  VENV="/var/www/archimedes"
  /usr/bin/python3 -m venv --system-site-packages --without-pip ${VENV}
  ${VENV}/bin/python3 archimedes/bin/get-pip.py --ignore-installed
  ${VENV}/bin/pip install -U pip
  ${VENV}/bin/pip install -r /vagrant/requirements/dev.txt

.. note::

  **TODO - when we provide a pre-built wheel, it will be possible to switch the above requirements to ``common.txt``**

Install supervisor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To install supervisor, run::

  sudo apt-get install -y supervisor

Create the supervisor configuration file `/etc/supervisor/supervisord.conf`:

.. code::

  [unix_http_server]
  file=/var/run/supervisor.sock   ; (the path to the socket file)
  chmod=0770                       ; socket file mode (default 0700)
  chown=root:supervisor

  [supervisord]
  logfile=/var/log/supervisor/supervisord.log ; (main log file;default $CWD/supervisord.log)
  pidfile=/var/run/supervisord.pid ; (supervisord pidfile;default supervisord.pid)
  childlogdir=/var/log/supervisor            ; (AUTO child log dir, default $TEMP)

  ; the below section must remain in the config file for RPC
  ; (supervisorctl/web interface) to work, additional interfaces may be
  ; added by defining them in separate rpcinterface: sections
  [rpcinterface:supervisor]
  supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

  [supervisorctl]
  serverurl=unix:///var/run/supervisor.sock ; use a unix:// URL  for a unix socket

  ; The [include] section can just contain the "files" setting.  This
  ; setting can list multiple files (separated by whitespace or
  ; newlines).  It can also contain wildcards.  The filenames are
  ; interpreted as relative to this file.  Included files *cannot*
  ; include files themselves.

  [include]
  files = /etc/supervisor/conf.d/*.conf

Now create the Mataara specific configuration file `/etc/supervisor/conf.d/archimedes.conf`:

.. code ::

  [program:gunicorn]
  command=${venv}/bin/gunicorn --log-level=DEBUG archimedes.wsgi --bind=0.0.0.0:4005 --reload --workers 4
  user=vagrant
  numprocs=1

  [program:celery]
  command=${venv}/bin/celery --app=archimedes.celeryapp --no-color worker --loglevel=DEBUG --beat --autoscale=6,2
  user=vagrant
  stopsignal=INT
  numprocs=1
  environment=AUTOSCALE_KEEPALIVE="120"

.. note::

  **TODO - work out replacement for ${venv} above**

Restart the supervisor service using::

  supervisorctl restart all

Install Mataara itself
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Steps involved in installing Mataara itself (when not using wheels)::

  ${VENV}/bin/pip install archimedes
  cd ${VENV}/bin
  archimedes migrate --noinput
  cd archimedes/frontend
  npm run build
  archimedes collectstatic --noinput --link --clear

If you will be receiving reports by email, and want to use encryption, you will need a public-private set of encryption keys.
For more information, see :ref:`server_generating_keypair`

Now customise your :ref:`server_configuration` in ``/etc/archimedes/archimedes.ini``.

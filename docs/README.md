# Mataara Documentation
Documentation for the Mataara client/server status (and security) reporting system for Drupal sites.

These docs are written in [reStructuredText](http://sphinx-doc.org/rest.html#rst-primer) and can be built with [Sphinx](http://sphinx-doc.org/) using Make.

## Read the Docs

A built version of these docs used to be available on RTD at http://mataara.readthedocs.io/

## Setting up Sphinx

  1. Install sphinx and the readthedocs theme:

  `pip install Sphinx sphinx_rtd_theme`

To manually compile the docs:

  ` make html`


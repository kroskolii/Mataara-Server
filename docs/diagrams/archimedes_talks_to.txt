@startuml
title Things Archimedes talks to
cloud {
  [Drupal website agents] as agents
}
() "Mailbox" as mb
database "Postgres" as db
node "archimedes components" {

  package "drupal" {
    Folder "management/commands"{
        [importadvisories] <<Command>>
    }
  }
  package "reports" {
    [reports/endpoint] <<reports API>>
    [getmail CMD] <<reports API>>
  }
  [.models] <<Django ORM>>
  [.tasks] <<celery tasks>>
  [authbackends] <<CayalystLDAPBackend>>
  note bottom of [authbackends] : authenticate user - calayst LDAP credentials
  [.admin] <<Django Admin>>
  [.views]
}
[Redis] <<task queue>>
[Celery]
[Advisory Source Website]
[LDAP server]


[agents] --> [reports/endpoint]: HTTP(S)
[agents] --> mb: SMTP
mb <-- [getmail CMD]: IMAP
[.models] --> db
[Advisory Source Website] --> [importadvisories] :Rss feed
:admin: --> [.admin]
:admin: --> [.views]
:staff: -->[.views]
[.admin] -[hidden]> [.views]
[LDAP server] <-- [authbackends]
[.tasks] --> [Redis]
[Redis] --> [Celery]
note right of [Celery]: celery workers complete tasks from the queue
@enduml

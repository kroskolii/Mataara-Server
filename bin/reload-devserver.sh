#!/bin/sh

##
## This script resets the database and static files of the instance
## running on the vagrant virtual machine.
##
APP=archimedes

if [ "$(whoami)" != "vagrant" ]
then
    echo "You probably didn't mean to run this as anybody but 'vagrant'"
    exit 1
fi

cd $(dirname $0)/../

# Stop any supervisor services that are already running
# (Note, '|| true' is to handle when services are not yet running)
supervisorctl stop all || true

## Drop and re-create the database
dropdb vagrant || true
createdb vagrant

## Activate the virtual environment
. /vagrant/venv/bin/activate

## Create the database
${APP} migrate --noinput

## Create a test user. We're throwing the python code that does this
## straight into the shell, which is kinda gross, but a lot less
## painful than database fixtures end up being.
##
## Username and password: 'vagrant'
echo "from django.contrib.auth.models import User; " \
     "User.objects.create_superuser('vagrant', 'vagrant@example.com', 'vagrant')" | ${APP} shell --plain > /dev/null

${APP} collectstatic --noinput --link --clear

## We're done, restart services
supervisorctl start all

#!/bin/bash

set -e
set -x

VENV=$1
PYTHON=python3

BASE_DIR=$(dirname $0)/..
GET_PIP=${BASE_DIR}/bin/get-pip.py

if [ "$VENV" == "" ]
then
    echo "You must specify a directory for the environment"
    exit 1
fi

## Prepare the virtual environment, we have to do this in two steps
## because Ubuntu has broken the built-in pip installation.
/usr/bin/${PYTHON} -m venv --system-site-packages --without-pip ${VENV}

## Use our new venv-provided python interpreter to install pip
${VENV}/bin/python3 ${GET_PIP} --ignore-installed

